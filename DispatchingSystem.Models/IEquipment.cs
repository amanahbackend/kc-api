﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IEquipment : IBaseEntity
    {
        int Id { get; set; }
        int Number { get; set; }

        int FK_EquipmentType_Id { get; set; }
        EquipmentType EquipmentType { get; set; }

        int? Fk_Factory_Id { get; set; }
        Factory Factory { get; set; }

        int? FK_WorkOrder_Id { get; set; }
        WorkOrder WorkOrder { get; set; }


        bool IsAvailable { get; set; }

    }
}

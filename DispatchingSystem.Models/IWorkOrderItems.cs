﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IWorkOrderItems
    {
        int Id { get; set; }

        int FK_Item_Id { get; set; }
        Item Item { get; set; }
        double Price { get; set; }

        int FK_Workorder_Id { get; set; }
        WorkOrder WorkOrder { get; set; }
    }
}

﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IContractItems : IBaseEntity
    {
        int Id { get; set; }

        int FK_Item_Id { get; set; }
        Item Item { get; set; }

        int FK_Contract_Id { get; set; }
        Contract Contract { get; set; }

        double Price { get; set; }
        //double AvailableAmount { get; set; }
    }
}

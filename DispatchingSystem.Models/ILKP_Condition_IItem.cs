﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface ILKP_Condition_IItem:IBaseEntity
    {
        int Id { get; set; }
         int FK_Item_Id { get; set; }
         Item Item { get; set; }

         int FK_Condition_Id { get; set; }
         LKPCondition Condition { get; set; }
        string Value { get; set; }
    }
}

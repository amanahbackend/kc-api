﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IApplicationConfiguration : IBaseEntity
    {
         int Id { get; set; }
         int TransportationTime { get; set; }

    }
}

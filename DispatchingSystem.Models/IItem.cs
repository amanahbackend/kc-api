﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IItem:IBaseEntity
    {
        int Id { get; set; }
        string Code { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        string Formula { get; set; }
        int AvailableAmount { get; set; }
        double Price { get; set; }

        ICollection<WorkOrder> WorkOrders { get; set; }
        [NotMapped]
         ICollection<ContractItems> ContractItems { get; set; }
    }
}

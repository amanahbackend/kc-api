﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IFactoryController : IBaseEntity
    {
        int Id { get; set; }

        int Fk_Factory_Id { get; set; }
        Factory Factory { get; set; }

        string Fk_Controller_Id { get; set; }
        ApplicationUser Controller { get; set; }
    }
}

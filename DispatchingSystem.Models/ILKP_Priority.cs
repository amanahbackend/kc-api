﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface ILKP_Priority: IBaseEntity
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}

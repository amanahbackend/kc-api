﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IFactorySubOrderPercentage : IBaseEntity
    {
        Factory Factory { get; set; }
        int FK_SubOrder_Id { get; set; }

        WorkOrder SubOrder { get; set; }
        double Percentage { get; set; }
    }
}

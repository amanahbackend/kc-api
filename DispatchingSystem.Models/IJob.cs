﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IJob : IBaseEntity
    {
        int Id { get; set; }

        int FK_Status_Id { get; set; }
        LKP_Status Status { get; set; }

        int? FK_Vehicle_Id { get; set; }
        Vehicle Vehicle { get; set; }

        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        
        double Amount { get; set; }

        int FK_WorkOrder_ID { get; set; }
        WorkOrder WorkOrder { get; set; }

        string VehicleNumber { get; set; }

        string DeliveryNote { get; set; }
    }
}

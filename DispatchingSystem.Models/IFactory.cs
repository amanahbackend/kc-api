﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IFactory : IBaseEntity
    {
        int Id { get; set; }
        string Name { get; set; }

        string Governorate { get; set; }
        string Area { get; set; }
        string Block { get; set; }
        string Street { get; set; }
        string AddressNote { get; set; }

        double Latitude { get; set; }
        double Longitude { get; set; }

        List<Vehicle> Vehicles { get; set; }
        ICollection<WorkOrder> WorkOrders { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    interface ICustomCustomer : IBaseEntity
    {
        Customer Customer { get; set; }
        WorkOrderFactories WorkOrderFactories { get; set; }
        Contract Contract { get; set; }
        Location Location { get; set; }
    }
}

﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IWorkOrderVehicle
    {
        int WorkOrderId { get; set; }
        WorkOrder WorkOrder { get; set; }

        int VehicleId { get; set; }
        Vehicle Vehicle { get; set; }

        DateTime AssignmentDate { get; set; }

        bool IsDeleted { get; set; }
    }
}

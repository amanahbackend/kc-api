﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface ILKPCondition: IBaseEntity
    {
        int Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IReportFilter : IBaseEntity
    {
        int Id { get; set; }
        string UserId { get; set; }
        string Type { get; set; }
        object Value { get; set; }
    }
}

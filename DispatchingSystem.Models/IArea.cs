﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IArea :IBaseEntity
    {
        string Id { get; set; }
        string Name { get; set; }
         string ArabicName { get; set; }

        string FK_Governrate_Id { get; set; }
    }
}

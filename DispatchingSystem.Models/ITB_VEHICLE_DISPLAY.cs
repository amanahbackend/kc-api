﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface ITB_VEHICLE_DISPLAY
    {
        int ID { get; set; }
        string VID { get; set; }
        string VReg { get; set; }
        string Vtype { get; set; }
        string PanicButton { get; set; }
        string DID { get; set; }
        string DriverName { get; set; }
        string Area { get; set; }
        string Address { get; set; }
        string Lat { get; set; }
        string Long { get; set; }
        DateTime? Date { get; set; }
        DateTime? CreatedDate { get; set; }
        DateTime? ModifyDate { get; set; }
    }
}

﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IBaseEntity
    {
        string FK_CreatedBy_Id { get; set; }


        string FK_UpdatedBy_Id { get; set; }
        

        bool IsDeleted { get; set; }
        DateTime CreatedDate { get; set; }
        DateTime UpdatedDate { get; set; }
        ApplicationUser CurrentUser { get; set; }
    }
}

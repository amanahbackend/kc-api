﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IUpcomingWorkOrder
    {
        int Id { get; set; }
        string WorkOrderCode { get; set; }
    }
}

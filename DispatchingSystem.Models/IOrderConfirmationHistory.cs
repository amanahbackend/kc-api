﻿using DispatchingSystem.Models.Entities;
using DispatchingSystem.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IOrderConfirmationHistory : IBaseEntity
    {
        int Id { get; set; }
        DateTime? OrderDate { get; set; }
        DateTime? OrderAlternativeDate { get; set; }
        string OrderNumber { get; set; }

        string CreatorName { get; set; }
        string CreatorRole { get; set; }

        int Fk_ConfirmationStatus_Id { get; set; }
        LKP_ConfirmationStatus ConfirmationStatus { get; set; }

        int ?Fk_WorkOrder_Id { get; set; }
         SelectedDate SelectedDate { get; set; }

        WorkOrder WorkOrder { get; set; }
    }
}

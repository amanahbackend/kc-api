﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface ILKP_Status:IBaseEntity
    {
        int Id {get; set;}
        string Name {get; set;}

         string ArabicName { get; set; }

    }
}

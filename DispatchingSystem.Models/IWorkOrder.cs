﻿using DispatchingSystem.Models.Entities;
using DispatchingSystem.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IWorkOrder : IBaseEntity
    {
        int Id { get; set; }

        string Code { get; set; }

        double PendingAmount { get; set; }
        double Amount { get; set; }

        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }


        bool HasSuborders { get; set; }

        ICollection<WorkOrder> WorkOrders { get; set; }

        double Percentage { get; set; }

        WorkOrder ParentWorkOrder { get; set; }

        int? FK_Parent_WorkOrder_Id { get; set; }

        int FK_Location_Id { get; set; }
        Location Location { get; set; }

        int FK_Contract_Id { get; set; }
        Contract Contract { get; set; }

        int FK_Priority_Id { get; set; }
        LKP_Priority Priority { get; set; }

        LKP_Status Status { get; set; }

        List<Job> Jobs { get; set; }

        Factory Factory { get; set; }

        int? FK_Item { get; set; }

        Item Items { get; set; }
        SelectedDate SelectedDate { get; set; }


    }
}

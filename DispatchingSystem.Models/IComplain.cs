﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IComplain:IBaseEntity
    {
        int Id { get; set; }
        string Note { get; set; }

        int FK_Customer_Id { get; set; }
        Customer Customer { get; set; }

    }
}

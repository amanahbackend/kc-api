﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IEquipmentType
    {
        int Id { get; set; }
        string Name { get; set; }
        bool IsDeleted { get; set; }
       
    }
}

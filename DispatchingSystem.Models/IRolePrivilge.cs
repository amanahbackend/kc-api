﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IRolePrivilge:IBaseEntity
    {
        int Id { get; set; }

        string FK_ApplicationRole_Id { get; set; }
        ApplicationRole ApplicationRole { get; set; }

        int FK_Privilge_Id { get; set; }
        Privilge Privilge  { get;set;}
    }
}

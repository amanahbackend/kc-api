﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DispatchingSystem.Models
{
    public interface ICustomer : IBaseEntity
    {
        int Id { get; set; }
        string Name { get; set; }
        string CivilId { get; set; }
        
        string Remarks { get; set; }
        string Messenger { get; set; }


        string CompanyName { get; set; }
        string Division { get; set; }
        
        int FK_CustomerType_Id { get; set; }
        CustomerType CustomerType { get; set; }

        List<CustomerPhone> Phones { get; set; }
    }
}

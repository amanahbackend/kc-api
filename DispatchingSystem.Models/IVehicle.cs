﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IVehicle : IBaseEntity
    {
        int Id { get; set; }
        string Number { get; set; }
        bool IsAvailable { get; set; }
        int? FK_Driver_Id { get; set; }
        Driver Driver { get; set; }
        int? FK_Factory_Id { get; set; }
        Job Job { get; set; }
        Factory Factory { get; set; }
        WashingStatus? Fk_WashingStatus_Id { get; set; }
        LKP_WashingStatus WashingStatus { get; set; }
        DateTime? WashingTime { get; set; }
    }
}

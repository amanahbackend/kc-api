﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IContract
    {
        int Id { get; set; }
        string ContractNumber { get; set; }

        DateTime StartDate { get; set; }
        DateTime? EndDate { get; set; }

        //double Amount { get; set; }
        //double PendingAmount { get; set; }
        //double Price { get; set; }

        string Remarks { get; set; }

        int FK_Customer_Id { get; set; }
        Customer Customer { get; set; }

        int FK_ContractType_Id { get; set; }
        ContractType ContractType { get; set; }

         ICollection<ContractItems> ContractItems { get; set; }

    }
}

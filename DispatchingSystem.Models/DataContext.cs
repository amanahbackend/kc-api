﻿using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace DispatchingSystem.Models
{
    public class DataContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }


        public DbSet<Complain> Complains { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<Governrate> Governrates { get; set; }
        public DbSet<ApplicationConfiguration> ApplicationConfiguration { get; set; }



        public DbSet<ContractType> ContractTypes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerType> CustometrTypes { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<EquipmentType> EquipmentTypes { get; set; }
        public DbSet<Factory> Factories { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<LKP_Condition_Item> LKP_Condition_Items { get; set; }
        public DbSet<LKP_Priority> LKP_Priorities { get; set; }
        public DbSet<LKP_Status> LKP_Status { get; set; }
        public DbSet<LKPCondition> LKPConditions { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Privilge> Privilge { get; set; }
        public DbSet<RolePrivilge> RolePrivilges { get; set; }
        public DbSet<CustomerPhone> CustomerPhones { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<WorkOrder> WorkOrders { get; set; }
        public DbSet<UpcomingWorkOrder> UpcomingWorkOrders { get; set; }
        public DbSet<FactoryController> FactoryControllers { get; set; }
        public DbSet<TB_VEHICLE_DISPLAY> TB_VEHICLE_DISPLAY { get; set; }
        public DbSet<ContractItems> ContractItems { get; set; }
        public DbSet<WashingArea> WashingAreas { get; set; }
        public DbSet<LKP_WashingStatus> LKP_WashingStatus { get; set; }
        public DbSet<LKP_ConfirmationStatus> LKP_ConfirmationStatus { get; set; }

        public DbSet<ElementToBePoured> ElementToBePoured { get; set; }

        public DbSet<OrderConfirmationHistory> OrderConfirmationHistory { get; set; }
        public DbSet<LKP_CustomerCategory> CustomerCategories { get; set; }



        //public DbSet<ReportFilter> ReportFilters { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Complain>().ToTable("Complain");
            modelBuilder.Entity<Contract>().ToTable("Contract");
            modelBuilder.Entity<ContractType>().ToTable("ContractType");
            modelBuilder.Entity<Customer>().ToTable("Customer");
            modelBuilder.Entity<CustomerType>().ToTable("CustomerType");
            modelBuilder.Entity<Driver>().ToTable("Driver");
            modelBuilder.Entity<Equipment>().ToTable("Equipment");
            modelBuilder.Entity<EquipmentType>().ToTable("EquipmentType");
            modelBuilder.Entity<Factory>().ToTable("Factory");
            modelBuilder.Entity<Item>().ToTable("Item");
            modelBuilder.Entity<Job>().ToTable("Job");
            modelBuilder.Entity<LKP_Condition_Item>().ToTable("LKP_Condition_Item");
            modelBuilder.Entity<LKP_Priority>().ToTable("LKP_Priority");
            modelBuilder.Entity<LKP_Status>().ToTable("LKP_Status");
            modelBuilder.Entity<LKPCondition>().ToTable("LKPCondition");
            modelBuilder.Entity<Location>().ToTable("Location");
            modelBuilder.Entity<Privilge>().ToTable("Privilge");
            modelBuilder.Entity<RolePrivilge>().ToTable("RolePrivilge");
            modelBuilder.Entity<CustomerPhone>().ToTable("CustomerPhone");
            modelBuilder.Entity<Vehicle>().ToTable("Vehicle");
            modelBuilder.Entity<WorkOrder>().ToTable("WorkOrder");
            modelBuilder.Entity<UpcomingWorkOrder>().ToTable("UpcomingWorkOrder");
            modelBuilder.Entity<FactoryController>().ToTable("FactoryController");
            modelBuilder.Entity<TB_VEHICLE_DISPLAY>().ToTable("TB_VEHICLE_DISPLAY");
            modelBuilder.Entity<ContractItems>().ToTable("ContractItems");
            modelBuilder.Entity<WashingArea>().ToTable("WashingArea");
            modelBuilder.Entity<LKP_WashingStatus>().ToTable("LKP_WashingStatus");
            modelBuilder.Entity<LKP_ConfirmationStatus>().ToTable("LKP_ConfirmationStatus");
            modelBuilder.Entity<OrderConfirmationHistory>().ToTable("OrderConfirmationHistory");
            modelBuilder.Entity<LKP_CustomerCategory>().ToTable("LKP_CustomerCategory");
            modelBuilder.Entity<ElementToBePoured>().ToTable("ElementToBePoured");
            //modelBuilder.Entity<ReportFilter>().ToTable("ReportFilter");

            // movement controller factory relations
            modelBuilder.Entity<FactoryController>()
                .HasOne(m => m.Factory)
                .WithMany()
                .HasForeignKey(x => x.Fk_Factory_Id)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<FactoryController>()
                .HasOne(m => m.Controller)
                .WithMany()
                .HasForeignKey(x => x.Fk_Controller_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // complain customer relation 
            modelBuilder.Entity<Complain>()
                .HasOne(c => c.Customer)
                .WithMany()
                .HasForeignKey(c => c.FK_Customer_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // contract customer relation
            modelBuilder.Entity<Contract>()
                .HasOne(c => c.Customer)
                .WithMany()
                .HasForeignKey(c => c.FK_Customer_Id)
                .OnDelete(DeleteBehavior.Restrict);


            //    modelBuilder.Entity<ContractItems>()
            //.HasKey(bc => new { bc.FK_Contract_Id, bc.FK_Item_Id });

            modelBuilder.Entity<ContractItems>()
                .HasOne(bc => bc.Contract)
                .WithMany(b => b.ContractItems)
                .HasForeignKey(bc => bc.FK_Contract_Id);

            modelBuilder.Entity<ContractItems>()
                .HasOne(bc => bc.Item)
                .WithMany(c => c.ContractItems)
                .HasForeignKey(bc => bc.FK_Item_Id);



            // contract contract type relation
            modelBuilder.Entity<Contract>()
                .HasOne(c => c.ContractType)
                .WithMany()
                .HasForeignKey(c => c.FK_ContractType_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // customer customer type relation
            modelBuilder.Entity<Customer>()
                .HasOne(c => c.CustomerType)
                .WithMany()
                .HasForeignKey(c => c.FK_CustomerType_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // customer customer category relation
            modelBuilder.Entity<Customer>()
                .HasOne(c => c.CustomerCategory)
                .WithMany()
                .HasForeignKey(c => c.FK_CustomerCategory_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // customer phones relation
            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Phones)
                .WithOne()
                .HasForeignKey(cp => cp.Fk_Customer_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // customer location relation
            modelBuilder.Entity<Location>()
                .HasOne(l => l.Contract)
                .WithMany()
                .HasForeignKey(l => l.Fk_Contract_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // equipment equipment type relation
            modelBuilder.Entity<Equipment>()
                .HasOne(e => e.EquipmentType)
                .WithMany()
                .HasForeignKey(e => e.FK_EquipmentType_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // equipment workorder relation
            modelBuilder.Entity<Equipment>()
                .HasOne(eq => eq.WorkOrder)
                .WithMany(wo => wo.Equipment)
                .HasForeignKey(eq => eq.FK_WorkOrder_Id)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<OrderConfirmationHistory>()
     .HasOne(eq => eq.WorkOrder)
     .WithMany(wo => wo.ConfirmationHistories)
     .HasForeignKey(eq => eq.Fk_WorkOrder_Id)
     .OnDelete(DeleteBehavior.Restrict);


            // equipment factory relation
            modelBuilder.Entity<Equipment>()
                .HasOne(eq => eq.Factory)
                .WithMany()
                .HasForeignKey(eq => eq.Fk_Factory_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // job status reltion
            modelBuilder.Entity<Job>()
                .HasOne(j => j.Status)
                .WithMany()
                .HasForeignKey(j => j.FK_Status_Id)
                .OnDelete(DeleteBehavior.Restrict);


            // job vehicle relation
            modelBuilder.Entity<Job>()
                .HasOne(j => j.Vehicle)
                .WithOne(v => v.Job)
                .HasForeignKey<Job>(j => j.FK_Vehicle_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // job work order relation
            modelBuilder.Entity<Job>()
                .HasOne(j => j.WorkOrder)
                .WithMany(w => w.Jobs)
                .HasForeignKey(j => j.FK_WorkOrder_ID)
                .OnDelete(DeleteBehavior.Restrict);

            // condition item relation
            modelBuilder.Entity<LKP_Condition_Item>()
                .HasOne(ci => ci.Condition)
                .WithMany()
                .HasForeignKey(ci => ci.FK_Condition_Id)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<LKP_Condition_Item>()
                .HasOne(ci => ci.Item)
                .WithMany()
                .HasForeignKey(ci => ci.FK_Item_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // role privilege relation
            modelBuilder.Entity<RolePrivilge>()
                .HasOne(rp => rp.ApplicationRole)
                .WithMany()
                .HasForeignKey(rp => rp.FK_ApplicationRole_Id)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<RolePrivilge>()
                .HasOne(rp => rp.Privilge)
                .WithMany()
                .HasForeignKey(rp => rp.FK_Privilge_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // vehicle driver relation
            modelBuilder.Entity<Vehicle>()
                .HasOne(v => v.Driver)
                .WithMany()
                .HasForeignKey(v => v.FK_Driver_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // vehicle factory relation
            modelBuilder.Entity<Vehicle>()
                .HasOne(v => v.Factory)
                .WithMany(f => f.Vehicles)
                .HasForeignKey(v => v.FK_Factory_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // workorder location relation
            modelBuilder.Entity<WorkOrder>()
                .HasOne(w => w.Location)
                .WithMany()
                .HasForeignKey(w => w.FK_Location_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // workorder customer relation
            modelBuilder.Entity<WorkOrder>()
                .HasOne(w => w.Customer)
                .WithMany()
                .HasForeignKey(w => w.FK_Customer_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // workorder contract relation
            modelBuilder.Entity<WorkOrder>()
                .HasOne(w => w.Contract)
                .WithMany()
                .HasForeignKey(w => w.FK_Contract_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // workorder priority relation
            modelBuilder.Entity<WorkOrder>()
                .HasOne(w => w.Priority)
                .WithMany()
                .HasForeignKey(w => w.FK_Priority_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // workorder status relation
            modelBuilder.Entity<WorkOrder>()
                .HasOne(w => w.Status)
                .WithMany()
                .HasForeignKey(w => w.FK_Status_Id)
                .OnDelete(DeleteBehavior.Restrict);




            modelBuilder.Entity<WorkOrder>()
               .HasOne(w => w.confirmationStatus)
               .WithMany()
               .HasForeignKey(w => w.FK_ConfirmationStatus_Id);

            modelBuilder.Entity<WorkOrder>()
              .HasOne(w => w.ElementToBePoured)
              .WithMany()
              .HasForeignKey(w => w.FK_ElementToBePoured_Id);
            // workorder suborder relation
            modelBuilder.Entity<WorkOrder>()
                .HasOne(w => w.ParentWorkOrder)
                .WithMany()
                .HasForeignKey(w => w.FK_Parent_WorkOrder_Id)
                .OnDelete(DeleteBehavior.Restrict);


            // workorder Factory relation
            modelBuilder.Entity<WorkOrder>()
                .HasOne(w => w.Factory)
                .WithMany(f => f.WorkOrders)
                .HasForeignKey(w => w.FK_Factory_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // workorder item relation

            modelBuilder.Entity<WorkOrderItems>()
                .HasOne(wi => wi.Item)
                .WithMany()
                .HasForeignKey(wi => wi.FK_Item_Id)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<WorkOrderItems>()
                .HasOne(wi => wi.WorkOrder)
                .WithMany()
                .HasForeignKey(wi => wi.FK_Workorder_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // contract item relation

            //modelBuilder.Entity<ContractItems>()
            //    .HasOne(ci => ci.Item)
            //    .WithMany()
            //    .HasForeignKey(ci => ci.FK_Item_Id)
            //    .OnDelete(DeleteBehavior.Restrict);

            //modelBuilder.Entity<ContractItems>()
            //    .HasOne(ci => ci.Contract)
            //    .WithMany()
            //    .HasForeignKey(ci => ci.FK_Contract_Id)
            //    .OnDelete(DeleteBehavior.Restrict);

            // factory washing area relation

            modelBuilder.Entity<WashingArea>()
                .HasOne(w => w.Factory)
                .WithMany()
                .HasForeignKey(w => w.Fk_Factory_Id)
                .OnDelete(DeleteBehavior.Restrict);

            // Vehicle Washing Status relation

            modelBuilder.Entity<Vehicle>()
                .HasOne(w => w.WashingStatus)
                .WithMany()
                .HasForeignKey(w => w.Fk_WashingStatus_Id)
                .OnDelete(DeleteBehavior.Restrict);


            modelBuilder.Entity<OrderConfirmationHistory>()
                .HasOne(h => h.ConfirmationStatus)
                .WithMany()
                .HasForeignKey(h => h.Fk_ConfirmationStatus_Id)
                .OnDelete(DeleteBehavior.Restrict);




        }

    }


}

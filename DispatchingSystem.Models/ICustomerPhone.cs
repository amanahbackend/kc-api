﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface ICustomerPhone : IBaseEntity
    {
        int Id { get; set; }
        string Phone { get; set; }
        int Fk_Customer_Id { get; set; }
    }
}

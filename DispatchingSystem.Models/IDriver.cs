﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface IDriver:IBaseEntity
    {
        int Id { get; set; }
        string Name { get; set; }
        string Phone { get; set; }
        bool IsAvailable { get; set; }
    }
}

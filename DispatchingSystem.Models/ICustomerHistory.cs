﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface ICustomerHistory:IBaseEntity
    {
         List<WorkOrderFactories> WorkOrders { get; set; }
         Customer Customer { get; set; }
         IEnumerable<Complain> Complains { get; set; }
         IEnumerable<Contract> Contracts { get; set; }

    }
}

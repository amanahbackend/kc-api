﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class CustomerPhone : BaseEntity, ICustomerPhone
    {
        [Key]
        public int Id { get; set; }
        public string Phone { get; set; }
        public int Fk_Customer_Id { get; set; }
        
    }
}

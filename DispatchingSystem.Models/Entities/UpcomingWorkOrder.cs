﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class UpcomingWorkOrder : IUpcomingWorkOrder
    {
        [Key]
        public int Id { get; set; }
        public string WorkOrderCode { get; set; }
    }
}

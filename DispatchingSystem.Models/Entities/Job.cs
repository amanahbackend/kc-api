﻿using System.ComponentModel.DataAnnotations.Schema;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class Job : BaseEntity, IJob
    {
        [Key]
        public int Id { get; set; }

        public int FK_Status_Id { get; set; }
        [ForeignKey("FK_Status_Id")]
        public LKP_Status Status { get; set; }

        public int? FK_Vehicle_Id { get; set; }
        [ForeignKey("FK_Vehicle_Id")]
        public Vehicle Vehicle { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public double Amount { get; set; }

        public int FK_WorkOrder_ID { get; set; }
        [ForeignKey("FK_WorkOrder_ID")]
        public WorkOrder WorkOrder { get; set; }

        public string VehicleNumber { get; set; }
        public string DeliveryNote { get; set; }

        public bool IsDamaged { get; set; }

        public string  DiscriptionOFDamage { get; set; }

        public string CancellationReason { get; set; }

    }
}

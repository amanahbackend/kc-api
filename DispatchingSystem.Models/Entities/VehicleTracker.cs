﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class VehicleTracker
    {
        public int DriverId { get; set; }
        public string DriverName { get; set; }
        public int FactoryId { get; set; }
        public string FactoryName { get; set; }
        public int JobId { get; set; }
        public int JobStatusId { get; set; }
        public string JobStatus { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
        public string OrderNumber { get; set; }
        public int OrderId { get; set; }
        public string VehicleNumber { get; set; }
        public int VehicleId { get; set; }
        public WashingStatus? WashingStatusId { get; set; }
        public string WashingStatus { get; set; }
    }
}

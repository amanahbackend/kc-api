﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public enum WashingStatus : int
    {
        NotWashed = 1,
        InWashing = 2,
        Washed = 3
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Utilites;

namespace DispatchingSystem.Models.Entities
{
    public class WashingArea : BaseEntity, IWashingArea
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public int? Fk_Factory_Id { get; set; }
        [ForeignKey("Fk_Factory_Id")]
        public Factory Factory { get; set; }

        public string Geometry { get; set; }
        [NotMapped]
        public List<Point> Polygon { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class LKP_Condition_Item:BaseEntity, ILKP_Condition_IItem
    {
        [Key]
        public int Id { get; set; }

        public int FK_Item_Id { get; set; }
        [ForeignKey("FK_Item_Id")]
        public Item Item { get; set;}

        public int FK_Condition_Id { get; set; }
        [ForeignKey("FK_Condition_Id")]
        public LKPCondition Condition { get; set; }

        public string Value { get; set; }
        
    }
}

﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace DispatchingSystem.Models.Entities
{
    public class CustomerType :BaseEntity, ICustomerType
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}

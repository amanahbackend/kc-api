﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class ElementToBePoured : BaseEntity, IElementToBePoured
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class LKP_WashingStatus : BaseEntity, ILKP_WashingStatus
    {
        //[Key]
        public WashingStatus Id { get; set; }
        public string Name { get; set; }
    }
}

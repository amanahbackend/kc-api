﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class BaseEntity : IBaseEntity
    {
        public virtual string FK_CreatedBy_Id { get; set; }

        public virtual string FK_UpdatedBy_Id { get; set; }


        public virtual bool IsDeleted { get; set; }
        public virtual DateTime CreatedDate { get; set; }
        public virtual DateTime UpdatedDate { get; set; }

        [NotMapped]
        public ApplicationUser CurrentUser { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class Suborder : ISuborder
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }

        [Required]
        public double PendingAmount { get; set; }
        [Required]
        public double Amount { get; set; }

        [Required]
        public int WorkOrderId { get; set; }
        public WorkOrder WorkOrder { get; set; }
        public bool IsDeleted { get; set; }
    }
}

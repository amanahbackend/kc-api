﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class Area : BaseEntity, IArea
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ArabicName { get; set; }

        public string FK_Governrate_Id { get; set; }
    }
}

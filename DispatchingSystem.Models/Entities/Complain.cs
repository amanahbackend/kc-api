﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class Complain : BaseEntity,IComplain
    {
        [Key]
        public int Id { get; set; }
        public string Note { get; set; }

        [Required]
        public int FK_Customer_Id { get; set; }
        [ForeignKey("FK_Customer_Id")]
        public Customer Customer { get; set; }

    }
}

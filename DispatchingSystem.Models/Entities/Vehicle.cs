﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class Vehicle : BaseEntity, IVehicle
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Number { get; set; }
        [Required]
        public bool IsAvailable { get; set; }

        public int? FK_Driver_Id { get; set; }
        [ForeignKey("FK_Driver_Id")]
        public Driver Driver { get; set; }

        public int? FK_Factory_Id { get; set; }
        [ForeignKey("FK_Factory_Id")]
        public Factory Factory { get; set; }

        public string  Description  { get; set; }

        public Job Job { get; set; }

        public WashingStatus? Fk_WashingStatus_Id { get; set; }
        [ForeignKey("Fk_WashingStatus_Id")]
        public LKP_WashingStatus WashingStatus { get; set; }

        public DateTime? WashingTime { get; set; }
    }
}

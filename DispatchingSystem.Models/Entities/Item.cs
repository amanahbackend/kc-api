﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class Item : BaseEntity, IItem
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Formula { get; set; }
        public int AvailableAmount { get; set; }
        public double Price { get; set; }
        public ICollection<WorkOrder> WorkOrders { get; set; }

        public ICollection<ContractItems> ContractItems { get; set; }
    }
}

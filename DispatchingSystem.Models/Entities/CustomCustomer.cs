﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class CustomCustomer : BaseEntity, ICustomCustomer
    {
        public Customer Customer { get; set; }
        public WorkOrderFactories WorkOrderFactories { get; set; }
        public Contract Contract { get; set; }
        public Location Location { get; set; }
    }
}

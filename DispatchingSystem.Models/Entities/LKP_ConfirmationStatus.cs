﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class LKP_ConfirmationStatus : BaseEntity, ILKP_ConfirmationStatus
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string ArabicName { get; set; }

    }
}

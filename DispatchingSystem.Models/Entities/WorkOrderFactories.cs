﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class WorkOrderFactories : BaseEntity, IWorkOrderFactories
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int FK_WorkOrder_Id { get; set; }
        [NotMapped]
        public int CreditAmount { get; set; }

        [NotMapped]

        public bool CreatingOrderFirstTime { get; set; }


        public WorkOrder WorkOrder { get; set; }

        public ICollection<FactorySubOrderPercentage> SubOrdersFactoryPercent { get; set; }
   
        [Required]
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

    }
}

﻿using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class CustomerHistory:BaseEntity, ICustomerHistory
    {
        public List<WorkOrderFactories> WorkOrders { get; set; }
        public Customer Customer { get; set; }
        public IEnumerable<Complain> Complains { get; set; }
        public IEnumerable<Contract> Contracts { get; set; }
    }
}

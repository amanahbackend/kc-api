﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class FactoryController : BaseEntity, IFactoryController
    {
        [Key]
        public int Id { get; set; }

        public int Fk_Factory_Id { get; set; }
        [ForeignKey("Fk_Factory_Id")]
        public Factory Factory { get; set; }

        public string Fk_Controller_Id { get; set; }
        [ForeignKey("Fk_Controller_Id")]
        public ApplicationUser Controller { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class WorkOrderItems : BaseEntity, IWorkOrderItems
    {
        [Key]
        public int Id { get; set; }

        public int FK_Item_Id { get; set; }
        [ForeignKey("FK_Item_Id")]
        public Item Item { get; set; }
        public double Price { get; set; }

        public int FK_Workorder_Id { get; set; }
        [ForeignKey("FK_Workorder_Id")]
        public WorkOrder WorkOrder { get; set; }

    }
}

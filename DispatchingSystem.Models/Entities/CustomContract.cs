﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class CustomContract : BaseEntity
    {
        public Contract Contract { get; set; }
        public List<Location> Locations { get; set; }
    }
}

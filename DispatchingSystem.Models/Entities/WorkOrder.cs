﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using DispatchingSystem.Models.Enums;

namespace DispatchingSystem.Models.Entities
{
    public class WorkOrder : BaseEntity, IWorkOrder
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Code { get; set; }

        public string Description { get; set; }


        public double Percentage { get; set; }

        [Required]
        public double PendingAmount { get; set; }
        [Required]
        public double Amount { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public bool HasSuborders { get; set; }
        [NotMapped]
        public ICollection<WorkOrder> WorkOrders { get; set; }

        [ForeignKey("FK_Parent_WorkOrder_Id")]
        public WorkOrder ParentWorkOrder { get; set; }

        public int? FK_Parent_WorkOrder_Id { get; set; }

        [Required]
        public int FK_Location_Id { get; set; }
        [ForeignKey("FK_Location_Id")]
        public Location Location { get; set; }

        [Required]
        public int FK_Customer_Id { get; set; }
        [ForeignKey("FK_Customer_Id")]
        public Customer Customer { get; set; }

        [Required]
        public int FK_Contract_Id { get; set; }
        [ForeignKey("FK_Contract_Id")]
        public Contract Contract { get; set; }

        [Required]
        public int FK_Priority_Id { get; set; }
        [ForeignKey("FK_Priority_Id")]
        public LKP_Priority Priority { get; set; }

        public int FK_Status_Id { get; set; }
        [ForeignKey("FK_Status_Id")]
        public LKP_Status Status { get; set; }

        public int? FK_Factory_Id { get; set; }
        [ForeignKey("FK_Factory_Id")]
        public Factory Factory { get; set; }
        public int? FK_Item { get; set; }
        [ForeignKey("FK_Item")]

        public Item Items { get; set; }

        public List<Job> Jobs { get; set; }


        public ICollection<Equipment> Equipment { get; set; }

        public ICollection<OrderConfirmationHistory> ConfirmationHistories { get; set; }

        public bool CanConfirm { get; set; }

        public SelectedDate SelectedDate { get; set; }

        public int? FK_ConfirmationStatus_Id { get; set; }
        [ForeignKey("FK_ConfirmationStatus_Id")]
        public LKP_ConfirmationStatus confirmationStatus { get; set; }


        public Decline IsDeclined { get; set; }

        public int? FK_ElementToBePoured_Id { get; set; }
        [ForeignKey("FK_ElementToBePoured_Id")]
        public ElementToBePoured ElementToBePoured { get; set; }
        public DateTime PouringTime { get; set; }

        public string Reason { get; set; }

        public string SelectedLang { get; set; }


    }


}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class RolePrivilge:BaseEntity,IRolePrivilge
    {
        [Key]
        public int Id { get; set; }
         
        public string FK_ApplicationRole_Id { get; set; }
        [ForeignKey("FK_ApplicationRole_Id")]
        public ApplicationRole ApplicationRole { get; set; }
         
        public int FK_Privilge_Id { get; set; }
        [ForeignKey("FK_Privilge_Id")]
        public Privilge Privilge { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class Governrate : BaseEntity,IGovernrate
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public string ArabicName { get; set; }

    }
}

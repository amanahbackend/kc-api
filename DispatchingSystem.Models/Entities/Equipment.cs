﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class Equipment : BaseEntity, IEquipment
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int Number { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int FK_EquipmentType_Id { get; set; }
        [ForeignKey("FK_EquipmentType_Id")]
        public EquipmentType EquipmentType { get; set; }

        public int? Fk_Factory_Id { get; set; }
        [ForeignKey("Fk_Factory_Id")]
        public Factory Factory { get; set; }

        public int? FK_WorkOrder_Id { get; set; }
        [ForeignKey("FK_WorkOrder_Id")]
        public WorkOrder WorkOrder { get; set; }

        [Required]
        public bool IsAvailable { get; set; }

    }
}

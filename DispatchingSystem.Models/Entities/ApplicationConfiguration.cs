﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class ApplicationConfiguration : BaseEntity, IApplicationConfiguration
    {

        [Key]
        public int Id { get; set; }
        public int TransportationTime { get; set; }


    }
}

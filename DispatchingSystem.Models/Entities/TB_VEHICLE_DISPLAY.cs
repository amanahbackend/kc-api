﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class TB_VEHICLE_DISPLAY : ITB_VEHICLE_DISPLAY
    {
        [Key]
        public int ID { get; set; }
        public string VID { get; set; }
        public string VReg { get; set; }
        public string Vtype { get; set; }
        public string PanicButton { get; set; }
        public string DID { get; set; }
        public string DriverName { get; set; }
        public string Area { get; set; }
        public string Address { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}

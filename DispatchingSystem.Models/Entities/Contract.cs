﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace DispatchingSystem.Models.Entities
{
    public class Contract : BaseEntity, IContract
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string ContractNumber { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        
        public DateTime? EndDate { get; set; }

        //[Required]
        //public double Amount { get; set; }
        //[Required]
        //public double PendingAmount { get; set; }

        //public double Price { get; set; }
        public string Remarks { get; set; }

        [Required]
        public int FK_Customer_Id { get; set; }
        [ForeignKey("FK_Customer_Id")]
        public Customer Customer { get; set; }

        [Required]
        public int FK_ContractType_Id { get; set; }
        [ForeignKey("FK_ContractType_Id")]
        public ContractType ContractType { get; set; }
        [NotMapped]
        public string File { get; set; }
        [NotMapped]
        public string FileName { get; set; }
        public string Path { get; set; }

        public ICollection<ContractItems> ContractItems { get; set; }
    }
}

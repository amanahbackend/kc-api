﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class WorkOrderReportResult
    {
        public int Id;
        public string FactoryName { get; set; }

        public string KCRM_Customer_Id { get; set; }

        public string Customer_Name { get; set; }

        public List<CustomerPhone> Customer_Phone { get; set; }

        public string Location { get; set; }

        public string Vechile_Name { get; set; }

        public string Poured_Element { get; set; }
        public string SelectedLang { get; set; }




        public string ReadyMixtype { get; set; }

        public double Amount { get; set; }



        public string Code { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        //public bool HasSuborders { get; set; }
        public string Priority { get; set; }
        public string Status { get; set; }
        //public int JobsCount { get; set; }
        //public string FK_CreatedBy_Id { get; set; }
        //public string FK_UpdatedBy_Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }
        public int Customer_ID { get; set; }
        public string LocationRemarks { get; set; }
        public string LocationBlock { get; set; }
        public string LocationStreet { get; set; }
        public string EngStatus { get; set; }
        public string ArStatus { get; set; }
    }

    public class WorkOrderReportResultToFile
    {
        public int Id;
        public string Code { get; set; }
        public double PendingAmount { get; set; }
        public double Amount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Percentage { get; set; }
        public string Priority { get; set; }
        public string Status { get; set; }
        public int JobsCount { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }


        public string CustomerName { get; set; }
        public string CustomerCivilId { get; set; }
        public string CustomerCompanyName { get; set; }
        public string CustomerDivision { get; set; }

        public string FactoryName { get; set; }

        public string ContractNumber { get; set; }
        public DateTime ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public double ContractAmount { get; set; }
        public double ContractPendingAmount { get; set; }

        public double ContractPrice { get; set; }
    }
}

﻿using DispatchingSystem.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class OrderConfirmationHistory : BaseEntity, IOrderConfirmationHistory
    {
        [Key]
        public int Id { get; set; }

        public DateTime? OrderDate { get; set; }
        public DateTime? OrderAlternativeDate { get; set; }
        public string OrderNumber { get; set; }

        public string CreatorName { get; set; }
        public string CreatorRole { get; set; }

        public int Fk_ConfirmationStatus_Id { get; set; }
        [ForeignKey("Fk_ConfirmationStatus_Id")]
        public LKP_ConfirmationStatus ConfirmationStatus { get; set; }
        public ConfirmType ConfirmType { get; set; }

        public int ? Fk_WorkOrder_Id { get; set; }
        [ForeignKey("Fk_WorkOrder_Id")]
        public WorkOrder WorkOrder { get; set; }
        [NotMapped]
        public SelectedDate SelectedDate { get; set; }
        [NotMapped]

        public Decline IsDeclined { get; set; }

        [NotMapped]
        public int? FactoryId { get; set; }

        [NotMapped]

        public string Reason { get; set; }
        [NotMapped]
        public string Description { get; set; }

    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class ApplicationRole : IdentityRole, IBaseEntity, IApplicationRole
    {
        IBaseEntity baseEntity;

        public ApplicationRole() : base()
        {

        }

        public ApplicationRole(IBaseEntity _baseEntity, string roleName) : base(roleName)
        {
            baseEntity = _baseEntity;
        }

        public string FK_CreatedBy_Id
        {
            get;
            set;
        }
        public string FK_UpdatedBy_Id
        {
            get;
            set;
        }
        public bool IsDeleted
        {
            get;
            set;
        }
        public DateTime CreatedDate
        {
            get;
            set;
        }
        public DateTime UpdatedDate
        {
            get;
            set;
        }

        [NotMapped]
        public ApplicationUser CurrentUser { get; set; }

    }
}

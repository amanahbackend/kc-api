﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DispatchingSystem.Models.Entities
{
    public class Customer : BaseEntity, ICustomer
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [MaxLength(12)]
        public string CivilId { get; set; }
        public string KCRM_Customer_Id { get; set; }
        public string Remarks { get; set; }
        public string CompanyName { get; set; }
        
        public string  Messenger { get; set; }


        public string Division { get; set; }
        public double CreditLimit { get; set; }
        public double Balance { get; set; }
        //public double AmountDue { get; set; }

            [NotMapped]
        public double IncreaseAmount { get; set; }

        public double Debit { get; set; }


        [Required]
        public int FK_CustomerType_Id { get; set; }
        [ForeignKey("FK_CustomerType_Id")]
        public CustomerType CustomerType { get; set; }

        [Required]
        public int? FK_CustomerCategory_Id { get; set; }
        [ForeignKey("FK_CustomerCategory_Id")]
        public LKP_CustomerCategory CustomerCategory { get; set; }

        public List<CustomerPhone> Phones { get; set; }
    }
}

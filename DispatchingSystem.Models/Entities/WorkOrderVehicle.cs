﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class WorkOrderVehicle : IWorkOrderVehicle
    {
        [Required]
        public int WorkOrderId { get; set; }
        public WorkOrder WorkOrder { get; set; }

        [Required]
        public int VehicleId { get; set; }
        public Vehicle Vehicle { get; set; }

        [Required]
        public DateTime AssignmentDate { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class ContractItems : BaseEntity, IContractItems
    {
        [Key]

        public int Id { get; set; }

        public int FK_Item_Id { get; set; }
        [ForeignKey("FK_Item_Id")]
        public Item Item { get; set; }

        public int FK_Contract_Id { get; set; }
        [ForeignKey("FK_Contract_Id")]
        public Contract Contract { get; set; }

        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string Description { get; set; }

        //[NotMapped]
        public double Price { get; set; }
        public bool IsDisabled { get; set; }
        public DateTime? DisableDate { get; set; }
    }
}

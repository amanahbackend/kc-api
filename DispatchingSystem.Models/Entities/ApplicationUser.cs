﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class ApplicationUser : IdentityUser, IBaseEntity, IApplicationUser
    {
        IBaseEntity baseEntity;

        public ApplicationUser(IBaseEntity _baseEntity)
        {
            baseEntity = _baseEntity;
        }
        public ApplicationUser() : base()
        {

        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FK_CreatedBy_Id
        {
            get;
            set;
        }
        public string FK_UpdatedBy_Id
        {
            get;
            set;
        }
        public bool IsDeleted
        {
            get;
            set;
        }
        public DateTime CreatedDate
        {
            get;
            set;
        }
        public DateTime UpdatedDate
        {
            get;
            set;
        }
        [NotMapped]
        public ApplicationUser CurrentUser { get; set; }
        [NotMapped]
        public string[] RoleNames { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class Location : BaseEntity, ILocation
    {
        [Key]
        public int Id { get; set; }

        //[Required, MaxLength(8)]
        public string PACINumber { get; set; }
        [Required]
        public string Title { get; set; }

        [Required]
        public string Governorate { get; set; }
        [Required]
        public string Area { get; set; }
        //[Required]
        public string Block { get; set; }
        //[Required]
        public string Street { get; set; }

        public string SelectedLang { get; set; }
        public string AddressNote { get; set; }

        [Required]
        public double Latitude { get; set; }
        [Required]
        public double Longitude { get; set; }

        public int? Fk_Contract_Id { get; set; }
        [ForeignKey("Fk_Contract_Id")]
        public Contract Contract { get; set; }
    }
}

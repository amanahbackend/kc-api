﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
   public class Privilge:BaseEntity,IPrivilge
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

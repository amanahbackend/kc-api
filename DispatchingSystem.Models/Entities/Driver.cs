﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class Driver : BaseEntity,IDriver
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public string Phone { get; set; }
        [Required]
        public bool IsAvailable { get; set; }
    }
}

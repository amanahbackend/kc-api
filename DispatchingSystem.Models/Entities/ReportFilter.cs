﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Utilities.Paging;

namespace DispatchingSystem.Models.Entities
{
    public class ReportFilter : BaseEntity, IReportFilter
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Type { get; set; }
        public object Value { get; set; }
        public PaginatedItems pagingParameterModel { get; set; }

    }
}

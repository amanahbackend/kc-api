﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class Factory : BaseEntity, IFactory
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public string Governorate { get; set; }        
        public string Area { get; set; }        
        public string Block { get; set; }        
        public string Street { get; set; }
        public string AddressNote { get; set; }

        [Required]
        public double Latitude { get; set; }
        [Required]
        public double Longitude { get; set; }

        public List<Vehicle> Vehicles { get; set; }
        public ICollection<WorkOrder> WorkOrders { get; set; }

    }
}

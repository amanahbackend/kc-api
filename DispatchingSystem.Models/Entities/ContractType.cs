﻿
using System.ComponentModel.DataAnnotations;

namespace DispatchingSystem.Models.Entities
{
    public class ContractType :BaseEntity,IContractType
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public class FactorySubOrderPercentage : BaseEntity, IFactorySubOrderPercentage
    {
        public Factory Factory { get; set; }
        public int FK_SubOrder_Id { get; set; }
        public WorkOrder SubOrder { get; set; }
        public double Percentage { get; set; }
        public double Amount { get; set; }
    }
}

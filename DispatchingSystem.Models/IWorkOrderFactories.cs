﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DispatchingSystem.Models.Entities
{
    public interface IWorkOrderFactories : IBaseEntity
    {
        int Id { get; set; }

        int FK_WorkOrder_Id { get; set; }


        WorkOrder WorkOrder { get; set; }

        ICollection<FactorySubOrderPercentage> SubOrdersFactoryPercent { get; set; }

        DateTime StartDate { get; set; }

        DateTime EndDate { get; set; }

    }
}

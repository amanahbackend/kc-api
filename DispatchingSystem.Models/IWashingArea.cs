﻿using DispatchingSystem.Models.Entities;
using System.Collections.Generic;
using Utilites;

namespace DispatchingSystem.Models
{
    public interface IWashingArea : IBaseEntity
    {
        int Id { get; set; }
        string Name { get; set; }

        int? Fk_Factory_Id { get; set; }
        Factory Factory { get; set; }

        string Geometry { get; set; }
        List<Point> Polygon { get; set; }
    }
}

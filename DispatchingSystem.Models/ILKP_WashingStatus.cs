﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface ILKP_WashingStatus : IBaseEntity
    {
        WashingStatus Id { get; set; }
        string Name { get; set; }
    }
}

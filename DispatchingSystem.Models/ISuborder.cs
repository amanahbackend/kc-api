﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.Models
{
    public interface ISuborder
    {
        int Id { get; set; }
        string Code { get; set; }
        double PendingAmount { get; set; }
        double Amount { get; set; }
        int WorkOrderId { get; set; }
        WorkOrder WorkOrder { get; set; }
        bool IsDeleted { get; set; }
    }
}

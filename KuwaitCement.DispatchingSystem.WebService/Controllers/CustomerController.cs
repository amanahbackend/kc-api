﻿using AutoMapper;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilities.ExcelToList;
using Utilities.Paging;
using Helper = KuwaitCement.DispatchingSystem.WebService.Utilites.Helper;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/Customer")]
    public class CustomerController : Controller
    {
        private ICustomerManager _manager;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private IConfigurationRoot _configurationRoot;
        private ICustomerPhoneManager _customerPhoneManager;
        private readonly ILKP_CustomerCategoryManager _categoryManager;
        private readonly IHostingEnvironment _hostingEnv;
        private readonly ICustomerTypeManager _customerTypeManager;

        public CustomerController(ICustomerManager manager, IMapper mapper,
            UserManager<ApplicationUser> userManager, IConfigurationRoot configurationRoot,
            ICustomerPhoneManager customerPhoneManager, ILKP_CustomerCategoryManager categoryManager,
            IHostingEnvironment hostingEnv, ICustomerTypeManager customerTypeManager)
        {
            _customerTypeManager = customerTypeManager;
            _manager = manager;
            _mapper = mapper;
            _userManager = userManager;
            _configurationRoot = configurationRoot;
            _customerPhoneManager = customerPhoneManager;
            _categoryManager = categoryManager;
            _hostingEnv = hostingEnv;
        }


        [Route("GetAll")]
        public IActionResult Index()
        {

            return Ok();
        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<CustomerViewModel> GetAsync(int id)
        {

            var entityResult = await _manager.GetAsync(id);
            var result = _mapper.Map<Customer, CustomerViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<List<CustomerViewModel>> GetAsync()
        {

            var entityResult = await _manager.GetAllAsync().ToListAsync();
            var result = _mapper.Map<List<Customer>, List<CustomerViewModel>>(entityResult);
            return result;
        }

        [Route("GetAllByPaging")]
        [HttpPost]
        public async Task<IActionResult> GetAsync([FromBody]PaginatedItems pagingParameterModel)
        {
            var entityResult = await _manager.GetAllCustomerByPagingAsync(pagingParameterModel);
            var result = _mapper.Map<List<Customer>, List<CustomerViewModel>>(entityResult.Result);
            return Ok((result, entityResult.TotalCount));
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CustomerViewModel model)
        {

            var entityResult = _mapper.Map<CustomerViewModel, Customer>(model);
            await FillEntityIdentity(entityResult);
            entityResult = await _manager.AddAsync(entityResult);
            var result = _mapper.Map<Customer, CustomerViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]CustomerViewModel model)
        {

            var entityResult = _mapper.Map<CustomerViewModel, Customer>(model);
            await FillEntityIdentity(entityResult);


            var result = await _manager.UpdateCustomerAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("DeleteCustomer")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery]int id)
        {
            bool result = false;
            Customer entity = await _manager.GetSelctedCustomer(id);
            await FillEntityIdentity(entity);
            result = await _manager.DeleteAsync(entity);
            return Ok(result);
        }


        [Route("GetSelctedCustomer")]
        [HttpDelete]
        public async Task<IActionResult> GetSelctedCustomer([FromQuery]int id)
        {
            bool result = false;
            Customer entity = await _manager.GetSelctedCustomer(id);
            return Ok(entity);
        }
        #endregion
        #endregion


        #region Custom Buisness
        //[Route("CustomerHistory/{id}")]
        //[HttpGet]
        //public IActionResult CustomerHistory(int id)
        //{
        //    CustomerHistoryViewModel result = new CustomerHistoryViewModel();
        //    CustomerHistory entityResult = new CustomerHistory();
        //    entityResult = _manager.GetCustomerHistory(id);
        //    result = _mapper.Map<CustomerHistory, CustomerHistoryViewModel>(entityResult);
        //    return Ok(result);
        //}

        [Route("GetCustomerDetailsByIdAsync/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetCustomerDetailsByIdAsync(int id)
        {

            var entityResult = await _manager.SearchForCustomerAsync(id);
            var result = _mapper.Map<Customer, CustomerViewModel>(entityResult);
            return Ok(result);
        }





        [Route("SearchCustomer")]
        [HttpGet]
        public async Task<IActionResult> SearchCustomerAsync([FromQuery]string searchToken)
        {

            var entityResult = await _manager.SearchForCustomerAsync(searchToken);
            var result = _mapper.Map<List<Customer>, List<CustomerViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("CreateDetailedCustomer")]
        [HttpPost]
        public async Task<IActionResult> CreateFullCustomer([FromBody]CustomCustomerViewModel model)
        {
            CustomCustomer entityResult = new CustomCustomer();
            entityResult = _mapper.Map<CustomCustomerViewModel, CustomCustomer>(model);
            await FillEntityIdentity(entityResult);

            //string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            //string paciServiceUrl = _configurationRoot["Location:PACIService:ServiceUrl"];
            //string paciFieldName = _configurationRoot["Location:PACIService:PACIFieldName"];
            //string streetServiceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
            //string streetFieldName = _configurationRoot["Location:StreetService:StreetFieldName"];
            //string blockNameFieldNameStreetService = _configurationRoot["Location:StreetService:BlockNameFieldName"];

            entityResult = await _manager.CreateCustomCustomerAsync(entityResult);
            model = _mapper.Map<CustomCustomer, CustomCustomerViewModel>(entityResult);
            return Ok(model);
        }

        [HttpGet]
        [Route("IsPhoneExist")]
        public async Task<IActionResult> IsPhoneExistAsync(string phone)
        {
            var result = await _customerPhoneManager.IsPhoneExistAsync(phone);
            return Ok(result);
        }

        [HttpGet]
        [Route("IsCustomerNameExist")]
        public async Task<IActionResult> IsCustomerNameExistAsync(string name)
        {
            var result = await _manager.IsCustomerNameExist(name);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetLatestCustomers")]
        public async Task<IActionResult> GetLatestCustomersAsync()
        {
            var entities = await _manager.GetLatestCustomers(10);
            var models = Mapper.Map<List<Customer>, List<CustomerViewModel>>(entities);
            return Ok(models);
        }

        [HttpGet]
        [Route("CivilIdExist")]
        public async Task<IActionResult> CivilIdExistAsync([FromQuery]string civilId)
        {
            var result = await _manager.CivilIdExist(civilId);
            return Ok(result);
        }

        [HttpGet]
        [Route("KCRMIdExist")]
        public async Task<IActionResult> KCRMIdExistAsync([FromQuery]string kcrmId)
        {
            var result = await _manager.KCRMIdExist(kcrmId);
            return Ok(result);
        }

        [HttpPost, Route("AddFromFile")]
        public IActionResult AddFromFile([FromBody]UploadFile file)
        {
            UploadExcelFileManager uploadExcelFileManager = new UploadExcelFileManager();
            var webRootInfo = _hostingEnv.WebRootPath;
            string path = webRootInfo + $@"\Attachments\Upload\customers_{new Random().Next()}.{StringUtilities.GetFileExtension(file.FileContent)}";
            path = uploadExcelFileManager.AddFile(file, path);
            //string excelPath = ExcelReader.CheckPath(file.FileName, path);
            IList<CustomerFromExcelViewModel> dataList = ExcelReader.GetDataToList(path, GetItems);
            var result = SaveCustomersAsync(dataList);
            return Ok(result);
        }

        private async Task<List<Customer>> SaveCustomersAsync(IList<CustomerFromExcelViewModel> dataList)
        {
            var result = new List<Customer>();
            foreach (var item in dataList)
            {
                List<string> phones = new List<string>();
                if (item.CustomerPhones.Equals('-'))
                {

                }
                else if (item.CustomerPhones.Contains('-'))
                {
                    phones = item.CustomerPhones.Split('-').ToList();
                }
                else
                {
                    phones.Add(item.CustomerPhones);
                }
                Customer customer = new Customer
                {
                    Balance = 0,
                    CreditLimit = item.CreditLimit,
                    FK_CustomerCategory_Id = (await _categoryManager.GetAllAsync().FirstOrDefaultAsync(x => x.Name.Equals(item.CustomerCategory)))?.Id,
                    KCRM_Customer_Id = item.KCRM_Customer_Id,
                    Name = item.Name,
                    FK_CustomerType_Id = (await _customerTypeManager.GetAllAsync().FirstOrDefaultAsync(x => x.Name.Equals(item.CustomerType, StringComparison.CurrentCultureIgnoreCase))).Id
                };
                customer = await _manager.AddAsync(customer);
                if (customer != null)
                {
                    foreach (var phone in phones)
                    {
                        CustomerPhone customerPhone = new CustomerPhone
                        {
                            Fk_Customer_Id = customer.Id,
                            Phone = phone.Trim()
                        };
                        await _customerPhoneManager.AddAsync(customerPhone);
                        customer.Phones.Add(customerPhone);
                    }
                    result.Add(customer);
                }
            }
            return result;
        }

        public CustomerFromExcelViewModel GetItems(IList<string> rowData, IList<string> columnNames)
        {
            var customer = new CustomerFromExcelViewModel()
            {
                KCRM_Customer_Id = rowData[1].ToString(),
                Name = rowData[2].ToString(),
                CreditLimit = rowData[8].ToDouble(),
                CustomerCategory = rowData[7].ToString(),
                CustomerPhones = rowData[9].ToString(),
                CustomerType = rowData[6].ToString()
            };
            return customer;
        }

        #endregion
        private async Task<IBaseEntity> FillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(_userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        private async Task<List<IBaseEntity>> FillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(_userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
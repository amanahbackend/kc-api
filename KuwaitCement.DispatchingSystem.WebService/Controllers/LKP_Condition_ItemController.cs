﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using AutoMapper;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/LKP_Condition_Item")]
    public class LKP_Condition_ItemController : Controller
    {
        public ILKP_Condition_ItemManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public LKP_Condition_ItemController(ILKP_Condition_ItemManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;

        }


        [Route("GetAll")]
        public IActionResult Index()
        {
          
            return Ok();
        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<LKP_Condition_ItemViewModel> GetAsync(int id)
        {
           
           var entityResult =await  manger.GetAsync(id);
          var  result = mapper.Map<LKP_Condition_Item, LKP_Condition_ItemViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public List<LKP_Condition_ItemViewModel> Get()
        {
            List<LKP_Condition_ItemViewModel> result = new List<LKP_Condition_ItemViewModel>();
            List<LKP_Condition_Item> entityResult = new List<LKP_Condition_Item>();
            entityResult = manger.GetAllAsync().ToList();
            result = mapper.Map<List<LKP_Condition_Item>, List<LKP_Condition_ItemViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]LKP_Condition_ItemViewModel model)
        {
           
           var entityResult = mapper.Map<LKP_Condition_ItemViewModel, LKP_Condition_Item>(model);
            await fillEntityIdentity(entityResult);
            entityResult = await manger.AddAsync(entityResult);
           var  result = mapper.Map<LKP_Condition_Item, LKP_Condition_ItemViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]LKP_Condition_ItemViewModel model)
        {
           
           var entityResult = mapper.Map<LKP_Condition_ItemViewModel, LKP_Condition_Item>(model);
            await fillEntityIdentity(entityResult);
           var  result = await manger.UpdateAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {

            LKP_Condition_ItemViewModel model = new LKP_Condition_ItemViewModel();
            model.Id = id;
           var entityResult = mapper.Map<LKP_Condition_ItemViewModel, LKP_Condition_Item>(model);
            await fillEntityIdentity(entityResult);
           var result = await manger.DeleteAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion
        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
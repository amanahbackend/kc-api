﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using AutoMapper;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/Job")]
    public class JobController : Controller
    {
        private IJobManager _manager;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfigurationRoot _configuration;
        private readonly IHubContext<DispatchingHub> _hubContext;

        public JobController(IJobManager manager,IHubContext<DispatchingHub> hubContext, IMapper mapper, UserManager<ApplicationUser> userManager, IConfigurationRoot configuration)
        {
            _manager = manager;
            _mapper = mapper;
            _userManager = userManager;
            _configuration = configuration;
            _hubContext = hubContext;
        }


        [Route("GetAll")]
        public IActionResult Index()
        {

            return Ok();
        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> GetAsync(int id)
        {

            var entityResult = await _manager.GetAsync(id);
            var result = _mapper.Map<Job, JobViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {

            var entityResult = await _manager.GetAllAsync().ToListAsync();
            var result = _mapper.Map<List<Job>, List<JobViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]JobViewModel model)
        {

            var entityResult = _mapper.Map<JobViewModel, Job>(model);
            await FillEntityIdentity(entityResult);
            entityResult = await _manager.AddJobAsync(entityResult);
            if (entityResult != null)
            {
                var result = _mapper.Map<Job, JobViewModel>(entityResult);
                return Ok(result);
            }
            else
            {
                return Ok(false);
            }
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]JobViewModel model)
        {
           var  entityResult = _mapper.Map<JobViewModel, Job>(model);
            await FillEntityIdentity(entityResult);
            bool isUpdated =await _manager.UpdateAsync(entityResult);
            if (isUpdated)
            {
                return Ok(entityResult);
            }
            else
            {
                return null;
            }
        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
         
            JobViewModel model = new JobViewModel();
            model.Id = id;
            var entityResult = _mapper.Map<JobViewModel, Job>(model);
            await FillEntityIdentity(entityResult);
            var result = await _manager.DeleteAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion

        [Route("UpdateDeliveryNote")]
        [HttpPost]
        public async Task<IActionResult> UpdateDeliveryNoteAsync([FromBody]JobViewModel model)
        {
            var job = Mapper.Map<JobViewModel, Job>(model);
            var result =await _manager.UpdateDeliveryNote(job);
            return Ok(result);
        }

        [Route("GetByWorkOrder")]
        [HttpGet]
        public async Task<IActionResult> GetJobsByWorkOrder(int workorderId)
        {
            var result =  await _manager.GetCompletedCancelledBy(workorderId);
          
            return Ok(result);
        }

        [Route("GetAllByWorkOrder")]
        [HttpGet]
        public async Task<IActionResult> GetAllByWorkOrder(int workorderId)
        {
            var result =await  _manager.GetAllByWorkOrder(workorderId);
            
            return Ok(result);
        }

        [AllowAnonymous]
        [Route("SetVehicleStatusCompleted")]
        [HttpGet]
        public async Task<IActionResult> SetVehicleStatusCompleted([FromQuery] string vechileNumber, [FromQuery] string pushButtonString)
        {
            Job dispatchedJob = new Job();
            if (pushButtonString == "PushButton")
            {
                var optsBuilder = new DbContextOptionsBuilder<DataContext>();
                string connectionString = _configuration.GetConnectionString("DefaultConnection");
                string migrationAssembly = _configuration["MigrationsAssembly"];
                optsBuilder.UseSqlServer(connectionString,
                            sqlOptions => sqlOptions.MigrationsAssembly(migrationAssembly));
                DataContext dataContext = new DataContext(optsBuilder.Options);
                dispatchedJob = await _manager.SetVehicleStatusCompleted(vechileNumber, pushButtonString, dataContext);

                string notificationMassage = "Job :" + " " + dispatchedJob.Id + " " + "on order :" + " " + dispatchedJob.WorkOrder.Code + " " + "Completed";
                await _hubContext.Clients.Group("DispatchersGroup").SendAsync("SignalMessageReceived", new { notificationMassage , dispatchedJob.Id , dispatchedJob.WorkOrder.Code });
            }

            return Ok(dispatchedJob);
        }

        [AllowAnonymous]
        [Route("CheckConcreteMixedTemperature")]
        [HttpGet]
        public async Task<IActionResult> CheckConcreteMixedTemperature([FromQuery] string vechileId, [FromQuery] string temperatureValue, [FromQuery] string name)
        {
            Job inprogressJob = new Job();

            if (name == "Temperature")
            {
                var optsBuilder = new DbContextOptionsBuilder<DataContext>();
                string connectionString = _configuration.GetConnectionString("DefaultConnection");
                string migrationAssembly = _configuration["MigrationsAssembly"];
                optsBuilder.UseSqlServer(connectionString,
                            sqlOptions => sqlOptions.MigrationsAssembly(migrationAssembly));
                DataContext dataContext = new DataContext(optsBuilder.Options);
                 inprogressJob = await _manager.CheckConcreteMixedTemperature(vechileId, temperatureValue, name, dataContext);

                string notificationMassage = "Job :" + " " + inprogressJob.Id + " " + "on order :" + " " + inprogressJob.WorkOrder.Code + " " + "has been exceeded the normal temperature ";

                await _hubContext.Clients.Group("DispatchersGroup").SendAsync("TemperatureExceeded", new { notificationMassage, inprogressJob.Id, inprogressJob.WorkOrder.Code, inprogressJob.IsDamaged });

            }



            return Ok(inprogressJob);
        }




        [Route("Reassign")]
        [HttpGet]
        public async Task<IActionResult> ReassignJob(int jobId, int workorderId)
        {
            var entity =await  _manager.GetAsync(jobId);
            await FillEntityIdentity(entity);
            Job result = await _manager.ReassignJob(entity, workorderId);
            return Ok(result);
        }

        [Route("Cancel")]
        [HttpGet]
        public async Task<IActionResult> CancelJob(int jobId,string cancelReason)
        {
            var entity =await  _manager.GetAsync(jobId);
            await FillEntityIdentity(entity);
            entity.CancellationReason = cancelReason;
            Job result = await _manager.CancelJob(entity);
            return Ok(result);
        }
        [Route("JobDone")]
        [HttpGet]
        public async Task<IActionResult> JobDone(int jobId)
        {
            var entity = await _manager.GetAsync(jobId);
            await FillEntityIdentity(entity);
            Job result = await _manager.JobDone(entity);
            return Ok(result);
        }

        public async Task<IBaseEntity> FillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(_userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> FillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(_userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }


}
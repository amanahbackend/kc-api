﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/applicationConfiguration")]

    public class ApplicationConfigurationController : Controller
    {

        public readonly IApplicationConfigurationManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public ApplicationConfigurationController(IApplicationConfigurationManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {

            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;
        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<ApplicationConfiguration> GetAsync(int id)
        {
            var entityResult = await manger.GetAsync(id);
            return entityResult;
        }
        [Route("GetAll")]
        [HttpGet]
        public List<ApplicationConfiguration> GetAll()
        {
           var entityResult = manger.GetAllAsync().ToList();
            return entityResult;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ApplicationConfiguration model)
        {
            await fillEntityIdentity(model);
            var entityResult = await manger.AddAsync(model);
            return Ok(entityResult);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]ApplicationConfiguration model)
        {
            await fillEntityIdentity(model);
           var  result = await manger.UpdateAsync(model);
            return Ok(result);

        }
        #endregion
        #region DeleteApi

        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {

            ApplicationConfiguration model = new ApplicationConfiguration();
            model.Id = id;
            await fillEntityIdentity(model);
            var result = await manger.DeleteById(model.Id);
            return Ok(result);

        }
        #endregion
        #endregion
        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Identity;
using DispatchingSystem.BLL;
using DispatchingSystem.BLL.IManagers;
using Microsoft.AspNetCore.Http.Extensions;
using KCCVehicleAPI.BL.Interface;
using KCCVehicleAPI.BL.Implementations;
using KCCVehicleAPI.Models;
using KCCVehicleAPI.Models.Outputs;
using KCCVehicleAPI.Models.Inputs;
using DispatchingSystem.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("KCCVehicle")]
    public class KCCVehicleController : Controller
    {
        IVT vt = new VT();
        // private readonly DataContext ctx;
        private IConfigurationRoot _configurationRoot;
        private IVehicleManager _vehicleManager;
        public KCCVehicleController(IConfigurationRoot configurationRoot, IVehicleManager vehicleManager)
        {
            //ctx = _ctx;
            _configurationRoot = configurationRoot;
            _vehicleManager = vehicleManager;
        }

        [Route("GetVehicles")]
        [HttpPost]
        public async Task<DTO<GetVehiclesDTO>> GetVehiclesAsync([FromBody] Input<GetVehicles> obj)
        {
            var optsBuilder = new DbContextOptionsBuilder<DataContext>();
            string connectionString = _configurationRoot.GetConnectionString("DefaultConnection");
            string migrationAssembly = _configurationRoot["MigrationsAssembly"];
            optsBuilder.UseSqlServer(connectionString,
                        sqlOptions => sqlOptions.MigrationsAssembly(migrationAssembly));
            var result = new DTO<GetVehiclesDTO>();
            result.objname = "Before get vehicles";
            DataContext dataContext = new DataContext(optsBuilder.Options);
            result = vt.GetVehicles(obj, dataContext);
            result.objname = "After get vehicles";
            try
            {
                await SendNotificationAsync(result, obj, dataContext);
                result.objname = "After send method";
                return result;
            }
            catch (Exception ex)
            {
                result.objname = ex.StackTrace;
                return result;
            }
        }

        public async Task SendNotificationAsync(DTO<GetVehiclesDTO> vehicles, Input<GetVehicles> obj, DataContext dataContext)
        {
            if (vehicles.status.statusdescription.Equals("SUCCESS"))
            {
                string methodName = _configurationRoot["Notifications:LiveTrackingClientMethodName"];
                double washingLeavingTimeInMins = Convert.ToDouble(_configurationRoot["Notifications:WashingLeavingTimeInMins"]);

                foreach (var item in obj.input.Vehicles)
                {
                   await  NotificationsManager.SendVehicleLocationAsync
                        (Convert.ToDouble(item.Latitude), Convert.ToDouble(item.Longtitude),
                        item.Panicbutton, item.Reg, methodName, dataContext, _vehicleManager,
                        washingLeavingTimeInMins);
                }
            }
        }

        //[Route("ShowVehicles")]
        //[HttpPost]
        //public DTO<ShowVehiclesDTO> ShowVehicles(Input<ShowVehicles> obj)
        //{
        //    return vt.ShowVehicles(obj, ctx);
        //}

        //[Route("ShowVehiclesReg")]
        //[HttpPost]
        //public DTO<ShowVehiclesRegDTO> ShowVehiclesReg(Input<ShowVehiclesReg> obj)
        //{
        //    return vt.ShowVehiclesReg(obj, ctx);
        //}

        //[Route("GetVehiclesLocation")]
        //[HttpPost]
        //public DTO<GetVehiclesLocationDTO> GetVehiclesLocation(Input<GetVehiclesLocation> obj)
        //{
        //    return vt.GetVehiclesLocation(obj, ctx);
        //}
    }
}
﻿using AutoMapper;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/OrderConfirmationHistory")]
    public class OrderConfirmationHistoryController : Controller
    {
        IOrderConfirmationHistoryManager _orderConfirmationHistoryManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWorkOrderManager _workOrderManager;
        private readonly IConfigurationRoot _configurationRoot;

        public OrderConfirmationHistoryController(IOrderConfirmationHistoryManager orderConfirmationHistoryManager,
            UserManager<ApplicationUser> userManager, IWorkOrderManager workOrderManager,
            IConfigurationRoot configurationRoot)
        {
            _configurationRoot = configurationRoot;
            _workOrderManager = workOrderManager;
            _userManager = userManager;
            _orderConfirmationHistoryManager = orderConfirmationHistoryManager;
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add([FromBody]OrderConfirmationHistoryViewModel model)
        {
            var entity = Mapper.Map<OrderConfirmationHistoryViewModel, OrderConfirmationHistory>(model);
            var restrictedHoursToConfirmOrder = Convert.ToInt32(_configurationRoot["Notifications:RestrictedHoursToConfirmOrder"]);

            await FillEntityIdentity(entity);
            string googleServiceUrl = _configurationRoot["GoogleMapsApi:DirectionsApiUrl"];
            string googleApiKey = _configurationRoot["GoogleMapsApi:Key"];
            entity = await _workOrderManager.AddOrderConfirmationHistory(entity, restrictedHoursToConfirmOrder, googleServiceUrl, googleApiKey); /*_orderConfirmationHistoryManager.Add(entity);*/
            model = Mapper.Map<OrderConfirmationHistory, OrderConfirmationHistoryViewModel>(entity);
            return Ok(model);
        }

        [HttpGet]
        [Route("GetByOrderId")]
        public async Task<IActionResult> GetByOrderIdAsync([FromQuery] int orderId)
        {
            var entities = await _orderConfirmationHistoryManager.GetByOrderId(orderId);
            var models = Mapper.Map<List<OrderConfirmationHistory>, List<OrderConfirmationHistoryViewModel>>(entities);
            return Ok(models);
        }



        private async Task<IBaseEntity> FillEntityIdentity(IBaseEntity baseEntity)
        {
            Utilites.Helper help = new Utilites.Helper();
            baseEntity = await help.FillEntityIdentity(_userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        private async Task<List<IBaseEntity>> FillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Utilites.Helper help = new Utilites.Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(_userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }

    }
}

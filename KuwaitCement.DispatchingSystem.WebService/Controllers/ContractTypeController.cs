﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using Microsoft.EntityFrameworkCore;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/ContractType")]
    public class ContractTypeController : Controller
    {
        public readonly IContractTypeManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public ContractTypeController(IContractTypeManager _manger, IMapper _mapper,UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;

        }


       
        public IActionResult Index()
        {
          
            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<ContractTypeViewModel> GetAsync(int id)
        {
            ContractTypeViewModel result = new ContractTypeViewModel();
            ContractType entityResult = new ContractType();
            entityResult = await manger.GetAsync(id);
            result = mapper.Map<ContractType, ContractTypeViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
      
          var  entityResult = await manger.GetAllAsync().ToListAsync();
            var result = mapper.Map<List<ContractType>, List<ContractTypeViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ContractTypeViewModel model)
        {
            ContractTypeViewModel result = new ContractTypeViewModel();
            ContractType entityResult = new ContractType();
            entityResult = mapper.Map<ContractTypeViewModel, ContractType>(model);
            await fillEntityIdentity(entityResult);
            entityResult = await manger.AddAsync(entityResult);
            result = mapper.Map<ContractType, ContractTypeViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]ContractTypeViewModel model)
        {
            bool result = false;
            ContractType entityResult = new ContractType();
            entityResult = mapper.Map<ContractTypeViewModel, ContractType>(model);
            await fillEntityIdentity(entityResult);
            result = await manger.UpdateAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
            bool result = false;
            //ContractType entityResult = new ContractType();
            //ContractTypeViewModel model = new ContractTypeViewModel();
            //model.Id = id;
            var entityResult =await  manger.GetAsync(id);
            await fillEntityIdentity(entityResult);
            result =await  manger.DeleteAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion
        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
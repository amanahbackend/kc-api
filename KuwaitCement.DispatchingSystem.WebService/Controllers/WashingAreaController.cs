﻿using AutoMapper;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/WashingArea")]
    public class WashingAreaController : Controller
    {
        private readonly IWashingAreaManager _washingAreaManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public WashingAreaController(IWashingAreaManager washingAreaManager,
            UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _washingAreaManager = washingAreaManager;
        }

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add([FromBody]WashingAreaViewModel model)
        {
            var entity = Mapper.Map<WashingAreaViewModel, WashingArea>(model);
            await FillEntityIdentity(entity);
            entity = await _washingAreaManager.AddAsync(entity);
            model = Mapper.Map<WashingArea, WashingAreaViewModel>(entity);
            return Ok(model);
        }

        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var entity =await  _washingAreaManager.GetAsync(id);
            var model = Mapper.Map<WashingArea, WashingAreaViewModel>(entity);
            return Ok(model);
        }

        [HttpGet]
        [Route("GetAll")]
        public IActionResult GetAll()
        {
            var entities = _washingAreaManager.GetAllAsync().ToList();
            var models = Mapper.Map<List<WashingArea>, List<WashingAreaViewModel>>(entities);
            return Ok(models);
        }

        private async Task<IBaseEntity> FillEntityIdentity(IBaseEntity baseEntity)
        {
            Utilites.Helper help = new Utilites.Helper();
            baseEntity = await help.FillEntityIdentity(_userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        private async Task<List<IBaseEntity>> FillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Utilites.Helper help = new Utilites.Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(_userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using AutoMapper;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using Microsoft.EntityFrameworkCore;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/Privilge")]
    public class PrivilgeController : Controller
    {
        public IPrivilgeManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public PrivilgeController(IPrivilgeManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.userManager = _userManager;
            this.mapper = _mapper;
        }


        [Route("GetAll")]
        public IActionResult Index()
        {
          
            return Ok();
        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<PrivilgeViewModel> GetAsync(int id)
        {
            PrivilgeViewModel result = new PrivilgeViewModel();
            Privilge entityResult = new Privilge();
            entityResult = await manger.GetAsync(id);
            result = mapper.Map<Privilge, PrivilgeViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<List<PrivilgeViewModel>> GetAsync()
        {
   
            var entityResult = await manger.GetAllAsync().ToListAsync();
            var result = mapper.Map<List<Privilge>, List<PrivilgeViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PrivilgeViewModel model)
        {
 
          var  entityResult = mapper.Map<PrivilgeViewModel, Privilge>(model);
            await fillEntityIdentity(entityResult);
            entityResult = await manger.AddAsync(entityResult);
           var  result = mapper.Map<Privilge, PrivilgeViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]PrivilgeViewModel model)
        {
           
           var entityResult = mapper.Map<PrivilgeViewModel, Privilge>(model);
            await fillEntityIdentity(entityResult);
          var   result = await manger.UpdateAsync(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
      
            PrivilgeViewModel model = new PrivilgeViewModel();
            model.Id = id;
           var entityResult = mapper.Map<PrivilgeViewModel, Privilge>(model);
            await fillEntityIdentity(entityResult);
           var result =await  manger.DeleteAsync(entityResult);
            return Ok(result);


        }
        #endregion
        #endregion
        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
﻿using AutoMapper;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/CustomerCategory")]
    public class CustomerCategoryController : Controller
    {
        ILKP_CustomerCategoryManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public CustomerCategoryController(ILKP_CustomerCategoryManager customerCategoryManager, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            manger = customerCategoryManager;
            mapper = _mapper;
            userManager = _userManager;

        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<CustomerCategoryViewModel> GetAsync(int id)
        {
            CustomerCategoryViewModel result = new CustomerCategoryViewModel();
            LKP_CustomerCategory entityResult = new LKP_CustomerCategory();
            entityResult = await manger.GetAsync(id);
            result = mapper.Map<LKP_CustomerCategory, CustomerCategoryViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {

            var entityResult =  await manger.GetAllAsync().ToListAsync();
            var result = mapper.Map<List<LKP_CustomerCategory>, List<CustomerCategoryViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CustomerCategoryViewModel model)
        {
            CustomerCategoryViewModel result = new CustomerCategoryViewModel();
            LKP_CustomerCategory entityResult = new LKP_CustomerCategory();
            entityResult = mapper.Map<CustomerCategoryViewModel, LKP_CustomerCategory>(model);
            await fillEntityIdentity(entityResult);
            entityResult = await manger.AddAsync(entityResult);
            result = mapper.Map<LKP_CustomerCategory, CustomerCategoryViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]CustomerCategoryViewModel model)
        {
            bool result = false;
            LKP_CustomerCategory entityResult = new LKP_CustomerCategory();
            entityResult = mapper.Map<CustomerCategoryViewModel, LKP_CustomerCategory>(model);
            await fillEntityIdentity(entityResult);
            result = await manger.UpdateAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
            bool result = false;
            var entityResult = await manger.GetAsync(id);
            await fillEntityIdentity(entityResult);
            result =await  manger.DeleteAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion

        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}

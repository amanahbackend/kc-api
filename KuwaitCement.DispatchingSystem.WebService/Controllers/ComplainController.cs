﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using Microsoft.AspNetCore.Authorization;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Authorize]
    [Route("api/Complain")]
    public class ComplainController : Controller
    {
        public IComplainManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;
        public ComplainController(IComplainManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;

        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> GetAsync(int id)
        {
            ComplainViewModel result = new ComplainViewModel();
            Complain entityResult = new Complain();
            entityResult =await  manger.GetAsync(id);
            result = mapper.Map<Complain, ComplainViewModel>(entityResult);
            return Ok(result);
        }

        [Route("getAllComplains")]
        [HttpGet]
        public async Task<IActionResult> GetAllComplainsAsync(int id)
        {
    
            var entityResult =await  manger.GetComplainsByCustomerIdAsync(id);
            var result = mapper.Map<List<Complain>, List<ComplainViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<ComplainViewModel> result = new List<ComplainViewModel>();
            List<Complain> entityResult = new List<Complain>();
            entityResult = manger.GetAllAsync().ToList();
            result = mapper.Map<List<Complain>, List<ComplainViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ComplainViewModel model)
        {
            ComplainViewModel result = new ComplainViewModel();
            Complain entityResult = new Complain();
            entityResult = mapper.Map<ComplainViewModel, Complain>(model);
            await fillEntityIdentity(entityResult);
            entityResult = await manger.AddAsync(entityResult);
            result = mapper.Map<Complain, ComplainViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]ComplainViewModel model)
        {
            bool result = false;
            Complain entityResult = new Complain();
            entityResult = mapper.Map<ComplainViewModel, Complain>(model);
            await fillEntityIdentity(entityResult);
            result = await manger.UpdateAsync(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery]int id)
        {
            bool result = false;
            Complain entity = await manger.GetAsync(id);
            await fillEntityIdentity(entity);
            result = await manger.DeleteAsync(entity);
            return Ok(result);
        }
        #endregion
        #endregion
        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
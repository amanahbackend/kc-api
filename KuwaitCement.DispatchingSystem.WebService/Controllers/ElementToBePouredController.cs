﻿using AutoMapper;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/ElementToBePoured")]
    public class ElementToBePouredController : Controller
    {
        public IElementToBePouredManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public ElementToBePouredController(IElementToBePouredManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;

        }


      
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<ElementToBePoured> GetAsync(int id)
        {
            ElementToBePoured entityResult = new ElementToBePoured();
            entityResult = await manger.GetAsync(id);

            return entityResult;
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
           var entityResult = await manger.GetAllAsync().OrderByDescending(x => x.Id).ToListAsync();
            return Ok(entityResult);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ElementToBePoured model)
        {
            await fillEntityIdentity(model);

           var entityResult = manger.AddAsync(model);
            return Ok(entityResult);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]ElementToBePoured model)
        {
            bool result = false;
            await fillEntityIdentity(model);
             result = await manger.UpdateAsync(model);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
            bool result = false;
            //Item entityResult = new Item();
            //ItemViewModel model = new ItemViewModel();
            //model.Id = id;
            var entityResult = await manger.GetAsync(id);
            await fillEntityIdentity(entityResult);
            result =await  manger.DeleteAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion

        //[HttpGet]
        //[Route("ItemCodeExists")]
        //public IActionResult ItemCodeExists([FromQuery]string code)
        //{
        //    var result = manger.ItemCodeExists(code);
        //    return Ok(result);
        //}


        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
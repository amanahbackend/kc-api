﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    //[Authorize]
    [Route("api/WorkOrderItems")]
    public class WorkOrderItemsController : Controller
    {
        public IWorkOrderItemsManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;
        public WorkOrderItemsController(IWorkOrderItemsManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;

        }



        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("api/WorkOrderItems/Get/{id}")]
        [HttpGet]
        public async Task<IActionResult> GetAsync(int id)
        {
           
            var entityResult = await manger.GetAsync(id);
           var  result = mapper.Map<WorkOrderItems, WorkOrderItemsViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<WorkOrderItemsViewModel> result = new List<WorkOrderItemsViewModel>();
            List<WorkOrderItems> entityResult = new List<WorkOrderItems>();
            entityResult = manger.GetAllAsync().ToList();
            result = mapper.Map<List<WorkOrderItems>, List<WorkOrderItemsViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]WorkOrderItemsViewModel model)
        {
          
           var  entityResult = mapper.Map<WorkOrderItemsViewModel, WorkOrderItems>(model);
            await fillEntityIdentity(entityResult);
            entityResult = await manger.AddAsync(entityResult);
           var  result = mapper.Map<WorkOrderItems, WorkOrderItemsViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]WorkOrderItemsViewModel model)
        {
            bool result = false;
            WorkOrderItems entityResult = new WorkOrderItems();
            entityResult = mapper.Map<WorkOrderItemsViewModel, WorkOrderItems>(model);
            await fillEntityIdentity(entityResult);
            result = await manger.UpdateAsync(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {

            WorkOrderItemsViewModel model = new WorkOrderItemsViewModel();
            model.Id = id;
          var   entityResult = mapper.Map<WorkOrderItemsViewModel, WorkOrderItems>(model);
            await fillEntityIdentity(entityResult);
          var  result = await manger.DeleteAsync(entityResult);
            return Ok(result);
        }
        #endregion
        #endregion
        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
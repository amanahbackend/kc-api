﻿using AutoMapper;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/Item")]
    public class ItemController : Controller
    {
        public IItemManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public ItemController(IItemManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;

        }


        [Route("GetAll")]
        public IActionResult Index()
        {

            return Ok();
        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<ItemViewModel> GetAsync(int id)
        {
          
          var  entityResult =await manger.GetAsync(id);

            var result = mapper.Map<Item, ItemViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<List<ItemViewModel>> GetAsync()
        {
       
            var entityResult =await manger.GetAllAsync().OrderByDescending(x => x.Id).ToListAsync();
          var  result = mapper.Map<List<Item>, List<ItemViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ItemViewModel model)
        {
       
           var entityResult = mapper.Map<ItemViewModel, Item>(model);
            await fillEntityIdentity(entityResult);

            entityResult = await manger.AddAsync(entityResult);
          var  result = mapper.Map<Item, ItemViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]ItemViewModel model)
        {
       
           var entityResult = mapper.Map<ItemViewModel, Item>(model);
            await fillEntityIdentity(entityResult);
            var result = await manger.UpdateAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
          
            var entityResult = await manger.GetAsync(id);
            await fillEntityIdentity(entityResult);
           var result =await manger.DeleteAsync (entityResult);
            return Ok(result);

        }
        #endregion
        #endregion

        [HttpGet]
        [Route("ItemCodeExists")]
        public async Task<IActionResult> ItemCodeExistsAsync([FromQuery]string code)
        {
            var result =await manger.ItemCodeExists(code);
            return Ok(result);
        }


        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
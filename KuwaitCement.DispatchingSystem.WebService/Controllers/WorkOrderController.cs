﻿using AutoMapper;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilites.Paging;
using Utilities.Paging;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/WorkOrder")]
    public class WorkOrderController : Controller
    {
        private readonly IFactoryManager factoryManager;
        private readonly IWorkOrderManager workorderManager;
        private readonly IConfigurationRoot configurationRoot;
        private readonly IMapper mapper;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IHostingEnvironment hostingEnv;
        private readonly IContractManager contractManager;
        private readonly IApplicationUserManager applicationUserManager;

        public WorkOrderController(IWorkOrderManager _workorderManager, IMapper _mapper, IApplicationUserManager _applicationUserManager,
            UserManager<ApplicationUser> _userManager, IFactoryManager _factoryManager,
            IConfigurationRoot _configurationRoot, IHostingEnvironment _hostingEnv, IContractManager _contractManager)
        {
            contractManager = _contractManager;
            configurationRoot = _configurationRoot;
            factoryManager = _factoryManager;
            workorderManager = _workorderManager;
            mapper = _mapper;
            applicationUserManager = _applicationUserManager;

            userManager = _userManager;
            hostingEnv = _hostingEnv;
        }


        [Route("GetAll")]
        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> GetAsync(int id)
        {
            var entityResult = await workorderManager.GetAsync(id);
            var result = mapper.Map<WorkOrder, WorkOrderViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {

            var entityResult = await workorderManager.GetAllAsync().ToListAsync();
            var result = mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetAllSuborders")]
        [HttpGet]
        public async Task<IActionResult> GetAllSuborders()
        {
            var user = await new Utilites.Helper().GetCurrentUser(userManager, HttpContext);
            string controllerId = user.Id;

            var entityResult = await workorderManager.GetAllSuborders(controllerId);
            var result = mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]WorkOrderViewModel model)
        {

            var entityResult = mapper.Map<WorkOrderViewModel, WorkOrder>(model);
            await FillEntityIdentity(entityResult);
            entityResult = await workorderManager.AddAsync(entityResult);
            var result = mapper.Map<WorkOrder, WorkOrderViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        [Route("UpdateSubOrder")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]WorkOrderViewModel model)
        {
            WorkOrder entityResult = mapper.Map<WorkOrderViewModel, WorkOrder>(model);
            await FillEntityIdentity(entityResult);
            //var result = workorderManager.UpdateSuborder(entityResult);
            var result = workorderManager.UpdateworkOrderAsync(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery]int id)
        {
            var entity = await workorderManager.GetAsync(id);
            await FillEntityIdentity(entity);
            bool result = await workorderManager.DeleteAsync(entity);
            return Ok(result);
        }
        #endregion
        #endregion
        #region CustomBuisness
        [Route("AddWorkOrder")]
        [HttpPost]
        public async Task<IActionResult> AddWorkOrder([FromBody]WorkOrderFactoriesViewModel model)
        {
            if (model.CreatingOrderFirstTime)
            {
                var result = false;
                var entityResult = mapper.Map<WorkOrderFactoriesViewModel, WorkOrderFactories>(model);
                await FillEntityIdentity(entityResult.WorkOrder);
                entityResult = await workorderManager.HandleWorkOrderAssigning(entityResult);
                if (entityResult.WorkOrder.Id > 0)
                {
                    result = true;
                }
                return Ok(result);

            }

            else
            {
                if (await IsOrderDividingValid(model))
                {
                    model = await ComputeAmountPercentage(model);
                    var entityResult = mapper.Map<WorkOrderFactoriesViewModel, WorkOrderFactories>(model);
                    await FillEntityIdentity(entityResult.WorkOrder);
                    //await FillEntitiesIdentites(entityResult.SubOrdersFactoryPercent.ToList().ConvertAll(x => (IBaseEntity)x));
                    entityResult = await workorderManager.HandleWorkOrderAssigning(entityResult);
                    model = mapper.Map<WorkOrderFactories, WorkOrderFactoriesViewModel>(entityResult);
                    return Ok(model);
                }
            }

            return Ok(null);
        }

        [Route("UpdateWorkOrder")]
        [HttpPost]
        public async Task<IActionResult> UpdateWorkOrder([FromBody]WorkOrderFactoriesViewModel model)
        {
            if (await IsOrderDividingValid(model))
            {
                model = await ComputeAmountPercentage(model);
                var entityResult = mapper.Map<WorkOrderFactoriesViewModel, WorkOrderFactories>(model);
                await FillEntityIdentity(entityResult.WorkOrder);
                //await FillEntitiesIdentites(entityResult.SubOrdersFactoryPercent.ToList().ConvertAll(x => (IBaseEntity)x));
                entityResult = await workorderManager.HandleWorkOrderAssigning(entityResult, true);
                model = mapper.Map<WorkOrderFactories, WorkOrderFactoriesViewModel>(entityResult);
                return Ok(model);
            }
            return Ok(null);
        }

        [HttpGet]
        [Route("DeleteSuborder")]
        public async Task<IActionResult> DeleteSuborderAsync(int parentWorkorderId, int factoryId)
        {
            bool result = await workorderManager.DeleteSuborder(parentWorkorderId, factoryId);
            return Ok(result);
        }


        [Route("GetDetailedWorkOrdersByCustomerIdAsync")]
        [HttpPost]
        public async Task<IActionResult> GetDetailedWorkOrdersByCustomerIdAsync([FromBody]PaginatedItems pagingParameterModel)
        {

            List<WorkOrderViewModel> result = new List<WorkOrderViewModel>();
            var entityResult = await workorderManager.GetDetailedWorkOrdersByCustomerIdAsync(pagingParameterModel);
            result = mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(entityResult.Result);
            return Ok(new { result, entityResult.TotalCount });

        }
        [HttpPost]
        [Route("getPendingApprovedOrdersAsync")]
        public async Task<IActionResult> GetPendingApprovedOrdersAsync([FromBody]PaginatedItems pagingParameterModel)
        {
            try
            {
                var entities = await workorderManager.GetAllPendingAndApprovedOrdersAsync(pagingParameterModel);
                var model = Mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(entities.Result);
                return Ok(new { model, entities.TotalCount });

            }
            catch (Exception e)
            {
                var message = e.Message;
                throw;
            }
        }






        [HttpGet]
        [Route("GetPendingApprovedOrders")]
        public async Task<IActionResult> GetPendingApprovedOrdersAsync()
        {
            try
            {
                var entities = await workorderManager.GetAllPendingAndApprovedOrders();
                var model = Mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(entities);
                return Ok(model);

            }
            catch (Exception e)
            {
                var message = e.Message;
                throw;
            }
        }





        [HttpGet]
        [Route("GetAllApprovedOrders")]
        public async Task<IActionResult> GetAllApprovedOrders()
        {
            var entities = await workorderManager.GetAllApprovedOrders();
            return Ok(entities);

        }




        [HttpGet]
        [Route("CanAssignToFactory")]
        public async Task<IActionResult> CanAssignToFactory(int workorderId, int factoryId)
        {
            string googleServiceUrl = configurationRoot["GoogleMapsApi:DirectionsApiUrl"];
            string googleApiKey = configurationRoot["GoogleMapsApi:Key"];
            bool result = await workorderManager.CanAssign(workorderId, factoryId, googleServiceUrl, googleApiKey);
            return Ok(result);
        }

        [HttpGet]
        [Route("UpdateWorkOrderStatus")]
        public async Task<IActionResult> UpdateWorkOrderStatus(int workOrderId, int statusId)
        {
            WorkOrder workOrder = await workorderManager.GetAsync(workOrderId);
            await FillEntityIdentity(workOrder);

            var result = await workorderManager.ChangeWorkOrderStatus(workOrder, statusId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetByCode")]
        public async Task<IActionResult> GetByCodeAsync([FromQuery] string code)
        {

            var entityResult = await workorderManager.GetByCode(code);
            var result = mapper.Map<WorkOrder, WorkOrderViewModel>(entityResult);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetByFactory")]
        public async Task<IActionResult> GetByFactoryAsync([FromQuery] int factoryId)
        {

            var entityResult = await workorderManager.GetByFactoryId(factoryId);
            var result = mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(entityResult);
            return Ok(result);
        }


        [HttpGet]
        [Route("GetCalenderSuborders")]
        public async Task<IActionResult> GetCalenderSuborders()
        {
            var user = await new Utilites.Helper().GetCurrentUser(userManager, HttpContext);
            string controllerId = user.Id;
            List<WorkOrder> entityResult = await workorderManager.GetCalendarSuborders(controllerId);
            List<WorkOrderViewModel> result = mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(entityResult);
            return Ok(result);
        }

        [HttpGet]
        [Route("UpdateOrderAssignmentToFactory")]
        public async Task<IActionResult> UpdateOrderAssignmentToFactory(int orderId, int factoryId)
        {
            var order = await workorderManager.GetAsync(orderId);
            await FillEntityIdentity(order);
            var result = await workorderManager.AssignToFactory(order, factoryId);
            return Ok(result);
        }

        [HttpPost]
        [Route("GetTodaySuborders")]
        public async Task<IActionResult> GetTodaySuborders([FromBody]PaginatedItems pagingParameterModel)
        {
            var user = await new Utilites.Helper().GetCurrentUser(userManager, HttpContext);
            string controllerId = user.Id;
            bool isMvmtCtrlUser = await applicationUserManager.IsUserInRole(controllerId, "MovementController");

            var entityResult = await workorderManager.GetTodaySubordersAsync(isMvmtCtrlUser, pagingParameterModel);
            var result = mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(entityResult.Result);
            return Ok((result, entityResult.TotalCount));
        }


        [HttpGet]
        [Route("GetTodaySubordersForDispatcher")]
        public async Task<IActionResult> GetTodaySubordersForDispatcher()
        {
            var user = await new Utilites.Helper().GetCurrentUser(userManager, HttpContext);
            string controllerId = user.Id;

            var entityResult = await workorderManager.GetTodaySubordersForDispatcher(controllerId);
            var result = mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(entityResult);
            return Ok(result);
        }



        [HttpGet]
        [Route("GetTodaySuborders")]
        public async Task<IActionResult> GetTodaySubordersForTracking()
        {
            var user = await new Utilites.Helper().GetCurrentUser(userManager, HttpContext);
            string controllerId = user.Id;
            bool isMvmtCtrlUser = await applicationUserManager.IsUserInRole(controllerId, "MovementController");

            var entityResult = await workorderManager.GetTodaySuborders(isMvmtCtrlUser);
            var result = mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(entityResult);
            return Ok(result);
        }
        [HttpPost]
        [Route("Filter")]
        public async Task<IActionResult> FilterAsync([FromBody] PaginatedItemsViewModel<ReportFilterViewModel> model)
        {


            List<ReportFilter> filters = mapper.Map<List<ReportFilterViewModel>, List<ReportFilter>>(model.Data.ToList());
            foreach (var item in filters)
            {
                item.Type = item.Type.ToPascalCase();
            }

            var result = await workorderManager.FilterWithPaging(model.PageIndex, model.PageSize, filters);
            return Ok(result);
        }

        [HttpPost]
        [Route("FilterSuborders")]
        public async Task<IActionResult> FilterSubordersAsync([FromBody]List<ReportFilterViewModel> model)
        {
            List<ReportFilter> filters = mapper.Map<List<ReportFilterViewModel>, List<ReportFilter>>(model);
            foreach (var item in filters)
            {
                item.Type = item.Type;
            }
            var result = await workorderManager.FilterSuborders(filters);

            return Ok(new { result, result.TotalCount });
        }
        [HttpPost]
        [Route("FilterSubordersForDispatcher")]
        public async Task<IActionResult> FilterSubordersForDispatcher([FromBody]List<ReportFilterViewModel> model)
        {
            List<ReportFilter> filters = mapper.Map<List<ReportFilterViewModel>, List<ReportFilter>>(model);
            foreach (var item in filters)
            {
                item.Type = item.Type;
            }
            var result = await workorderManager.FilterSubordersForDispatcher(filters);

            return Ok(result);
        }



        [HttpPost]
        [Route("FilterToExcel")]
        public async Task<IActionResult> FilterToExcel([FromBody] PaginatedItemsViewModel<ReportFilterViewModel> model)
        {
            List<ReportFilter> filters = mapper.Map<List<ReportFilterViewModel>, List<ReportFilter>>(model.Data.ToList());
            foreach (var item in filters)
            {
                item.Type = item.Type.ToPascalCase();
            }
            var user = await new Utilites.Helper().GetCurrentUser(userManager, HttpContext);
            var objects = await workorderManager.Filter(filters);
            var result = await WriteWorkOrdersToExcelAsync(objects, user.UserName);
            return Ok(new { fileName = result });
        }

        [HttpGet]
        [Route("DownloadReportFile")]
        public async Task<IActionResult> DownloadReportFile([FromQuery] string fileName)
        {
            var webRootInfo = hostingEnv.WebRootPath;
            var dirPath = webRootInfo + @"\Attachments\Reports";
            string filePath = dirPath + @"\" + fileName;
            var memoryStream = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memoryStream);
            }
            memoryStream.Position = 0;
            return File(memoryStream, FileUtilities.GetContentType(filePath), Path.GetFileName(filePath));
        }

        [HttpGet]
        [Route("GetByContract")]
        public async Task<IActionResult> GetByContractAsync([FromQuery] int contractId)
        {
            var result = await workorderManager.GetByContractId(contractId);
            var model = mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(result);
            return Ok(model);
        }

        [HttpGet]
        [Route("GetPendingWorOrders")]
        public async Task<IActionResult> GetPendingWorOrdersAsync()
        {
            var entities = await workorderManager.GetPendingUnassignedOrders();
            var model = Mapper.Map<List<WorkOrder>, List<WorkOrderViewModel>>(entities);
            return Ok(model);
        }



        [HttpGet]
        [Route("GetCountForDate")]
        public async Task<IActionResult> GetOrdersCountForDateAsync([FromQuery]DateTime startDate)
        {
            var count = await workorderManager.GetOrdersCount(startDate);
            return Ok(count);
        }

        [HttpGet, Route("IsOrderHaveContractItem")]
        public async Task<IActionResult> IsOrderHaveContractItemAsync([FromQuery]int contractId, [FromQuery]int itemId)
        {
            var result = await workorderManager.IsOrderHaveContractItem(contractId, itemId);
            return Ok(result);
        }
        #endregion
        private async Task<IBaseEntity> FillEntityIdentity(IBaseEntity baseEntity)
        {
            Utilites.Helper help = new Utilites.Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        private async Task<List<IBaseEntity>> FillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Utilites.Helper help = new Utilites.Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }

        private async Task<string> WriteWorkOrdersToExcelAsync(List<WorkOrderReportResultToFile> orders, string username)
        {
            var properties = typeof(WorkOrderReportResultToFile).GetProperties();
            WorkOrderReportResultToFile order = new WorkOrderReportResultToFile();

            var webRootInfo = hostingEnv.WebRootPath;
            var dirPath = webRootInfo + @"\Attachments\Reports";
            await FileUtilities.CreateIfMissingAsync(dirPath);
            string fileName = String.Format("Report_{0}.xls", username);
            string filePath = dirPath + @"\" + fileName;
            FileUtilities.DeleteFileIfExist(filePath);
            FileStream stream = new FileStream(filePath, FileMode.OpenOrCreate);
            ExcelWriter writer = new ExcelWriter(stream);
            writer.BeginWrite();
            for (int row = 0; row < orders.Count + 1; row++)
            {
                if (row != 0)
                {
                    order = orders.Skip(row - 1).Take(1).FirstOrDefault();
                }
                for (int col = 0; col < properties.Length; col++)
                {
                    if (row == 0)
                    {
                        writer.WriteCell(row, col, properties[col].Name);
                    }
                    else
                    {
                        object value = order.GetType().GetProperty(properties[col].Name).GetValue(order);
                        if (value != null)
                        {
                            writer.WriteCell(row, col, value.ToString());
                        }
                        else
                        {
                            writer.WriteCell(row, col, string.Empty);
                        }
                    }
                }
            }
            writer.EndWrite();
            stream.Close();
            return fileName;
        }

        private Task<bool> IsOrderDividingValid(WorkOrderFactoriesViewModel model)
        {
            return Task.Run(() =>
            {
                bool isValid = false;
                if (model.SubOrdersFactoryPercent == null)
                {
                    isValid = true;
                }
                else
                {
                    if (model.SubOrdersFactoryPercent.Count() == 1)
                    {
                        if (model.SubOrdersFactoryPercent.First().Amount == model.WorkOrder.Amount)
                        {
                            isValid = true;
                        }
                    }
                    else if (model.SubOrdersFactoryPercent.Count() > 1)
                    {
                        double totalAmount = 0;
                        foreach (var item in model.SubOrdersFactoryPercent)
                        {
                            totalAmount += item.Amount;
                        }
                        if (totalAmount == model.WorkOrder.Amount)
                        {
                            return true;
                        }
                    }
                    else if (model.SubOrdersFactoryPercent.Count() == 0)
                    {
                        isValid = true;
                    }
                }
                return isValid;
            });
        }

        private Task<WorkOrderFactoriesViewModel> ComputeAmountPercentage(WorkOrderFactoriesViewModel model)
        {
            return Task.Run(() =>
             {

                 if (model.SubOrdersFactoryPercent != null)
                 {
                     if (model.SubOrdersFactoryPercent.Count() == 1)
                     {
                         model.SubOrdersFactoryPercent.First().Percentage = 100;
                     }
                     else if (model.SubOrdersFactoryPercent.Count() > 1)
                     {
                         double totalAmount = model.WorkOrder.Amount;
                         foreach (var item in model.SubOrdersFactoryPercent)
                         {
                             item.Percentage = (item.Amount / totalAmount) * 100;
                         }
                     }
                 }
                 return model;

             });
        }



    }
}
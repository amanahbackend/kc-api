﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.BLL.IManagers;
using Microsoft.AspNetCore.Identity;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using Models = DispatchingSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/User")]
    public class UserController : Controller
    {
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;
        public readonly IFactoryControllerManager controllerFactoryManager;

        public UserController(IMapper _mapper, UserManager<ApplicationUser> _userManager,
            IFactoryControllerManager _controllerFactoryManager)
        {
            this.mapper = _mapper;
            this.userManager = _userManager;
            controllerFactoryManager = _controllerFactoryManager;
        }
        public IActionResult Index()
        {
            return View();
        }

        #region DefaultCrudOperation
        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get(ApplicationUserViewModel model)
        {
            ApplicationUserViewModel result = new ApplicationUserViewModel();
            ApplicationUser entityResult = new ApplicationUser();
            entityResult = mapper.Map<ApplicationUserViewModel, ApplicationUser>(result);
            ApplicationUserManager appUserManager = new ApplicationUserManager(userManager);
            entityResult = await appUserManager.GetAsync(entityResult);
            result = mapper.Map<ApplicationUser, ApplicationUserViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            ApplicationUserManager appUserManager = new ApplicationUserManager(userManager);
            entityResult = await appUserManager.GetAll();
            result = mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion



        #region PostApi

        [HttpPost]
        [Route("Add")]
        public async Task<IActionResult> Add([FromBody] ApplicationUserViewModel model)
        {
            IActionResult result = BadRequest();
            if (ModelState.IsValid)
            {
                var entityResult = new ApplicationUser();
                ApplicationUserManager appUserManager = new ApplicationUserManager(userManager);
                entityResult = mapper.Map<ApplicationUserViewModel, ApplicationUser>(model);
                await FillEntityIdentity(userManager, HttpContext, entityResult);
                entityResult = await appUserManager.AddUserAsync(entityResult, model.Password, model.RoleNames);
                model = mapper.Map<ApplicationUser, ApplicationUserViewModel>(entityResult);
                if (model != null)
                {
                    result = Ok(model);
                }
            }
            return result;
        }


        #endregion


        [AllowAnonymous]
        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] ApplicationUserViewModel model)
        {
            ApplicationUser result = new ApplicationUser();
            var entityResult = new ApplicationUser();
            ApplicationUserManager appUserManager = new ApplicationUserManager(userManager);
            entityResult = mapper.Map<ApplicationUserViewModel, ApplicationUser>(model);
            await FillEntityIdentity(userManager, HttpContext, entityResult);
            result = await appUserManager.UpdateUserAsync(entityResult, model.Password);
            return Ok(result);
        }
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]string username)
        {
            bool result = false;
            ApplicationUserManager appUserManager = new ApplicationUserManager(userManager);
            ApplicationUser entity = await appUserManager.GetByUserNameAsync(username);
            await FillEntityIdentity(userManager, HttpContext, entity);
            result = await appUserManager.DeleteAsync(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        [HttpGet]
        [Route("UsernameExists")]
        public async Task<IActionResult> UsernameExists(string username)
        {
            ApplicationUserManager appUserManager = new ApplicationUserManager(userManager);
            bool result = await appUserManager.IsUserNameExistAsync(username);
            return Ok(result);
        }

        [HttpGet]
        [Route("EmailExists")]
        public async Task<IActionResult> EmailExists(string email)
        {
            ApplicationUserManager appUserManager = new ApplicationUserManager(userManager);
            bool result = await appUserManager.IsEmailExistAsync(email);
            return Ok(result);
        }

        [HttpGet]
        [Route("AssignUserToRole")]
        public async Task<IActionResult> AssignUserToRole(string username, string roleName)
        {
            ApplicationUserManager appUserManager = new ApplicationUserManager(userManager);
            bool result = await appUserManager.AddUserToRoleAsync(username, roleName);
            return Ok(result);
        }

        [HttpGet]
        [Route("AssignControllerToFactory")]
        public async Task<IActionResult> AssignControllerToFactory(string userId, int factoryId)
        {
            FactoryControllerViewModel result = new FactoryControllerViewModel()
            {
                Fk_Controller_Id = userId,
                Fk_Factory_Id = factoryId
            };

            Models.Entities.FactoryController entityResult = new Models.Entities.FactoryController();
            entityResult = mapper.Map<FactoryControllerViewModel, Models.Entities.FactoryController>(result);
            await FillEntityIdentity(entityResult);
            entityResult = await controllerFactoryManager.AddAsync(entityResult);
            result = mapper.Map<Models.Entities.FactoryController, FactoryControllerViewModel>(entityResult);
            return Ok(result);
        }

        [HttpGet]
        [Route("UpdateControllerFactoryAssignment")]
        public async Task<IActionResult> UpdateControllerFactoryAssignment(int id, string userId, int factoryId)
        {
            var ctrlFac = controllerFactoryManager.GetAllAsync().FirstOrDefault(x => x.Fk_Controller_Id.Equals(userId) && x.Id == id);
            if (ctrlFac != null)
            {
                await FillEntityIdentity(ctrlFac);
                ctrlFac.Fk_Factory_Id = factoryId;
                bool isUpdated = await controllerFactoryManager.UpdateAsync(ctrlFac);
                if (isUpdated)
                {
                    var ctrlFacModel = mapper.Map<Models.Entities.FactoryController, FactoryControllerViewModel>(ctrlFac);
                    return Ok(ctrlFacModel);
                }
                else
                {
                    return Ok("Failed to update");
                }
            }
            else
            {
                return Ok("This user is not assigned to that factory");
            }
        }

        [HttpDelete]
        [Route("DeleteControllerFactoryAssignment/{id}")]
        public async Task<IActionResult> DeleteControllerFactoryAssignmentAsync([FromRoute]int id)
        {
            bool result = await  controllerFactoryManager.DeleteById(id);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllFactoryControllers")]
        public IActionResult GetAllFactoryControllers()
        {
            var entities = controllerFactoryManager.GetAllAsync().Include(x => x.Controller).Include(x => x.Factory).ToList();
            var models = mapper.Map<List<Models.Entities.FactoryController>, List<FactoryControllerViewModel>>(entities);
            return Ok(models);
        }

        [HttpGet]
        [Route("GetByRole")]
        public async Task<IActionResult> GetByRole([FromQuery] string role)
        {
            var users = (List<ApplicationUser>)await userManager.GetUsersInRoleAsync(role);
            var userModels = Mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(users);
            return Ok(userModels);
        }

        [HttpPost, Route("ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody]ApplicationUserViewModel model)
        {
            ApplicationUserManager manager = new ApplicationUserManager(userManager);
            var isReset = await manager.ResetPassword(model.UserName, model.Password);
            return Ok(isReset);
        }



        private async Task<ApplicationUser> FillEntityIdentity(UserManager<ApplicationUser> userManager, HttpContext httpContext, ApplicationUser entity)
        {
            if (entity != null)
            {
                ApplicationUser user = await new Helper().GetCurrentUser(userManager, httpContext);
                entity.CurrentUser = user;
            }
            return entity;
        }

        public async Task<Models.IBaseEntity> FillEntityIdentity(Models.IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
    }
}
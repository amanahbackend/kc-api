﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/driver")]
    public class DriverController : Controller
    {
        public readonly IDriverManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public DriverController(IDriverManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;

        }


        
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<DriverViewModel> GetAsync(int id)
        {
            DriverViewModel result = new DriverViewModel();
            Driver entityResult = new Driver();
            entityResult = await manger.GetAsync(id);
            result = mapper.Map<Driver, DriverViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public List<DriverViewModel> GetAll()
        {
            List<DriverViewModel> result = new List<DriverViewModel>();
            List<Driver> entityResult = new List<Driver>();
            entityResult = manger.GetAllAsync().ToList();
            result = mapper.Map<List<Driver>, List<DriverViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]DriverViewModel model)
        {
            DriverViewModel result = new DriverViewModel();
            Driver entityResult = new Driver();
            entityResult = mapper.Map<DriverViewModel, Driver>(model);
            await fillEntityIdentity(entityResult);
            entityResult = await manger.AddAsync(entityResult);
            result = mapper.Map<Driver, DriverViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]DriverViewModel model)
        {
            bool result = false;
            Driver entityResult = new Driver();
            entityResult = mapper.Map<DriverViewModel, Driver>(model);
            await fillEntityIdentity(entityResult);
            result = await manger.UpdateAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi

        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
            bool result = false;

            Driver entityResult = new Driver();
            DriverViewModel model = new DriverViewModel();
            model.Id = id;
            entityResult = mapper.Map<DriverViewModel, Driver>(model);
            await fillEntityIdentity(entityResult);
            result = await manger.DeleteAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion
        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
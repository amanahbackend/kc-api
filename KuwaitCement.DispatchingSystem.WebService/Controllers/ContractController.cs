﻿using AutoMapper;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilities.ExcelToList;
using Utilities.Paging;
using Helper = KuwaitCement.DispatchingSystem.WebService.Utilites.Helper;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/Contract")]
    public class ContractController : Controller
    {
        public IContractManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IConfigurationRoot _configurationRoot;
        private readonly ICustomerManager _customerManager;
        private readonly IContractTypeManager _contractTypeManager;

        public ContractController(IContractManager _manger, IMapper _mapper,
            UserManager<ApplicationUser> _userManager, IConfigurationRoot configurationRoot,
            ICustomerManager customerManager, IContractTypeManager contractTypeManager,
            IHostingEnvironment hostingEnv)
        {
            _hostingEnv = hostingEnv;
            _contractTypeManager = contractTypeManager;
            _configurationRoot = configurationRoot;
            manger = _manger;
            mapper = _mapper;
            userManager = _userManager;
            _customerManager = customerManager;
        }


        [Route("GetAll")]
        public IActionResult Index()
        {
            return Ok();
        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> GetAsync(int id)
        {
            ContractViewModel result = new ContractViewModel();
            Contract entityResult = new Contract();
            entityResult = await manger.GetAsync(id);
            result = mapper.Map<Contract, ContractViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetByCustomerId")]
        [HttpGet]
        public async Task<IActionResult> GetByCustomerIdAsync(int customerId)
        {
            List<ContractViewModel> result = new List<ContractViewModel>();
            List<Contract> entityResult = new List<Contract>();
            entityResult =await manger.GetContractsByCustomerIdAsync(customerId);
            result = mapper.Map<List<Contract>, List<ContractViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetAllContractsByCustomerIdAsync")]
        [HttpPost]
        public async Task<IActionResult> GetAllContractsByCustomerIdAsync([FromBody]PaginatedItems pagingParameterModel)
        {
            List<ContractViewModel> result = new List<ContractViewModel>();
            var entityResult =await manger.GetAllContractsByCustomerIdAsync(pagingParameterModel);
            result = mapper.Map<List<Contract>, List<ContractViewModel>>(entityResult.Result);
            return Ok(new { result, entityResult.TotalCount });
        }





        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            List<ContractViewModel> result = new List<ContractViewModel>();
            List<Contract> entityResult = new List<Contract>();
            entityResult = manger.GetAllAsync().ToList();
            result = mapper.Map<List<Contract>, List<ContractViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetContractFile")]
        [HttpGet]
        public async Task<IActionResult> GetContractFileAsync(int contractId, int customerId)
        {
            string result = await manger.GetContractFileAsync(contractId, customerId);
            return Ok(result);
        }

        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ContractViewModel model)
        {
            ContractViewModel result = new ContractViewModel();
            Contract entityResult = new Contract();
            entityResult = mapper.Map<ContractViewModel, Contract>(model);
            await FillEntityIdentity(entityResult);
            entityResult = await manger.AddAsync(entityResult);
            result = mapper.Map<Contract, ContractViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]ContractViewModel model)
        {
            bool result = false;
            Contract entityResult = new Contract();
            entityResult = mapper.Map<ContractViewModel, Contract>(model);
            await FillEntityIdentity(entityResult);
            result = await manger.UpdateAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery]int id)
        {
            bool result = false;
            Contract entity = await manger.GetAsync(id);
            await FillEntityIdentity(entity);
            result = await manger.DeleteAsync(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        [Route("GetNonExpiredContracts")]
        [HttpGet]
        public async Task<IActionResult> GetNonExpiredContractsAsync([FromQuery]int customerId)
        {
            try
            {
                var entities = await manger.GetNonexpiredContractByCustomerIdAsync(customerId);
                var models = mapper.Map<List<Contract>, List<ContractViewModel>>(entities);
                return Ok(models);
            }
            catch (Exception e)
            {


                throw;
            }
        }

        [Route("CreateCustomContract")]
        [HttpPost]
        public async Task<IActionResult> CreateCustomContract([FromBody]CustomContractViewModel model)
        {
            CustomContract entityResult = new CustomContract();
            entityResult = mapper.Map<CustomContractViewModel, CustomContract>(model);
            await FillEntityIdentity(entityResult);

            //string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            //string paciServiceUrl = _configurationRoot["Location:PACIService:ServiceUrl"];
            //string paciFieldName = _configurationRoot["Location:PACIService:PACIFieldName"];
            //string streetServiceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
            //string streetFieldName = _configurationRoot["Location:StreetService:StreetFieldName"];
            //string blockNameFieldNameStreetService = _configurationRoot["Location:StreetService:BlockNameFieldName"];

            entityResult =await  manger.CreateCustomContractAsync(entityResult);
            model = mapper.Map<CustomContract, CustomContractViewModel>(entityResult);
            return Ok(model);
        }

        [HttpGet]
        [Route("ContractNumberExists")]
        public async Task<IActionResult> ContractNumberExistsAsync([FromQuery] string number)
        {
            var result = await manger.ContractNumberExists(number);
            return Ok(result);
        }

        [HttpPost, Route("AddFromFile")]
        public IActionResult AddFromFile([FromBody]UploadFile file)
        {
            UploadExcelFileManager uploadExcelFileManager = new UploadExcelFileManager();
            var webRootInfo = _hostingEnv.WebRootPath;
            string path = webRootInfo + $@"\Attachments\Upload\contracts_{new Random().Next()}.{StringUtilities.GetFileExtension(file.FileContent)}";
            path = uploadExcelFileManager.AddFile(file, path);
            IList<ContractFromExcelViewModel> dataList = ExcelReader.GetDataToList(path, GetItems);
            var result = SaveContractsAsync(dataList);
            return Ok(result);
        }

        private async Task<List<Contract>> SaveContractsAsync(IList<ContractFromExcelViewModel> dataList)
        {
            var result = new List<Contract>();
            foreach (var item in dataList)
            {
                var customer = _customerManager.GetAllAsync().FirstOrDefault(x => x.KCRM_Customer_Id.Equals(item.CustomerNumber));
                if (customer != null)
                {
                    var type = _contractTypeManager.GetAllAsync().FirstOrDefault(x => x.Name.Equals(item.ContractType, StringComparison.CurrentCultureIgnoreCase));
                    var contract = new Contract
                    {
                        //Amount = item.Amount,
                        ContractNumber = item.ContractNumber,
                        FK_ContractType_Id = type != null ? type.Id : _contractTypeManager.GetAllAsync().FirstOrDefault().Id,
                        FK_Customer_Id = customer.Id,
                        EndDate = item.EndDate,
                        //PendingAmount = item.PendingAmount,
                        //Price = item.Price,
                        Remarks = item.Remarks,
                        StartDate = item.StartDate                       
                    };
                    contract = await manger.AddAsync(contract);
                    result.Add(contract);
                }
            }
            return result;
        }

        public ContractFromExcelViewModel GetItems(IList<string> rowData, IList<string> columnNames)
        {
            var contract = new ContractFromExcelViewModel()
            {
                Amount = rowData[3].ToDouble(),
                ContractNumber = rowData[0].ToString(),
                ContractType = rowData[8].ToString(),
                CustomerNumber = rowData[7].ToString(),
                EndDate = rowData[2].ToDateTime(),
                StartDate = rowData[1].ToDateTime(),
                PendingAmount = rowData[4].ToDouble(),
                Price = rowData[5].ToDouble(),
                Remarks = rowData[6].ToString()
            };
            return contract;
        }

        public async Task<IBaseEntity> FillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> FillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
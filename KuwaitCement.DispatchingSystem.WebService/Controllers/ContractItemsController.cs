﻿using AutoMapper;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilities.ExcelToList;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/ContractItems")]
    public class ContractItemsController : Controller
    {
        private readonly IContractItemsManager _contractItemsManager;
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IContractManager _contractManager;
        private readonly IItemManager _itemManager;
        public ContractItemsController(IContractItemsManager contractItemsManager, IHostingEnvironment hostingEnv,
            IContractManager contractManager, IItemManager itemManager)
        {
            _itemManager = itemManager;
            _contractManager = contractManager;
            _hostingEnv = hostingEnv;
            _contractItemsManager = contractItemsManager;
        }

        [HttpGet]
        [Route("GetByContract")]
        public async Task<IActionResult> GetByContractAsync(int contractId)
        {
            var entity = await _contractItemsManager.GetByContractIdAsync(contractId);
            var result = Mapper.Map<List<ContractItems>, List<ContractItemsViewModel>>(entity);
            return Ok(result);
        }

        [HttpPost, Route("AddFromFile")]
        public IActionResult AddFromFile([FromBody]UploadFile file)
        {
            UploadExcelFileManager uploadExcelFileManager = new UploadExcelFileManager();
            var webRootInfo = _hostingEnv.WebRootPath;
            string path = webRootInfo + $@"\Attachments\Upload\contracts_{new Random().Next()}.{StringUtilities.GetFileExtension(file.FileContent)}";
            path = uploadExcelFileManager.AddFile(file, path);
            IList<ContractItemFromExcelViewModel> dataList = ExcelReader.GetDataToList(path, GetItems);
            var result = SaveContractItemsAsync(dataList);
            return Ok(result);
        }

        private async Task<List<ContractItems>> SaveContractItemsAsync(IList<ContractItemFromExcelViewModel> dataList)
        {
            var result = new List<ContractItems>();
            foreach (var item in dataList)
            {
                var contract = await _contractManager.GetAllAsync().FirstOrDefaultAsync(x => x.ContractNumber.Equals(item.ContractNumber));
                var itemObj =await  _itemManager.GetAllAsync().FirstOrDefaultAsync(x => x.Code.Equals(item.ItemCode));
                if (itemObj == null)
                {
                    itemObj = await _itemManager.AddAsync(new Item
                    {
                        AvailableAmount = Convert.ToInt32(item.AvailableAmount),
                        Code = item.ItemCode,
                        Description = item.Description,
                        Formula = item.Formula,
                        Name = item.Name,
                        //Price = item.Price
                    });
                }
                if (contract != null && itemObj != null)
                {
                    var contractItem = new ContractItems
                    {
                        FK_Contract_Id = contract.Id,
                        FK_Item_Id = itemObj.Id,
                        Price = item.Price
                    };
                    contractItem = await _contractItemsManager.AddAsync(contractItem);
                    result.Add(contractItem);
                }
            }
            return result;
        }

        public ContractItemFromExcelViewModel GetItems(IList<string> rowData, IList<string> columnNames)
        {
            var contractItem = new ContractItemFromExcelViewModel()
            {
                Amount = rowData[2].ToDouble(),
                ContractNumber = rowData[0].ToString(),
                Price = rowData[4].ToDouble(),
                AvailableAmount = rowData[3].ToDouble(),
                ItemCode = rowData[1].ToString(),
                Name = rowData[5].ToString(),
                Description = rowData[6].ToString(),
                Formula = rowData[7].ToString(),
                ItemAvailableAmount = rowData[8].ToDouble(),
            };
            return contractItem;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using AutoMapper;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using Microsoft.EntityFrameworkCore;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/CustomerType")]
    public class CustomerTypeController : Controller
    {
        public readonly ICustomerTypeManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public CustomerTypeController(ICustomerTypeManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;

        }


        public IActionResult Index()
        {
          
            return Ok();
        }


        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<CustomerTypeViewModel> GetAsync(int id)
        {
            CustomerTypeViewModel result = new CustomerTypeViewModel();
            CustomerType entityResult = new CustomerType();
            entityResult=await manger.GetAsync(id);
            result = mapper.Map<CustomerType,CustomerTypeViewModel> (entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
       
           var entityResult =await manger.GetAllAsync().ToListAsync();
            var result= mapper.Map < List <CustomerType> ,List<CustomerTypeViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CustomerTypeViewModel model)
        {
            CustomerTypeViewModel result = new CustomerTypeViewModel();
            CustomerType entityResult = new CustomerType();
            entityResult = mapper.Map<CustomerTypeViewModel, CustomerType> (model);

            await fillEntityIdentity(entityResult);

            entityResult = await manger.AddAsync(entityResult);
            result = mapper.Map<CustomerType, CustomerTypeViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]CustomerTypeViewModel model)
        {
            bool result = false;
            CustomerType entityResult = new CustomerType();
            entityResult = mapper.Map<CustomerTypeViewModel, CustomerType>(model);
            await fillEntityIdentity(entityResult);

            result = await manger.UpdateAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi

        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
            bool result = false;
            //CustomerType entityResult = new CustomerType();
            //CustomerTypeViewModel model = new CustomerTypeViewModel();
            //model.Id = id;
            var entityResult = await manger.GetAsync(id);
            await fillEntityIdentity(entityResult);
            result =await  manger.DeleteAsync(entityResult);
            return Ok(result);
        }
        #endregion
        #endregion
        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using AutoMapper;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using Microsoft.EntityFrameworkCore;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/Equipment")]
    public class EquipmentController : Controller
    {
        public IEquipmentManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public EquipmentController(IEquipmentManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;

        }


        [Route("GetAll")]
        public IActionResult Index()
        {

            return Ok();
        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<EquipmentViewModel> GetAsync(int id)
        {
            EquipmentViewModel result = new EquipmentViewModel();
            Equipment entityResult = new Equipment();
            entityResult = await manger.GetAsync(id);
            result = mapper.Map<Equipment, EquipmentViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<List<EquipmentViewModel>> GetAsync()
        {
            List<EquipmentViewModel> result = new List<EquipmentViewModel>();
            List<Equipment> entityResult = new List<Equipment>();
            entityResult =await  manger.GetAllAsync().ToListAsync();
            result = mapper.Map<List<Equipment>, List<EquipmentViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]EquipmentViewModel model)
        {
            EquipmentViewModel result = new EquipmentViewModel();
            Equipment entityResult = new Equipment();
            entityResult = mapper.Map<EquipmentViewModel, Equipment>(model);
            await FillEntityIdentity(entityResult);
            entityResult = await manger.AddAsync(entityResult);
            result = mapper.Map<Equipment, EquipmentViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]EquipmentViewModel model)
        {
            bool result = false;
            Equipment entityResult = new Equipment();
            entityResult = mapper.Map<EquipmentViewModel, Equipment>(model);
            await FillEntityIdentity(entityResult);
            result = await manger.UpdateAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi

        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
            bool result = false;
            //Equipment entityResult = new Equipment();
            //EquipmentViewModel model = new EquipmentViewModel();
            //model.Id = id;
            var entityResult =await  manger.GetAsync(id);
            await FillEntityIdentity(entityResult);
            result =await  manger.DeleteAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion

        [Route("UpdateAssignmentToWorkOrder")]
        [HttpPost]
        public async Task<IActionResult> UpdateAssignmentToWorkOrder([FromBody]WorkOrderEquipmentViewModel model)
        {
            var entityLst = new List<Equipment>();
            foreach (var item in model.Equipment)
            {
                var entity = await manger.GetAsync(item.Id);
                await FillEntityIdentity(entity);
                entityLst.Add(entity);
            }
            var result = manger.AssignEquipmentToWorkOrder(model.WorkOrder.Id, entityLst);
            return Ok(result);
        }

        [HttpPost]
        [Route("UnassignEquipmentFromWorkOrder")]
        public async Task<IActionResult> UnassignEquipmentFromWorkOrder([FromBody]WorkOrderEquipmentViewModel model)
        {
            var result = false;
            foreach (var item in model.Equipment)
            {
                var equipment = await manger.GetAsync(item.Id);
                await FillEntityIdentity(equipment);
                result = await manger.UnAssignEquipmentFromWorkOrder(model.WorkOrder.Id, equipment);
            }
            return Ok(result);
        }

        [Route("UpdateAssignmentToFactory")]
        [HttpGet]
        public async Task<IActionResult> UpdateAssignmentToFactory(int factoryId, int equipmentId)
        {
            var entity = await manger.GetAsync(equipmentId);
            await FillEntityIdentity(entity);
            var result = await manger.AssignEquipmentToFactory(factoryId, entity);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAvailableEquipmentInFactory")]
        public async Task<IActionResult> GetAvailableEquipmentInFactory(int factoryId)
        {
            var result = await manger.GetAvailableEquipmentInFactory(factoryId);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetEquipmentUnassignedToFactory")]
        public async Task<IActionResult> GetEquipmentUnassignedToFactoryAsync()
        {
            var result = await manger.GetEquipmentUnassignedToFactory();
            return Ok(result);
        }

        [HttpGet]
        [Route("GetEquipmentAssignedToWorkOrder")]
        public async Task<IActionResult> GetEquipmentAssignedToWorkOrderAsync(int workOrderId)
        {
            var result = await manger.GetEquipmentAssignedToWorkOrder(workOrderId);
            return Ok(result);
        }



        public async Task<IBaseEntity> FillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> FillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
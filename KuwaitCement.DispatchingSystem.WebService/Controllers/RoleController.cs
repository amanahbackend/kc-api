﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.BLL.IManagers;
using AutoMapper;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/Role")]

    public class RoleController : Controller
    {
        private readonly IApplicationRoleManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;
        public RoleController(IApplicationRoleManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            mapper = _mapper;
            manger = _manger;
            userManager = _userManager;
        }
        public IActionResult Index()
        {
            return View();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get(ApplicationRoleViewModel model)
        {
            ApplicationRoleViewModel result = new ApplicationRoleViewModel();
            var entityResult = new ApplicationRole();
            entityResult = mapper.Map<ApplicationRoleViewModel, ApplicationRole>(model);
            entityResult = await manger.GetRoleAsync(entityResult);
            result = mapper.Map<ApplicationRole, ApplicationRoleViewModel>(entityResult);
            return Ok(result);
        }
        [Route("GetAll")]
        [HttpGet]
        public IActionResult Get()
        {
            var entityResult = manger.GetAllRoles();
            var result = mapper.Map<List<ApplicationRole>, List<ApplicationRoleViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ApplicationRoleViewModel model)
        {
            ApplicationRoleViewModel result = new ApplicationRoleViewModel();
            ApplicationRole entityResult = new ApplicationRole(new BaseEntity(), model.Name);
            entityResult = mapper.Map<ApplicationRoleViewModel, ApplicationRole>(model);
            await FillEntityIdentity(userManager, HttpContext, entityResult);
            entityResult = await manger.AddRoleAsync(entityResult);
            result = mapper.Map<ApplicationRole, ApplicationRoleViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]ApplicationRoleViewModel model)
        {
            bool result = false;
            ApplicationRole entityResult = new ApplicationRole();
            entityResult = mapper.Map<ApplicationRoleViewModel, ApplicationRole>(model);
            await FillEntityIdentity(userManager, HttpContext, entityResult);
            result = await manger.UpdateRoleAsync(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]string roleName)
        {
            bool result = false;
            ApplicationRole entity = await manger.GetRoleAsyncByName(roleName);
            await FillEntityIdentity(userManager, HttpContext, entity);
            result = await manger.DeleteRoleAsync(entity);
            return Ok(result);
        }
        #endregion
        #endregion

        private async Task<ApplicationRole> FillEntityIdentity(UserManager<ApplicationUser> userManager, HttpContext httpContext, ApplicationRole entity)
        {
            if (entity != null)
            {
                ApplicationUser user = await new Helper().GetCurrentUser(userManager, httpContext);
                entity.CurrentUser = user;
            }
            return entity;
        }
    }
}
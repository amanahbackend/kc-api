﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using AutoMapper;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/Vehicle")]
    public class VehicleController : Controller
    {
        public IVehicleManager manger;
        private IJobManager jobManager;
        private IConfigurationRoot _configurationRoot;

        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public VehicleController(IVehicleManager _manger, IConfigurationRoot configurationRoot, IJobManager manager, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;
            this.jobManager = manager;
            _configurationRoot = configurationRoot;

        }


        [Route("GetAll")]
        public IActionResult Index()
        {

            return Ok();
        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<VehicleViewModel> GetAsync(int id)
        {
           var entityResult = await manger.GetAsync(id);
           var result = mapper.Map<Vehicle, VehicleViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<List<VehicleViewModel>> GetAsync()
        {
            var entityResult = await manger.GetAllAsync().ToListAsync();
            var result = mapper.Map<List<Vehicle>, List<VehicleViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]VehicleViewModel model)
        {
            VehicleViewModel result = new VehicleViewModel();
            Vehicle entityResult = new Vehicle();
            entityResult = mapper.Map<VehicleViewModel, Vehicle>(model);

            await FillEntityIdentity(entityResult);

            entityResult = await manger.AddAsync(entityResult);
            result = mapper.Map<Vehicle, VehicleViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]VehicleViewModel model)
        {
            bool result = false;
            Vehicle entityResult = new Vehicle();
                
            entityResult = mapper.Map<VehicleViewModel, Vehicle>(model);
            await FillEntityIdentity(entityResult);
             result = await manger.UpdateAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
            bool result = false;
            Vehicle entityResult = await manger.GetAsync(id);
            await FillEntityIdentity(entityResult);
            result = await manger.DeleteAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion

        [Route("UpdateVehicleAssignmentToDriver")]
        [HttpGet]
        public async Task<IActionResult> UpdateVehicleAssignmentToDriver(int vehicleId, int driverId)
        {
            var entity = await manger.GetAsync(vehicleId);
            await FillEntityIdentity(entity);
            var result =await manger.AssignDriverToVehicle(driverId, entity);
            return Ok(result);
        }

        [Route("UpdateVehicleAssignmentToFactory")]
        [HttpGet]
        public async Task<IActionResult> UpdateVehicleAssignmentToFactory(int vehicleId, int factoryId)
        {
            var entity = await manger.GetAsync(vehicleId);
            await FillEntityIdentity(entity);
            var result =await manger.AssignVehicleToFactory(factoryId, entity);
            return Ok(result);
        }

        [Route("GetAvailableVehicleInFactory")]
        [HttpGet]
        public async Task<IActionResult> GetAvailableVehicleInFactoryAsync([FromQuery] int factoryId)
        {
            var result =await manger.GetAvailableVehiclesInFactory(factoryId);
            var model = Mapper.Map<List<Vehicle>, List<VehicleViewModel>>(result);
            return Ok(result);
        }

        [Route("GetAvailableVehicleByOrder")]
        [HttpGet]
        public async Task<IActionResult> GetAvailableVehicleByOrderAsync([FromQuery] int factoryId, [FromQuery] int skippedNumber)
        {
            var result = await manger.GetAvailableVehicleByOrder(factoryId, skippedNumber);
            var model = Mapper.Map<Vehicle, VehicleViewModel>(result);
            return Ok(result);
        }

        [Route("GetByWorkOrder")]
        [HttpGet]
        public IActionResult GetByWorkOrder(int orderId)
        {
            var vehicles = manger.GetByWorkorder(orderId);
            return Ok(vehicles);
        }

        [Route("GetByFactory")]
        [HttpGet]
        public IActionResult GetByFactory([FromQuery] int factoryId)
        {
            var vehicles = manger.GetByFactory(factoryId);
            return Ok(vehicles);
        }
        
        [AllowAnonymous]
        [Route("SetVehicleAvailableKCAVIAsync")]
        [HttpGet]
        public async Task<IActionResult> SetVehicleAvailableAsync([FromQuery] string vechileId,[FromQuery] string pushButtonString)
        {
            var optsBuilder = new DbContextOptionsBuilder<DataContext>();
            string connectionString = _configurationRoot.GetConnectionString("DefaultConnection");
            string migrationAssembly = _configurationRoot["MigrationsAssembly"];
            optsBuilder.UseSqlServer(connectionString,
                        sqlOptions => sqlOptions.MigrationsAssembly(migrationAssembly));
            DataContext dataContext = new DataContext(optsBuilder.Options);
            var vehicles =await manger.SetVehicleAvailableAsync(vechileId, dataContext);
            return Ok(vehicles);
        }

     

        [Route("GetAllVehiclesLocations")]
        [HttpGet]
        public async Task<IActionResult> GetAllVehiclesLocations()
        {
            var user = await new Helper().GetCurrentUser(userManager, HttpContext);
            var roles = await userManager.GetRolesAsync(user);
            var result = manger.GetAllVehicleLocations(roles.ToList(), user.Id);
            return Ok(result);
        }
        [Route("GetVehicleJobs")]
        [HttpGet]
        public async Task<IActionResult> GetVehicleJobsAsync([FromQuery] int vechileId)
        {
            var jobs = await manger.GetVehicleJobs(vechileId);
            return Ok(jobs);
        }
        public async Task<IBaseEntity> FillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> FillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
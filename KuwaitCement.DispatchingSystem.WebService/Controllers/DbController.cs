﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Identity;
using DispatchingSystem.BLL;
using DispatchingSystem.BLL.IManagers;
using Microsoft.AspNetCore.Http.Extensions;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Produces("application/json")]
    [Route("api/Db")]
    public class DbController : Controller
    {
        public readonly UserManager<ApplicationUser> _identityUserManager;
        public readonly RoleManager<ApplicationRole> _identityRoleManager;
        public readonly IPrivilgeManager _privilgeManager;
        public readonly IRolePrivilgeManager _rolePrivilgeManager;
        public readonly ILKP_PriorityManager _priorityManager;
        public readonly ILKP_StatusManager _statusManager;
        public readonly ICustomerTypeManager _customerTypeManager;
        public readonly ILKPConditionManager _conditionManager;
        public readonly IDriverManager _driverManager;
        public readonly IVehicleManager _vehicleManager;

        public DbController(UserManager<ApplicationUser> identityUserManager,
            RoleManager<ApplicationRole> identityRoleManager,
            IPrivilgeManager privilgeManager, IRolePrivilgeManager rolePrivilgeManager,
            ILKP_PriorityManager priorityManager, ILKP_StatusManager statusManager,
            ICustomerTypeManager customerTypeManager, ILKPConditionManager conditionManager,
            IDriverManager driverManager,IVehicleManager vehicleManager)
        {
            _identityRoleManager = identityRoleManager;
            _identityUserManager = identityUserManager;
            _privilgeManager = privilgeManager;
            _rolePrivilgeManager = rolePrivilgeManager;
            _priorityManager = priorityManager;
            _statusManager = statusManager;
            _customerTypeManager = customerTypeManager;
            _conditionManager = conditionManager;
            _driverManager = driverManager;
            _vehicleManager = vehicleManager;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("initusersroles")]
        public async Task<IActionResult> InitUsersRoles()
        {
            DbInitializer dbInitializer = new DbInitializer(_identityUserManager, _identityRoleManager);
            await dbInitializer.InitializeUsersRoles();
            return Ok();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("initprivileges")]
        public async Task<IActionResult> InitPrivileges()
        {
            DbInitializer dbInitializer = new DbInitializer(_identityUserManager, _identityRoleManager);
            await dbInitializer.InitializePriveleges(_privilgeManager, _rolePrivilgeManager);
            return Ok();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("initlookups")]
        public async Task<IActionResult> InitLookups()
        {
            DbInitializer dbInitializer = new DbInitializer(_identityUserManager, _identityRoleManager);
            await dbInitializer.InitializeLookups(_priorityManager, _statusManager, _customerTypeManager, _conditionManager);
            return Ok();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("InitDrivers")]
        public async Task<IActionResult> InitDrivers()
        {
            DbInitializer dbInitializer = new DbInitializer(_identityUserManager, _identityRoleManager);
            await dbInitializer.InitializeDrivers(_driverManager);
            return Ok();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("InitVehicles")]
        public async Task<IActionResult> InitVehicles()
        {
            DbInitializer dbInitializer = new DbInitializer(_identityUserManager, _identityRoleManager);
            await dbInitializer.InitializeVehicles(_vehicleManager,_driverManager);
            return Ok();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using AutoMapper;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/Status")]
    public class LKP_StatusController : Controller
    {
        public ILKP_StatusManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public LKP_StatusController(ILKP_StatusManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.mapper = _mapper;
            this.userManager = _userManager;

        }


        [Route("GetAll")]
        public IActionResult Index()
        {

            return Ok();
        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<LKP_StatusViewModel> GetAsync(int id)
        {
           
           var entityResult = await manger.GetAsync(id);
           var  result = mapper.Map<LKP_Status, LKP_StatusViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public List<LKP_StatusViewModel> Get()
        {
            List<LKP_StatusViewModel> result = new List<LKP_StatusViewModel>();
            List<LKP_Status> entityResult = new List<LKP_Status>();
            entityResult = manger.GetAllAsync().ToList();
            result = mapper.Map<List<LKP_Status>, List<LKP_StatusViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]LKP_StatusViewModel model)
        {
         
           var  entityResult = mapper.Map<LKP_StatusViewModel, LKP_Status>(model);
            await FillEntityIdentity(entityResult);

           entityResult = await manger.AddAsync(entityResult);
           var  result = mapper.Map<LKP_Status, LKP_StatusViewModel>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]LKP_StatusViewModel model)
        {
           
          var  entityResult = mapper.Map<LKP_StatusViewModel, LKP_Status>(model);
            await FillEntityIdentity(entityResult);
           var result =await  manger.UpdateAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
            LKP_StatusViewModel model = new LKP_StatusViewModel();
            model.Id = id;
           var entityResult = mapper.Map<LKP_StatusViewModel, LKP_Status>(model);
            await FillEntityIdentity(entityResult);
          var  result = await manger.DeleteAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion
        public async Task<IBaseEntity> FillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> FillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }

        [Route("GetAllByRole")]
        [HttpGet]
        public async Task<IActionResult> GetAllByRole()
        {
            var user = await new Helper().GetCurrentUser(userManager, HttpContext);
            var roles = await userManager.GetRolesAsync(user);
            if (roles != null && roles.Count > 0)
            {
                List<LKP_Status> entityResult =await manger.GetByRole(roles.First());
                List<LKP_StatusViewModel> result = mapper.Map<List<LKP_Status>, List<LKP_StatusViewModel>>(entityResult);
                return Ok(result);
            }
            return Ok(null);
        }
    }
}
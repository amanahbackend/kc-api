﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using AutoMapper;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using Utilites.PACI;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/Location")]
    public class LocationController : Controller
    {


        private ILocationManager _manger;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAreaManager areaManager;
        private readonly IGovernrateManager governrateManager;
        private IConfigurationRoot _configurationRoot;


        public LocationController(ILocationManager manger, IMapper mapper,
                                    UserManager<ApplicationUser> userManager, IAreaManager areaManager, IGovernrateManager governrateManager, IConfigurationRoot configurationRoot)
        {
            _manger = manger;
            _userManager = userManager;
            this.areaManager = areaManager;
            this.governrateManager = governrateManager;
            _mapper = mapper;
            _configurationRoot = configurationRoot;
        }


        [Route("GetAll")]
        public IActionResult Index()
        {

            return Ok();
        }

        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<LocationViewModel> GetAsync(int id)
        {
  
           var  entityResult = await _manger.GetAsync(id);
           var  result = _mapper.Map<Location, LocationViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
           var entityResult = await _manger.GetAllAsync().ToListAsync();
            var result = _mapper.Map<List<Location>, List<LocationViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]LocationViewModel model)
        {
            LocationViewModel result = new LocationViewModel();
            Location entityResult = new Location();
            entityResult = _mapper.Map<LocationViewModel, Location>(model);
            await fillEntityIdentity(entityResult);

            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string paciServiceUrl = _configurationRoot["Location:PACIService:ServiceUrl"];
            string paciFieldName = _configurationRoot["Location:PACIService:PACIFieldName"];
            string streetServiceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
            string streetFieldName = _configurationRoot["Location:StreetService:StreetFieldName"];
            string blockNameFieldNameStreetService = _configurationRoot["Location:StreetService:BlockNameFieldName"];

            entityResult =await  _manger.Add(entityResult, proxyUrl, paciServiceUrl, streetServiceUrl,
                paciFieldName, streetFieldName, blockNameFieldNameStreetService);

            result = _mapper.Map<Location, LocationViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]LocationViewModel model)
        {
        
           var entityResult = _mapper.Map<LocationViewModel, Location>(model);
            await fillEntityIdentity(entityResult);

           var result =await  _manger.UpdateAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #region DeleteApi

        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
          
            LocationViewModel model = new LocationViewModel();
            model.Id = id;
           var  entityResult = _mapper.Map<LocationViewModel, Location>(model);
            await fillEntityIdentity(entityResult);
           var  result = await _manger.DeleteAsync(entityResult);
            return Ok(result);

        }
        #endregion
        #endregion
        private async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(_userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        private async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(_userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }


        [HttpGet]
        [Route("GetLocationByPaci")]
        public IActionResult GetLocationByPaci(string paciNumber)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string paciServiceUrl = _configurationRoot["Location:PACIService:ServiceUrl"];
            string paciFieldName = _configurationRoot["Location:PACIService:PACIFieldName"];
            string blockServiceUrl = _configurationRoot["Location:BlockService:ServiceUrl"];
            string streetServiceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
            string blockNameFieldNameBlockService = _configurationRoot["Location:BlockService:BlockNameFieldName"];
            string blockNameFieldNameStreetService = _configurationRoot["Location:StreetService:BlockNameFieldName"];
            string streetFieldName = _configurationRoot["Location:StreetService:StreetFieldName"];
            string nhoodNameFieldName = _configurationRoot["Location:BlockService:AreaNameFieldName"];

            var result = PACIHelper.GetLocationByPACI(Convert.ToInt32(paciNumber), proxyUrl, paciServiceUrl,
                                        paciFieldName, blockServiceUrl, blockNameFieldNameBlockService,
                                        nhoodNameFieldName, streetServiceUrl, blockNameFieldNameStreetService,
                                        streetFieldName);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetAllGovernorates")]
        public async Task<IActionResult> GetAllGovernoratesAsync()
        {
            //  string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            //string serviceUrl = _configurationRoot["Location:GovernorateService:ServiceUrl"];

            ///var result = PACIHelper.GetAllGovernorates(proxyUrl, serviceUrl);

            var allGovernates = await governrateManager.GetAllGovernoratesAsync();

            return Ok(allGovernates);
        }

        [HttpGet]
        [Route("GetAreas")]
        public async Task<IActionResult> GetAreasAsync(int? govId)
        {
           // string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            //string serviceUrl = _configurationRoot["Location:AreaService:ServiceUrl"];
            //string govIdfieldName = _configurationRoot["Location:AreaService:GovernorateIdFieldName"];
            //var result = PACIHelper.GetAreas(govId, govIdfieldName, proxyUrl, serviceUrl);
            //if (result == null && govId != null)
            //{
                var allAreas = await areaManager.GetAreasAsync(govId.Value.ToString());
            //}
            return Ok(allAreas);
        }

        [HttpGet]
        [Route("GetBlocks")]
        public IActionResult GetBlocks(int? areaId)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:BlockService:ServiceUrl"];
            string areaIdfieldName = _configurationRoot["Location:BlockService:AreaIdFieldName"];
            var result = PACIHelper.GetBlocks(areaId, areaIdfieldName, proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetStreets")]
        public IActionResult GetStreets(int? govId, int? areaId, string blockName)
        {
            string proxyUrl = _configurationRoot["Location:ProxyUrl"];
            string serviceUrl = _configurationRoot["Location:StreetService:ServiceUrl"];
            string govIdFieldName = _configurationRoot["Location:StreetService:GovIdFieldName"];
            string areaIdFieldName = _configurationRoot["Location:StreetService:AreaIdFieldName"];
            string blockNameFieldName = _configurationRoot["Location:StreetService:BlockNameFieldName"];
            var result = PACIHelper.GetStreets(govId, govIdFieldName, areaId, areaIdFieldName,
                                                blockName, blockNameFieldName, proxyUrl, serviceUrl);
            return Ok(result);
        }

        [HttpGet]
        [Route("GetLocationByContractId")]
        public async Task<IActionResult> GetLocationByContractIdAsync(int contractId)
        {
            var result = await _manger.GetByContractIdAsync(contractId);
            return Ok(result);
        }

        [HttpPost]
        [Route("GetLocationByContractIds")]
        public async Task<IActionResult> GetLocationByContractIdsAsync([FromBody]int[] contractIds)
        {
            var result = await _manger.GetByContractIdsAsync(contractIds);
            return Ok(result);
        }
    }
}
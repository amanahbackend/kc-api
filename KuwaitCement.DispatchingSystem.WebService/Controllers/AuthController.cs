﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using DispatchingSystem.Models.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using System.IdentityModel.Tokens.Jwt;

using System.Net;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using KuwaitCement.DispatchingSystem.WebService.Filters;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.BLL.IManagers;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Produces("application/json")]
    [Route("api/Auth")]
    public class AuthController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _siginManager;
        private IPasswordHasher<ApplicationUser> _passwordHasher;
        private IConfigurationRoot _configurationRoot;
        private ILogger<AuthController> _logger;
        private readonly ISecurityManager _securityManager;
        public AuthController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            RoleManager<ApplicationRole> roleManager, IPasswordHasher<ApplicationUser> passwordHasher, IConfigurationRoot configurationRoot
            , ILogger<AuthController> logger, ISecurityManager securityManager)
        {
            _userManager = userManager;
            _siginManager = signInManager;
            _roleManager = roleManager;
            _passwordHasher = passwordHasher;
            _configurationRoot = configurationRoot;
            _logger = logger;
            _securityManager = securityManager;
        }


        //[AllowAnonymous]
        //[HttpPost]
        //[Route("register")]
        //public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest();
        //    }
        //    BaseEntity baseEntity = new BaseEntity();
        //    var user = new ApplicationUser(baseEntity)
        //    {
        //        UserName = model.Email,
        //        Email = model.Email
        //    };
        //    ApplicationUserManager appUserManager = new ApplicationUserManager(_userManager);
        //    var result = await appUserManager.AddUserAsync(user, model.Password);

        //    if (result != null)
        //    {
        //        return Ok(result);
        //    }
        //    return BadRequest();
        //}

        [ValidateForm]
        [HttpPost("CreateToken")]
        [Route("token")]
        public async Task<IActionResult> CreateToken([FromBody] LoginViewModel model)
        {
            try
            {
                var jwtSecurityTokenKey = _configurationRoot["JwtSecurityToken:Key"];
                var issuer = _configurationRoot["JwtSecurityToken:Issuer"];
                var audience = _configurationRoot["JwtSecurityToken:Audience"];
                var token = await _securityManager.GetToken(model.Username, model.Password, jwtSecurityTokenKey, issuer, audience);
                ApplicationUserManager applicationUserManager = new ApplicationUserManager(_userManager);

                var roleNames = await applicationUserManager.GetRolesAsync(model.Username);

                if (token != null)
                {
                    return Ok(new
                    {
                        Token = new JwtSecurityTokenHandler().WriteToken(token),
                        ExpirationTimeUTC = token.ValidTo,
                        UserName = model.Username,
                        Roles = roleNames
                    });
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"error while creating token: {ex}");
                return StatusCode((int)HttpStatusCode.InternalServerError, "error while creating token");
            }
        }
    }
}
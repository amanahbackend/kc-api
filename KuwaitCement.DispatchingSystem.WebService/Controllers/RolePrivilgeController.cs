﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using AutoMapper;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using KuwaitCement.DispatchingSystem.WebService.Utilites;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/RolePrivilge")]
    public class RolePrivilgeController : Controller
    {
        public IRolePrivilgeManager manger;
        public readonly IMapper mapper;
        public readonly UserManager<ApplicationUser> userManager;

        public RolePrivilgeController(IRolePrivilgeManager _manger, IMapper _mapper, UserManager<ApplicationUser> _userManager)
        {
            this.manger = _manger;
            this.userManager = _userManager;
            this.mapper = _mapper;
        }


        [Route("GetAll")]
        public IActionResult Index()
        {
          
            return Ok();
        }
        #region DefaultCrudOperation

        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<RolePrivilgeViewModel> GetAsync(int id)
        {
        
           var entityResult = await manger.GetAsync(id);
          var   result = mapper.Map<RolePrivilge, RolePrivilgeViewModel>(entityResult);
            return result;
        }
        [Route("GetAll")]
        [HttpGet]
        public List<RolePrivilgeViewModel> Get()
        {
            List<RolePrivilgeViewModel> result = new List<RolePrivilgeViewModel>();
            List<RolePrivilge> entityResult = new List<RolePrivilge>();
            entityResult = manger.GetAllAsync().ToList();
            result = mapper.Map<List<RolePrivilge>, List<RolePrivilgeViewModel>>(entityResult);
            return result;
        }
        #endregion

        #region PostApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Add")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]RolePrivilgeViewModel model)
        {
        
          var  entityResult = mapper.Map<RolePrivilgeViewModel, RolePrivilge>(model);

            await fillEntityIdentity(entityResult);

            entityResult = await manger.AddAsync(entityResult);
          var  result = mapper.Map<RolePrivilge, RolePrivilgeViewModel>(entityResult);
            return Ok(result);

        }
        #endregion

        #region PutApi
        //string should be replced based on each Controller EntityDTO Object 
        [Route("Update")]
        [HttpPost]
        public async Task<IActionResult> Put([FromBody]RolePrivilgeViewModel model)
        {
        
           var entityResult = mapper.Map<RolePrivilgeViewModel, RolePrivilge>(model);
            await fillEntityIdentity(entityResult);

          var   result = await manger.UpdateAsync(entityResult);
            return Ok(result);
        }
        #endregion
        #region DeleteApi
        [Route("Delete")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
  
            RolePrivilgeViewModel model = new RolePrivilgeViewModel();
            model.Id = id;
         var   entityResult = mapper.Map<RolePrivilgeViewModel, RolePrivilge>(model);
            await fillEntityIdentity(entityResult);
         var   result = await manger.DeleteAsync(entityResult);
            return Ok(result);
        }
        #endregion
        #endregion
        public async Task<IBaseEntity> fillEntityIdentity(IBaseEntity baseEntity)
        {
            Helper help = new Helper();
            baseEntity = await help.FillEntityIdentity(userManager, HttpContext, baseEntity);
            return baseEntity;
        }
        public async Task<List<IBaseEntity>> fillEntitiesIdentites(List<IBaseEntity> lstBaseEntities)
        {
            Helper help = new Helper();
            lstBaseEntities = await help.FillListEntitiesIdentites(userManager, HttpContext, lstBaseEntities);
            return lstBaseEntities;
        }
    }
}
﻿using AutoMapper;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.Controllers
{
    [Route("api/LKP_ConfirmationStatus")]
    public class LKP_ConfirmationStatusController : Controller
    {
        private ILKP_ConfirmationStatusManager _confirmationStatusManager;
        private readonly UserManager<ApplicationUser> _userManager;
        public LKP_ConfirmationStatusController(ILKP_ConfirmationStatusManager confirmationStatusManager,
             UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _confirmationStatusManager = confirmationStatusManager;
        }

        [HttpGet]
        [Route("Seed")]
        public IActionResult Seed()
        {
            _confirmationStatusManager.Seed();
            return Ok();
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            var user = await new Utilites.Helper().GetCurrentUser(_userManager, HttpContext);
            string role = user.RoleNames.First();
            var entities = await  _confirmationStatusManager.GetByRole(role);
            var models = Mapper.Map<List<LKP_ConfirmationStatus>, List<LKP_ConfirmationStatusViewModel>>(entities);
            return Ok(models);
        }

        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> GetAsync([FromQuery]int id)
        {
            var entity = await _confirmationStatusManager.GetAsync(id);
            var model = Mapper.Map<LKP_ConfirmationStatus, LKP_ConfirmationStatusViewModel>(entity);
            return Ok(model);
        }
    }
}

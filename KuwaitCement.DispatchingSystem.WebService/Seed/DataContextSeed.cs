﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.Seed
{
    public class DataContextSeed
    {
        private readonly ILKP_ConfirmationStatusManager _confirmationStatusManager;
        private readonly ILKP_PriorityManager _priorityManager;
        private readonly ILKP_StatusManager _statusManager;
        private readonly ILKPConditionManager _conditionManager;
        private readonly ICustomerTypeManager _customerTypeManager;
        private readonly IDriverManager _driverManager;
        private readonly IVehicleManager _vehicleManager;
        private readonly IApplicationRoleManager _applicationRoleManager;
        private readonly IApplicationUserManager _applicationUserManager;
        private readonly IFactoryManager _factoryManager;
        private readonly ILKP_CustomerCategoryManager _customerCategoryManager;


        public DataContextSeed(ILKP_ConfirmationStatusManager confirmationStatusManager,
            ILKP_PriorityManager priorityManager, ILKP_StatusManager statusManager,
            ILKPConditionManager conditionManager, ICustomerTypeManager customerTypeManager,
            IDriverManager driverManager, IVehicleManager vehicleManager,
            IApplicationRoleManager applicationRoleManager,
            IApplicationUserManager applicationUserManager, IFactoryManager factoryManager,
            ILKP_CustomerCategoryManager customerCategoryManager)
        {
            _customerCategoryManager = customerCategoryManager;
            _factoryManager = factoryManager;
            _applicationUserManager = applicationUserManager;
            _applicationRoleManager = applicationRoleManager;
            _vehicleManager = vehicleManager;
            _driverManager = driverManager;
            _customerTypeManager = customerTypeManager;
            _conditionManager = conditionManager;
            _statusManager = statusManager;
            _priorityManager = priorityManager;
            _confirmationStatusManager = confirmationStatusManager;
        }


        public async Task SeedAsync(DataContext context, ILogger<DataContextSeed> logger, int? retry = 0)
        {
            int retryForAvaiability = retry.Value;
            try
            {
                if (!context.Users.Any() && !context.Roles.Any())
                {
                    await SeedUsersRoles(_applicationRoleManager, _applicationUserManager);
                }
                if (context.LKP_ConfirmationStatus.Any())
                {
                    await SeedConfirmationStatusAsync(_confirmationStatusManager);
                }
                if (!context.LKP_Priorities.Any())
                {
                    await SeedPrioritiesAsync(_priorityManager);
                }
                if (context.LKP_Status.Any())
                {
                    await SeedStatusAsync(_statusManager);
                }
                if (!context.CustometrTypes.Any())
                {
                    await SeedCutomerTypesAsync(_customerTypeManager);
                }
                if (!context.LKPConditions.Any())
                {
                    await SeedConditionsAsync(_conditionManager);
                }
                if (!context.Factories.Any())
                {
                   await  SeedFactoriesAsync(_factoryManager);
                }
                if (!context.Drivers.Any())
                {
                    await SeedDriversAsync(_driverManager);
                }
                if (!context.LKP_WashingStatus.Any())
                {
                    await SeedWashingStatusAsync(context);
                }
                if (!context.Vehicles.Any())
                {
                    await SeedVehiclesAsync(_vehicleManager, _driverManager, _factoryManager);
                }
                if (!context.CustomerCategories.Any())
                {
                    await SeedCustomerCategoriesAsync(_customerCategoryManager);
                }
            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 10)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for Customer DataContext");

                  await  SeedAsync(context, logger, retryForAvaiability);
                }
            }
        }

        private async Task SeedConfirmationStatusAsync(ILKP_ConfirmationStatusManager confirmationStatusManager)
        {

            string movementCtrlConfirmation = "Need Movement Controller Confirmation";
            string confirmedByMovement = "Confirmed By Movement Controller";
            string needToChangebyMovement = "Need To Change Date By Movement Controller";
            string confirmedByCustomer = "Confirmed By Customer";
            string reschedule = "Need To Reschedule By Customer";
            string Declined = "Declined";

            if (!await _confirmationStatusManager.StatusExists(movementCtrlConfirmation))
            {
                await _confirmationStatusManager.AddAsync(new LKP_ConfirmationStatus()
                {
                    Name = _confirmationStatusManager.NeedMovementCtrlConfirmation
                });
            }
            if (!await _confirmationStatusManager.StatusExists(confirmedByMovement))
            {
                await _confirmationStatusManager.AddAsync(new LKP_ConfirmationStatus()
                {
                    Name = _confirmationStatusManager.ConfirmedByMovementCtrl
                });
            }
            if (!await confirmationStatusManager.StatusExists(needToChangebyMovement))
            {
               await  _confirmationStatusManager.AddAsync(new LKP_ConfirmationStatus()
                {
                    Name = _confirmationStatusManager.NeedToChangeDateByMovementCtrl
                });
            }
            if (!await confirmationStatusManager.StatusExists(confirmedByCustomer))
            {
               await  _confirmationStatusManager.AddAsync(new LKP_ConfirmationStatus()
                {
                    Name = _confirmationStatusManager.ConfirmedByCustomer
                });
            }

            if (!await confirmationStatusManager.StatusExists(reschedule))
            {
                await _confirmationStatusManager.AddAsync(new LKP_ConfirmationStatus()
                {
                    Name = _confirmationStatusManager.NeedToRescheduleByCustomer
                });
            }

            if (!await confirmationStatusManager.StatusExists(Declined))
            {
                await _confirmationStatusManager.AddAsync(new LKP_ConfirmationStatus()
                {
                    Name = _confirmationStatusManager.Declined
                });
            }
        }

        private async Task SeedPrioritiesAsync(ILKP_PriorityManager priorityManager)
        {
            string urgentPriority = "Urgent";
            string importantPriority = "Important";
            string normalPriority = "Normal";
            string lowPriority = "Low";

            if (!await priorityManager.PriorityExits(urgentPriority))
                await priorityManager.AddAsync(new LKP_Priority()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    Name = urgentPriority,
                    IsDeleted = false
                });
            if (!await priorityManager.PriorityExits(importantPriority))
                await priorityManager.AddAsync(new LKP_Priority()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    Name = importantPriority,
                    IsDeleted = false
                });
            if (!await priorityManager.PriorityExits(normalPriority))
                await priorityManager.AddAsync(new LKP_Priority()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    Name = normalPriority,
                    IsDeleted = false
                });
            if (!await priorityManager.PriorityExits(lowPriority))
                await priorityManager.AddAsync(new LKP_Priority()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    Name = lowPriority,
                    IsDeleted = false
                });
        }

        private async Task SeedStatusAsync(ILKP_StatusManager statusManager)
        {
            string apprrovedStatus = "Approved";
            string pendingStatus = "Pending";
            string rejectedStatus = "Rejected";
            string cancelledStatus = "Cancelled";
            string completedStatus = "Completed";
            string inProgressStatus = "In Progress";
            string dispatchedStatus = "Dispatched";
            string changeDateSatus = "Need To Change Date By Movement Ctrl";
            string confirmedCustomer = "Confirmed by customer";

            string Declined = "Declined ";

            if (!await statusManager.StatusExists(apprrovedStatus))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = apprrovedStatus
                });
            if (!await statusManager.StatusExists(pendingStatus))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = pendingStatus
                });
            if (!await statusManager.StatusExists(rejectedStatus))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = rejectedStatus
                });
            if (!await statusManager.StatusExists(cancelledStatus))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = cancelledStatus
                });
            if (!await statusManager.StatusExists(completedStatus))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = completedStatus
                });
            if (!await statusManager.StatusExists(inProgressStatus))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = inProgressStatus
                });
            if (!await statusManager.StatusExists(dispatchedStatus))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = dispatchedStatus
                });
            if (!await statusManager.StatusExists(changeDateSatus))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = changeDateSatus
                });
            if (!await statusManager.StatusExists(confirmedCustomer))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = confirmedCustomer
                });
            if (!await statusManager.StatusExists(Declined))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = Declined
                });



        }

        private async Task SeedCutomerTypesAsync(ICustomerTypeManager customerTypeManager)
        {
            string maleType = "Male";
            string femaleType = "Female";
            string companyType = "Company";

            if (!await customerTypeManager.CustomerTypeExists(maleType))
                await customerTypeManager.AddAsync(new CustomerType()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = maleType
                });
            if (!await customerTypeManager.CustomerTypeExists(femaleType))
                await customerTypeManager.AddAsync(new CustomerType()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = femaleType
                });
            if (!await customerTypeManager.CustomerTypeExists(companyType))
                await customerTypeManager.AddAsync(new CustomerType()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = companyType
                });
        }

        private async Task SeedConditionsAsync(ILKPConditionManager conditionManager)
        {
            string validityCondition = "Validity Time";

            if (!await conditionManager.ConditionExists(validityCondition))
                await conditionManager.AddAsync(new LKPCondition()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    Name = "Validity Time"
                });
        }

        private async Task SeedDriversAsync(IDriverManager driverManager)
        {
            await driverManager.AddAsync(new Driver()
            {
                IsAvailable = true,
                IsDeleted = false,
                Name = "Ahmed"
            });
            await driverManager.AddAsync(new Driver()
            {
                IsAvailable = true,
                IsDeleted = false,
                Name = "Mohamed"
            });
            await driverManager.AddAsync(new Driver()
            {
                IsAvailable = true,
                IsDeleted = false,
                Name = "Ismail"
            });
            await driverManager.AddAsync(new Driver()
            {
                IsAvailable = true,
                IsDeleted = false,
                Name = "Hossam"
            });

        }

        private async Task SeedWashingStatusAsync(DataContext context)
        {
           await context.LKP_WashingStatus.AddAsync(new LKP_WashingStatus
            {
                Id = WashingStatus.NotWashed,
                Name = Enum.GetName(typeof(WashingStatus), WashingStatus.NotWashed)
            });
            await context.LKP_WashingStatus.AddAsync(new LKP_WashingStatus
            {
                Id = WashingStatus.InWashing,
                Name = Enum.GetName(typeof(WashingStatus), WashingStatus.InWashing)
            });
            await context.LKP_WashingStatus.AddAsync(new LKP_WashingStatus
            {
                Id = WashingStatus.Washed,
                Name = Enum.GetName(typeof(WashingStatus), WashingStatus.Washed)
            });
        }

        private Task SeedFactoriesAsync(IFactoryManager factoryManager)
        {
            return factoryManager.AddAsync(new Factory
            {
                Name = "others",
                Latitude = 29.3750747,
                Longitude = 47.9847351
            });
        }
        private async Task SeedVehiclesAsync(IVehicleManager vehicleManager, IDriverManager driverManager, IFactoryManager factoryManager)
        {
            var factoryId =(await factoryManager.GetAsync("Name", "others")).FirstOrDefault()?.Id;
            var driverId = driverManager.GetAllAsync().First().Id;
            await vehicleManager.AddAsync(new Vehicle()
            {
                IsAvailable = true,
                IsDeleted = false,
                Number = "Vehicle1234",
                FK_Driver_Id = driverId,
                Fk_WashingStatus_Id = WashingStatus.Washed,
                FK_Factory_Id = factoryId
            });
           await vehicleManager.AddAsync(new Vehicle()
            {
                IsAvailable = true,
                IsDeleted = false,
                Number = "Vehicle4567",
                FK_Driver_Id = driverId,
                Fk_WashingStatus_Id = WashingStatus.Washed,
                FK_Factory_Id = factoryId
            });
           await  vehicleManager.AddAsync(new Vehicle()
            {
                IsAvailable = true,
                IsDeleted = false,
                Number = "Vehicle7890",
                FK_Driver_Id = driverId,
                Fk_WashingStatus_Id = WashingStatus.Washed,
                FK_Factory_Id = factoryId
            });
          await  vehicleManager.AddAsync(new Vehicle()
            {
                IsAvailable = true,
                IsDeleted = false,
                Number = "Vehicle3456",
                FK_Driver_Id = driverId,
                Fk_WashingStatus_Id = WashingStatus.Washed,
                FK_Factory_Id = factoryId
            });
        }

        private async Task SeedUsersRoles(IApplicationRoleManager applicationRoleManager,
            IApplicationUserManager applicationUserManager)
        {
            // Add Roles

            if (!await applicationRoleManager.IsRoleExistAsync("Admin"))
                await applicationRoleManager.AddRoleAsync("Admin");

            if (!await applicationRoleManager.IsRoleExistAsync("CallCenter"))
                await applicationRoleManager.AddRoleAsync("CallCenter");

            if (!await applicationRoleManager.IsRoleExistAsync("MovementController"))
                await applicationRoleManager.AddRoleAsync("MovementController");

            if (!await applicationRoleManager.IsRoleExistAsync("Dispatcher"))
                await applicationRoleManager.AddRoleAsync("Dispatcher");

            if (!await applicationRoleManager.IsRoleExistAsync("Sales"))
                await applicationRoleManager.AddRoleAsync("Sales");


            //Add users
            if (!await applicationUserManager.IsUserNameExistAsync("User1"))
                await applicationUserManager.AddUserAsync("User1", "abc@xyz.com", "P@ssw0rd");

            if (!await applicationUserManager.IsUserNameExistAsync("User2"))
                await applicationUserManager.AddUserAsync("User2", "dyz@xyz.com", "P@ssw0rd");

            if (!await applicationUserManager.IsUserNameExistAsync("User3"))
                await applicationUserManager.AddUserAsync("User3", "asd@xyz.com", "P@ssw0rd");

            if (!await applicationUserManager.IsUserNameExistAsync("User4"))
                await applicationUserManager.AddUserAsync("User4", "qwe@xyz.com", "P@ssw0rd");

            if (!await applicationUserManager.IsUserNameExistAsync("User5"))
                await applicationUserManager.AddUserAsync("User5", "zxc@xyz.com", "P@ssw0rd");


            await applicationUserManager.AddUserToRoleAsync("Admin", "Admin");
            await applicationUserManager.AddUserToRoleAsync("Dispatcher", "Dispatcher");
            await applicationUserManager.AddUserToRoleAsync("CallCenter", "CallCenter");
            await applicationUserManager.AddUserToRoleAsync("MovementController", "MovementController");
            await applicationUserManager.AddUserToRoleAsync("Sales", "Sales");
        }

        private async Task SeedCustomerCategoriesAsync(ILKP_CustomerCategoryManager manager)
        {
           await manager.AddAsync(new LKP_CustomerCategory
            {
                Name = "A"
            });
            await manager.AddAsync(new LKP_CustomerCategory
            {
                Name = "B"
            });
            await manager.AddAsync(new LKP_CustomerCategory
            {
                Name = "C"
            });
            await manager.AddAsync(new LKP_CustomerCategory
            {
                Name = "D"
            });
            await manager.AddAsync(new LKP_CustomerCategory
            {
                Name = "E"
            });
        }
    }
}

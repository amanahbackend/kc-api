﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.SignalR;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.Filters
{
    public class NotificationsFilter : IActionFilter
    {
        IHubContext<DispatchingHub> _dispatchingHub;
        IConfigurationRoot _configuration;

        public NotificationsFilter(IHubContext<DispatchingHub> dispatchingHub, IConfigurationRoot configuration)
        {
            _configuration = configuration;
            _dispatchingHub = dispatchingHub;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            object resultObj = null;
            string operation = "";
            var controllerType = context.Controller.GetType();
            var entityName = controllerType.Name.Substring(0, controllerType.Name.IndexOf("Controller"));
            var entities = _configuration.GetSection("Notifications:UpdatesListenerEntities").Get<string[]>();
            if (entities.Contains(entityName))
            {
                var userName = "";
                object update = new object();
                var clientMethodName = _configuration["Notifications:UpdatesClientMethod"];
                string requestedAction = context.HttpContext.Request.Path.ToUriComponent();

                if (requestedAction.ToLower().Contains("add") &&
                    !entityName.ToLower().Equals("workorder"))
                {
                    operation = "add";
                    if (context.Result is OkObjectResult result)
                    {
                        resultObj = result.Value;
                    }
                    userName = context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                    update = new { Operation = operation, Entity = entityName, ResultObject = resultObj };
                    new DispatchingHub().SendUpdates(update, clientMethodName, _dispatchingHub, userName);
                }
                else if (requestedAction.ToLower().Contains("update") ||
                         requestedAction.ToLower().Contains("cancel") ||
                         requestedAction.ToLower().Contains("reassign"))
                {
                    operation = "update";
                    if (context.Result is OkObjectResult result)
                    {
                        try
                        {
                            dynamic resultValue = result.Value as dynamic;
                            resultObj = resultValue;
                        }
                        catch (RuntimeBinderException)
                        {
                            var query = context.HttpContext.Request.Query.FirstOrDefault(s => s.Key.ToLower().Equals(entityName.ToLower() + "id"));
                            resultObj = query.Value;
                        }
                    }
                    userName = context.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                    update = new { Operation = operation, Entity = entityName, ResultObject = resultObj };
                    new DispatchingHub().SendUpdates(update, clientMethodName, _dispatchingHub, userName);
                }
            }
           //NotifyOrderConfirmation(context);
        }

        private void NotifyOrderConfirmation(ActionExecutedContext context)
        {
            string requestedAction = context.HttpContext.Request.Path.ToUriComponent();

            var controllerType = context.Controller.GetType();
            var entityName = controllerType.Name.Substring(0, controllerType.Name.IndexOf("Controller"));

            var clientMethodName = _configuration["Notifications:OrderConfirmationClientMethod"];
            var mvmtGroup = _configuration["Notifications:MovementControllerGroup"];
            var callCenterGroup = _configuration["Notifications:CallCenterGroup"];

            if (entityName.Equals("OrderConfirmationHistory")
                && requestedAction.ToLower().Contains("add"))
            {
                if (context.Result is OkObjectResult result)
                {
                    string creatorRole = result.Value != null ? (result.Value as dynamic).CreatorRole : "";
                    if (creatorRole.Equals("CallCenter"))
                    {
                        var update = new { Operation = "Add", Entity = entityName, ResultObject = result.Value };

                        new DispatchingHub().SendOrderConfirmationUpdate(update, mvmtGroup, clientMethodName, _dispatchingHub);
                    }
                    else if (creatorRole.Equals("MovementController"))
                    {
                        var update = new { Operation = "Add", Entity = entityName, ResultObject = result.Value };

                        new DispatchingHub().SendOrderConfirmationUpdate(update, callCenterGroup, clientMethodName, _dispatchingHub);
                    }
                }
            }
            else if (entityName.Equals("WorkOrder") && requestedAction.ToLower().Contains("add"))
            {
                if (context.Result is OkObjectResult result)
                {
                    var update = new { Operation = "Add", Entity = entityName, ResultObject = result.Value };

                    new DispatchingHub().SendOrderConfirmationUpdate(update, mvmtGroup, clientMethodName, _dispatchingHub);
                }
            }
        }


        public void OnActionExecuting(ActionExecutingContext context)
        {
        }
    }
}

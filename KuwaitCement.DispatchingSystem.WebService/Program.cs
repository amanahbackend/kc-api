﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using DispatchingSystem.Models;
using Utilities;
using KuwaitCement.DispatchingSystem.WebService.Seed;
using DispatchingSystem.BLL.IManagers;

namespace KuwaitCement.DispatchingSystem.WebService
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args)
                .MigrateDbContext<DataContext>(async (context, services) =>
                {
                    var logger = services.GetService<ILogger<DataContextSeed>>();
                    ILKP_ConfirmationStatusManager confirmationStatusManager = services.GetService<ILKP_ConfirmationStatusManager>();
                    ILKP_PriorityManager priorityManager = services.GetService<ILKP_PriorityManager>();
                    ILKP_StatusManager statusManager = services.GetService<ILKP_StatusManager>();
                    ILKPConditionManager conditionManager = services.GetService<ILKPConditionManager>();
                    ICustomerTypeManager customerTypeManager = services.GetService<ICustomerTypeManager>();
                    IDriverManager driverManager = services.GetService<IDriverManager>();
                    IVehicleManager vehicleManager = services.GetService<IVehicleManager>();
                    IApplicationRoleManager roleManager = services.GetService<IApplicationRoleManager>();
                    IApplicationUserManager userManager = services.GetService<IApplicationUserManager>();
                    IFactoryManager factoryManager = services.GetService<IFactoryManager>();
                    ILKP_CustomerCategoryManager customerCategoryManager = services.GetService<ILKP_CustomerCategoryManager>();

                    await new DataContextSeed(confirmationStatusManager, priorityManager,
                        statusManager, conditionManager, customerTypeManager,
                        driverManager, vehicleManager, roleManager, userManager, factoryManager, customerCategoryManager)
                        .SeedAsync(context, logger);
                })
                .Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseSetting("detailedErrors", "true")
                .CaptureStartupErrors(true)
                .Build();
    }
}

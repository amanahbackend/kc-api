﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService
{
    public class SignalRConnection
    {
        public string ConnectionId { get; set; }
        public string UserId { get; set; }
        public List<string> Roles { get; set; }
        public string Token { get; set; }
    }
}

﻿using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService
{
    public class DispatchingHub : Hub
    {
        private static Dictionary<string, string> _connectedClients = new Dictionary<string, string>();

        public Task SendLateJob(Job job, string[] groupNames, string clientMethodName, IHubContext<DispatchingHub> dispatchingHub)
        {
            foreach (var item in groupNames)
            {
                if (dispatchingHub.Clients != null)
                {
                    return dispatchingHub.Clients.Group(item).SendAsync(clientMethodName, job);
                }
            }
            return null;
        }

        public Task SendUpcomingWorkOrders(List<string> upcomingOrders, string clientMethodName, IHubContext<DispatchingHub> dispatchingHub)
        {
            if (dispatchingHub.Clients != null)
            {
                return dispatchingHub.Clients.All.SendAsync(clientMethodName, upcomingOrders);
            }
            return null;
        }

        public Task SendLiveJobLocation(VehicleTracker tracker, string clientMethodName, IHubContext<DispatchingHub> dispatchingHub)
        {
            if (dispatchingHub.Clients != null)
            {
                return dispatchingHub.Clients.All.SendAsync(clientMethodName, tracker);
            }
            return null;
        }

        public Task SendUpdates(object updatesObj, string clientMethodName, IHubContext<DispatchingHub> dispatchingHub, string userName)
        {
            if (dispatchingHub.Clients != null)
            {
                if (_connectedClients.ContainsKey(userName))
                {
                    var connectionId = _connectedClients[userName];
                    var excludedClients = new List<string>() { connectionId };
                    return dispatchingHub.Clients.AllExcept(excludedClients).SendAsync(clientMethodName, updatesObj);
                }
                else
                {
                    return dispatchingHub.Clients.All.SendAsync(clientMethodName, updatesObj);
                }
            }
            return null;
        }

        public Task SendOrderConfirmationUpdate(object confirmHistoryObj, string groupName, string clientMethodeName, IHubContext<DispatchingHub> dispatchingHub)
        {
            if (dispatchingHub.Clients != null)
            {
                return dispatchingHub.Clients.Group(groupName).SendAsync(clientMethodeName, confirmHistoryObj);
            }
            return null;
        }

        public Task PublishReport(string[] groupNames, string reportName)
        {
            foreach (var item in groupNames)
            {
                return Clients.Group(item).SendAsync("OnReportPublished", $"{Context.ConnectionId}@{item}: {reportName}");
            }
            return null;
        }

        public async Task JoinGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }

        public async Task LeaveGroup(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }









        public override Task OnConnectedAsync()
        {
            var username = Context.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if (!_connectedClients.ContainsKey(username))
            {
                _connectedClients.Add(username, Context.ConnectionId);
            }
            return base.OnConnectedAsync();
        }

    }
}

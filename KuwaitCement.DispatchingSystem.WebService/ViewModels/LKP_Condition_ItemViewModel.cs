﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class LKP_Condition_ItemViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public int FK_Item_Id { get; set; }
        public ItemViewModel Item { get; set; }
        public int FK_Condition_Id { get; set; }
        public LKPConditionViewModel Condition { get; set; }
        public string Value { get; set; }
    }

    public class LKP_Condition_ItemMapper : MapperBase<LKP_Condition_Item, LKP_Condition_ItemViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public BaseEntityMapper _baseEntityMapper = new BaseEntityMapper();
        public override Expression<Func<LKP_Condition_Item, LKP_Condition_ItemViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<LKP_Condition_Item, LKP_Condition_ItemViewModel>>)(p => new LKP_Condition_ItemViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    FK_Item_Id = p.FK_Item_Id,
                    FK_Condition_Id = p.FK_Condition_Id,
                    Value = p.Value
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(LKP_Condition_ItemViewModel dto, LKP_Condition_Item model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.FK_Item_Id = dto.FK_Item_Id;
            model.FK_Condition_Id = dto.FK_Condition_Id;
            model.Value = dto.Value;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}

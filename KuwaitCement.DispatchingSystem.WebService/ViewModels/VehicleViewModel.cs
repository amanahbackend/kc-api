﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class VehicleViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Number { get; set; }
        public bool IsAvailable { get; set; }
        public int? FK_Driver_Id { get; set; }
        public int? FK_Factory_Id { get; set; }
        public FactoryViewModel Factory { get; set; }
        public DriverViewModel Driver { get; set; }
        public JobViewModel Job { get; set; }
        public WashingStatus Fk_WashingStatus_Id { get; set; }
        public LKP_WashingStatusViewModel WashingStatus { get; set; }
        public string Description { get; set; }

    }

    public class VehicleMapper : MapperBase<Vehicle, VehicleViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        private BaseEntityMapper _baseEntityMapper = new BaseEntityMapper();
        public override Expression<Func<Vehicle, VehicleViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Vehicle, VehicleViewModel>>)(p => new VehicleViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Number = p.Number,
                    Description = p.Description,
                    IsAvailable = p.IsAvailable,
                    FK_Driver_Id = p.FK_Driver_Id,
                    FK_Factory_Id = p.FK_Factory_Id
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(VehicleViewModel dto, Vehicle model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Description = dto.Description;
            model.Number = dto.Number;
            model.IsAvailable = dto.IsAvailable;
            model.FK_Driver_Id = dto.FK_Driver_Id;
            model.FK_Factory_Id = dto.FK_Factory_Id;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}

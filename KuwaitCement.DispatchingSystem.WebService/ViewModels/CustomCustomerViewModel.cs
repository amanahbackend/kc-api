﻿using System;
using System.Linq.Expressions;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class CustomCustomerViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public CustomerViewModel Customer { get; set; }
        public WorkOrderFactoriesViewModel WorkOrderFactories { get; set; }
        public ContractViewModel Contract { get; set; }
        public LocationViewModel Location{ get; set; }
    }

    public class CustomCustomerViewModelMapper : MapperBase<CustomCustomer, CustomCustomerViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<CustomCustomer, CustomCustomerViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<CustomCustomer, CustomCustomerViewModel>>)(p => new CustomCustomerViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 

                }));
            }
        }

        public override void MapToModel(CustomCustomerViewModel dto, CustomCustomer model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class CustomerViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Name { get; set; }
        public string CivilId { get; set; }
        public string Remarks { get; set; }
        public string Messenger { get; set; }
        public double Debit { get; set; }


        public string CompanyName { get; set; }
        public string Division { get; set; }
        public double CreditLimit { get; set; }
        public double Balance { get; set; }
        //public double AmountDue { get; set; }
        public double IncreaseAmount { get; set; }

        public int FK_CustomerType_Id { get; set; }
        public CustomerTypeViewModel CustomerType { get; set; }
        public List<string> Phones { get; set; }
        public string KCRM_Customer_Id { get; set; }
        public int? FK_CustomerCategory_Id { get; set; }
        public CustomerCategoryViewModel CustomerCategory { get; set; }
    }

    public class CustomerMapper : MapperBase<Customer, CustomerViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        private ComplainMapper _complainMapper = new ComplainMapper();
        private ContractMapper _contractMapper = new ContractMapper();

        public override Expression<Func<Customer, CustomerViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Customer, CustomerViewModel>>)(p => new CustomerViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Name = p.Name,
                    CivilId = p.CivilId,
                    Messenger = p.Messenger,
                    Remarks = p.Remarks,
                    CompanyName = p.CompanyName,
                    Division = p.Division,
                    FK_CustomerType_Id = p.FK_CustomerType_Id                  
                }));
            }
        }

        public override void MapToModel(CustomerViewModel dto, Customer model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Name = dto.Name;
            model.CivilId = dto.CivilId;
            model.Remarks = dto.Remarks;
            model.Messenger = dto.Messenger;
            model.CompanyName = dto.CompanyName;
            model.Division = dto.Division;
            model.FK_CustomerType_Id = dto.FK_CustomerType_Id;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class FactoryViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Name { get; set; }


        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string AddressNote { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public IEnumerable<VehicleViewModel> Vehicles { get; set; }
        public IEnumerable<WorkOrderViewModel> WorkOrders { get; set; }
    }

    public class FactoryMapper : MapperBase<Factory, FactoryViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        private VehicleMapper _vehicleMapper = new VehicleMapper();
        private WorkOrderMapper _workOrderMapper = new WorkOrderMapper();
        private BaseEntityMapper _baseEntityViewModelMapper = new BaseEntityMapper();
        public override Expression<Func<Factory, FactoryViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Factory, FactoryViewModel>>)(p => new FactoryViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Name = p.Name,
                    Governorate = p.Governorate,
                    Area = p.Area,
                    Block = p.Block,
                    Street = p.Street,
                    AddressNote = p.AddressNote,
                    Latitude = p.Latitude,
                    Longitude = p.Longitude,
                    Vehicles = p.Vehicles.AsQueryable().Select(this._vehicleMapper.SelectorExpression),
                    ////WorkOrders = p.WorkOrders.AsQueryable().Select(this._workOrderMapper.SelectorExpression)
                })).MergeWith(this._baseEntityViewModelMapper.SelectorExpression);
            }
        }

        public override void MapToModel(FactoryViewModel dto, Factory model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Name = dto.Name;
            model.Governorate = dto.Governorate;
            model.Area = dto.Area;
            model.Block = dto.Block;
            model.Street = dto.Street;
            model.AddressNote = dto.AddressNote;
            model.Latitude = dto.Latitude;
            model.Longitude = dto.Longitude;
            this._baseEntityViewModelMapper.MapToModel(dto, model);
        }
    }
}

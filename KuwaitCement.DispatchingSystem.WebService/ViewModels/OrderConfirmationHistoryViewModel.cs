﻿using DispatchingSystem.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels

{
    public class OrderConfirmationHistoryViewModel : BaseEntityViewModel
    {        
        public int Id { get; set; }

        public DateTime? OrderDate { get; set; }
        public DateTime? OrderAlternativeDate { get; set; }
        public string OrderNumber { get; set; }

        public string CreatorName { get; set; }
        public string CreatorRole { get; set; }

        public int ? Fk_ConfirmationStatus_Id { get; set; }
        public LKP_ConfirmationStatusViewModel ConfirmationStatus { get; set; }

        public int ?Fk_WorkOrder_Id { get; set; }
        public WorkOrderViewModel WorkOrder { get; set; }

        public ConfirmType ?ConfirmType { get; set; }
        public SelectedDate ?SelectedDate { get; set; }

        public Decline ?IsDeclined { get; set; }
        public string Reason { get; set; }
        public string Description { get; set; }

        public int? FactoryId { get; set; }


    }

}

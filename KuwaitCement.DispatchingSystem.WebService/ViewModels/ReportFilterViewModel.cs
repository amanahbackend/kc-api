﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;
using Utilities.Paging;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class ReportFilterViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Type { get; set; }
        public object Value { get; set; }
        public PaginatedItems pagingParameterModel { get; set; }
    }

    
}

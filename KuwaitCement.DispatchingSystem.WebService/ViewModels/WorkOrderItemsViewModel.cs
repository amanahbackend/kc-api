﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;


namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class WorkOrderItemsViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public int FK_Item_Id { get; set; }
        public ItemViewModel Item { get; set; }
        public double Price { get; set; }

        public int FK_Workorder_Id { get; set; }
        public WorkOrderViewModel WorkOrder { get; set; }
    }

    public class WorkOrderItemsViewModelMapper : MapperBase<WorkOrderItems, WorkOrderItemsViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public BaseEntityMapper _baseEntityMapper = new BaseEntityMapper();
        public override Expression<Func<WorkOrderItems, WorkOrderItemsViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<WorkOrderItems, WorkOrderItemsViewModel>>)(p => new WorkOrderItemsViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    FK_Item_Id = p.FK_Item_Id,
                    FK_Workorder_Id = p.FK_Workorder_Id,
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(WorkOrderItemsViewModel dto, WorkOrderItems model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.FK_Item_Id = dto.FK_Item_Id;
            model.FK_Workorder_Id = dto.FK_Workorder_Id;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class BaseEntityViewModel : IBaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public string FK_CreatedBy_Id { get; set; }
        public string FK_UpdatedBy_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

    public class BaseEntityMapper : MapperBase<BaseEntity, BaseEntityViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<BaseEntity, BaseEntityViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<BaseEntity, BaseEntityViewModel>>)(p => new BaseEntityViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    FK_CreatedBy_Id = p.FK_CreatedBy_Id,
                    FK_UpdatedBy_Id = p.FK_UpdatedBy_Id,
                    IsDeleted = p.IsDeleted,
                    CreatedDate = p.CreatedDate,
                    UpdatedDate = p.UpdatedDate
                }));
            }
        }

        public override void MapToModel(BaseEntityViewModel dto, BaseEntity model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.FK_CreatedBy_Id = dto.FK_CreatedBy_Id;
            model.FK_UpdatedBy_Id = dto.FK_UpdatedBy_Id;
            model.IsDeleted = dto.IsDeleted;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedDate = dto.UpdatedDate;
        }
    }
}

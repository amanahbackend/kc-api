﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class RolePrivilgeViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public ApplicationRoleViewModel ApplicationRole {get;set;}
        public string FK_ApplicationRole_Id { get; set; }
        public int FK_Privilge_Id { get; set; }
        public PrivilgeViewModel Privilge { get; set; }
    }

    public class RolePrivilgeMapper : MapperBase<RolePrivilge, RolePrivilgeViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public BaseEntityMapper _baseEntityMapper = new BaseEntityMapper();
        public override Expression<Func<RolePrivilge, RolePrivilgeViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<RolePrivilge, RolePrivilgeViewModel>>)(p => new RolePrivilgeViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    FK_ApplicationRole_Id = p.FK_ApplicationRole_Id,
                    FK_Privilge_Id = p.FK_Privilge_Id,
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(RolePrivilgeViewModel dto, RolePrivilge model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.FK_ApplicationRole_Id = dto.FK_ApplicationRole_Id;
            model.FK_Privilge_Id = dto.FK_Privilge_Id;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class ContractTypeViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ContractTypeMapper : MapperBase<ContractType, ContractTypeViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public BaseEntityMapper _baseEntityMapper = new BaseEntityMapper();
        public override Expression<Func<ContractType, ContractTypeViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<ContractType, ContractTypeViewModel>>)(p => new ContractTypeViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Name = p.Name
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(ContractTypeViewModel dto, ContractType model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Name = dto.Name;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}

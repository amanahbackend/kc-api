﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class ItemViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Formula { get; set; }
        public int AvailableAmount { get; set; }
        public double Price { get; set; }

        public IEnumerable<WorkOrderViewModel> WorkOrders { get; set; }
        public ICollection<ContractItems> ContractItems { get; set; }

    }

    public class ItemMapper : MapperBase<Item, ItemViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public BaseEntityMapper _baseEntityMapper = new BaseEntityMapper();
        public override Expression<Func<Item, ItemViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Item, ItemViewModel>>)(p => new ItemViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Code = p.Code,
                    Name = p.Name,
                    Description = p.Description,
                    Formula = p.Formula,
                    AvailableAmount = p.AvailableAmount
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(ItemViewModel dto, Item model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Code = dto.Code;
            model.Name = dto.Name;
            model.Description = dto.Description;
            model.Formula = dto.Formula;
            model.AvailableAmount = dto.AvailableAmount;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}

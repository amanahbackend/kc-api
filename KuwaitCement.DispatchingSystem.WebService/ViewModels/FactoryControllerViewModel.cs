﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class FactoryControllerViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public int Fk_Factory_Id { get; set; }
        public string Fk_Controller_Id { get; set; }
        public FactoryViewModel Factory { get; set; }
        public ApplicationUserViewModel Controller { get; set; }
    }

   
    
}

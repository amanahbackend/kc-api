﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;
using DispatchingSystem.Models.Enums;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class WorkOrderViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string SelectedLang { get; set; }

        public string Code { get; set; }
        public double Percentage { get; set; }
        public double PendingAmount { get; set; }
        public double Amount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool HasSuborders { get; set; }
        public IEnumerable<WorkOrderViewModel> WorkOrders { get; set; }
        public int FK_Location_Id { get; set; }
        public LocationViewModel Location { get; set; }
        public int FK_Customer_Id { get; set; }
        public CustomerViewModel Customer { get; set; }
        public int FK_Contract_Id { get; set; }
        public ContractViewModel Contract { get; set; }
        public int FK_Priority_Id { get; set; }
        public LKP_PriorityViewModel Priority { get; set; }
        public int FK_Status_Id { get; set; }
        public LKP_StatusViewModel Status { get; set; }
        public int? FK_Parent_WorkOrder_Id { get; set; }
        public WorkOrderViewModel ParentWorkOrder { get; set; }
        public string Description { get; set; }

        public int? FK_Factory_Id { get; set; }
        public FactoryViewModel Factory { get; set; }
        public string FactoryID { get; set; }


        public int? FK_Item { get; set; }

        public ItemViewModel Items { get; set; }
        public IEnumerable<JobViewModel> Jobs { get; set; }
        public IEnumerable<EquipmentViewModel> Equipment { get; set; }
        public List<OrderConfirmationHistoryViewModel> ConfirmationHistories { get; set; }
        public bool CanConfirm { get; set; }
        public SelectedDate SelectedDate { get; set; }

        public int FK_ConfirmationStatus_Id { get; set; }
        public LKP_ConfirmationStatus confirmationStatus { get; set; }
        public int? FK_ElementToBePoured_Id { get; set; }
        public ElementToBePoured ElementToBePoured { get; set; }

        public DateTime PouringTime { get; set; }

        public Decline IsDeclined { get; set; }

        public string Reason { get; set; }



    }

    public class WorkOrderMapper : MapperBase<WorkOrder, WorkOrderViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        private WorkOrderMapper _workOrderMapper = new WorkOrderMapper();
        private JobMapper _jobMapper = new JobMapper();
        public override Expression<Func<WorkOrder, WorkOrderViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<WorkOrder, WorkOrderViewModel>>)(p => new WorkOrderViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Code = p.Code,
                    PendingAmount = p.PendingAmount,
                    Amount = p.Amount,
                    StartDate = p.StartDate,
                    EndDate = p.EndDate,
                    SelectedDate = p.SelectedDate,
                    HasSuborders = p.HasSuborders,
                    WorkOrders = p.WorkOrders.AsQueryable().Select(this._workOrderMapper.SelectorExpression),
                    FK_Location_Id = p.FK_Location_Id,
                    FK_Customer_Id = p.FK_Customer_Id,
                    FK_Contract_Id = p.FK_Contract_Id,
                    FK_Priority_Id = p.FK_Priority_Id,
                    FK_Status_Id = p.FK_Status_Id,
                    IsDeclined = p.IsDeclined,
                    SelectedLang=p.SelectedLang,
                    Jobs = p.Jobs.AsQueryable().Select(this._jobMapper.SelectorExpression)
                })); ;
            }
        }

        public override void MapToModel(WorkOrderViewModel dto, WorkOrder model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Code = dto.Code;
            model.PendingAmount = dto.PendingAmount;
            model.Amount = dto.Amount;
            model.StartDate = dto.StartDate;
            model.HasSuborders = dto.HasSuborders;
            model.FK_Location_Id = dto.FK_Location_Id;
            model.FK_Customer_Id = dto.FK_Customer_Id;
            model.FK_Contract_Id = dto.FK_Contract_Id;
            model.FK_Priority_Id = dto.FK_Priority_Id;
            model.FK_Status_Id = dto.FK_Status_Id;
            model.SelectedDate = dto.SelectedDate;
            model.IsDeclined = dto.IsDeclined;
            model.SelectedLang = dto.SelectedLang;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class UpcomingWorkOrderViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string WorkOrderCode { get; set; }
    }

    public class UpcomingWorkOrderViewModelMapper : MapperBase<UpcomingWorkOrder, UpcomingWorkOrderViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public override Expression<Func<UpcomingWorkOrder, UpcomingWorkOrderViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<UpcomingWorkOrder, UpcomingWorkOrderViewModel>>)(p => new UpcomingWorkOrderViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    WorkOrderCode = p.WorkOrderCode
                }));
            }
        }

        public override void MapToModel(UpcomingWorkOrderViewModel dto, UpcomingWorkOrder model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.WorkOrderCode = dto.WorkOrderCode;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class ComplainViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Note { get; set; }
        public int FK_Customer_Id { get; set; }
        public CustomerViewModel Customer { get; set; }
        
    }

    public class ComplainMapper : MapperBase<Complain, ComplainViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public BaseEntityMapper _baseEntityMapper = new BaseEntityMapper();
        public override Expression<Func<Complain, ComplainViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Complain, ComplainViewModel>>)(p => new ComplainViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Note = p.Note,
                    FK_Customer_Id = p.FK_Customer_Id,
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(ComplainViewModel dto, Complain model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Note = dto.Note;
            model.FK_Customer_Id = dto.FK_Customer_Id;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}

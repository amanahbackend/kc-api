﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class CustomerHistoryViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        public IEnumerable<WorkOrderFactoriesViewModel> WorkOrders { get; set; }
        ////ECC/ END CUSTOM CODE SECTION 
        public CustomerViewModel Customer { get; set; }
        public IEnumerable<ComplainViewModel> Complains { get; set; }
        public IEnumerable<ContractViewModel> Contracts { get; set; }


    }

    public class CustomerHistoryViewModelMapper : MapperBase<Customer, CustomerHistoryViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public ComplainMapper _complainMapper = new ComplainMapper();
        public ContractMapper _contractMapper = new ContractMapper();
        public BaseEntityMapper _baseEntityViewModelMapper = new BaseEntityMapper();
        public override Expression<Func<Customer, CustomerHistoryViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Customer, CustomerHistoryViewModel>>)(p => new CustomerHistoryViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    
                })).MergeWith(this._baseEntityViewModelMapper.SelectorExpression);
            }
        }

        public override void MapToModel(CustomerHistoryViewModel dto, Customer model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            this._baseEntityViewModelMapper.MapToModel(dto, model);
        }
    }
}

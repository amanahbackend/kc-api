﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class JobViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public int FK_Status_Id { get; set; }
        public LKP_StatusViewModel Status { get; set; }
        public int FK_Vehicle_Id { get; set; }
        public VehicleViewModel Vehicle { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Amount { get; set; }
        public int FK_WorkOrder_ID { get; set; }
        public WorkOrderViewModel WorkOrder { get; set; }
        public string VehicleNumber { get; set; }
        public string DeliveryNote { get; set; }
        public string CancellationReason { get; set; }

        public bool IsDamaged { get; set; }

        public string DiscriptionOFDamage { get; set; }


    }

    public class JobMapper : MapperBase<Job, JobViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public BaseEntityMapper _baseEntityMapper = new BaseEntityMapper();
        public override Expression<Func<Job, JobViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Job, JobViewModel>>)(p => new JobViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    FK_Status_Id = p.FK_Status_Id,
                    StartDate = p.StartDate,
                    EndDate = p.EndDate,
                    CancellationReason = p.CancellationReason,

                    Amount = p.Amount,
                    FK_WorkOrder_ID = p.FK_WorkOrder_ID,
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(JobViewModel dto, Job model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.FK_Status_Id = dto.FK_Status_Id;
            model.FK_Vehicle_Id = dto.FK_Vehicle_Id;
            model.StartDate = dto.StartDate;
            model.EndDate = dto.EndDate;
            model.Amount = dto.Amount;
           model.CancellationReason = dto.CancellationReason;

            model.FK_WorkOrder_ID = dto.FK_WorkOrder_ID;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}

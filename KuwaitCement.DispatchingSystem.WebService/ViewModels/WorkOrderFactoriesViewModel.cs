﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class WorkOrderFactoriesViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        //public int Id { get; set; }

        //public int FK_WorkOrder_Id { get; set; }

        public int CreditAmount { get; set; }

        public WorkOrderViewModel WorkOrder { get; set; }

        public IEnumerable<FactorySubOrderPercentageViewModel> SubOrdersFactoryPercent { get; set; }

        public bool CreatingOrderFirstTime { get; set; }
        //public DateTime EndDate { get; set; }

    }

    public class SubOrderFactoriesMapper : MapperBase<WorkOrderFactories, WorkOrderFactoriesViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        private BaseEntityMapper _baseEntityMapper = new BaseEntityMapper();
        public override Expression<Func<WorkOrderFactories, WorkOrderFactoriesViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<WorkOrderFactories, WorkOrderFactoriesViewModel>>)(p => new WorkOrderFactoriesViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 

                    ////ECC/ END CUSTOM CODE SECTION 
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(WorkOrderFactoriesViewModel dto, WorkOrderFactories model)
        {
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class WorkOrderEquipmentViewModel
    {
        public WorkOrderViewModel WorkOrder { get; set; }
        public List<EquipmentViewModel> Equipment { get; set; }
    }
}

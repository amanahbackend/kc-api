﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class CustomerTypeViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class CustomerTypeMapper : MapperBase<CustomerType, CustomerTypeViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public BaseEntityMapper _baseEntityMapper = new BaseEntityMapper();
        public override Expression<Func<CustomerType, CustomerTypeViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<CustomerType, CustomerTypeViewModel>>)(p => new CustomerTypeViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Name = p.Name
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(CustomerTypeViewModel dto, CustomerType model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Name = dto.Name;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}

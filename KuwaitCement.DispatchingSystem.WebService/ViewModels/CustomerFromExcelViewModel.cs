﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class CustomerFromExcelViewModel
    {
        public string Name { get; set; }
        public string KCRM_Customer_Id { get; set; }
        public double CreditLimit { get; set; }
        public string CustomerCategory { get; set; }
        public string CustomerPhones { get; set; }
        public string CustomerType { get; set; }
    }
}

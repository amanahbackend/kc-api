﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class CustomerCategoryViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

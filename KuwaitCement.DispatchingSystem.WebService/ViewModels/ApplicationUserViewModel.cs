﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;
using System.ComponentModel.DataAnnotations;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class ApplicationUserViewModel
    {

        ////BCC/ BEGIN CUSTOM CODE SECTION 
        public string PhoneNumber { get; set; }

        public string Id { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        ////ECC/ END CUSTOM CODE SECTION 
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FK_CreatedBy_Id { get; set; }
        public string FK_UpdatedBy_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public ApplicationUser CurrentUser { get; set; }
        public string[] RoleNames { get; set; }
    }

    public class ApplicationUserMapper : MapperBase<ApplicationUser, ApplicationUserViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 

        public override Expression<Func<ApplicationUser, ApplicationUserViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<ApplicationUser, ApplicationUserViewModel>>)(p => new ApplicationUserViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    FirstName = p.FirstName,
                    LastName = p.LastName,
                    FK_CreatedBy_Id = p.FK_CreatedBy_Id,
                    FK_UpdatedBy_Id = p.FK_UpdatedBy_Id,
                    IsDeleted = p.IsDeleted,
                    CreatedDate = p.CreatedDate,
                    UpdatedDate = p.UpdatedDate,
                    CurrentUser = p.CurrentUser,
                    RoleNames = p.RoleNames
                }));
            }
        }

        public override void MapToModel(ApplicationUserViewModel dto, ApplicationUser model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.FirstName = dto.FirstName;
            model.LastName = dto.LastName;
            model.FK_CreatedBy_Id = dto.FK_CreatedBy_Id;
            model.FK_UpdatedBy_Id = dto.FK_UpdatedBy_Id;
            model.IsDeleted = dto.IsDeleted;
            model.CreatedDate = dto.CreatedDate;
            model.UpdatedDate = dto.UpdatedDate;
            model.CurrentUser = dto.CurrentUser;
            model.RoleNames = dto.RoleNames;
        }
    }
}

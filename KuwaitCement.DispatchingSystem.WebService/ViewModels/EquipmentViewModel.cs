﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class EquipmentViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
        public int FK_EquipmentType_Id { get; set; }
        public int? FK_WorkOrder_Id { get; set; }
        public int? Fk_Factory_Id { get; set; }
        public bool IsAvailable { get; set; }
        public WorkOrderViewModel WorkOrder { get; set; }
        public EquipmentTypeViewModel EquipmentType { get; set; }
        public Factory Factory { get; set; }
    }

    public class EquipmentMapper : MapperBase<Equipment, EquipmentViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 

        public override Expression<Func<Equipment, EquipmentViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Equipment, EquipmentViewModel>>)(p => new EquipmentViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Number = p.Number,
                    Name = p.Name,
                    FK_EquipmentType_Id = p.FK_EquipmentType_Id,
                    FK_WorkOrder_Id = p.FK_WorkOrder_Id,
                    IsAvailable = p.IsAvailable
                }));
            }
        }

        public override void MapToModel(EquipmentViewModel dto, Equipment model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Number = dto.Number;
            model.Name = dto.Name;
            model.FK_EquipmentType_Id = dto.FK_EquipmentType_Id;
            model.FK_WorkOrder_Id = dto.FK_WorkOrder_Id;
            model.IsAvailable = dto.IsAvailable;
        }
    }
}

﻿namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class ContractItemFromExcelViewModel
    {
        public string ContractNumber { get; set; }
        public string ItemCode { get; set; }
        public double Amount { get; set; }
        public double AvailableAmount { get; set; }
        public double Price { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Formula { get; set; }
        public double ItemAvailableAmount { get; set; }
    }
}

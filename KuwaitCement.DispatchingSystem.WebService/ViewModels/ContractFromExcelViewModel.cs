﻿using System;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class ContractFromExcelViewModel
    {
        public string ContractNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Amount { get; set; }
        public double PendingAmount { get; set; }
        public double Price { get; set; }
        public string Remarks { get; set; }
        public string CustomerNumber { get; set; }
        public string ContractType { get; set; }
    }
}

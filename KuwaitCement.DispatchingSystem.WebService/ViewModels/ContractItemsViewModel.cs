﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class ContractItemsViewModel
    {
        public int Id { get; set; }
        public int Fk_Item_Id { get; set; }
        public ItemViewModel Item { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public bool IsDisabled { get; set; }
        public DateTime? DisableDate { get; set; }
        public string Description { get; set; }
    }
}

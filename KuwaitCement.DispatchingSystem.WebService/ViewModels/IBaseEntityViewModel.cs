﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public interface IBaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
         string FK_CreatedBy_Id { get; set; }
         string FK_UpdatedBy_Id { get; set; }
         bool IsDeleted { get; set; }
         DateTime CreatedDate { get; set; }
         DateTime UpdatedDate { get; set; }
    }

   
}

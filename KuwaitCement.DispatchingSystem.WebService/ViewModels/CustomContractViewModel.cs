﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class CustomContractViewModel
    {
        public ContractViewModel Contract { get; set; }
        public List<LocationViewModel> Locations { get; set; }
    }
}

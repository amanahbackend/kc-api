﻿using Utilites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class WashingAreaViewModel : BaseEntityViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int? Fk_Factory_Id { get; set; }
        public FactoryViewModel Factory { get; set; }

        public List<Point> Polygon { get; set; }
    }
}

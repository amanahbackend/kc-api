﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class ContractViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string ContractNumber { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        //public double Amount { get; set; }
        //public double PendingAmount { get; set; }
        //public double Price { get; set; }
        public string Remarks { get; set; }
        public int FK_Customer_Id { get; set; }
        public CustomerViewModel Customer { get; set; }
        public int FK_ContractType_Id { get; set; }
        public ContractTypeViewModel ContractType { get; set; }
        public string File { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public IEnumerable<ContractItemsViewModel> Items { get; set; }
    }

    public class ContractMapper : MapperBase<Contract, ContractViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 

        public override Expression<Func<Contract, ContractViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Contract, ContractViewModel>>)(p => new ContractViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    ContractNumber = p.ContractNumber,
                    StartDate = p.StartDate,
                    EndDate = p.EndDate,
                    Remarks = p.Remarks,
                    FK_Customer_Id = p.FK_Customer_Id,
                    FK_ContractType_Id = p.FK_ContractType_Id,
                    File = p.File,
                    FileName = p.FileName,
                    Path = p.Path
                }));
            }
        }

        public override void MapToModel(ContractViewModel dto, Contract model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.ContractNumber = dto.ContractNumber;
            model.StartDate = dto.StartDate;
            model.EndDate = dto.EndDate;
            model.Remarks = dto.Remarks;
            model.FK_Customer_Id = dto.FK_Customer_Id;
            model.FK_ContractType_Id = dto.FK_ContractType_Id;
            model.File = dto.File;
            model.FileName = dto.FileName;
            model.Path = dto.Path;
        }
    }
}

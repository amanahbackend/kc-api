﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using KuwaitCement.DispatchingSystem.WebService.ViewModels.Infrastructure;
using DispatchingSystem.Models.Entities;

namespace KuwaitCement.DispatchingSystem.WebService.ViewModels
{
    public class DriverViewModel : BaseEntityViewModel
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public bool IsAvailable { get; set; }
    }

    public class DriverMapper : MapperBase<Driver, DriverViewModel>
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public BaseEntityMapper _baseEntityMapper = new BaseEntityMapper();
        public override Expression<Func<Driver, DriverViewModel>> SelectorExpression
        {
            get
            {
                return ((Expression<Func<Driver, DriverViewModel>>)(p => new DriverViewModel()
                {
                    ////BCC/ BEGIN CUSTOM CODE SECTION 
                    ////ECC/ END CUSTOM CODE SECTION 
                    Id = p.Id,
                    Name = p.Name,
                    Phone = p.Phone,
                    IsAvailable = p.IsAvailable
                })).MergeWith(this._baseEntityMapper.SelectorExpression);
            }
        }

        public override void MapToModel(DriverViewModel dto, Driver model)
        {
            ////BCC/ BEGIN CUSTOM CODE SECTION 
            ////ECC/ END CUSTOM CODE SECTION 
            model.Id = dto.Id;
            model.Name = dto.Name;
            model.Phone = dto.Phone;
            model.IsAvailable = dto.IsAvailable;
            this._baseEntityMapper.MapToModel(dto, model);
        }
    }
}

﻿using AutoMapper;
using KuwaitCement.DispatchingSystem.WebService.ViewModels;
using DispatchingSystem.Models.Entities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<Complain, ComplainViewModel>()
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer));
            CreateMap<ComplainViewModel, Complain>()
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<FactoryController, FactoryControllerViewModel>()
                .ForMember(dest => dest.Factory, opt => opt.MapFrom(src => src.Factory))
                .ForMember(dest => dest.Controller, opt => opt.MapFrom(src => src.Controller));
            CreateMap<FactoryControllerViewModel, FactoryController>()
                .ForMember(dest => dest.Factory, opt => opt.MapFrom(src => src.Factory))
                .ForMember(dest => dest.Controller, opt => opt.MapFrom(src => src.Controller))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<ApplicationRoleViewModel, ApplicationRole>()
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedName, opt => opt.Ignore())
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore());
            CreateMap<ApplicationRole, ApplicationRoleViewModel>();

            CreateMap<ReportFilter, ReportFilterViewModel>();
            CreateMap<ReportFilterViewModel, ReportFilter>()
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());


            CreateMap<WashingArea, WashingAreaViewModel>()
                .ForMember(dest => dest.Factory, opt => opt.MapFrom(src => src.Factory))
                .ForMember(dest => dest.Polygon, opt => opt.MapFrom(src => src.Polygon));

            CreateMap<WashingAreaViewModel, WashingArea>()
                .ForMember(dest => dest.Factory, opt => opt.MapFrom(src => src.Factory))
                .ForMember(dest => dest.Polygon, opt => opt.MapFrom(src => src.Polygon))
                .ForMember(dest => dest.Geometry, opt => opt.Ignore())
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<LKP_WashingStatus, LKP_WashingStatusViewModel>();
            CreateMap<LKP_WashingStatusViewModel, LKP_WashingStatus>()
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<ApplicationUserViewModel, ApplicationUser>()
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());

            CreateMap<ApplicationUser, ApplicationUserViewModel>()
                .ForMember(dest => dest.Password, opt => opt.Ignore());

            CreateMap<ContractType, ContractTypeViewModel>();
            CreateMap<ContractTypeViewModel, ContractType>()
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<ContractItems, ContractItemsViewModel>();
            CreateMap<ContractItemsViewModel, ContractItems>()
                .ForMember(dest => dest.Contract, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.FK_Contract_Id, opt => opt.Ignore())
                .ForMember(dest => dest.FK_CreatedBy_Id, opt => opt.Ignore())
                .ForMember(dest => dest.FK_UpdatedBy_Id, opt => opt.Ignore())
                .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
                .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item))
                .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore());


            CreateMap<Contract, ContractViewModel>()
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer))
                .ForMember(dest => dest.Items, opt => opt.MapFrom(src => src.ContractItems));
            CreateMap<ContractViewModel, Contract>()
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer))
                .ForMember(dest => dest.ContractItems, opt => opt.MapFrom(src => src.Items))
                .ForMember(dest => dest.ContractType, opt => opt.MapFrom(src => src.ContractType))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<CustomerType, CustomerTypeViewModel>();
            CreateMap<CustomerTypeViewModel, CustomerType>()
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<CustomCustomer, CustomCustomerViewModel>()
                .ForMember(dest => dest.Contract, opt => opt.MapFrom(src => src.Contract)).MaxDepth(1)
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer)).MaxDepth(1)
                .ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Location)).MaxDepth(1)
                .ForMember(dest => dest.WorkOrderFactories, opt => opt.MapFrom(src => src.WorkOrderFactories)).MaxDepth(1);

            CreateMap<CustomCustomerViewModel, CustomCustomer>()
                .ForMember(dest => dest.Contract, opt => opt.MapFrom(src => src.Contract)).MaxDepth(1)
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer)).MaxDepth(1)
                .ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Location)).MaxDepth(1)
                .ForMember(dest => dest.WorkOrderFactories, opt => opt.MapFrom(src => src.WorkOrderFactories)).MaxDepth(1)
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.FK_CreatedBy_Id, opt => opt.Ignore())
                .ForMember(dest => dest.FK_UpdatedBy_Id, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.IsDeleted, opt => opt.Ignore());

            CreateMap<CustomContract, CustomContractViewModel>()
               .ForMember(dest => dest.Contract, opt => opt.MapFrom(src => src.Contract))
               .ForMember(dest => dest.Locations, opt => opt.MapFrom(src => src.Locations));

            CreateMap<CustomContractViewModel, CustomContract>()
                .ForMember(dest => dest.Contract, opt => opt.MapFrom(src => src.Contract))
                .ForMember(dest => dest.Locations, opt => opt.MapFrom(src => src.Locations))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.FK_CreatedBy_Id, opt => opt.Ignore())
                .ForMember(dest => dest.FK_UpdatedBy_Id, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.IsDeleted, opt => opt.Ignore());

            //CreateMap<CustomerPhone, string>()
            //   .ForMember(dest => dest, opt => opt.MapFrom(src => src.Phone));

            //CreateMap<string, CustomerPhone>()
            //   .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src));

            CreateMap<string, CustomerPhone>()
                .ConstructUsing(str => new CustomerPhone { Phone = str })
                .ForAllOtherMembers(o => o.Ignore());

            CreateMap<CustomerPhone, string>()
                .ConstructUsing(str => str.Phone);

            CreateMap<Customer, CustomerViewModel>()
                .ForMember(dest => dest.Phones, opt => opt.MapFrom(src => src.Phones));


            CreateMap<LKP_CustomerCategory, CustomerCategoryViewModel>();
            CreateMap<CustomerCategoryViewModel, LKP_CustomerCategory>()
                 .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());


            CreateMap<CustomerViewModel, Customer>()
                .ForMember(dest => dest.Phones, opt => opt.MapFrom(src => src.Phones))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.FK_CreatedBy_Id, opt => opt.Ignore())
                .ForMember(dest => dest.FK_UpdatedBy_Id, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.IsDeleted, opt => opt.Ignore());

            CreateMap<CustomerHistory, CustomerHistoryViewModel>()
                .ForMember(dest => dest.Complains, opt => opt.MapFrom(src => src.Complains))
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer))
                .ForMember(dest => dest.WorkOrders, opt => opt.MapFrom(src => src.WorkOrders));
            CreateMap<CustomerHistoryViewModel, CustomerHistory>()
                .ForMember(dest => dest.Complains, opt => opt.MapFrom(src => src.Complains))
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer))
                .ForMember(dest => dest.WorkOrders, opt => opt.MapFrom(src => src.WorkOrders))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());


            CreateMap<Driver, DriverViewModel>();
            CreateMap<DriverViewModel, Driver>()
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<EquipmentType, EquipmentTypeViewModel>();
            CreateMap<EquipmentTypeViewModel, EquipmentType>()
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<Equipment, EquipmentViewModel>()
                .ForMember(dest => dest.EquipmentType, opt => opt.MapFrom(src => src.EquipmentType))
                .ForMember(dest => dest.Factory, opt => opt.MapFrom(src => src.Factory))
                .ForMember(dest => dest.WorkOrder, opt => opt.MapFrom(src => src.WorkOrder));
            CreateMap<EquipmentViewModel, Equipment>()
                .ForMember(dest => dest.EquipmentType, opt => opt.MapFrom(src => src.EquipmentType))
                .ForMember(dest => dest.Factory, opt => opt.MapFrom(src => src.Factory))
                .ForMember(dest => dest.WorkOrder, opt => opt.MapFrom(src => src.WorkOrder))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<Factory, FactoryViewModel>()
                .ForMember(dest => dest.Vehicles, opt => opt.MapFrom(src => src.Vehicles))
                .ForMember(dest => dest.WorkOrders, opt => opt.MapFrom(src => src.WorkOrders));
            CreateMap<FactoryViewModel, Factory>()
                .ForMember(dest => dest.Vehicles, opt => opt.MapFrom(src => src.Vehicles))
                .ForMember(dest => dest.WorkOrders, opt => opt.MapFrom(src => src.WorkOrders))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());
            CreateMap<FactorySubOrderPercentage, FactorySubOrderPercentageViewModel>()
                .ForMember(dest => dest.Factory, opt => opt.MapFrom(src => src.Factory))
                .ForMember(dest => dest.Percentage, opt => opt.MapFrom(src => src.Percentage))
                .ForMember(dest => dest.WorkOrder, opt => opt.MapFrom(src => src.SubOrder))
                /*.ForMember(dest => dest.Amount, opt => opt.Ignore())*/;
            CreateMap<FactorySubOrderPercentageViewModel, FactorySubOrderPercentage>()
                .ForMember(dest => dest.Factory, opt => opt.MapFrom(src => src.Factory))
                .ForMember(dest => dest.Percentage, opt => opt.MapFrom(src => src.Percentage))
                .ForMember(dest => dest.SubOrder, opt => opt.MapFrom(src => src.WorkOrder))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.FK_SubOrder_Id, opt => opt.Ignore());

            CreateMap<Item, ItemViewModel>()
                 .ForMember(dest => dest.WorkOrders, opt => opt.MapFrom(src => src.WorkOrders))
                 .ForMember(dest => dest.ContractItems, opt => opt.Ignore());

            CreateMap<ItemViewModel, Item>()
                 .ForMember(dest => dest.WorkOrders, opt => opt.MapFrom(src => src.WorkOrders))
                 .ForMember(dest => dest.ContractItems, opt => opt.MapFrom(src => src.ContractItems))
                 .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<Job, JobViewModel>()
                 .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                 .ForMember(dest => dest.Vehicle, opt => opt.MapFrom(src => src.Vehicle))
                 .ForMember(dest => dest.WorkOrder, opt => opt.Ignore());
            CreateMap<JobViewModel, Job>()
                 .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                 .ForMember(dest => dest.Vehicle, opt => opt.MapFrom(src => src.Vehicle))
                 .ForMember(dest => dest.WorkOrder, opt => opt.Ignore())
                 .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<LKP_Condition_Item, LKP_Condition_ItemViewModel>()
                 .ForMember(dest => dest.Condition, opt => opt.MapFrom(src => src.Condition))
                 .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item));
            CreateMap<LKP_Condition_ItemViewModel, LKP_Condition_Item>()
                 .ForMember(dest => dest.Condition, opt => opt.MapFrom(src => src.Condition))
                 .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item))
                 .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<LKP_Priority, LKP_PriorityViewModel>();
            CreateMap<LKP_PriorityViewModel, LKP_Priority>()
                 .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<LKP_Status, LKP_StatusViewModel>();
            CreateMap<LKP_StatusViewModel, LKP_Status>()
                 .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<LKPCondition, LKPConditionViewModel>();
            CreateMap<LKPConditionViewModel, LKPCondition>()
                 .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<Location, LocationViewModel>()
                .ForMember(dest => dest.Contract, opt => opt.MapFrom(src => src.Contract)).MaxDepth(1);
            CreateMap<LocationViewModel, Location>()
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.Contract, opt => opt.MapFrom(src => src.Contract)).MaxDepth(1);

            CreateMap<Privilge, PrivilgeViewModel>();
            CreateMap<PrivilgeViewModel, Privilge>()
                 .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<RolePrivilge, RolePrivilgeViewModel>()
                .ForMember(dest => dest.ApplicationRole, opt => opt.MapFrom(src => src.ApplicationRole))
                .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge));
            CreateMap<RolePrivilgeViewModel, RolePrivilge>()
                .ForMember(dest => dest.ApplicationRole, opt => opt.MapFrom(src => src.ApplicationRole))
                .ForMember(dest => dest.Privilge, opt => opt.MapFrom(src => src.Privilge))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());

            CreateMap<Vehicle, VehicleViewModel>()
                .ForMember(dest => dest.Driver, opt => opt.MapFrom(src => src.Driver))
                .ForMember(dest => dest.Job, opt => opt.MapFrom(src => src.Job));

            CreateMap<VehicleViewModel, Vehicle>()
                .ForMember(dest => dest.Driver, opt => opt.MapFrom(src => src.Driver))
                .ForMember(dest => dest.Job, opt => opt.MapFrom(src => src.Job))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.WashingTime, opt => opt.Ignore());

            CreateMap<WorkOrder, WorkOrderViewModel>()
                .ForMember(dest => dest.Items, opt => opt.MapFrom(src => src.Items)).MaxDepth(2)
            .ForMember(dest => dest.WorkOrders, opt => opt.MapFrom(src => src.WorkOrders))
            .ForMember(dest => dest.Equipment, opt => opt.MapFrom(src => src.Equipment))
            .ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Location))
            .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer))
            .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority))
            .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
            .ForMember(dest => dest.Jobs, opt => opt.MapFrom(src => src.Jobs))
            .ForMember(dest => dest.Factory, opt => opt.Ignore())

            .ForMember(dest => dest.ParentWorkOrder, opt => opt.MapFrom(src => src.ParentWorkOrder))
            .ForMember(dest => dest.FK_Factory_Id, opt => opt.MapFrom(src => src.Factory.Id));
            CreateMap<WorkOrderViewModel, WorkOrder>()
                .ForMember(dest => dest.Items, opt => opt.MapFrom(src => src.Items)).MaxDepth(2)
            .ForMember(dest => dest.WorkOrders, opt => opt.MapFrom(src => src.WorkOrders))
            .ForMember(dest => dest.Equipment, opt => opt.MapFrom(src => src.Equipment))
            .ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Location))
            .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer))
            .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority))
            .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
            .ForMember(dest => dest.Jobs, opt => opt.MapFrom(src => src.Jobs))
            .ForMember(dest => dest.ParentWorkOrder, opt => opt.MapFrom(src => src.ParentWorkOrder))
            .ForMember(dest => dest.Factory, opt => opt.MapFrom(src => src.Factory))
            .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
            .ForMember(dest => dest.FK_CreatedBy_Id, opt => opt.Ignore())
            .ForMember(dest => dest.FK_UpdatedBy_Id, opt => opt.Ignore())
            .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
            .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore())
            .ForMember(dest => dest.IsDeleted, opt => opt.Ignore());



            CreateMap<WorkOrderItems, WorkOrderItemsViewModel>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item))
            .ForMember(dest => dest.WorkOrder, opt => opt.MapFrom(src => src.WorkOrder));
            CreateMap<WorkOrderItemsViewModel, WorkOrderItems>()
            .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item))
            .ForMember(dest => dest.WorkOrder, opt => opt.MapFrom(src => src.WorkOrder))
            .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());


            CreateMap<WorkOrderFactories, WorkOrderFactoriesViewModel>()
                .ForMember(dest => dest.SubOrdersFactoryPercent, opt => opt.MapFrom(src => src.SubOrdersFactoryPercent)).MaxDepth(3)
                .ForMember(dest => dest.WorkOrder, opt => opt.MapFrom(src => src.WorkOrder)).MaxDepth(3);

            CreateMap<WorkOrderFactoriesViewModel, WorkOrderFactories>()
                .ForMember(dest => dest.SubOrdersFactoryPercent, opt => opt.MapFrom(src => src.SubOrdersFactoryPercent)).MaxDepth(3)
                .ForMember(dest => dest.WorkOrder, opt => opt.MapFrom(src => src.WorkOrder)).MaxDepth(3)
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.FK_WorkOrder_Id, opt => opt.Ignore())
                .ForMember(dest => dest.StartDate, opt => opt.Ignore())
                .ForMember(dest => dest.EndDate, opt => opt.Ignore());


            CreateMap<LKP_ConfirmationStatus, LKP_ConfirmationStatusViewModel>();

            CreateMap<LKP_ConfirmationStatusViewModel, LKP_ConfirmationStatus>()
                .ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore())
                .ForMember(dest => dest.FK_CreatedBy_Id, opt => opt.Ignore())
                .ForMember(dest => dest.FK_UpdatedBy_Id, opt => opt.Ignore())
                .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
                .ForMember(dest => dest.UpdatedDate, opt => opt.Ignore());

            CreateMap<OrderConfirmationHistory, OrderConfirmationHistoryViewModel>()
                .ForMember(dest => dest.ConfirmationStatus, opt => opt.MapFrom(src => src.ConfirmationStatus))
                .ForMember(dest => dest.WorkOrder, opt => opt.MapFrom(src => src.WorkOrder));

            CreateMap<OrderConfirmationHistoryViewModel, OrderConfirmationHistory>()
                .ForMember(dest => dest.ConfirmationStatus, opt => opt.MapFrom(src => src.ConfirmationStatus))
                .ForMember(dest => dest.WorkOrder, opt => opt.MapFrom(src => src.WorkOrder))
                .ForMember(dest => dest.CurrentUser, opt => opt.Ignore());
        }
    }
}

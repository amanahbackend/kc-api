﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace KuwaitCement.DispatchingSystem.WebService
{
    public static class NotificationsManager
    {
        private static Timer _jobsTimer;
        private static IJobManager _jobManager;
        private static string[] _groupNames;
        private static string _connectionString;
        private static string _migrationAssembly;
        private static string _jobsClientMethodName;
        private static IHubContext<DispatchingHub> _dispatchingHub;

        private static IWorkOrderManager _workOrderManager;
        private static string _upcomingOrdersClientMethodName;
        private static Timer _ordersTimer;
        private static bool _isUpcomingOrdersSent = false;
        private static int _endWorkHour;
        private static double _upcomingOrdersMinutesBeforeLeave;
        private static IUpcomingWorkOrderManager _upcomingWorkOrderManager;

        private static int _restrictedHoursToConfirmOrder;
        private static Timer _canConfirmTimer;

        public static void CheckJobLateDelivery(IJobManager jobManager, string[] groupNames,
            string connectionString, string migrationAssembly, string clientMethodName,
            IHubContext<DispatchingHub> dispatchingHub)
        {
            var minutesInterval = 5;
            _connectionString = connectionString;
            _migrationAssembly = migrationAssembly;
            _groupNames = groupNames;
            _jobManager = jobManager;
            _jobManager = jobManager;
            _jobsClientMethodName = clientMethodName;
            _dispatchingHub = dispatchingHub;
            if (_jobsTimer == null)
            {
                _jobsTimer = new Timer
                {
                    Interval = (minutesInterval * 60) * 1000
                };
                _jobsTimer.Elapsed += new ElapsedEventHandler(LateJobTimer_TickAsync);
                _jobsTimer.Enabled = true;
            }
        }

        private static void LateJobTimer_TickAsync(object sender, ElapsedEventArgs e)
        {
            var lateDeliveryMaxMinutes = 90;
            var optsBuilder = new DbContextOptionsBuilder<DataContext>();
            optsBuilder.UseSqlServer(_connectionString,
                        sqlOptions => sqlOptions.MigrationsAssembly(_migrationAssembly));
            DataContext dataContext = new DataContext(optsBuilder.Options);

            var inProgressJobs =  _jobManager.GetInProgressJobsSync(dataContext);

            foreach (var job in inProgressJobs)
            {
                TimeSpan timeDiff = DateTime.UtcNow - job.StartDate;
                if (timeDiff.Minutes >= lateDeliveryMaxMinutes)
                {
                      new DispatchingHub().SendLateJob(job, _groupNames, _jobsClientMethodName, _dispatchingHub);
                }
            }
        }

        public static void CheckUpcomingWorkOrders(IWorkOrderManager workOrderManager,
            string upcomingOrdersClientMethodName, IHubContext<DispatchingHub> dispatchingHub,
            int endWorkHour, double upcomingOrdersMinutesBeforeLeave,
            IUpcomingWorkOrderManager upcomingWorkOrderManager)
        {
            var minutesInterval = 0.2;

            _workOrderManager = workOrderManager;
            _upcomingOrdersClientMethodName = upcomingOrdersClientMethodName;
            _endWorkHour = endWorkHour;
            _upcomingOrdersMinutesBeforeLeave = upcomingOrdersMinutesBeforeLeave;
            _upcomingWorkOrderManager = upcomingWorkOrderManager;

            if (_ordersTimer == null)
            {
                _ordersTimer = new Timer
                {
                    Interval = (minutesInterval * 60) * 1000
                };
                _ordersTimer.Elapsed += new ElapsedEventHandler(UpcomingOrdersTimer_Tick);
                _ordersTimer.Enabled = true;
            }
        }

        private static void UpcomingOrdersTimer_Tick(object sender, ElapsedEventArgs e)
        {
            _ordersTimer.Stop();
            var optsBuilder = new DbContextOptionsBuilder<DataContext>();
            optsBuilder.UseSqlServer(_connectionString,
                        sqlOptions => sqlOptions.MigrationsAssembly(_migrationAssembly));
            using (DataContext dataContext = new DataContext(optsBuilder.Options))
            {
                if (DateTime.UtcNow.Hour == _endWorkHour - (_upcomingOrdersMinutesBeforeLeave / 60)
                && !_isUpcomingOrdersSent)
                {
                    DateTime tomorrow = DateTime.Today.AddDays(1);

                    var upcomingOrderCodes = _workOrderManager.GetByStartDateSync(tomorrow, dataContext).Select(w => w.Code).ToList();
                    new DispatchingHub().SendUpcomingWorkOrders(upcomingOrderCodes, _upcomingOrdersClientMethodName, _dispatchingHub);
                    //_upcomingWorkOrderManager.Add(dataContext, upcomingOrderCodes);
                    //_isUpcomingOrdersSent = true;
                }
                else if (DateTime.UtcNow.Hour == _endWorkHour)
                {
                    _isUpcomingOrdersSent = false;
                    _upcomingWorkOrderManager.DeleteAllSync(dataContext);
                }
            }
            _ordersTimer.Start();

        }

        public static async Task SendVehicleLocationAsync(double latitude, double longitude,
            string panicButtonValue, string vehicleNumber, string liveTrackingClientMethodName,
            DataContext dataContext, IVehicleManager vehicleManager, double washingLeavingTimeInMins)
        {
            var tracker = await vehicleManager.GetByVehicleNumber(panicButtonValue, vehicleNumber, dataContext);
            if (tracker != null)
            {
                tracker.Lat = latitude;
                tracker.Long = longitude;
                await new DispatchingHub().SendLiveJobLocation(tracker, liveTrackingClientMethodName, _dispatchingHub);
              await  _jobManager.HandlePanicButton(panicButtonValue, tracker.JobId, dataContext);
                await vehicleManager.HandleVehicleAvailability(vehicleNumber, dataContext, latitude,
                                                            longitude, washingLeavingTimeInMins,
                                                            panicButtonValue);
            }
            else
            {
                tracker = await vehicleManager.AddUnassignedVehicle(vehicleNumber, dataContext);
                tracker.Lat = latitude;
                tracker.Long = longitude;
               await  new DispatchingHub().SendLiveJobLocation(tracker, liveTrackingClientMethodName, _dispatchingHub);
            }
        }

        public static void CheckCanConfirmFlag(IWorkOrderManager workOrderManager, int restrictedHoursToConfirmOrder,
             string connectionString, string migrationAssembly)
        {
            var minutesInterval = 10;

            _workOrderManager = workOrderManager;
            _restrictedHoursToConfirmOrder = restrictedHoursToConfirmOrder;
            _connectionString = connectionString;
            _migrationAssembly = migrationAssembly;

            if (_canConfirmTimer == null)
            {
                _canConfirmTimer = new Timer
                {
                    Interval = (minutesInterval * 60) * 1000
                };
                _canConfirmTimer.Elapsed += new ElapsedEventHandler(CanConfirmTimer_Tick);
                _canConfirmTimer.Enabled = true;
            }
        }



        private static void CanConfirmTimer_Tick(object sender, ElapsedEventArgs e)
        {
            _canConfirmTimer.Stop();
            var optsBuilder = new DbContextOptionsBuilder<DataContext>();
            optsBuilder.UseSqlServer(_connectionString,
                        sqlOptions => sqlOptions.MigrationsAssembly(_migrationAssembly));
            using (DataContext dataContext = new DataContext(optsBuilder.Options))
            {
                _workOrderManager.UpdateCanConfirmFlag(_restrictedHoursToConfirmOrder, dataContext);
            }
            _canConfirmTimer.Start();
        }
    }
}

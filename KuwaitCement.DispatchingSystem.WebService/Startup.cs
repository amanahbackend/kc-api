﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DispatchingSystem.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using KuwaitCement.DispatchingSystem.WebService.Middleware;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.BLL.IManagers;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using AutoMapper;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using Microsoft.AspNetCore.SignalR;
using KuwaitCement.DispatchingSystem.WebService.Filters;
using DispatchingSystem.Models.Entities;
using Newtonsoft.Json;

namespace KuwaitCement.DispatchingSystem.WebService
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        public IHostingEnvironment _env;

        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();

            services.AddSingleton(provider => Configuration);
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
                options.Filters.Add(typeof(NotificationsFilter));
            }).AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            });
            services.AddAutoMapper(typeof(Startup));
            Mapper.AssertConfigurationIsValid();

            services.AddDbContext<DataContext>(options =>
                        options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                        sqlOptions => sqlOptions.MigrationsAssembly(Configuration["MigrationsAssembly"])));

            services.ConfigureApplicationCookie(cfg =>
            {
                cfg.Events = new CookieAuthenticationEvents
                {
                    OnRedirectToLogin = ctx =>
                    {
                        if (ctx.Request.Path.StartsWithSegments("/api"))
                        {
                            ctx.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                        }
                        return Task.FromResult(0);
                    }
                };
            })
                .AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<DataContext>()
                .AddDefaultTokenProviders();

            // Add web based authentication service using JWT
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidIssuer = Configuration["JwtSecurityToken:Issuer"],
                    ValidAudience = Configuration["JwtSecurityToken:Audience"],
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtSecurityToken:Key"])),
                    ValidateLifetime = true
                };
            });

            #region Inject Models
            services.AddScoped<DbContext, DataContext>();
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IArea), typeof(Area));
            services.AddScoped(typeof(IGovernrate), typeof(Governrate));

            services.AddScoped(typeof(IComplain), typeof(Complain));
            services.AddScoped(typeof(IContract), typeof(Contract));
            services.AddScoped(typeof(IContractType), typeof(ContractType));
            services.AddScoped(typeof(IApplicationConfiguration), typeof(ApplicationConfiguration));


            services.AddScoped(typeof(ICustomer), typeof(Customer));
            services.AddScoped(typeof(ICustomerType), typeof(CustomerType));
            services.AddScoped(typeof(IDriver), typeof(Driver));
            services.AddScoped(typeof(IEquipment), typeof(Equipment));
            services.AddScoped(typeof(IEquipmentType), typeof(EquipmentType));
            services.AddScoped(typeof(IFactory), typeof(Factory));
            services.AddScoped(typeof(IItem), typeof(Item));
            services.AddScoped(typeof(IJob), typeof(Job));
            services.AddScoped(typeof(ILKP_Condition_IItem), typeof(LKP_Condition_Item));
            services.AddScoped(typeof(ILKP_Priority), typeof(LKP_Priority));
            services.AddScoped(typeof(ILKP_Status), typeof(LKP_Status));
            services.AddScoped(typeof(ILKPCondition), typeof(LKPCondition));
            services.AddScoped(typeof(ILocation), typeof(Location));
            services.AddScoped(typeof(IPrivilge), typeof(Privilge));
            services.AddScoped(typeof(IRolePrivilge), typeof(RolePrivilge));
            services.AddScoped(typeof(IVehicle), typeof(Vehicle));
            services.AddScoped(typeof(IWorkOrder), typeof(WorkOrder));
            services.AddScoped(typeof(ICustomerPhone), typeof(CustomerPhone));
            services.AddScoped(typeof(IUpcomingWorkOrder), typeof(UpcomingWorkOrder));
            services.AddScoped(typeof(IFactoryController), typeof(FactoryController));
            services.AddScoped(typeof(IReportFilter), typeof(ReportFilter));
            services.AddScoped(typeof(ITB_VEHICLE_DISPLAY), typeof(TB_VEHICLE_DISPLAY));
            services.AddScoped(typeof(ILKP_WashingStatus), typeof(LKP_WashingStatus));
            services.AddScoped(typeof(ILKP_ConfirmationStatus), typeof(LKP_ConfirmationStatus));
            services.AddScoped(typeof(IOrderConfirmationHistory), typeof(OrderConfirmationHistory));
            services.AddScoped(typeof(IElementToBePoured), typeof(ElementToBePoured));

            services.AddScoped(typeof(IBaseEntity), typeof(BaseEntity));
            #endregion Inject Models

            #region Inject Managers
            services.AddScoped(typeof(IComplainManager), typeof(ComplainManager));
            services.AddScoped(typeof(IContractManager), typeof(ContractManager));
            services.AddScoped(typeof(IContractTypeManager), typeof(ContractTypeManager));
            services.AddScoped(typeof(ICustomerManager), typeof(CustomerManager));
            services.AddScoped(typeof(ICustomerTypeManager), typeof(CustomerTypeManager));
            services.AddScoped(typeof(IDriverManager), typeof(DriverManager));
            services.AddScoped(typeof(IEquipmentManager), typeof(EquipmentManager));

            services.AddScoped(typeof(IElementToBePouredManager), typeof(ElementToBePouredManager));
            services.AddScoped(typeof(IEquipmentTypeManager), typeof(EquipmentTypeManager));
            services.AddScoped(typeof(IFactoryManager), typeof(FactoryManager));
            services.AddScoped(typeof(IItemManager), typeof(ItemManager));
            services.AddScoped(typeof(IJobManager), typeof(JobManager));
            services.AddScoped(typeof(ILKP_Condition_ItemManager), typeof(LKP_Condition_ItemManager));
            services.AddScoped(typeof(ILKP_PriorityManager), typeof(LKP_PriorityManager));
            services.AddScoped(typeof(ILKP_StatusManager), typeof(LKP_StatusManager));
            services.AddScoped(typeof(ILKPConditionManager), typeof(LKPConditionManager));
            services.AddScoped(typeof(ILocationManager), typeof(LocationManager));
            services.AddScoped(typeof(IAreaManager), typeof(AreaManager));
            services.AddScoped(typeof(IGovernrateManager), typeof(GovernrateManager));

            services.AddScoped(typeof(IApplicationConfigurationManager), typeof(ApplicationConfigurationManager));

            services.AddScoped(typeof(IPrivilgeManager), typeof(PrivilgeManager));
            services.AddScoped(typeof(IRolePrivilgeManager), typeof(RolePrivilgeManager));
            services.AddScoped(typeof(IVehicleManager), typeof(VehicleManager));
            services.AddScoped(typeof(IWorkOrderManager), typeof(WorkOrderManager));
            services.AddScoped(typeof(IWorkOrderItemsManager), typeof(WorkOrderItemsManager));
            services.AddScoped(typeof(IApplicationRoleManager), typeof(ApplicationRoleManager));
            services.AddScoped(typeof(IApplicationUserManager), typeof(ApplicationUserManager));
            services.AddScoped(typeof(ISecurityManager), typeof(SecurityManager));
            services.AddScoped(typeof(ICustomerPhoneManager), typeof(CustomerPhoneManager));
            services.AddScoped(typeof(IUpcomingWorkOrderManager), typeof(UpcomingWorkOrderManager));
            services.AddScoped(typeof(IFactoryControllerManager), typeof(FactoryControllerManager));
            services.AddScoped(typeof(IReportFilterManager), typeof(ReportFilterManager));
            services.AddScoped(typeof(IContractItemsManager), typeof(ContractItemsManager));
            services.AddScoped(typeof(IWashingAreaManager), typeof(WashingAreaManager));
            services.AddScoped(typeof(ILKP_ConfirmationStatusManager), typeof(LKP_ConfirmationStatusManager));
            services.AddScoped(typeof(IOrderConfirmationHistoryManager), typeof(OrderConfirmationHistoryManager));
            services.AddScoped(typeof(ILKP_CustomerCategoryManager), typeof(LKP_CustomerCategoryManager));

            #endregion Inject Managers

            services.AddSignalR(); // <-- SignalR
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            ILoggerFactory loggerFactory, IJobManager jobManager,
            IHubContext<DispatchingHub> dispatchingHub, IWorkOrderManager workorderManager,
            IUpcomingWorkOrderManager upcomingWorkOrderManager)
        {
            //if (env.IsDevelopment())
            //{
            app.UseDeveloperExceptionPage();
            //}
            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();

            app.UseCors(builder =>
            {
                builder.WithOrigins("http://localhost:4200")
                .AllowAnyHeader().AllowAnyMethod().AllowCredentials();
            });
            app.UseMiddleware<SignalRAuthenticationMiddleware>();

            app.UseAuthentication();


            //using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
            //  .CreateScope())
            //{
            //    var dbContext = serviceScope.ServiceProvider.GetService<DataContext>();
            //    dbContext.Database.Migrate();
            //}

            app.UseMiddleware<AuthorizationMiddleware>();

            app.UseSignalR(routes =>  // <-- SignalR
            {
                routes.MapHub<DispatchingHub>("/dispatchingHub");
            });
            app.UseMvc();

            //InitializeLateDeliveryNotifier(jobManager, dispatchingHub);
            //InitializeUcomingOrdersNotifier(workorderManager, dispatchingHub, upcomingWorkOrderManager);
            //InitializeCanConfirmJob(workorderManager);
        }

        private void InitializeLateDeliveryNotifier(IJobManager jobManager,
            IHubContext<DispatchingHub> lateJobsHub)
        {
            IConfigurationSection jobNotificationGroupsSection = Configuration.GetSection("Notifications:JobNotificationGroups");
            string[] jobNotificationGroups = jobNotificationGroupsSection.Get<string[]>();
            string connString = Configuration.GetConnectionString("DefaultConnection");
            string migrationAssembly = Configuration["MigrationsAssembly"];
            string clientMethodName = Configuration["Notifications:JobNotificationClientMethod"];

            NotificationsManager.CheckJobLateDelivery(jobManager, jobNotificationGroups,
                connString, migrationAssembly, clientMethodName, lateJobsHub);
        }

        private void InitializeUcomingOrdersNotifier(IWorkOrderManager workorderManager,
            IHubContext<DispatchingHub> dispatchingHub, IUpcomingWorkOrderManager upcomingWorkOrderManager)
        {
            string clientMethodName = Configuration["Notifications:UpcomingWorkOrdersClientMethod"];
            int workEndHour = Convert.ToInt32(Configuration["Notifications:WorkEndHourUTC"]);
            double upcomingOrdersTimeBeforeLeave = Convert.ToDouble(Configuration["Notifications:UpcomingOrdersTimeBeforeLeave"]);

            NotificationsManager.CheckUpcomingWorkOrders(workorderManager, clientMethodName,
                dispatchingHub, workEndHour, upcomingOrdersTimeBeforeLeave, upcomingWorkOrderManager);
        }

        private void InitializeCanConfirmJob(IWorkOrderManager workOrderManager)
        {
            string connString = Configuration.GetConnectionString("DefaultConnection");
            string migrationAssembly = Configuration["MigrationsAssembly"];
            var restrictedHoursToConfirmOrder = Convert.ToInt32(Configuration["Notifications:RestrictedHoursToConfirmOrder"]);

            NotificationsManager.CheckCanConfirmFlag(workOrderManager, restrictedHoursToConfirmOrder, connString, migrationAssembly);
        }
    }
}

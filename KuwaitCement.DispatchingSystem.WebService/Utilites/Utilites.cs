﻿using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.Utilites
{
    public class Helper
    {
        public async Task<ApplicationUser> GetCurrentUser(UserManager<ApplicationUser> userManager, HttpContext httpContext)
        {
            ApplicationUser result = null;
            ApplicationUserManager appUserManager = new ApplicationUserManager(userManager);
            var userName = httpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            result = await appUserManager.GetByUserNameAsync(userName);
            return result;
        }
        public async Task<IBaseEntity> FillEntityIdentity(UserManager<ApplicationUser> userManager, HttpContext httpContext, IBaseEntity entity)
        {
            if (entity != null)
            {
                ApplicationUser user = await GetCurrentUser(userManager, httpContext);
                entity.CurrentUser = user;
                var type = entity.GetType();
                var propertiesInfo = type.GetProperties();
                foreach (var propertyInfo in propertiesInfo)
                {
                    var nestedEntity = propertyInfo.GetValue(entity) as BaseEntity;
                    if (nestedEntity != null)
                    {
                        nestedEntity.CurrentUser = user;
                    }
                    else
                    {
                        var nestedCollectionEntity = propertyInfo.GetValue(entity) as ICollection<BaseEntity>;
                        if (nestedCollectionEntity != null)
                        {
                            foreach (var item in nestedCollectionEntity)
                            {
                                item.CurrentUser = user;
                            }
                        }
                    }
                }
            }
            return entity;
        }
        public async Task<List<IBaseEntity>> FillListEntitiesIdentites(UserManager<ApplicationUser> userManager, HttpContext httpContext, List<IBaseEntity> lstbaseEntities)
        {
            foreach (var item in lstbaseEntities)
            {
                await FillEntityIdentity(userManager, httpContext, item);
            }
            return lstbaseEntities;
        }


    }

}

﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using KuwaitCement.DispatchingSystem.WebService.Utilites;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace KuwaitCement.DispatchingSystem.WebService.Middleware
{
    public class AuthorizationMiddleware
    {
        public readonly RequestDelegate _next;

        public AuthorizationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, DataContext dbContext,
            UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager,
            IRolePrivilgeManager rolePrivilgeManager, ISecurityManager securityManager)
        {

            if (httpContext.Request.Path.Value.ToLower().Contains("availablekcavi")||
                httpContext.Request.Path.Value.ToLower().Contains("statuscompleted") || httpContext.Request.Path.Value.ToLower().Contains("concretemixed"))
            {
                await _next(httpContext);

            }
            else
            {
                string authorizationValue = httpContext.Request.Headers[HeaderNames.Authorization];
                string requestedAction = httpContext.Request.Path.Value.ToLower();
                if (!requestedAction.Contains("token") &&
                    !requestedAction.Contains("db") &&
                    !requestedAction.Contains("register") &&
                    !(requestedAction.IndexOf("KCCVehicle", StringComparison.CurrentCultureIgnoreCase) >= 0))
                {
                    if (!String.IsNullOrEmpty(authorizationValue))
                    {
                        if (authorizationValue.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
                        {
                            string token = authorizationValue.Substring("Bearer ".Length).Trim();
                            JwtSecurityToken jwtTokenObj = new JwtSecurityToken(token);
                            if (await securityManager.IsTokenValid(jwtTokenObj))
                            {
                                string username = jwtTokenObj.Payload.Sub;
                                ApplicationUserManager applicationUserManager = new ApplicationUserManager(userManager);
                                IList<string> roleNamess = await applicationUserManager.GetRolesAsync(username);

                                ApplicationRoleManager applicationRoleManager = new ApplicationRoleManager(roleManager);
                                List<string> userPrivileges = new List<string>();
                                foreach (var roleName in roleNamess)
                                {
                                    var privilges = await rolePrivilgeManager.GetPrivelegesByRole(roleName);
                                    userPrivileges.AddRange(privilges);
                                }

                                if (!userPrivileges.Contains(requestedAction))
                                {
                                    // httpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                                    await _next(httpContext);
                                }
                                else
                                {
                                    await _next(httpContext);
                                }
                            }
                            else
                            {
                                httpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                            }
                        }
                        else
                        {
                            //httpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                        }
                    }
                    else
                    {
                        httpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                    }
                }
                else
                {
                    await _next(httpContext);
                }
            }
        }
    }
}
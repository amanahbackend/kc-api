﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utilites
{
    public static class DateTimeUtilities
    {
        public static Task<DateTime> GetDateTimeFromEpochSecond(int secondsFromEpoch)
        {
          return  Task.Run(() =>
            {


                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                return epoch.AddSeconds(secondsFromEpoch);

            });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.Paging
{

    public class PagedResult<T>
    {

        public List<T> Result { set; get; }

        public int TotalCount { set; get; }

    }
}

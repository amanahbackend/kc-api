﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.Paging
{
    public class PaginatedItems
    {
        //const int MaxPageSize = 10;
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchBy { get; set; }

        public int Id { get; set; }
        public int totalNumbers { get; set; }
    }
}

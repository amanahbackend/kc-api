﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Utilites
{
    public static class FileUtilities
    {
        public static  Task CreateIfMissingAsync(string path)
        {
            return Task.Run(() =>
            {

                bool folderExists = Directory.Exists(path);
                if (!folderExists)
                {
                    Directory.CreateDirectory(path);
                }
            });
        }

        public static void DeleteFileIfExist(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.SetAttributes(filePath, FileAttributes.Normal);
                File.Delete(filePath);
            }
        }

        public static void WriteFileToPath(string fileBase64, string filePath)
        {
            using (var stream = File.Create(filePath))
            {
                File.SetAttributes(filePath, FileAttributes.Normal);
                var bytes = Convert.FromBase64String(fileBase64);
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        public static string GetFileBase64(string path)
        {
            Byte[] bytes = File.ReadAllBytes(path);
            string file = Convert.ToBase64String(bytes);
            return file;
        }

        public static string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        public static Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }
}

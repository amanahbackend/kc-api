﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilites
{
    public static class GeoUtilities
    {
        public static bool IsPointInPolygon(List<Point> polygon, Point point)
        {
            int i, j;
            bool result = false;
            for (i = 0, j = polygon.Count - 1; i < polygon.Count; j = i++)
            {
                if ((((polygon[i].Lat <= point.Lat) && (point.Lat < polygon[j].Lat))
                        || ((polygon[j].Lat <= point.Lat) && (point.Lat < polygon[i].Lat)))
                        && (point.Lng < (polygon[j].Lng - polygon[i].Lng) * (point.Lat - polygon[i].Lat)
                            / (polygon[j].Lat - polygon[i].Lat) + polygon[i].Lng))
                {
                    result = !result;
                }
            }
            return result;
        }
    }

    public class Point
    {
        public double Lng { get; set; }
        public double Lat { get; set; }
    }
}

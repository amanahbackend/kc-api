﻿namespace Utilities.ExcelToList
{
    public class UploadFile : IUploadFile
    {
        public string FileContent { get; set; }

        public string FileName { get; set; }

        public string FileRelativePath { get; set; }
    }
}

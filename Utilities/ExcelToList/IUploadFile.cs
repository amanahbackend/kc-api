﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.ExcelToList
{
    public interface IUploadFile
    {
        string FileContent { get; set; }

        string FileName { get; set; }

        string FileRelativePath { get; set; }
    }
}

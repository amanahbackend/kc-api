﻿using System.IO;
using Utilites;

namespace Utilities.ExcelToList
{
    public class UploadFileManager : IUploadFileManager
    {
        public virtual string AddFile(UploadFile file, string path)
        {
            string fileBase64 = file.FileContent.Clone().ToString();
            if (fileBase64 != null)
            {
                //FileUtilities.CreateIfMissing(path);
                string filePath = path;
                FileUtilities.WriteFileToPath(fileBase64, filePath);
                file.FileRelativePath = filePath;
            }
            return file.FileRelativePath;
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities.ExcelToList
{
    public interface IUploadFileManager
    {
        string AddFile(UploadFile file, string path);
    }
}

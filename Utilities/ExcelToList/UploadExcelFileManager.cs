﻿using System.IO;

namespace Utilities.ExcelToList
{
    public class UploadExcelFileManager : UploadFileManager
    {
        public override string AddFile(UploadFile file, string filePath)
        {
            string result = "";
            var extension = Path.GetExtension(filePath);
            if (extension == ".xls" || extension == ".xlsx")
            {
                result = base.AddFile(file, filePath);
            }
            return result;
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class RemoveTheItemRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FK_Item",
                table: "WorkOrder",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrder_FK_Item",
                table: "WorkOrder",
                column: "FK_Item");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkOrder_Item_FK_Item",
                table: "WorkOrder",
                column: "FK_Item",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkOrder_Item_FK_Item",
                table: "WorkOrder");

            migrationBuilder.DropIndex(
                name: "IX_WorkOrder_FK_Item",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "FK_Item",
                table: "WorkOrder");
        }
    }
}

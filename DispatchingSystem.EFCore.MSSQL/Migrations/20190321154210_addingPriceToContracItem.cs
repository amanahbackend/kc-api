﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class addingPriceToContracItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AmountDue",
                table: "Customer");

            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "ContractItems",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price",
                table: "ContractItems");

            migrationBuilder.AddColumn<double>(
                name: "AmountDue",
                table: "Customer",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}

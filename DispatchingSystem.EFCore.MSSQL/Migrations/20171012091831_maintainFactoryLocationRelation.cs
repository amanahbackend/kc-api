﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class maintainFactoryLocationRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Factory_Location_FK_Location_Id",
                table: "Factory");

            migrationBuilder.DropIndex(
                name: "IX_Factory_FK_Location_Id",
                table: "Factory");

            migrationBuilder.DropColumn(
                name: "FK_Location_Id",
                table: "Factory");

            migrationBuilder.AddColumn<string>(
                name: "AddressNote",
                table: "Factory",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Area",
                table: "Factory",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Block",
                table: "Factory",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Governorate",
                table: "Factory",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Latitude",
                table: "Factory",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Longitude",
                table: "Factory",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<string>(
                name: "Street",
                table: "Factory",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddressNote",
                table: "Factory");

            migrationBuilder.DropColumn(
                name: "Area",
                table: "Factory");

            migrationBuilder.DropColumn(
                name: "Block",
                table: "Factory");

            migrationBuilder.DropColumn(
                name: "Governorate",
                table: "Factory");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "Factory");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "Factory");

            migrationBuilder.DropColumn(
                name: "Street",
                table: "Factory");

            migrationBuilder.AddColumn<int>(
                name: "FK_Location_Id",
                table: "Factory",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Factory_FK_Location_Id",
                table: "Factory",
                column: "FK_Location_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Factory_Location_FK_Location_Id",
                table: "Factory",
                column: "FK_Location_Id",
                principalTable: "Location",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

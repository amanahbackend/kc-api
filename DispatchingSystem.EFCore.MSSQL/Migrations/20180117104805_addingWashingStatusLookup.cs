﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class addingWashingStatusLookup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Fk_WashingStatus_Id",
                table: "Vehicle",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LKP_WashingStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LKP_WashingStatus", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_Fk_WashingStatus_Id",
                table: "Vehicle",
                column: "Fk_WashingStatus_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_LKP_WashingStatus_Fk_WashingStatus_Id",
                table: "Vehicle",
                column: "Fk_WashingStatus_Id",
                principalTable: "LKP_WashingStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_LKP_WashingStatus_Fk_WashingStatus_Id",
                table: "Vehicle");

            migrationBuilder.DropTable(
                name: "LKP_WashingStatus");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_Fk_WashingStatus_Id",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "Fk_WashingStatus_Id",
                table: "Vehicle");
        }
    }
}

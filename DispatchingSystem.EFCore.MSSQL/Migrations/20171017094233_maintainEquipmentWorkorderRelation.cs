﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class maintainEquipmentWorkorderRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FK_WorkOrder_Id",
                table: "Equipment",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Equipment_FK_WorkOrder_Id",
                table: "Equipment",
                column: "FK_WorkOrder_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Equipment_WorkOrder_FK_WorkOrder_Id",
                table: "Equipment",
                column: "FK_WorkOrder_Id",
                principalTable: "WorkOrder",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Equipment_WorkOrder_FK_WorkOrder_Id",
                table: "Equipment");

            migrationBuilder.DropIndex(
                name: "IX_Equipment_FK_WorkOrder_Id",
                table: "Equipment");

            migrationBuilder.DropColumn(
                name: "FK_WorkOrder_Id",
                table: "Equipment");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class AddConfirmationStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FK_ConfirmationStatus_Id",
                table: "WorkOrder",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrder_FK_ConfirmationStatus_Id",
                table: "WorkOrder",
                column: "FK_ConfirmationStatus_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkOrder_LKP_ConfirmationStatus_FK_ConfirmationStatus_Id",
                table: "WorkOrder",
                column: "FK_ConfirmationStatus_Id",
                principalTable: "LKP_ConfirmationStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkOrder_LKP_ConfirmationStatus_FK_ConfirmationStatus_Id",
                table: "WorkOrder");

            migrationBuilder.DropIndex(
                name: "IX_WorkOrder_FK_ConfirmationStatus_Id",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "FK_ConfirmationStatus_Id",
                table: "WorkOrder");
        }
    }
}

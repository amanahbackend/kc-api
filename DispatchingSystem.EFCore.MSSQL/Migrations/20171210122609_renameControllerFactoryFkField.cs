﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class renameControllerFactoryFkField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FactoryController_AspNetUsers_Fk_MovementController_Id",
                table: "FactoryController");

            migrationBuilder.DropIndex(
                name: "IX_FactoryController_Fk_MovementController_Id",
                table: "FactoryController");

            migrationBuilder.DropColumn(
                name: "Fk_MovementController_Id",
                table: "FactoryController");

            migrationBuilder.AddColumn<string>(
                name: "Fk_Controller_Id",
                table: "FactoryController",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FactoryController_Fk_Controller_Id",
                table: "FactoryController",
                column: "Fk_Controller_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FactoryController_AspNetUsers_Fk_Controller_Id",
                table: "FactoryController",
                column: "Fk_Controller_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FactoryController_AspNetUsers_Fk_Controller_Id",
                table: "FactoryController");

            migrationBuilder.DropIndex(
                name: "IX_FactoryController_Fk_Controller_Id",
                table: "FactoryController");

            migrationBuilder.DropColumn(
                name: "Fk_Controller_Id",
                table: "FactoryController");

            migrationBuilder.AddColumn<string>(
                name: "Fk_MovementController_Id",
                table: "FactoryController",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FactoryController_Fk_MovementController_Id",
                table: "FactoryController",
                column: "Fk_MovementController_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FactoryController_AspNetUsers_Fk_MovementController_Id",
                table: "FactoryController",
                column: "Fk_MovementController_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

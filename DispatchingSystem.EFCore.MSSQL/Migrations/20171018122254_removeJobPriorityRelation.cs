﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class removeJobPriorityRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Job_LKP_Priority_FK_Priority_Id",
                table: "Job");

            migrationBuilder.DropIndex(
                name: "IX_Job_FK_Priority_Id",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "FK_Priority_Id",
                table: "Job");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FK_Priority_Id",
                table: "Job",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Job_FK_Priority_Id",
                table: "Job",
                column: "FK_Priority_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Job_LKP_Priority_FK_Priority_Id",
                table: "Job",
                column: "FK_Priority_Id",
                principalTable: "LKP_Priority",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

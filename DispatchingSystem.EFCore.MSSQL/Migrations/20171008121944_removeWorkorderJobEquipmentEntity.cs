﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class removeWorkorderJobEquipmentEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkOrderJobEquipment");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WorkOrderJobEquipment",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: true),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_DeletedBy_Id = table.Column<string>(nullable: true),
                    FK_Equipment_Id = table.Column<int>(nullable: false),
                    FK_Job_Id = table.Column<int>(nullable: false),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    FK_WorkOrder_Id = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: true),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkOrderJobEquipment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkOrderJobEquipment_Equipment_FK_Equipment_Id",
                        column: x => x.FK_Equipment_Id,
                        principalTable: "Equipment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkOrderJobEquipment_Job_FK_Job_Id",
                        column: x => x.FK_Job_Id,
                        principalTable: "Job",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkOrderJobEquipment_WorkOrder_FK_WorkOrder_Id",
                        column: x => x.FK_WorkOrder_Id,
                        principalTable: "WorkOrder",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrderJobEquipment_FK_Equipment_Id",
                table: "WorkOrderJobEquipment",
                column: "FK_Equipment_Id");

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrderJobEquipment_FK_Job_Id",
                table: "WorkOrderJobEquipment",
                column: "FK_Job_Id");

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrderJobEquipment_FK_WorkOrder_Id",
                table: "WorkOrderJobEquipment",
                column: "FK_WorkOrder_Id");
        }
    }
}

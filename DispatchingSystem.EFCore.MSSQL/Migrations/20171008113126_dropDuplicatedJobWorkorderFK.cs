﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class dropDuplicatedJobWorkorderFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Job_LKP_Status_FK_Priority_Id",
                table: "Job");

            migrationBuilder.DropForeignKey(
                name: "FK_Job_WorkOrder_WorkOrderId",
                table: "Job");

            migrationBuilder.DropIndex(
                name: "IX_Job_WorkOrderId",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "WorkOrderId",
                table: "Job");

            migrationBuilder.AddForeignKey(
                name: "FK_Job_LKP_Priority_FK_Priority_Id",
                table: "Job",
                column: "FK_Priority_Id",
                principalTable: "LKP_Priority",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Job_LKP_Priority_FK_Priority_Id",
                table: "Job");

            migrationBuilder.AddColumn<int>(
                name: "WorkOrderId",
                table: "Job",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Job_WorkOrderId",
                table: "Job",
                column: "WorkOrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Job_LKP_Status_FK_Priority_Id",
                table: "Job",
                column: "FK_Priority_Id",
                principalTable: "LKP_Status",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Job_WorkOrder_WorkOrderId",
                table: "Job",
                column: "WorkOrderId",
                principalTable: "WorkOrder",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

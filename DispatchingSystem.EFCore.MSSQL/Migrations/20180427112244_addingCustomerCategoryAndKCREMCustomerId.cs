﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class addingCustomerCategoryAndKCREMCustomerId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FK_CustomerCategory_Id",
                table: "Customer",
                type: "int",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "KCRM_Customer_Id",
                table: "Customer",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LKP_CustomerCategory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LKP_CustomerCategory", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Customer_FK_CustomerCategory_Id",
                table: "Customer",
                column: "FK_CustomerCategory_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Customer_LKP_CustomerCategory_FK_CustomerCategory_Id",
                table: "Customer",
                column: "FK_CustomerCategory_Id",
                principalTable: "LKP_CustomerCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customer_LKP_CustomerCategory_FK_CustomerCategory_Id",
                table: "Customer");

            migrationBuilder.DropTable(
                name: "LKP_CustomerCategory");

            migrationBuilder.DropIndex(
                name: "IX_Customer_FK_CustomerCategory_Id",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "FK_CustomerCategory_Id",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "KCRM_Customer_Id",
                table: "Customer");
        }
    }
}

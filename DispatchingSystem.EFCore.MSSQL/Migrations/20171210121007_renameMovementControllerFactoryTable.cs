﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class renameMovementControllerFactoryTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovementControllerFactory");

            migrationBuilder.CreateTable(
                name: "FactoryController",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Fk_Factory_Id = table.Column<int>(type: "int", nullable: false),
                    Fk_MovementController_Id = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FactoryController", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FactoryController_Factory_Fk_Factory_Id",
                        column: x => x.Fk_Factory_Id,
                        principalTable: "Factory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FactoryController_AspNetUsers_Fk_MovementController_Id",
                        column: x => x.Fk_MovementController_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FactoryController_Fk_Factory_Id",
                table: "FactoryController",
                column: "Fk_Factory_Id");

            migrationBuilder.CreateIndex(
                name: "IX_FactoryController_Fk_MovementController_Id",
                table: "FactoryController",
                column: "Fk_MovementController_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FactoryController");

            migrationBuilder.CreateTable(
                name: "MovementControllerFactory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(nullable: true),
                    Fk_Factory_Id = table.Column<int>(nullable: false),
                    Fk_MovementController_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovementControllerFactory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MovementControllerFactory_Factory_Fk_Factory_Id",
                        column: x => x.Fk_Factory_Id,
                        principalTable: "Factory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MovementControllerFactory_AspNetUsers_Fk_MovementController_Id",
                        column: x => x.Fk_MovementController_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovementControllerFactory_Fk_Factory_Id",
                table: "MovementControllerFactory",
                column: "Fk_Factory_Id");

            migrationBuilder.CreateIndex(
                name: "IX_MovementControllerFactory_Fk_MovementController_Id",
                table: "MovementControllerFactory",
                column: "Fk_MovementController_Id");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class AddConfHistfv : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractItems_Contract_FK_Contract_Id",
                table: "ContractItems");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractItems_Item_FK_Item_Id",
                table: "ContractItems");

            migrationBuilder.DropColumn(
                name: "CancellationReason",
                table: "Job");

            migrationBuilder.AlterColumn<int>(
                name: "Fk_WorkOrder_Id",
                table: "OrderConfirmationHistory",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "Amount",
                table: "Job",
                type: "int",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AddForeignKey(
                name: "FK_ContractItems_Contract_FK_Contract_Id",
                table: "ContractItems",
                column: "FK_Contract_Id",
                principalTable: "Contract",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractItems_Item_FK_Item_Id",
                table: "ContractItems",
                column: "FK_Item_Id",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ContractItems_Contract_FK_Contract_Id",
                table: "ContractItems");

            migrationBuilder.DropForeignKey(
                name: "FK_ContractItems_Item_FK_Item_Id",
                table: "ContractItems");

            migrationBuilder.AlterColumn<int>(
                name: "Fk_WorkOrder_Id",
                table: "OrderConfirmationHistory",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Amount",
                table: "Job",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "CancellationReason",
                table: "Job",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractItems_Contract_FK_Contract_Id",
                table: "ContractItems",
                column: "FK_Contract_Id",
                principalTable: "Contract",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ContractItems_Item_FK_Item_Id",
                table: "ContractItems",
                column: "FK_Item_Id",
                principalTable: "Item",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class AddEnumIsDecline : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "IsDeclined",
                table: "WorkOrder",
                type: "int",
                nullable: false,
            defaultValue: 0,

        oldClrType: typeof(bool));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsDeclined",
                table: "WorkOrder",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");
        }
    }
}

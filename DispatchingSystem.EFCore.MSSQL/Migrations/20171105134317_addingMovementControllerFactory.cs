﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class addingMovementControllerFactory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MovementControllerFactory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Fk_Factory_Id = table.Column<int>(type: "int", nullable: false),
                    Fk_MovementController_Id = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovementControllerFactory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MovementControllerFactory_Factory_Fk_Factory_Id",
                        column: x => x.Fk_Factory_Id,
                        principalTable: "Factory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MovementControllerFactory_AspNetUsers_Fk_MovementController_Id",
                        column: x => x.Fk_MovementController_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovementControllerFactory_Fk_Factory_Id",
                table: "MovementControllerFactory",
                column: "Fk_Factory_Id");

            migrationBuilder.CreateIndex(
                name: "IX_MovementControllerFactory_Fk_MovementController_Id",
                table: "MovementControllerFactory",
                column: "Fk_MovementController_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovementControllerFactory");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class removeDeletedByAndDeletedDateFromBaseEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "WorkOrderItems");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "WorkOrderItems");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "RolePrivilge");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "RolePrivilge");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Privilge");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Privilge");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "LKPCondition");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "LKPCondition");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "LKP_Status");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "LKP_Status");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "LKP_Priority");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "LKP_Priority");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "LKP_Condition_Item");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "LKP_Condition_Item");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Item");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Item");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Factory");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Factory");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "EquipmentType");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "EquipmentType");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Equipment");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Equipment");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "CustomerType");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "CustomerType");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ContractType");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "ContractType");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "Complain");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "Complain");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "AspNetRoles");

            migrationBuilder.DropColumn(
                name: "FK_DeletedBy_Id",
                table: "AspNetRoles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "WorkOrderItems",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "WorkOrderItems",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "WorkOrder",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "WorkOrder",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Vehicle",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Vehicle",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "RolePrivilge",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "RolePrivilge",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Privilge",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Privilge",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Location",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "LKPCondition",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "LKPCondition",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "LKP_Status",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "LKP_Status",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "LKP_Priority",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "LKP_Priority",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "LKP_Condition_Item",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "LKP_Condition_Item",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Job",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Job",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Item",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Item",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Factory",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Factory",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "EquipmentType",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "EquipmentType",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Equipment",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Equipment",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Driver",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Driver",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "CustomerType",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "CustomerType",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Customer",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Customer",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ContractType",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "ContractType",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Contract",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Contract",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "Complain",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "Complain",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "AspNetRoles",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_DeletedBy_Id",
                table: "AspNetRoles",
                nullable: true);
        }
    }
}

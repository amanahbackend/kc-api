﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class AddItemToPoured : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FK_ElementToBePoured_Id",
                table: "WorkOrder",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeclined",
                table: "WorkOrder",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "ElementToBePoured",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FK_CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FK_UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElementToBePoured", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrder_FK_ElementToBePoured_Id",
                table: "WorkOrder",
                column: "FK_ElementToBePoured_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkOrder_ElementToBePoured_FK_ElementToBePoured_Id",
                table: "WorkOrder",
                column: "FK_ElementToBePoured_Id",
                principalTable: "ElementToBePoured",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkOrder_ElementToBePoured_FK_ElementToBePoured_Id",
                table: "WorkOrder");

            migrationBuilder.DropTable(
                name: "ElementToBePoured");

            migrationBuilder.DropIndex(
                name: "IX_WorkOrder_FK_ElementToBePoured_Id",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "FK_ElementToBePoured_Id",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "IsDeclined",
                table: "WorkOrder");
        }
    }
}

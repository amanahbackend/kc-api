﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class maintainVehicleFactoryRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_Factory_FactoryId",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_FactoryId",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "FactoryId",
                table: "Vehicle");

            migrationBuilder.AddColumn<int>(
                name: "FK_Factory_Id",
                table: "Vehicle",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_FK_Factory_Id",
                table: "Vehicle",
                column: "FK_Factory_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Factory_FK_Factory_Id",
                table: "Vehicle",
                column: "FK_Factory_Id",
                principalTable: "Factory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vehicle_Factory_FK_Factory_Id",
                table: "Vehicle");

            migrationBuilder.DropIndex(
                name: "IX_Vehicle_FK_Factory_Id",
                table: "Vehicle");

            migrationBuilder.DropColumn(
                name: "FK_Factory_Id",
                table: "Vehicle");

            migrationBuilder.AddColumn<int>(
                name: "FactoryId",
                table: "Vehicle",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vehicle_FactoryId",
                table: "Vehicle",
                column: "FactoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Factory_FactoryId",
                table: "Vehicle",
                column: "FactoryId",
                principalTable: "Factory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class modifyVehicleIdinJobTableToBeNull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Job_FK_Vehicle_Id",
                table: "Job");

            migrationBuilder.AlterColumn<int>(
                name: "FK_Vehicle_Id",
                table: "Job",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "VehicleNumber",
                table: "Job",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Job_FK_Vehicle_Id",
                table: "Job",
                column: "FK_Vehicle_Id",
                unique: true,
                filter: "[FK_Vehicle_Id] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Job_FK_Vehicle_Id",
                table: "Job");

            migrationBuilder.DropColumn(
                name: "VehicleNumber",
                table: "Job");

            migrationBuilder.AlterColumn<int>(
                name: "FK_Vehicle_Id",
                table: "Job",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Job_FK_Vehicle_Id",
                table: "Job",
                column: "FK_Vehicle_Id",
                unique: true);
        }
    }
}

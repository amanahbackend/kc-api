﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class modifyingLocationContractRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_Customer_Fk_Customer_Id",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_Fk_Customer_Id",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "Fk_Customer_Id",
                table: "Location");

            migrationBuilder.AddColumn<int>(
                name: "Fk_Contract_Id",
                table: "Location",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Location_Fk_Contract_Id",
                table: "Location",
                column: "Fk_Contract_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Contract_Fk_Contract_Id",
                table: "Location",
                column: "Fk_Contract_Id",
                principalTable: "Contract",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_Contract_Fk_Contract_Id",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_Fk_Contract_Id",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "Fk_Contract_Id",
                table: "Location");

            migrationBuilder.AddColumn<int>(
                name: "Fk_Customer_Id",
                table: "Location",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Location_Fk_Customer_Id",
                table: "Location",
                column: "Fk_Customer_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Customer_Fk_Customer_Id",
                table: "Location",
                column: "Fk_Customer_Id",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

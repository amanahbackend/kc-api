﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class maintainEquipmentFactoryRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Fk_Factory_Id",
                table: "Equipment",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Equipment_Fk_Factory_Id",
                table: "Equipment",
                column: "Fk_Factory_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Equipment_Factory_Fk_Factory_Id",
                table: "Equipment",
                column: "Fk_Factory_Id",
                principalTable: "Factory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Equipment_Factory_Fk_Factory_Id",
                table: "Equipment");

            migrationBuilder.DropIndex(
                name: "IX_Equipment_Fk_Factory_Id",
                table: "Equipment");

            migrationBuilder.DropColumn(
                name: "Fk_Factory_Id",
                table: "Equipment");
        }
    }
}

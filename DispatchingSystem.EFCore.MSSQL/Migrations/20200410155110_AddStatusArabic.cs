﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class AddStatusArabic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SelectedLang",
                table: "WorkOrder",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ArabicName",
                table: "LKP_Status",
                type: "nvarchar(max)",
                nullable: true);

          
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SelectedLang",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "ArabicName",
                table: "LKP_Status");

         
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class addingAmountToContractItemsModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "ContractItems");

            migrationBuilder.AddColumn<double>(
                name: "Amount",
                table: "ContractItems",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "ContractItems");

            migrationBuilder.AddColumn<double>(
                name: "Quantity",
                table: "ContractItems",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}

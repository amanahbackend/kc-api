﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class addingMovementControllerFactory2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "MovementControllerFactory",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "FK_CreatedBy_Id",
                table: "MovementControllerFactory",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FK_UpdatedBy_Id",
                table: "MovementControllerFactory",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "MovementControllerFactory",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "MovementControllerFactory",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "MovementControllerFactory");

            migrationBuilder.DropColumn(
                name: "FK_CreatedBy_Id",
                table: "MovementControllerFactory");

            migrationBuilder.DropColumn(
                name: "FK_UpdatedBy_Id",
                table: "MovementControllerFactory");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "MovementControllerFactory");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "MovementControllerFactory");
        }
    }
}

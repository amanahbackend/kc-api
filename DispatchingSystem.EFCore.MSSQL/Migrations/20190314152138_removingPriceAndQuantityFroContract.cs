﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class removingPriceAndQuantityFroContract : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "ContractItems");

            migrationBuilder.DropColumn(
                name: "AvailableAmount",
                table: "ContractItems");

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "PendingAmount",
                table: "Contract");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "Contract");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Amount",
                table: "ContractItems",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "AvailableAmount",
                table: "ContractItems",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Amount",
                table: "Contract",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "PendingAmount",
                table: "Contract",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "Contract",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}

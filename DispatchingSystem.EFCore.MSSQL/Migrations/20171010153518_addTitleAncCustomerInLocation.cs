﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class addTitleAncCustomerInLocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customer_Location_FK_Location_Id",
                table: "Customer");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkOrder_Factory_FactoryId",
                table: "WorkOrder");

            migrationBuilder.DropIndex(
                name: "IX_WorkOrder_FactoryId",
                table: "WorkOrder");

            migrationBuilder.DropIndex(
                name: "IX_Customer_FK_Location_Id",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "FactoryId",
                table: "WorkOrder");

            migrationBuilder.DropColumn(
                name: "FK_Location_Id",
                table: "Customer");

            migrationBuilder.AddColumn<int>(
                name: "Fk_Customer_Id",
                table: "Location",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Location",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Location_Fk_Customer_Id",
                table: "Location",
                column: "Fk_Customer_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Customer_Fk_Customer_Id",
                table: "Location",
                column: "Fk_Customer_Id",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_Customer_Fk_Customer_Id",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_Fk_Customer_Id",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "Fk_Customer_Id",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Location");

            migrationBuilder.AddColumn<int>(
                name: "FactoryId",
                table: "WorkOrder",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FK_Location_Id",
                table: "Customer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_WorkOrder_FactoryId",
                table: "WorkOrder",
                column: "FactoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_FK_Location_Id",
                table: "Customer",
                column: "FK_Location_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Customer_Location_FK_Location_Id",
                table: "Customer",
                column: "FK_Location_Id",
                principalTable: "Location",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkOrder_Factory_FactoryId",
                table: "WorkOrder",
                column: "FactoryId",
                principalTable: "Factory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

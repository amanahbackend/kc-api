﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.EFCore.MSSQL.Migrations
{
    public partial class maintainJobVehicleRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Job_FK_Vehicle_Id",
                table: "Job");

            migrationBuilder.CreateIndex(
                name: "IX_Job_FK_Vehicle_Id",
                table: "Job",
                column: "FK_Vehicle_Id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Job_FK_Vehicle_Id",
                table: "Job");

            migrationBuilder.CreateIndex(
                name: "IX_Job_FK_Vehicle_Id",
                table: "Job",
                column: "FK_Vehicle_Id");
        }
    }
}

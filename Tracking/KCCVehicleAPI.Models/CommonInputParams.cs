﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KCCVehicleAPI.Models
{
    public class CommonInputParams
    {
        public string deviceuniqueid { get; set; }
        public string appusername { get; set; }
        public string apppassword { get; set; }
        public string appos { get; set; }
     
    }
}

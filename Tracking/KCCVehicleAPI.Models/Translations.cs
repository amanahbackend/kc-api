﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KCCVehicleAPI.Models
{
    public class Translations
    {
        public string languagecode { get; set; }
        public string sourcetext { get; set; }
        public string translatedtext { get; set; }
    }
}

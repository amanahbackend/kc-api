﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KCCVehicleAPI.Models
{
    public class Enums
    {
        public enum ConfigType
        {
            Package = 1,
            Device = 2,
            Plan=3,
            DeviceType = 4
        }
        public enum PackageType
        {
            Plan = 1,
            PackageWithDevice = 2,
            Both = 3
        }
    }
}

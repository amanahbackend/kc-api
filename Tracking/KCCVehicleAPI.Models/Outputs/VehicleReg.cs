﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KCCVehicleAPI.Models.Outputs
{
  public  class VehicleReg
    {
        public string VID { get; set; }
        public string Reg { get; set; }
        public string Vtype { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KCCVehicleAPI.Models.Outputs
{
    public class VehicleDTO
    {
        public string VID { get; set; }
        public string Reg { get; set; }
        public string Vtype { get; set; }
        public string Panicbutton { get; set; }
        public string DID { get; set; }
        public string Drivername { get; set; }
        public string Area { get; set; }
        public string Address { get; set; }
        public string Latitude { get; set; }
        public string Longtitude { get; set; }
        public string Datetime { get; set; }
    }
}

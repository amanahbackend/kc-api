﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KCCVehicleAPI.Models.Inputs
{
    public class GetVehicles
    {
        public List<Vehicle> Vehicles { get; set; }
    }
}

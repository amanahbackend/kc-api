﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KCCVehicleAPI.Models.Inputs
{
   public class GetVehiclesLocation
    {
        public List<VehiclesByVID> vehiclesbyvids { get; set; }
    }
}

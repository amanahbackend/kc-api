﻿using KCCVehicleAPI.Models;
using KCCVehicleAPI.Models.Outputs;
using KCCVehicleAPI.Models.Inputs;
using DispatchingSystem.Models;

namespace KCCVehicleAPI.BL.Interface
{
    public interface IVT
    {
        DTO<GetVehiclesDTO> GetVehicles(Input<GetVehicles> obj, DataContext db);
        DTO<ShowVehiclesDTO> ShowVehicles(Input<ShowVehicles> obj, DataContext db);
        DTO<ShowVehiclesRegDTO> ShowVehiclesReg(Input<ShowVehiclesReg> obj, DataContext db);
        DTO<GetVehiclesLocationDTO> GetVehiclesLocation(Input<GetVehiclesLocation> obj, DataContext db);
    }
}

﻿using DispatchingSystem.Models.Entities;

using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface ILocationManager : IRepository<Location>
    {
        Task<Location> Add(Location entity, string proxyUrl, string paciServiceUrl, string streetServiceUrl,
                string paciNumberFieldName, string streetNameFieldName, string blockNameFieldName);
        Task<Location> AddLocationAsync(Location entity);
        Task<bool> DeleteByContractIdAsync(int contractId);
        Task<List<Location>> GetByContractIdAsync(int contractId);
        Task<List<Location>> GetByContractIdsAsync(int[] contractIds);


    }
}

﻿using DispatchingSystem.BLL.Managers;
using Utilites;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DispatchingSystem.Models;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IWashingAreaManager : IRepository<WashingArea>
    {
        IQueryable<WashingArea> GetAll(DataContext dataContext);
    }
}

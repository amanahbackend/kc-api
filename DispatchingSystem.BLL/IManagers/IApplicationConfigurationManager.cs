﻿using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IApplicationConfigurationManager : IRepository<ApplicationConfiguration>
    {

    }
}

﻿
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Paging;

namespace DispatchingSystem.BLL.IManagers
{
    public interface ICustomerManager : IRepository<Customer>
    {
        Task<PagedResult<Customer>> GetAllCustomerByPagingAsync(PaginatedItems pagingParameterModel);
        Task<List<Customer>> SearchForCustomerAsync(string searchToken);
        Task<CustomCustomer> CreateCustomCustomerAsync(CustomCustomer customer);
        //CustomerHistory GetCustomerHistory(int customerId);
        Task<bool> IsCustomerNameExist(string name);
        Task<List<Customer>> GetLatestCustomers(int number);
        Task<bool> CivilIdExist(string civilId);
        Task<Customer> GetSelctedCustomer(int id);
        Task UpdateBalanceAsync(Customer customer);
        Task<Customer> SearchForCustomerAsync(int ID);
        Task<bool> KCRMIdExist(string kcrmId);

        Task<Customer> AddCustomerAsync(Customer entity);

        Task<bool> UpdateCustomerAsync(Customer entity);
    }
}

﻿using DispatchingSystem.Models.Entities;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Utilites.Paging;
using System.Linq;
using Utilities.Paging;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IWorkOrderManager : IRepository<WorkOrder>
    {

        Task<PagedResult<WorkOrder>> GetAllPendingAndApprovedOrdersAsync(PaginatedItems pagingParameterModel);
        Task<WorkOrderFactories> HandleWorkOrderAssigning(WorkOrderFactories workOrderFactories, bool isEdit = false);
        Task<WorkOrderFactories> GetWorkOrdersById(int workOrderId);
        Task<WorkOrder> UpdateworkOrderAsync(WorkOrder workOrder);
        //Task<List<WorkOrderFactories>> GetDetailedWorkOrdersByCustomerId(int customerId);
        Task<PagedResult<WorkOrder>> GetDetailedWorkOrdersByCustomerIdAsync(PaginatedItems pagingParameterModel);
        Task<WorkOrder> ChangeWorkOrderStatus(WorkOrder workOrder, int statusId);
        Task<bool> DeleteSuborder(int parentWorkOrderId, int factoryId);

        Task<List<WorkOrder>> GetParentOrdersByContractId(int contractId);

        Task<List<WorkOrder>> FilterSubordersForDispatcher(List<ReportFilter> filters);
        Task<bool> DeleteByContractId(int contractId);
        Task<List<WorkOrder>> GetByStartDate(DateTime startDate, DataContext dataContext);
        List<WorkOrder> GetByStartDateSync(DateTime startDate, DataContext dataContext);

        Task<WorkOrder> GetByCode(string workOrderCode);
        Task<List<WorkOrder>> GetByFactoryId(int factoryId);
        Task<List<WorkOrder>> GetTodaySubordersForDispatcher(string controllerId);
        Task<List<WorkOrder>> GetTodaySubordersByFactoryId(int factoryId);
        //Task<List<WorkOrder>> GetTodaySuborders(string controllerId);
        Task<PagedResult<WorkOrder>> GetTodaySubordersAsync(bool isMvmtUser, PaginatedItems pagingParameterModel);
        Task<List<WorkOrder>> GetTodaySuborders(bool isMvmtUser);
        Task<WorkOrder> AssignToFactory(WorkOrder workOrder, int factoryId);
        Task<WorkOrder> UpdateSuborderAsync(WorkOrder suborder);
        Task<List<WorkOrder>> GetAllSuborders();
        Task<bool> CanAssign(int workOrderId, int? factoryId, string googleServiceUrl, string googleApiKey);
        Task<PaginatedItemsViewModel<WorkOrderReportResult>> FilterWithPaging(int pageIndex, int pageSize, List<ReportFilter> filters);
        Task<List<WorkOrderReportResultToFile>> Filter(List<ReportFilter> filters);
        Task<WorkOrder> Get(DataContext dataContext, params object[] id);
        Task<WorkOrder> UpdateSuborder(WorkOrder suborder, DataContext dataContext);
        Task ChangeStatusToInProgress(int orderId);
      Task  ChangeStatusToDispatched(int orderId);
        Task<List<WorkOrder>> GetAllSuborders(string controllerId);
        Task<bool> IsContractHasOrders(int contractId);
        Task<List<WorkOrder>> GetByContractId(int contractId);
        Task<List<WorkOrder>> GetPendingUnassignedOrders();
        Task<List<WorkOrder>> GetAllPendingAndApprovedOrders();

        Task<List<WorkOrder>> GetAllApprovedOrders();

        Task<OrderConfirmationHistory> AddOrderConfirmationHistory(OrderConfirmationHistory confirmationHistory, int restrictedHoursToConfirmOrder, string googleServiceUrl, string googleApiKey);
        Task<PagedResult<WorkOrder>> FilterSuborders(List<ReportFilter> filters);
        Task<int> GetOrdersCount(DateTime starDate);
        Task<List<WorkOrder>> GetCalendarSuborders(string controllerId);
        Task UpdateCanConfirmFlag(int restrictedHoursToConfirmOrder, DataContext dataContext);
        Task<bool> ValidateCustomerBalance(WorkOrderFactories workOrderFactories);
        Task UpdateCustomerBalanceWithinJob(WorkOrderFactories workOrderFactories, double amount);
        Task<bool> ValidateCustomerBalanceWithinJob(WorkOrderFactories workOrderFactories, double amount);
        Task<bool> CheckIfCustomerVIP(WorkOrderFactories workOrderFactories);
        Task<WorkOrder> GetAllOrdersByIdAsync(int ordrID);
        Task<bool> IsOrderHaveContractItem(int contractId, int itemId);
        Task UpdateCustomerBalanceAfterCancelJob(WorkOrder workOrder, double amount);

    }
}

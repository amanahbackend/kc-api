﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IApplicationRoleManager
    {
        Task<ApplicationRole> GetRoleAsyncByName(string roleName);
        Task<ApplicationRole> GetRoleAsync(ApplicationRole role);
        Task<ApplicationRole> AddRoleAsync(ApplicationRole applicationRole);
        Task<ApplicationRole> AddRoleAsync(string roleName);
        Task<bool> IsRoleExistAsync(string roleName);
        List<ApplicationRole> GetAllRoles();
        Task<bool> DeleteRoleAsync(ApplicationRole applicationRole);
        Task<bool> UpdateRoleAsync(ApplicationRole applicationRole);
    }
}

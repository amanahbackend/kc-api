﻿using DispatchingSystem.Models.Entities;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface ICustomerTypeManager : IRepository<CustomerType>
    {
        Task<bool> CustomerTypeExists(string type);
    }
}

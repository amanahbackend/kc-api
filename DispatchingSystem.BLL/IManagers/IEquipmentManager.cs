﻿using DispatchingSystem.Models.Entities;

using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IEquipmentManager : IRepository<Equipment>
    {
        Task<List<Equipment>> AssignEquipmentToWorkOrder(int workOrderId, List<Equipment> equipmentLst);
        Task<bool> AssignEquipmentToFactory(int factoryId, Equipment equipment);

        Task<List<Equipment>> GetByFactory(int factoryId);
        Task<List<Equipment>> GetAvailableEquipment();
        Task<List<Equipment>> GetAvailableEquipmentInFactory(int factoryId);
        Task<List<Equipment>> GetEquipmentUnassignedToFactory();
        Task<List<Equipment>> GetEquipmentAssignedToWorkOrder(int workorderId);
        Task<bool> UnAssignEquipmentFromWorkOrder(int workOrderId, Equipment equipment);
        Task<bool> UnassignAllEquipmentFromWorkOrder(int workOrderId);
        Task<bool> UnassignAllEquipmentFromWorkOrder(int workOrderId, DataContext dataContext);
        Task<Equipment> AddEquipmentAsync(Equipment entity);
    }
}

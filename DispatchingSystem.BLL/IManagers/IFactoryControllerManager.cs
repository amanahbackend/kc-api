﻿using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IFactoryControllerManager : IRepository<FactoryController>
    {
        Task<int> GetControllerFactory(string movementControllerId);
    }
}

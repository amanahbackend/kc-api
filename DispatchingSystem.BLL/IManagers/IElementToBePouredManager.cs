﻿using DispatchingSystem.Models.Entities;

using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IElementToBePouredManager : IRepository<ElementToBePoured>
    {
    }
}

﻿
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IWorkOrderItemsManager : IRepository<WorkOrderItems>
    {
        Task<bool> DeleteItemsWorkOrder(int workorderId);
        Task<List<Item>> GetItemsWorkOrder(int workorderId);
        Task<WorkOrder> AddItemsWorkOrder(WorkOrder workOrder);
        Task<bool> DeleteItemsWorkOrderPhysically(int workorderId);
        Task<WorkOrderItems> GetBy(DataContext dataContext, int workOrderId, int itemId);
        Task UpdateItemsWorkOrder(WorkOrder workOrder);
    }
}

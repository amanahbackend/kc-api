﻿using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface ISecurityManager
    {
        Task<JwtSecurityToken> GetToken(string userName, string password, string jwtSecurityTokenKey, string issuer, string audience);
        Task<bool> IsTokenValid(JwtSecurityToken jwtTokenObj);
    }
}

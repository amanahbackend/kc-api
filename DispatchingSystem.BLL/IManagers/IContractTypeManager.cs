﻿
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IContractTypeManager : IRepository<ContractType>
    {
        
    }
}

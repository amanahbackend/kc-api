﻿using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IUpcomingWorkOrderManager
    {
        Task Add(DataContext dataContext, List<string> orderCodes);
        Task<List<UpcomingWorkOrder>> GetAll(DataContext dataContext);
        Task DeleteAll(DataContext dataContext);
        void DeleteAllSync(DataContext dataContext);
        List<UpcomingWorkOrder> GetAllSync(DataContext dataContext);
    }
}

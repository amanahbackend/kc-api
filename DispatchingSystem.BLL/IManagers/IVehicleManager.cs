﻿using DispatchingSystem.Models.Entities;

using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using System.Collections.Generic;
using Utilites;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IVehicleManager : IRepository<Vehicle>
    {
        Task<bool> AssignVehicleToFactory(int factoryId, Vehicle vehicle);
        Task<bool> SetVehicleAvailableAsync(string vehicleId, DataContext dataContext);


        Task<List<Job>> GetVehicleJobs(int vehicleId);
         Task<Vehicle> GetVechileById(int vechileID);
        Task<bool> AssignDriverToVehicle(int driverId, Vehicle vehicle);
        Task<List<Vehicle>> GetAvailableVehiclesInFactory(int factoryId);
        Task<bool> SetVehicleAvailable(int vehicleId);
        Task<bool> IsVehicleAvailable(int vehicleId);
        Task<bool> SetVehicleAvailable(int vehicleId, DataContext dataContext);
        Task<int?> GetVichileIDByNumber(string number);
        Task<VehicleTracker> GetByVehicleNumber(string panicButtonValue, string vehicleNumber, DataContext dataContext);
        Task<List<VehicleTracker>> GetByWorkorder(int orderId);
       Task<List<VehicleTracker>> GetByFactory(int factoryId);
        Task<bool> SetVehicleWashed(int vehicleId);
        Task<bool> SetVehicleInWashing(int vehicleId);
        Task HandleVehicleAvailability(string vehicleNumber, DataContext dataContext, double lat, double lng, double washingLeavingTimeInMins,string panicButtonValue);
        Task<List<VehicleTracker>> GetAllVehicleLocations(List<string> roles, string userId);
        Task<Vehicle> GetAvailableVehicleByOrder(int factoryId, int skippedNumber);
        Task<VehicleTracker> AddUnassignedVehicle(string vehicleNumber, DataContext dataContext);
        Task<Vehicle> SetVehicleNotAvailable(int vehicleId);
    }
}

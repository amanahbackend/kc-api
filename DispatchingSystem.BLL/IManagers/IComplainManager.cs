﻿
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface  IComplainManager: IRepository<Complain>
    {
        Task<List<Complain>> GetComplainsByCustomerIdAsync(int customerId);
        Task<bool> DeleteByCustomerIdAsync(int customerId);
    }
}

﻿using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilites.PACI;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IGovernrateManager : IRepository<Governrate>
    {
        Task<List<Governrate>> GetAllGovernoratesAsync();
        Task<Governrate> GetGovernrate(string governrateId);
        Task<List<Governrate>> SaveGovernrates(List<PACIHelper.DropPACI> governrates);
    }
}

﻿using DispatchingSystem.Models.Entities;

using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IFactoryManager : IRepository<Factory>
    {
        Task<Factory> AddOthersFactory(DataContext dataContext);
        Task<Factory> AddOthersFactory();

    }
}

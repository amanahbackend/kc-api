﻿using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface ILKP_ConfirmationStatusManager : IRepository<LKP_ConfirmationStatus>
    {
        string NeedMovementCtrlConfirmation { get; }
        string ConfirmedByMovementCtrl { get; }
        string NeedToChangeDateByMovementCtrl { get; }
        string ConfirmedByCustomer { get; }
        string NeedToRescheduleByCustomer { get; }
        string Declined { get; }

        Task<List<LKP_ConfirmationStatus>> GetByRole(string roleName);
        Task Seed();
        Task<LKP_ConfirmationStatus> GetByName(string name);
        Task<LKP_ConfirmationStatus> GetById(int  id);


        Task<bool> StatusExists(string name);

    }
}

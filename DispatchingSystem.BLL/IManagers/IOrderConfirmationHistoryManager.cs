﻿using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IOrderConfirmationHistoryManager : IRepository<OrderConfirmationHistory>
    {
        Task<List<OrderConfirmationHistory>> GetByOrderId(int orderId);
        Task<bool> DeleteByOrderId(int orderId);
        Task<List<OrderConfirmationHistory>> GetLatestByOrderId(int orderId);
        Task<OrderConfirmationHistory> AddOrderConfirmationHistory(OrderConfirmationHistory entity);
    }
}

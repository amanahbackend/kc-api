﻿using DispatchingSystem.Models.Entities;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DispatchingSystem.BLL.IManagers
{
    public interface ILKP_StatusManager : IRepository<LKP_Status>
    {
        Task<bool> StatusExists(string name);
        Task<LKP_Status> Get(string statusName);
        Task<bool> IsCompleted(int statusId);
        Task<bool> IsCancelled(int statusId);
        Task<LKP_Status> Get(string statusName, DataContext dataContext);
        Task<List<LKP_Status>> GetByRole(string role);
        Task<bool> IsClosed(int statusId);
    }
}

﻿using DispatchingSystem.Models.Entities;

using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;

namespace DispatchingSystem.BLL.IManagers
{
    public interface ILKP_Condition_ItemManager : IRepository<LKP_Condition_Item>
    {
        
    }
}

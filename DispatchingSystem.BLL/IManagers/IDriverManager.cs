﻿using DispatchingSystem.Models.Entities;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using System.Collections.Generic;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IDriverManager : IRepository<Driver>
    {
        List<Driver> GetAvailableDrivers();
    }
}

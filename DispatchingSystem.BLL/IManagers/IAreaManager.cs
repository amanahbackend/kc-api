﻿using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Utilites.PACI;

namespace DispatchingSystem.BLL.IManagers
{
   public interface IAreaManager : IRepository<Area>
    {
        Task<List<Area>> GetAreasAsync(string govId);
        //Task SaveAreas(List<PACIHelper.DropPACI> Areas, string GovernrateId);
    }
}

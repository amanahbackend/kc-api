﻿using DispatchingSystem.Models.Entities;

using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IPrivilgeManager : IRepository<Privilge>
    {
        Task<bool> PrivelegeExistsAsync(string privelegeName);

        Task<Privilge> GetByAsync(string privelegeName);

        Task Add(string privilegeName, string userId);

        Task<Privilge> GetByAsync(int id);
    }
}

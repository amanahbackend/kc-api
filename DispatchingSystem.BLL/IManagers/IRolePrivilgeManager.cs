﻿using DispatchingSystem.Models.Entities;

using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IRolePrivilgeManager : IRepository<RolePrivilge>
    {
        Task AddPrivelegToRole(string roleName, string privilegeName, RoleManager<ApplicationRole> identityRoleManager, string userId);
        Task<List<string>> GetPrivelegesByRole(string roleName);
    }
}

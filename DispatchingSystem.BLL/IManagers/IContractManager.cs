﻿
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Paging;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IContractManager : IRepository<Contract>
    {
        Task<PagedResult<Contract>> GetAllContractsByCustomerIdAsync(PaginatedItems pagingParameterModel);
        Task<List<Contract>> GetContractsByCustomerIdAsync(int customerId);
         Task<bool> DeleteByCustomerIdAsync(int customerId);
        Task<string> GetContractFileAsync(int contractId, int customerId);
        Task<Contract> AddContractAsync(Contract entity);
        Task<bool> CanDelete(int contractId);
        Task<List<Contract>> GetNonexpiredContractByCustomerIdAsync(int customerId);
        Task<CustomContract> CreateCustomContractAsync(CustomContract customContract);
        Task<bool> ContractNumberExists(string number);
    }
}

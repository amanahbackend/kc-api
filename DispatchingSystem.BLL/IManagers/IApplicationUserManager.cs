﻿using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IApplicationUserManager
    {
        Task<ApplicationUser> UpdateUserAsync(ApplicationUser user, string password);
        Task<ApplicationUser> AddUserAsync(ApplicationUser user, string password, string[] roleNames);
        Task<ApplicationUser> AddUserAsync(ApplicationUser user, string password);
        Task<ApplicationUser> AddUserAsync(string userName, string email, string password);
        Task<ApplicationUser> GetByUserNameAsync(string userName);
        Task<IList<Claim>> GetClaimsAsync(ApplicationUser user);
        Task<IList<string>> GetRolesAsync(string userName);
        Task<bool> AddUserToRoleAsync(string userName, string roleName);
        Task<bool> AddUserToRoleAsync(ApplicationUser applicationUser, ApplicationRole applicationRole);
        Task<bool> IsUserNameExistAsync(string userName);
        Task<bool> IsEmailExistAsync(string email);
        Task<bool> DeleteAsync(ApplicationUser user);
        Task<IList<ApplicationUser>> GetUsersInRole(string roleName);
        Task<bool> IsUserInRole(string userId, string roleName);
        Task<bool> ResetPassword(string username, string newPassword);
    }
}

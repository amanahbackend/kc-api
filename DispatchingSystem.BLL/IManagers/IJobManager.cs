﻿using DispatchingSystem.Models.Entities;

using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IJobManager : IRepository<Job>
    {
        Task<List<Job>> GetCompletedCancelledBy(int workOrderId);

        Task<Job> AddJobAsync(Job entity);

        Task<Job> SetVehicleStatusCompleted(string vehicleId, string pushButtonMsg, DataContext dataContext);

        Task<Job> CheckConcreteMixedTemperature(string vehicleId, string tempDegree,string name, DataContext dataContext);


        Task<Job> ReassignJob(Job job, int workorderId);
        Task ChangeJobToCompleted(Job job, DataContext dataContext);
        Task ChangeJobToCompletedManual(Job job);
        Task<Job> CancelJob(Job job);
        Task<bool> IsInProgressJob(int jobId);
        Task<bool> IsInProgressJob(int jobId, DataContext context);
        Task<List<Job>> GetInProgressJobs(DataContext context);
        List<Job> GetInProgressJobsSync(DataContext context);
        Task HandlePanicButton(string panicButtonValue, int jobId, DataContext dataContext);
        Task<List<Job>> GetAllByWorkOrder(int orderId);
        Task<bool> UpdateDeliveryNote(Job entity);
        Task<Job> JobDone(Job job);
    }
}

﻿using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface ICustomerPhoneManager : IRepository<CustomerPhone>
    {
        Task<List<CustomerPhone>> GetBy(string phone);
        Task<List<CustomerPhone>> GetByAsync(int customerId);
        Task<bool> IsPhoneExistAsync(string phone);
        Task<bool> DeleteByCustomerIdAsync(int customerId);
        Task UpdateCustomerPhonesAsync(IEnumerable<CustomerPhone> entityLst);
    }
}

﻿using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IContractItemsManager : IRepository<ContractItems>
    {
        Task<List<ContractItems>> GetContractItsmsAsync(int contractId);
        Task<List<ContractItems>> GetByContractIdAsync(int contractId);
        Task<bool> DeleteByContractIdAsync(int contractId);
        Task<bool> UpdateContractItems(IEnumerable<ContractItems> entityLst);
    }
}

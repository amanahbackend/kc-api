﻿using DispatchingSystem.Models.Entities;

using DispatchingSystem.BLL.Managers;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface IItemManager : IRepository<Item>
    {
        Task<bool> ItemCodeExists(string code);
    }
}

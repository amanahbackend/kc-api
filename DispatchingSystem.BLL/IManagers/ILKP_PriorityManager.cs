﻿using DispatchingSystem.Models.Entities;
using DispatchingSystem.BLL.Managers;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public interface ILKP_PriorityManager : IRepository<LKP_Priority>
    {
        Task<bool> PriorityExits(string name);
    }
}

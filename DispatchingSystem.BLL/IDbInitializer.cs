﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL
{
    public interface IDbInitializer
    {
        Task InitializeUsersRoles();
        Task InitializePriveleges(IPrivilgeManager privilgeManager, IRolePrivilgeManager rolePrivilgeManager);
        Task InitializeLookups(ILKP_PriorityManager priorityManager, ILKP_StatusManager statusManager, 
            ICustomerTypeManager customerTypeManager, ILKPConditionManager conditionManager);
    }
}

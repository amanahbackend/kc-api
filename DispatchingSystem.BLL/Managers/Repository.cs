﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using Microsoft.AspNetCore.Http;
using System.Linq.Expressions;
using Utilites;
using System.Collections;
using Newtonsoft.Json.Linq;
using Utilities.Paging;
using AutoMapper.QueryableExtensions;

namespace DispatchingSystem.BLL.Managers
{
    public class Repository<T> : IRepository<T> where T : class, IBaseEntity
    {
        private DataContext _context;

        public DataContext Context
        {
            get { return _context; }
        }
        private DbSet<T> _set;

        public Repository(DataContext context)
        {
            _context = context;
            _set = _context.Set<T>();
        }
        public virtual IQueryable<T> GetAllAsync()
        {
            return _set.AsNoTracking();
        }
        public Task<PagedResult<T>> GetAllByPaginationAsync(IQueryable<T> listQuery, PaginatedItems pagingparametermodel)
        {
            return Task.Run(() =>
            {
                var pagedResult = new PagedResult<T>();


                if (pagingparametermodel.PageNumber == 0)
                    pagingparametermodel.PageNumber = 1;

                //var source = listQuery.ProjectTo<T>();

                // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
                int CurrentPage = pagingparametermodel.PageNumber;

                // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
                int PageSize = pagingparametermodel.PageSize;
                pagedResult.TotalCount = listQuery.ToListAsync().Result.Count;
                pagedResult.Result = listQuery.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();

                return pagedResult;

            }
          );

        }
        public PagedResult<T> GetAllByPagination(IQueryable<T> listQuery, PaginatedItems pagingparametermodel)
        {
            try
            {

                var pagedResult = new PagedResult<T>();


                if (pagingparametermodel.PageNumber == 0)
                    pagingparametermodel.PageNumber = 1;

                //var source = listQuery.ProjectTo<T>();

                // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
                int CurrentPage = pagingparametermodel.PageNumber;

                // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
                int PageSize = pagingparametermodel.PageSize;
                pagedResult.TotalCount = listQuery.ToList().Count;
                pagedResult.Result = listQuery.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();

                return pagedResult;
            }
            catch (Exception e)
            {

                throw;
            }


        }
        public virtual Task<T> GetAsync(params object[] id)
        {
            return _set.FindAsync(id);
        }

        //public virtual T Add(T entity)
        //{
        //    T result = null;
        //    if (Validator.IsValid(entity))
        //    {
        //        entity.IsDeleted = false;
        //        entity = _set.Add(entity).Entity;
        //        if (SaveChanges() > 0)
        //        {
        //            result = entity;
        //        }
        //    }
        //    else
        //    {
        //        StringBuilder exceptionMsgs = new StringBuilder();
        //        List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
        //        foreach (var errmsg in errorMsgs)
        //        {
        //            exceptionMsgs.Append(errmsg);
        //            exceptionMsgs.Append("/n");
        //        }
        //        throw new Exception(exceptionMsgs.ToString());
        //    }
        //    return result;
        //}


        public virtual async Task<T> AddAsync(T entity)
        {
            try
            {
                T result = null;
                if (Validator.IsValid(entity))
                {
                    entity.IsDeleted = false;
                    await _set.AddAsync(entity);
                    if (await SaveChangesAsync() > 0)
                    {
                        result = entity;
                    }
                }
                else
                {
                    StringBuilder exceptionMsgs = new StringBuilder();
                    List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                    foreach (var errmsg in errorMsgs)
                    {
                        exceptionMsgs.Append(errmsg);
                        exceptionMsgs.Append("/n");
                    }
                    throw new Exception(exceptionMsgs.ToString());
                }
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public virtual async Task AddRangeAsync(IEnumerable<T> entityLst)
        {
           await _set.AddRangeAsync(entityLst);
            await SaveChangesAsync();
        }
        public virtual async Task<bool> UpdateAsync(T entity)
        {
            bool result = false;
            if (Validator.IsValid(entity))
            {
                try
                {

                   await Task.Run(async () =>
                    {


                        _context.Entry<T>(entity).State = EntityState.Modified;
                        _context.Entry<T>(entity).Property(x => x.FK_CreatedBy_Id).IsModified = false;
                        _context.Entry<T>(entity).Property(x => x.CreatedDate).IsModified = false;
                        if (await SaveChangesAsync() > 0)
                        {
                            result = true;
                        }

                    });
                }
                catch (Exception e)
                {

                    throw;
                }
            }
            else
            {
                StringBuilder exceptionMsgs = new StringBuilder();
                List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                foreach (var errmsg in errorMsgs)
                {
                    exceptionMsgs.Append(errmsg);
                    exceptionMsgs.Append("/n");
                }
                throw new Exception(exceptionMsgs.ToString());
            }
            return result;
        }
        public virtual async Task<bool> UpdateAsync(T entity, DataContext dataContext)
        {
            bool result = false;
            if (Validator.IsValid(entity))
            {
                dataContext.Entry<T>(entity).State = EntityState.Modified;
                if (await SaveChangesAsync(dataContext)> 0)
                {
                    result = true;
                }
            }
            else
            {
                StringBuilder exceptionMsgs = new StringBuilder();
                List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                foreach (var errmsg in errorMsgs)
                {
                    exceptionMsgs.Append(errmsg);
                    exceptionMsgs.Append("/n");
                }
                throw new Exception(exceptionMsgs.ToString());
            }
            return result;
        }
        public virtual async Task<bool> UpdateAsync(IEnumerable<T> entityLst)
        {
            bool result = false;
            foreach (var entity in entityLst)
            {
                if (Validator.IsValid(entity))
                {
                    _context.Entry<T>(entity).State = EntityState.Modified;

                }

                else
                {
                    StringBuilder exceptionMsgs = new StringBuilder();
                    List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                    foreach (var errmsg in errorMsgs)
                    {
                        exceptionMsgs.Append(errmsg);
                        exceptionMsgs.Append("/n");
                    }
                    throw new Exception(exceptionMsgs.ToString());
                }
                if (await SaveChangesAsync() > 0)
                {
                    result = true;
                }
            }
            return result;
        }
        public virtual  Task<bool> DeleteAsync(T entity)
        {
            entity.IsDeleted = true;
            return  UpdateAsync(entity);
        }
        public virtual Task<bool> DeleteById(params object[] id)
        {
            T entity = _set.Find(id);
            entity.IsDeleted = true;
            return UpdateAsync(entity);
        }
        public virtual async Task<bool> Delete(List<T> entitylst)
        {
            bool result = false;
            if (entitylst.Count > 0)
            {
                foreach (var entity in entitylst)
                {
                    result = await  DeleteAsync(entity);
                }
                //result = SaveChanges() > 0;
            }
            return result;
        }
        
        public virtual int SaveChanges()
        {
            // this method handle any exception so no need to put it in try
            BaseEntityManager.AddAuditingData(_context.ChangeTracker.Entries());
            return _context.SaveChanges();
        }
        public virtual Task<int> SaveChangesAsync()
        {
            // this method handle any exception so no need to put it in try
            BaseEntityManager.AddAuditingData(_context.ChangeTracker.Entries());
            return _context.SaveChangesAsync();
        }

        public virtual Task<int> SaveChangesAsync(DataContext dataContext)
        {
            // this method handle any exception so no need to put it in try
            BaseEntityManager.AddAuditingData(dataContext.ChangeTracker.Entries());
            return dataContext.SaveChangesAsync();
        }

        public  Task<List<T>> GetAsync(string property, object value)
        {
            var lambda = CreateEqualSingleExpression(property, value);

            return   _set.Where(lambda.And(wo => wo.IsDeleted == false)).ToListAsync();
        }

        public Expression<Func<T, bool>> CreateEqualSingleExpression(string property, object value)
        {
            Expression<Func<T, bool>> result = null;
            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            if (p.Type.GetProperty(property) != null)
            {
                Expression propertyExpression = Expression.Property(p, property);
                var propertyType = Nullable.GetUnderlyingType(propertyExpression.Type) ?? propertyExpression.Type;
                propertyExpression = Expression.Convert(propertyExpression, propertyType);
                if (value is IEnumerable<object>)
                {
                    var valueLst = value as JArray;
                    var predicate = PredicateBuilder.False<T>();
                    foreach (var item in valueLst)
                    {
                        //p.Property == value
                        var equalsExpression = Expression.Equal(propertyExpression, Expression.Constant(Convert.ChangeType(((JValue)item).Value, propertyType)));

                        //p => p.Property == value
                        var lambda = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                        predicate = predicate.Or(lambda);
                    }
                    result = predicate;
                }
                else
                {
                    var equalsExpression = Expression.Equal(propertyExpression, Expression.Constant(Convert.ChangeType(value, propertyType)));
                    //p => p.Property == value
                    result = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                }
            }
            return result;
        }

        public Expression<Func<T, bool>> CreateGreaterThanOrLessThanSingleExpression(string property, object value)
        {
            Expression<Func<T, bool>> result = null;
            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            if (p.Type.GetProperty(property) != null)
            {
                Expression propertyExpression = Expression.Property(p, property);
                var propertyType = Nullable.GetUnderlyingType(propertyExpression.Type) ?? propertyExpression.Type;
                propertyExpression = Expression.Convert(propertyExpression, propertyType);

                if (value is IEnumerable<object>)
                {
                    var valueLst = value as JArray;
                    var predicate = PredicateBuilder.False<T>();
                    JToken firstValue = valueLst.First();
                    var greaterThanExpr = Expression.GreaterThanOrEqual(propertyExpression, Expression.Constant(Convert.ChangeType(((JValue)firstValue).Value, propertyType)));
                    var lambdaGreater = Expression.Lambda<Func<T, bool>>(greaterThanExpr, p);
                    predicate = predicate.Or(lambdaGreater);

                    JToken secondValue = valueLst.Skip(1).Take(1).First();
                    if (property.ToLower().Contains("date"))
                    {
                        var dateObj = Convert.ChangeType(((JValue)secondValue).Value, typeof(DateTime));
                        //secondValue = ((DateTime)dateObj).AddDays(1);
                    }
                    var lessThanExpr = Expression.LessThanOrEqual(propertyExpression, Expression.Constant(Convert.ChangeType(((JValue)secondValue).Value, propertyType)));
                    var lambdaLess = Expression.Lambda<Func<T, bool>>(lessThanExpr, p);
                    result = predicate.And(lambdaLess);

                }
                //else
                //{
                //    var equalsExpression = Expression.GreaterThan(propertyExpression, Expression.Constant(Convert.ChangeType(value, propertyType)));
                //    //p => p.Property == value
                //    result = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                //}
            }
            return result;
        }

        public Expression<Func<T, bool>> CreateLessThanOrEqualSingleExpression(string property, object value)
        {
            Expression<Func<T, bool>> result = null;
            //p
            var p = Expression.Parameter(typeof(T));

            //p.Property
            if (p.Type.GetProperty(property) != null)
            {
                Expression propertyExpression = Expression.Property(p, property);
                var propertyType = Nullable.GetUnderlyingType(propertyExpression.Type) ?? propertyExpression.Type;
                propertyExpression = Expression.Convert(propertyExpression, propertyType);

                if (value is IEnumerable<object>)
                {
                    var valueLst = value as JArray;
                    var predicate = PredicateBuilder.False<T>();
                    foreach (var item in valueLst)
                    {
                        //p.Property == value
                        var equalsExpression = Expression.LessThanOrEqual(propertyExpression, Expression.Constant(Convert.ChangeType(((JValue)item).Value, propertyType)));

                        //p => p.Property == value
                        var lambda = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                        predicate = predicate.Or(lambda);
                    }
                    result = predicate;
                }
                else
                {
                    var equalsExpression = Expression.LessThanOrEqual(propertyExpression, Expression.Constant(Convert.ChangeType(value, propertyType)));
                    //p => p.Property == value
                    result = Expression.Lambda<Func<T, bool>>(equalsExpression, p);
                }
            }
            return result;
        }

    }


}

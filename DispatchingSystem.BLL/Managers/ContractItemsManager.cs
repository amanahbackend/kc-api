﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class ContractItemsManager : Repository<ContractItems>, IContractItemsManager
    {
        private readonly IItemManager _itemManager;

        public ContractItemsManager(DataContext context, IItemManager itemManager) : base(context)
        {
            _itemManager = itemManager;
        }

        //public override ContractItems Add(ContractItems entity)
        //{
        //    //entity.AvailableAmount = entity.Amount;
        //    return base.Add(entity);
        //}

        //public override void Add(IEnumerable<ContractItems> entityLst)
        //{
        //    //foreach (var item in entityLst)
        //    //{
        //    //    item.AvailableAmount = item.Amount;
        //    //}
        //    base.Add(entityLst);
        //}

        public async Task<List<ContractItems>> GetByContractIdAsync(int contractId)
        {
            var result = GetAllAsync().Where(ci => ci.FK_Contract_Id == contractId).ToList();
            foreach (var item in result)
            {
                var dbItem =await  _itemManager.GetAsync(item.FK_Item_Id);
                item.Name = dbItem.Name;
                item.Description = dbItem.Description;
                //item.Price = dbItem.Price;
            }
            return result;
        }


        public async Task<List<ContractItems>> GetContractItsmsAsync(int contractId)
        {
            var result = await GetAllAsync().Where(ci => ci.FK_Contract_Id == contractId).ToListAsync();
            foreach (var item in result)
            {
                var dbItem = await _itemManager.GetAsync(item.FK_Item_Id);
                item.Name = dbItem.Name;
                item.Description = dbItem.Description;
                //item.Price = dbItem.Price;
            }
            return result;
        }

        public Task<bool> DeleteByContractIdAsync(int contractId)
        {
            var contractItems = GetAllAsync().Where(ci => ci.FK_Contract_Id == contractId).ToList();
            return base.Delete(contractItems);
        }

        //public override bool Update(ContractItems entity)
        //{
        //    var result = false;
        //    var dbContractItem = Context.ContractItems.AsNoTracking().FirstOrDefault(c => c.Id == entity.Id);
        //    if (dbContractItem != null)
        //    {
        //        var amountDiff = dbContractItem.Amount - dbContractItem.AvailableAmount;
        //        if (entity.Amount > dbContractItem.Amount)
        //        {
        //            entity.AvailableAmount = entity.Amount - amountDiff;
        //            result = base.Update(entity);
        //        }
        //        else if (entity.Amount < dbContractItem.Amount)
        //        {
        //            if (entity.Amount >= amountDiff)
        //            {
        //                entity.AvailableAmount = entity.Amount - amountDiff;
        //                result = base.Update(entity);
        //            }
        //        }
        //        else
        //        {
        //            result = base.Update(entity);
        //        }
        //    }
        //    return result;
        //}


        public async Task<bool> UpdateContractItems(IEnumerable<ContractItems> entityLst)
        {
            try
            {
                var result = true;
                if (entityLst.Count() > 0)
                {
                    var contractId = entityLst.FirstOrDefault().FK_Contract_Id;
                    Context.ContractItems.RemoveRange(Context.ContractItems.Where(x => x.FK_Contract_Id == contractId).ToList());
                    await Context.SaveChangesAsync();
                    foreach (var item in entityLst)
                    {
                        if (item.IsDisabled)
                        {
                            item.DisableDate = DateTime.UtcNow;
                        }
                        item.Item = null;
                       await AddAsync(item);
                    }
                }
                return result;
            }
            catch (Exception er) 
            {

                throw;
            }
        }

        public async Task<bool> DisableAsync(int contractId, int itemId)
        {
            var contItem = Context.ContractItems.FirstOrDefault(x => !x.IsDisabled && 
                                                                    x.FK_Contract_Id == contractId && 
                                                                    x.FK_Item_Id == itemId);
            if (contItem != null)
            {
                contItem.IsDisabled = true;
                contItem.DisableDate = DateTime.UtcNow;
                return await UpdateAsync(contItem);
            }
            else
            {
                return true;
            }
        }
    }
}

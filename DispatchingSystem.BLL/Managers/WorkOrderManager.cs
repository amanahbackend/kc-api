﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using DispatchingSystem.Models.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilites.GoogleMapsAPI;
using Utilites.Paging;
using Utilities.Paging;

namespace DispatchingSystem.BLL.Managers
{
    public class WorkOrderManager : Repository<WorkOrder>, IWorkOrderManager
    {
        private readonly IFactoryManager factoryManager;
        private readonly IWorkOrderItemsManager orderitemsManger;
        private readonly IItemManager itemManger;
        private readonly ILKP_StatusManager statusManager;
        private readonly IWorkOrderItemsManager workOrderItemsManager;
        private readonly IFactoryControllerManager controllerFactoryManager;
        private readonly ILocationManager locationManager;
        private readonly IEquipmentManager equipmentManager;
        private readonly IApplicationUserManager applicationUserManager;
        private readonly IContractItemsManager contractItemsManager;
        private readonly IOrderConfirmationHistoryManager orderConfirmationHistoryManager;
        private readonly ILKP_ConfirmationStatusManager confirmationStatusManager;
        private readonly IServiceProvider _serviceprovider;
        private readonly ICustomerPhoneManager _customerPhoneManager;

        public WorkOrderManager(DataContext context, IFactoryManager _factoryManager,
            IWorkOrderItemsManager _orderitemsManger, IItemManager _itemManger,
            ILKP_StatusManager _statusManager, IWorkOrderItemsManager _workOrderItemsManager,
            IFactoryControllerManager _controllerFactoryManager, ILocationManager _locationManager,
            IEquipmentManager _equipmentManager, IApplicationUserManager _applicationUserManager,
            IContractItemsManager _contractItemsManager, IOrderConfirmationHistoryManager _orderConfirmationHistoryManager,
            ILKP_ConfirmationStatusManager _confirmationStatusManager, IServiceProvider serviceprovider, ICustomerPhoneManager customerPhoneManager)
            : base(context)
        {
            _serviceprovider = serviceprovider;
            confirmationStatusManager = _confirmationStatusManager;
            orderConfirmationHistoryManager = _orderConfirmationHistoryManager;
            contractItemsManager = _contractItemsManager;
            statusManager = _statusManager;
            factoryManager = _factoryManager;
            orderitemsManger = _orderitemsManger;
            itemManger = _itemManger;
            workOrderItemsManager = _workOrderItemsManager;
            controllerFactoryManager = _controllerFactoryManager;
            locationManager = _locationManager;
            equipmentManager = _equipmentManager;
            applicationUserManager = _applicationUserManager;
            _customerPhoneManager = customerPhoneManager;
        }


        private ICustomerManager CustomerManager
        {
            get
            {
                return _serviceprovider.GetService<ICustomerManager>();
            }
        }
        public async Task<OrderConfirmationHistory> AddOrderConfirmationHistory(OrderConfirmationHistory confirmationHistory, int restrictedHoursToConfirmOrder, string googleServiceUrl, string googleApiKey)
        {
            var order = await GetAsync(confirmationHistory.Fk_WorkOrder_Id);


            if (confirmationHistory.IsDeclined == Decline.Yes)
            {
                order.IsDeclined = confirmationHistory.IsDeclined;
                order.Reason = confirmationHistory.Reason;
                order.Description = confirmationHistory.Description;
                order.FK_Status_Id = (await statusManager.Get("Declined")).Id;

                await UpdateAsync(order);
                return confirmationHistory;
            }
            else
            {


                bool result = await CanAssign(order.Id, confirmationHistory.FactoryId, googleServiceUrl, googleApiKey);
                if (result)
                {
                    var status = (await statusManager.Get("Pending")).Id;

                    if (confirmationHistory.IsDeclined == Decline.Yes)
                    {
                        order.IsDeclined = confirmationHistory.IsDeclined;
                        order.Reason = confirmationHistory.Reason;
                        order.Description = confirmationHistory.Description;
                        order.FK_Factory_Id = confirmationHistory.FactoryId;
                        order.FK_Status_Id = (await statusManager.Get("Declined")).Id;
                        order.FK_UpdatedBy_Id = confirmationHistory.CurrentUser.Id;

                        await UpdateAsync(order);
                        return null;
                    }
                    else
                    {
                        order.IsDeclined = confirmationHistory.IsDeclined;
                        order.Reason = confirmationHistory.Reason;
                        order.Description = confirmationHistory.Description;
                        order.FK_Factory_Id = confirmationHistory.FactoryId;
                        order.UpdatedDate = DateTime.Now;
                        order.FK_UpdatedBy_Id = confirmationHistory.CurrentUser.Id;
                        await UpdateAsync(order);
                    }


                    confirmationHistory.CreatorName = confirmationHistory.CurrentUser.UserName;
                    confirmationHistory.CreatorRole = confirmationHistory.CurrentUser.RoleNames[0];

                    confirmationHistory = await orderConfirmationHistoryManager.AddAsync(confirmationHistory);


                    if (confirmationHistory != null)
                    {
                        var confirStatus = (await confirmationStatusManager.GetById(confirmationHistory.Fk_ConfirmationStatus_Id)).Name;

                        order.FK_ConfirmationStatus_Id = confirmationHistory.Fk_ConfirmationStatus_Id;

                        if (confirmationHistory.OrderAlternativeDate != null && confirStatus == confirmationStatusManager.NeedToChangeDateByMovementCtrl)
                        {
                            order.EndDate = (DateTime)confirmationHistory.OrderAlternativeDate;
                            order.SelectedDate = confirmationHistory.SelectedDate;
                            order.IsDeclined = confirmationHistory.IsDeclined;
                            order.Reason = confirmationHistory.Reason;
                            order.Description = confirmationHistory.Description;
                            order.FK_Status_Id = (await statusManager.Get("Need To Change Date By Movement Ctrl")).Id;

                        }
                        if (confirStatus == confirmationStatusManager.ConfirmedByMovementCtrl)
                        {
                            order.SelectedDate = confirmationHistory.SelectedDate;
                            order.IsDeclined = confirmationHistory.IsDeclined;
                            order.Reason = confirmationHistory.Reason;
                            order.Description = confirmationHistory.Description;
                            order.FK_Status_Id = (await statusManager.Get("approved")).Id;

                        }
                        if (confirStatus == confirmationStatusManager.ConfirmedByCustomer)
                        {
                            order.FK_Status_Id = (await statusManager.Get("Confirmed by customer")).Id;
                            order.CanConfirm = false;
                        }
                        order.FK_UpdatedBy_Id = confirmationHistory.CurrentUser.Id;

                        await UpdateAsync(order);
                        return confirmationHistory;
                    }

                }
                return null;
            }
        }


        public async Task<WorkOrderFactories> HandleWorkOrderAssigning(WorkOrderFactories workOrderFactories, bool isEdit = false)
        {

            var isVIP = await CheckIfCustomerVIP(workOrderFactories);

            if (workOrderFactories.WorkOrder.FK_ConfirmationStatus_Id == 0)
            {
                workOrderFactories.WorkOrder.FK_ConfirmationStatus_Id = null;
            }
            if (workOrderFactories.WorkOrder != null && (workOrderFactories.WorkOrder.Items != null || workOrderFactories.WorkOrder.FK_Item > 0))
            {
                workOrderFactories.WorkOrder.PendingAmount = workOrderFactories.WorkOrder.Amount;
                if (isEdit)
                {
                    var pendingStatus = await statusManager.Get("pending");
                    var cancelledStatus = await statusManager.Get("cancelled");

                    var dbWorkOrder = await Context.WorkOrders.AsNoTracking().FirstOrDefaultAsync(w => w.Id == workOrderFactories.WorkOrder.Id);
                    if (workOrderFactories.WorkOrder.FK_Status_Id != pendingStatus.Id)
                    {
                        if (dbWorkOrder.Amount == workOrderFactories.WorkOrder.Amount)
                        {
                            var isCustomerHasBalance = await ValidateCustomerBalance(workOrderFactories);
                            if (isCustomerHasBalance || isVIP && workOrderFactories.WorkOrder.FK_Item.Value > 0)
                            {
                                workOrderFactories = await EditWorkOrder(workOrderFactories);
                                //var orderPrice = (await itemManger.GetAsync(workOrderFactories.WorkOrder.Items.Id)).Price;
                                //var dbOrderPrice = (await workOrderItemsManager.GetAllAsync().FirstOrDefaultAsync(x => x.FK_Workorder_Id == dbWorkOrder.Id)).Price;
                                //if (orderPrice != dbOrderPrice)
                                //{
                                //    //UpdateCustomerBalance(workOrderFactories, isEdit);
                                //}
                                //await workOrderItemsManager.UpdateItemsWorkOrder(workOrderFactories.WorkOrder);
                            }
                        }
                        else
                        {
                            var isItemAmountValid = await ValidateItemsAvailabilityInContract(workOrderFactories);
                            if (isItemAmountValid)
                            {
                                var isCustomerHasBalance = await ValidateCustomerBalance(workOrderFactories);
                                if (isCustomerHasBalance && workOrderFactories.WorkOrder.FK_Item.Value > 0)
                                {
                                    workOrderFactories = await EditWorkOrder(workOrderFactories);
                                    //UpdateCustomerBalance(workOrderFactories, isEdit);
                                    //await workOrderItemsManager.UpdateItemsWorkOrder(workOrderFactories.WorkOrder);
                                }
                            }
                            else
                            {
                                workOrderFactories = null;
                            }
                        }
                    }
                    else
                    {
                        var isCustomerHasBalance = await ValidateCustomerBalance(workOrderFactories);
                        if (isCustomerHasBalance || isVIP)
                        {
                            //UpdateCustomerBalance(workOrderFactories, isEdit);
                            // await workOrderItemsManager.UpdateItemsWorkOrder(workOrderFactories.WorkOrder);

                            if (workOrderFactories.WorkOrder.FK_Status_Id == pendingStatus.Id)
                            {
                                workOrderFactories.WorkOrder.CanConfirm = true;
                            }
                            if (workOrderFactories.SubOrdersFactoryPercent != null &&
                                   workOrderFactories.SubOrdersFactoryPercent.Count > 0)
                            {
                                if (workOrderFactories.SubOrdersFactoryPercent.Count == 1)
                                {
                                    workOrderFactories = await AssignWorkOrder(workOrderFactories);
                                }
                                else
                                {
                                    workOrderFactories.WorkOrder.HasSuborders = true;
                                    await UpdateAsync(workOrderFactories.WorkOrder);
                                    workOrderFactories = await DivideWorkOrder(workOrderFactories);
                                }
                            }

                            else
                            {
                                await UpdateAsync(workOrderFactories.WorkOrder);

                            }

                        }
                    }

                }
                else
                {
                    var isItemAmountValid = await ValidateItemsAvailabilityInContract(workOrderFactories);

                    if (isItemAmountValid)
                    {

                        var isCustomerHasBalance = await ValidateCustomerBalance(workOrderFactories);

                        if (isCustomerHasBalance || isVIP)
                        {
                            var lastId = (from n in base.Context.WorkOrders
                                          orderby n.Id descending
                                          select n.Id).FirstOrDefault().ToString();
                            workOrderFactories.WorkOrder.Code = await GeneratOrderUniqueCodeByDateAsync(lastId);
                            workOrderFactories.WorkOrder.FK_Status_Id = (await statusManager.Get("Pending")).Id;

                            workOrderFactories.WorkOrder.CanConfirm = true;

                            workOrderFactories.WorkOrder = await AddAsync(workOrderFactories.WorkOrder);
                            await workOrderItemsManager.AddItemsWorkOrder(workOrderFactories.WorkOrder);
                            //UpdateCustomerBalance(workOrderFactories, isEdit);
                            await orderConfirmationHistoryManager.AddAsync(new OrderConfirmationHistory()
                            {
                                CurrentUser = workOrderFactories.WorkOrder.CurrentUser,
                                CreatorName = workOrderFactories.WorkOrder.CurrentUser.UserName,
                                CreatorRole = workOrderFactories.WorkOrder.CurrentUser.RoleNames[0],
                                Fk_ConfirmationStatus_Id = (await confirmationStatusManager.GetByName(confirmationStatusManager.NeedMovementCtrlConfirmation)).Id,
                                Fk_WorkOrder_Id = workOrderFactories.WorkOrder.Id,
                                OrderDate = workOrderFactories.WorkOrder.StartDate,
                                OrderNumber = workOrderFactories.WorkOrder.Code
                            });

                            if (workOrderFactories.SubOrdersFactoryPercent != null &&
                                workOrderFactories.SubOrdersFactoryPercent.Count > 0)
                            {
                                if (workOrderFactories.SubOrdersFactoryPercent.Count == 1)
                                {
                                    workOrderFactories = await AssignWorkOrder(workOrderFactories);
                                }
                                else
                                {
                                    workOrderFactories.WorkOrder.HasSuborders = true;
                                    await UpdateAsync(workOrderFactories.WorkOrder);
                                    workOrderFactories = await DivideWorkOrder(workOrderFactories);
                                }
                            }
                        }
                        else
                        {
                            workOrderFactories = null;
                        }
                    }
                    else
                    {
                        workOrderFactories = null;
                    }
                }
            }
            else
            {
                workOrderFactories = null;
            }
            return workOrderFactories;
        }

        //private void CancelOrder(WorkOrder order)
        //{
        //    var items = workOrderItemsManager.GetItemsWorkOrder(order.Id);
        //    var orderAmount = order.Amount;
        //    var contractItems = contractItemsManager.GetByContractId(order.FK_Contract_Id);
        //    var contractItem = contractItems.FirstOrDefault(x => x.FK_Item_Id == items.FirstOrDefault().Id);
        //    contractItem.AvailableAmount = contractItem.AvailableAmount + orderAmount;
        //    contractItemsManager.Update(contractItem);
        //}

        private async Task<bool> ValidateItemsAvailabilityInContract(WorkOrderFactories workOrderFactories)
        {
            bool result = false;
            var contractItems = await contractItemsManager.GetByContractIdAsync(workOrderFactories.WorkOrder.FK_Contract_Id);

            var items = workOrderFactories.WorkOrder.Items;
            var orderAmount = workOrderFactories.WorkOrder.Amount;
            bool isContractHaveItem = contractItems.Select(x => x.FK_Item_Id).ToList().Contains(workOrderFactories.WorkOrder.FK_Item.Value);

            if (isContractHaveItem)
            {
                //var contractItem = contractItems.FirstOrDefault(x => x.FK_Item_Id == items.FirstOrDefault().Id);
                //if (contractItem.AvailableAmount >= orderAmount)
                //{
                result = true;
                //}
            }
            return result;
        }
        public async Task<bool> CheckIfCustomerVIP(WorkOrderFactories workOrderFactories)
        {
            var customerId = workOrderFactories.WorkOrder.FK_Customer_Id;
            var customer = await Context.Customers.FirstOrDefaultAsync(x => x.Id == customerId);

            if (customer.CreditLimit == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public async Task<bool> ValidateCustomerBalance(WorkOrderFactories workOrderFactories)
        {
            try
            {
                bool result = false;
                var customerId = workOrderFactories.WorkOrder.FK_Customer_Id;
                var customer = await Context.Customers.FirstOrDefaultAsync(x => x.Id == customerId);
                var contractItem = await contractItemsManager.GetAllAsync().AsNoTracking()
                                    .FirstOrDefaultAsync(x => x.FK_Item_Id == workOrderFactories.WorkOrder.FK_Item
                                                        && x.FK_Contract_Id == workOrderFactories.WorkOrder.FK_Contract_Id &&
                                                        !x.IsDisabled);
                if (contractItem != null)
                {
                    var itemPrice = contractItem.Price;
                    var orderPrice = workOrderFactories.WorkOrder.Amount * itemPrice;
                    if (customer.Debit == 0 && (customer.CreditLimit - customer.Balance) >= orderPrice)
                    {
                        result = true;

                    }
                    else if (customer.Debit > 0 && customer.Debit >= orderPrice)
                    {
                        result = true;

                    }
                    else if (customer.Debit > 0 && customer.Debit < orderPrice)
                    {
                        var diff = orderPrice - customer.Debit;
                        if ((customer.CreditLimit - customer.Balance) >= diff)
                        {
                            result = true;

                        }
                        else
                        {
                            result = false;

                        }


                    }

                }
                return result;
            }
            catch (Exception e)
            {
                var message = e.Message;
                throw;
            }
        }

        public async Task<bool> ValidateCustomerBalanceWithinJob(WorkOrderFactories workOrderFactories, double amount)
        {
            try
            {

                bool result = false;
                var customerId = workOrderFactories.WorkOrder.FK_Customer_Id;
                var customer = await Context.Customers.FirstOrDefaultAsync(x => x.Id == customerId);
                var contractItem = await contractItemsManager.GetAllAsync().AsNoTracking()
                                    .FirstOrDefaultAsync(x => x.FK_Item_Id == workOrderFactories.WorkOrder.Items.Id
                                                        && x.FK_Contract_Id == workOrderFactories.WorkOrder.FK_Contract_Id &&
                                                        !x.IsDisabled);
                if (contractItem != null)
                {
                    var itemPrice = contractItem.Price;
                    var orderPrice = amount * itemPrice;
                    if (customer.Debit == 0 && (customer.CreditLimit - customer.Balance) >= orderPrice)
                    {
                        result = true;

                    }
                    else if (customer.Debit > 0 && customer.Debit >= orderPrice)
                    {
                        result = true;

                    }
                    else if (customer.Debit > 0 && customer.Debit < orderPrice)
                    {
                        var diff = orderPrice - customer.Debit;
                        if ((customer.CreditLimit - customer.Balance) >= diff)
                        {
                            result = true;

                        }
                        else
                        {
                            result = false;

                        }


                    }

                }
                return result;


            }
            catch (Exception e)
            {
                var message = e.Message;
                throw;
            }
        }


        private void UpdateCustomerBalance(WorkOrderFactories workOrderFactories, bool isEdit)
        {
            var customerId = workOrderFactories.WorkOrder.FK_Customer_Id;
            var customer = Context.Customers.FirstOrDefault(x => x.Id == customerId);
            if (isEdit)
            {
                var dbWorkOrder = Context.WorkOrders.AsNoTracking().FirstOrDefault(w => w.Id == workOrderFactories.WorkOrder.Id);
                var itemPrice = contractItemsManager.GetAllAsync().AsNoTracking()
                                .FirstOrDefault(x => x.FK_Item_Id == workOrderFactories.WorkOrder.Items.Id
                                                    && x.FK_Contract_Id == workOrderFactories.WorkOrder.FK_Contract_Id &&
                                                    !x.IsDisabled).Price;
                var orderPrice = workOrderFactories.WorkOrder.Amount * itemPrice;
                var dbOrderPrice = workOrderItemsManager.GetAllAsync().FirstOrDefault(x => x.FK_Workorder_Id == dbWorkOrder.Id).Price * dbWorkOrder.Amount;
                if (orderPrice > dbOrderPrice)
                {
                    var diff = orderPrice - dbOrderPrice;
                    if (customer.Debit > 0 && customer.Debit >= diff)
                    {
                        customer.Debit = customer.Debit - diff;

                    }
                    else if (customer.Debit < diff)
                    {
                        var customerDiff = diff - customer.Debit;
                        customer.Debit = 0;
                        customer.Balance = customer.Balance + customerDiff;

                    }
                    CustomerManager.UpdateAsync(customer);
                }
                else
                {
                    var diff = dbOrderPrice - orderPrice;

                    if (customer.Debit > 0)
                    {
                        customer.Debit = customer.Debit + diff;

                    }
                    else
                    {

                        customer.Balance = customer.Balance - diff;

                    }
                    CustomerManager.UpdateAsync(customer);
                }
            }
            else
            {
                var orderPrice = workOrderItemsManager.GetAllAsync().FirstOrDefault(x => x.FK_Workorder_Id == workOrderFactories.WorkOrder.Id).Price * workOrderFactories.WorkOrder.Amount;

                if (customer.Debit > 0 && customer.Debit >= orderPrice)
                {
                    customer.Debit = customer.Debit - orderPrice;

                }
                else if (customer.Debit < orderPrice)
                {
                    var customerDiff = orderPrice - customer.Debit;
                    customer.Debit = 0;
                    customer.Balance = customer.Balance + customerDiff;


                    CustomerManager.UpdateAsync(customer);
                }
            }
        }

        public async Task UpdateCustomerBalanceWithinJob(WorkOrderFactories workOrderFactories, double amount)
        {
            await Task.Run(async () =>
             {
                 var customerId = workOrderFactories.WorkOrder.FK_Customer_Id;
                 var customer = await Context.Customers.FirstOrDefaultAsync(x => x.Id == customerId);

                 var orderPrice = (await workOrderItemsManager.GetAllAsync().FirstOrDefaultAsync(x => x.FK_Workorder_Id == workOrderFactories.WorkOrder.Id)).Price * amount;

                 if (customer.Debit > 0 && customer.Debit >= orderPrice)
                 {
                     customer.Debit -= orderPrice;

                 }
                 else if (customer.Debit < orderPrice)
                 {
                     var customerDiff = orderPrice - customer.Debit;
                     customer.Debit = 0;
                     customer.Balance += customerDiff;


                     await CustomerManager.UpdateAsync(customer);
                 }




             });

        }

        public async Task UpdateCustomerBalanceAfterCancelJob(WorkOrder workOrder, double amount)
        {
            var customer = await Context.Customers.FirstOrDefaultAsync(x => x.Id == workOrder.FK_Customer_Id);

            var orderPrice = (await workOrderItemsManager.GetAllAsync().FirstOrDefaultAsync(x => x.FK_Workorder_Id == workOrder.Id)).Price * amount;


            if (customer.Balance > 0 && customer.Balance >= orderPrice)
            {
                customer.Balance -= orderPrice;
            }

            else if (customer.Balance > 0 && customer.Balance < orderPrice)
            {
                var cusDiff = orderPrice - customer.Balance;
                customer.Balance = 0;
                customer.Debit += cusDiff;
            }
            else if (customer.Balance == 0)
            {
                customer.Debit += orderPrice;

            }

            await CustomerManager.UpdateAsync(customer);


        }



        //private void UpdateContractItemAvailableAmount(WorkOrderFactories workOrderFactories)
        //{
        //    var items = workOrderFactories.WorkOrder.Items;
        //    var orderAmount = workOrderFactories.WorkOrder.Amount;
        //    var contractItems = contractItemsManager.GetByContractId(workOrderFactories.WorkOrder.FK_Contract_Id);
        //    var contractItem = contractItems.FirstOrDefault(x => x.FK_Item_Id == items.FirstOrDefault().Id);
        //    contractItem.AvailableAmount = contractItem.AvailableAmount - orderAmount;
        //    contractItemsManager.Update(contractItem);
        //}

        public Task<WorkOrder> Get(DataContext dataContext, params object[] id)
        {
            return dataContext.WorkOrders.FirstOrDefaultAsync(w => w.Id == (int)id[0] && w.IsDeleted == false);
        }

        private async Task<string> GeneratOrderUniqueCode()
        {

            var orderCode = Helper.GenerateUniqueId();
            for (int i = 0; i < 5; i++)
            {
                if (await IsOrderCodeExist(orderCode))
                {
                    orderCode = Helper.GenerateUniqueId();
                }
                else
                {
                    break;
                }
            }
            return orderCode;
        }
        private async Task<string> GeneratOrderUniqueCodeByDateAsync(string lastIdInDb)
        {
            var Date = DateTime.Now.Year.ToString();
            var orderCode = string.Concat(Date, "-", lastIdInDb);
            for (int i = 0; i < 5; i++)
            {
                if (await IsOrderCodeExist(orderCode))
                {
                    orderCode = string.Concat(orderCode, "0");
                }
                else
                {
                    break;
                }
            }
            return orderCode;
        }
        public Task<WorkOrder> GetAllOrdersByIdAsync(int ordrID)
        {
            return GetAllAsync().Include(w => w.Status)
                                     .Include(w => w.Customer)
                                     .Include(w => w.Priority)
                                    .Include(w => w.Items)
                                     .Include(w => w.Location)
                                     .Where(ord => ord.Id == ordrID).FirstOrDefaultAsync();


        }
        private Task<bool> IsOrderCodeExist(string orderCode)
        {
            return Task.Run(() =>
            {

                var count = Context.WorkOrders.Select(w => w.Code).Where(c => c.Equals(orderCode)).Count();
                return count > 0;
            });
        }

        public async Task<WorkOrderFactories> GetWorkOrdersById(int workOrderId)
        {
            WorkOrderFactories result = null;
            var order = await GetAsync(workOrderId);
            if (order != null)
            {
                var workOrderFac = await GetDetailedWorkOrder(order);
                if (workOrderFac != null)
                {
                    result = workOrderFac;
                }
            }
            return result;
        }

        //public List<WorkOrderFactories> GetDetailedWorkOrdersByCustomerId(int usrId)
        //{
        //    List<WorkOrderFactories> result = new List<WorkOrderFactories>();

        //    try
        //    {

        //        var orders = GetAllAsync()
        //                         .Include(w => w.Status)
        //                         .Include(w => w.Priority)
        //                         .Include(w => w.Location)
        //                         .Where(ord => ord.FK_Customer_Id == usrId);

        //        foreach (var order in orders)
        //        {
        //            var workOrderFac = await GetDetailedWorkOrder(order);
        //            if (workOrderFac != null)
        //            {
        //                result.Add(workOrderFac);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        throw;
        //    }
        //    return result;
        //}




        public async Task<List<WorkOrder>> GetTodaySubordersForDispatcher(string controllerId)
        {

            var factoryId = await controllerFactoryManager.GetControllerFactory(controllerId);
            var result = await GetTodaySubordersByFactoryId(factoryId);

            return result;
        }


        public async Task<PagedResult<WorkOrder>> GetDetailedWorkOrdersByCustomerIdAsync(PaginatedItems pagingParameterModel)
        {

            var allOrders = GetAllAsync()
                             .Include(w => w.Status)
                             .Include(w => w.Priority)
                              .Include(w => w.Items)
                             .Include(w => w.Location)
                             .Where(ord => ord.FK_Customer_Id == pagingParameterModel.Id).Where(x => x.IsDeleted == false);

            var orders = await GetAllByPaginationAsync(allOrders, pagingParameterModel);

            return orders;
        }

        public async Task<PagedResult<WorkOrder>> GetAllPendingAndApprovedOrdersAsync(PaginatedItems pagingParameterModel)
        {

            try
            {

                var pendingStatusId = (await statusManager.Get("pending")).Id;
                var approvedStatusId = (await statusManager.Get("approved")).Id;


                var result = GetAllAsync()
                                .Include(w => w.Factory)
                                .Include(w => w.Status)
                                .Include(w => w.Priority)
                                .Include(w => w.Location)
                                 .Include(w => w.ConfirmationHistories)
                                  .ThenInclude(x => x.ConfirmationStatus)
                                .Include(w => w.Contract)
                                                            .Include(w => w.Items)

                                .Include(w => w.Customer)
                                    .ThenInclude(c => c.Phones)
                                .Where(w => w.FK_Status_Id == pendingStatusId || w.FK_Status_Id == approvedStatusId).Where(x => x.IsDeleted == false);


                var orders = await GetAllByPaginationAsync(result, pagingParameterModel);

                foreach (var item in orders.Result)
                {
                    item.ConfirmationHistories = item.ConfirmationHistories.OrderByDescending(x => x.CreatedDate).Take(1).ToList();
                }
                return orders;
            }
            catch (Exception ex)
            {

                throw;
            }


            //foreach (var item in orders.Result)
            //{
            //    item.ConfirmationHistories = await orderConfirmationHistoryManager.GetLatestByOrderId(item.Id);
            //    //item.Items = workOrderItemsManager.GetItemsWorkOrder(item.Id);

            //}
        }


        private async Task<WorkOrderFactories> GetDetailedWorkOrder(WorkOrder order)
        {
            WorkOrderFactories result = null;
            if (order.FK_Parent_WorkOrder_Id == null && !order.IsDeleted)
            {
                result = new WorkOrderFactories();
                result.WorkOrder = order;
                //result.WorkOrder.Items = workOrderItemsManager.GetItemsWorkOrder(order.Id);
                result.SubOrdersFactoryPercent = new List<FactorySubOrderPercentage>();
                if (order.HasSuborders)
                {
                    var subOrders = await GetAllAsync().Where(ordrs => ordrs.FK_Parent_WorkOrder_Id == order.Id).ToListAsync();
                    foreach (var suborder in subOrders)
                    {
                        //suborder.Items = workOrderItemsManager.GetItemsWorkOrder(suborder.Id);
                        result.SubOrdersFactoryPercent.Add(await GetFactorySubOrderPercentage(suborder, true));
                    }
                }
                else
                {
                    result.SubOrdersFactoryPercent.Add(await GetFactorySubOrderPercentage(order, false));
                }

            }
            return result;
        }

        private async Task<FactorySubOrderPercentage> GetFactorySubOrderPercentage(WorkOrder order, bool hasParent)
        {
            var suborderFacPercentage = new FactorySubOrderPercentage();
            if (order.FK_Factory_Id != null)
            {
                suborderFacPercentage.Factory = await factoryManager.GetAsync(order.FK_Factory_Id);
                suborderFacPercentage.Percentage = order.Percentage;
            }
            else
            {
                suborderFacPercentage.Percentage = order.Percentage;
            }

            if (hasParent)
            {
                suborderFacPercentage.SubOrder = order;
            }
            return suborderFacPercentage;
        }

        private async Task<WorkOrder> UpdatePendingAmount(WorkOrder order)
        {
            var dbSuborder = await Context.WorkOrders.AsNoTracking().FirstOrDefaultAsync(w => w.Id == order.Id);
            var deliveredAmount = dbSuborder.Amount - dbSuborder.PendingAmount;
            order.PendingAmount = order.Amount - deliveredAmount;
            return order;
        }

        private async Task<WorkOrderFactories> AssignWorkOrder(WorkOrderFactories workOrderFactories)
        {
            workOrderFactories.WorkOrder = await UpdatePendingAmount(workOrderFactories.WorkOrder);
            workOrderFactories.WorkOrder.HasSuborders = false;
            workOrderFactories.WorkOrder.Percentage = 100;
            workOrderFactories.WorkOrder.FK_Factory_Id
                =
            workOrderFactories.SubOrdersFactoryPercent.FirstOrDefault().Factory.Id;
            //workOrderFactories.WorkOrder.FK_Status_Id = (int)statusManager.Get("dispatched")?.Id;

            await UpdateAsync(workOrderFactories.WorkOrder);
            return workOrderFactories;
        }

        private async Task<WorkOrderFactories> DivideWorkOrder(WorkOrderFactories workOrderFactories)
        {
            foreach (var SubOrderFacPercent in workOrderFactories.SubOrdersFactoryPercent)
            {
                if (SubOrderFacPercent != null && SubOrderFacPercent.Percentage > 0)
                {
                    SubOrderFacPercent.SubOrder = new WorkOrder();
                    await FillSubOrderProperties(workOrderFactories.WorkOrder, SubOrderFacPercent.SubOrder,
                        SubOrderFacPercent.Percentage, SubOrderFacPercent.Factory.Id);

                    //SubOrderFacPercent.SubOrder.FK_Status_Id = (int)statusManager.Get("dispatched")?.Id;

                    SubOrderFacPercent.SubOrder = await AddAsync(SubOrderFacPercent.SubOrder);
                    await workOrderItemsManager.AddItemsWorkOrder(SubOrderFacPercent.SubOrder);
                    SubOrderFacPercent.FK_SubOrder_Id = SubOrderFacPercent.Factory.Id;
                }
            }
            return workOrderFactories;
        }

        private async Task<WorkOrderFactories> EditWorkOrder(WorkOrderFactories workOrderFactories)
        {
            var dbSuborders = await GetSuborders(workOrderFactories.WorkOrder.Id);
            var updatedFactorySuborders = workOrderFactories.SubOrdersFactoryPercent.ToList();
            var parentOrder = workOrderFactories.WorkOrder;

            if (dbSuborders.Count > 0)
            {
                if (await IsCancelledOrder(parentOrder) ||
                await IsClosedOrder(parentOrder) ||
                 await IsCompletedOrder(parentOrder) ||
                await IsInProgressOrder(parentOrder))
                {
                    workOrderFactories = null;
                }
                else
                {
                    await DeleteAssignedSubOrdersPhysically(dbSuborders);

                    if (updatedFactorySuborders.Count > 1)
                    {
                        workOrderFactories.WorkOrder.HasSuborders = true;
                        //workOrderFactories.WorkOrder.FK_Status_Id = (int)statusManager.Get("dispatched")?.Id;
                        await UpdateAsync(workOrderFactories.WorkOrder);
                        workOrderFactories = await DivideWorkOrder(workOrderFactories);
                    }
                    else if (updatedFactorySuborders.Count == 1)
                    {
                        workOrderFactories = await AssignWorkOrder(workOrderFactories);
                    }
                }
            }
            else
            {
                if (updatedFactorySuborders.Count > 1)
                {
                    if (await IsCancelledOrder(parentOrder) ||
                       await IsClosedOrder(parentOrder) ||
                        await IsCompletedOrder(parentOrder) ||
                       await IsInProgressOrder(parentOrder))
                    {
                        workOrderFactories = null;
                    }
                    else
                    {
                        workOrderFactories.WorkOrder.HasSuborders = true;
                        //workOrderFactories.WorkOrder.FK_Status_Id = (int)statusManager.Get("dispatched")?.Id;
                        await UpdateAsync(workOrderFactories.WorkOrder);
                        workOrderFactories = await DivideWorkOrder(workOrderFactories);
                    }
                }
                else if (updatedFactorySuborders.Count == 1)
                {
                    if (await IsUpdatable(parentOrder))
                    {
                        workOrderFactories = await AssignWorkOrder(workOrderFactories);
                    }
                    else
                    {
                        workOrderFactories = null;
                    }
                }
            }

            if (dbSuborders.Count == 0)
            {
                await UpdateAsync(workOrderFactories.WorkOrder);
            }
            return workOrderFactories;
        }

        private async Task<bool> IsInProgressOrder(WorkOrder order)
        {
            bool result = false;
            var dbSuborder = await Context.WorkOrders.AsNoTracking().FirstOrDefaultAsync(w => w.Id == order.Id);
            var inProgressStatusId = (await statusManager.Get("in progress")).Id;
            if (dbSuborder.FK_Status_Id == inProgressStatusId)
            {
                result = true;
            }
            return result;
        }
        private async Task<bool> IsCompletedOrder(WorkOrder order)
        {
            bool result = false;
            var dbSuborder = await Context.WorkOrders.AsNoTracking().FirstOrDefaultAsync(w => w.Id == order.Id);
            var completedStatusId = (await statusManager.Get("completed")).Id;
            if (dbSuborder.FK_Status_Id == completedStatusId)
            {
                result = true;
            }
            return result;
        }
        private async Task<bool> IsCancelledOrder(WorkOrder order)
        {
            bool result = false;
            var dbSuborder = await Context.WorkOrders.AsNoTracking().FirstOrDefaultAsync(w => w.Id == order.Id);
            var cancelledStatusId = (await statusManager.Get("cancelled")).Id;
            if (dbSuborder.FK_Status_Id == cancelledStatusId)
            {
                result = true;
            }
            return result;
        }

        private async Task<bool> IsConfirmedByCustomer(WorkOrder order)
        {
            bool result = false;
            var dbSuborder = await Context.WorkOrders.AsNoTracking().FirstOrDefaultAsync(w => w.Id == order.Id);
            var confirmed = (await statusManager.Get("Confirmed by customer")).Id;
            if (dbSuborder.FK_Status_Id == confirmed)
            {
                result = true;
            }
            return result;
        }
        private async Task<bool> IsClosedOrder(WorkOrder order)
        {
            bool result = false;
            var dbSuborder = await Context.WorkOrders.AsNoTracking().FirstOrDefaultAsync(w => w.Id == order.Id);
            var cancelledStatusId = (await statusManager.Get("closed")).Id;
            if (dbSuborder.FK_Status_Id == cancelledStatusId)
            {
                result = true;
            }
            return result;
        }
        private async Task<bool> IsDispatchedOrder(WorkOrder order)
        {
            bool result = false;
            var dbSuborder = await Context.WorkOrders.AsNoTracking().FirstOrDefaultAsync(w => w.Id == order.Id);
            var dispatchedStatusId = (await statusManager.Get("dispatched")).Id;
            if (dbSuborder.FK_Status_Id == dispatchedStatusId)
            {
                result = true;
            }
            return result;
        }
        private async Task<bool> IsApprovedOrder(WorkOrder order)
        {
            bool result = false;
            var dbSuborder = await Context.WorkOrders.AsNoTracking().FirstOrDefaultAsync(w => w.Id == order.Id);
            var approvedStatusId = (await statusManager.Get("approved")).Id;
            if (dbSuborder.FK_Status_Id == approvedStatusId)
            {
                result = true;
            }
            return result;
        }
        private async Task<bool> IsPendingOrder(WorkOrder order)
        {
            bool result = false;
            var dbSuborder = await Context.WorkOrders.AsNoTracking().FirstOrDefaultAsync(w => w.Id == order.Id);
            var pendingStatusId = (await statusManager.Get("pending")).Id;
            if (dbSuborder.FK_Status_Id == pendingStatusId)
            {
                result = true;
            }
            return result;
        }

        private async Task<bool> IsDeletable(WorkOrder order)
        {
            bool result = false;
            var dbSuborder = await Context.WorkOrders.AsNoTracking().FirstOrDefaultAsync(w => w.Id == order.Id);
            var pendingStatusId = statusManager.Get("pending").Id;
            var cancelledStatusId = statusManager.Get("cancelled").Id;
            var closedStatusId = statusManager.Get("closed").Id;
            if (dbSuborder.FK_Status_Id == pendingStatusId ||
                dbSuborder.FK_Status_Id == cancelledStatusId ||
                dbSuborder.FK_Status_Id == closedStatusId)
            {
                result = true;
            }
            return result;
        }
        private async Task<bool> IsDeletable(List<WorkOrder> orders)
        {
            bool result = false;
            foreach (var order in orders)
            {
                result = await IsDeletable(order);
            }
            return result;
        }

        private async Task<bool> IsUpdatable(WorkOrder order)
        {
            bool result = false;
            var dbSuborder = Context.WorkOrders.AsNoTracking().FirstOrDefault(w => w.Id == order.Id);

            if (await IsPendingOrder(order) || await IsApprovedOrder(order) || await IsDispatchedOrder(order))
            {
                result = true;
            }
            else if (await IsInProgressOrder(order))
            {
                var deliveredAmount = dbSuborder.Amount - dbSuborder.PendingAmount;
                if (order.Amount > deliveredAmount)
                {
                    result = true;
                }
            }
            return result;
        }
        private async Task<bool> IsUpdatable(List<WorkOrder> orders)
        {
            bool result = false;
            foreach (var order in orders)
            {
                result = await IsUpdatable(order);
            }
            return result;
        }

        public async Task<bool> DeleteSuborder(int parentWorkOrderId, int factoryId)
        {
            var result = false;
            var suborder = await GetSuborder(parentWorkOrderId, factoryId);
            if (suborder != null)
            {
                string status = (await statusManager.GetAsync(suborder.FK_Status_Id)).Name;
                if (status.ToLower().Equals("pending") ||
                    status.ToLower().Equals("cancelled") ||
                    status.ToLower().Equals("closed"))
                {
                    if (await workOrderItemsManager.DeleteItemsWorkOrder(suborder.Id))
                        result = await base.DeleteAsync(suborder);
                }
            }
            return result;
        }
        public async Task<WorkOrder> UpdateworkOrderAsync(WorkOrder workOrder)
        {
            var order = Context.WorkOrders.FirstOrDefault(w => w.Id == workOrder.Id);
            order.FK_Status_Id = workOrder.FK_Status_Id;
            await base.UpdateAsync(order);
            return workOrder;


        }

        public async Task<WorkOrder> UpdateSuborderAsync(WorkOrder suborder)
        {
            bool isUpdated = false;
            WorkOrder dbSuborder;
            if (suborder.FK_Parent_WorkOrder_Id != null)
            {
                dbSuborder = await GetSuborder((int)suborder.FK_Parent_WorkOrder_Id, (int)suborder.FK_Factory_Id);
            }
            //else
            //{
            //    dbSuborder = Get(suborder.Id);
            //}
            var deliveredAmount = suborder.Amount - suborder.PendingAmount;
            if (suborder.Amount <= deliveredAmount)
            {
                isUpdated = false;
            }
            else
            {
                if (suborder.FK_Parent_WorkOrder_Id != null)
                {
                    var dbAmount = suborder.Amount;
                    var parenetOrder = await GetAsync(suborder.FK_Parent_WorkOrder_Id);
                    if (dbAmount > suborder.Amount)
                    {
                        parenetOrder.Amount -= (dbAmount - suborder.Amount);
                    }
                    else if (dbAmount < suborder.Amount)
                    {
                        parenetOrder.Amount += (suborder.Amount - dbAmount);
                    }
                    await UpdateAsync(parenetOrder);
                }
                await UpdateParentPendingAmount(suborder);
                suborder.Location = null;
                suborder.CopyProperties(suborder);
                isUpdated = await UpdateAsync(suborder);
            }
            if (isUpdated)
            {
                return suborder;
            }
            else
            {
                return null;
            }
        }

        private async Task UpdateParentPendingAmount(WorkOrder suborder)
        {
            if (suborder.PendingAmount <= 0)
            {
                suborder.FK_Status_Id = statusManager.Get("completed").Id;
                await equipmentManager.UnassignAllEquipmentFromWorkOrder(suborder.Id);
            }
            if (suborder.FK_Parent_WorkOrder_Id != null)
            {
                var parenetOrder = await GetAsync(suborder.FK_Parent_WorkOrder_Id);
                var deliveredAmount = suborder.Amount - suborder.PendingAmount;
                parenetOrder.PendingAmount = parenetOrder.Amount - deliveredAmount;
            }
        }

        public async Task<WorkOrder> UpdateSuborder(WorkOrder suborder, DataContext dataContext)
        {
            bool isUpdated = false;
            WorkOrder dbSuborder;
            if (suborder.FK_Parent_WorkOrder_Id != null)
            {
                dbSuborder = await GetSuborder((int)suborder.FK_Parent_WorkOrder_Id, (int)suborder.FK_Factory_Id, dataContext);
            }
            else
            {
                dbSuborder = dataContext.WorkOrders.FirstOrDefault(w => w.Id == suborder.Id && w.IsDeleted == false);
            }
            var deliveredAmount = dbSuborder.Amount - dbSuborder.PendingAmount;
            if (suborder.Amount <= deliveredAmount)
            {
                isUpdated = false;
            }
            else
            {
                if (suborder.FK_Parent_WorkOrder_Id != null)
                {
                    var dbAmount = dbSuborder.Amount;
                    var parenetOrder = dataContext.WorkOrders.FirstOrDefault(w => w.Id == suborder.FK_Parent_WorkOrder_Id && w.IsDeleted == false);
                    if (dbAmount > suborder.Amount)
                    {
                        parenetOrder.Amount = parenetOrder.Amount - (dbAmount - suborder.Amount);
                    }
                    else if (dbAmount < suborder.Amount)
                    {
                        parenetOrder.Amount = parenetOrder.Amount + (suborder.Amount - dbAmount);
                    }
                    await UpdateAsync(parenetOrder, dataContext);
                }
                UpdateParentPendingAmount(suborder, dataContext);
                suborder.Location = null;
                suborder.CopyProperties(dbSuborder);
                isUpdated = await UpdateAsync(dbSuborder, dataContext);
            }
            if (isUpdated)
            {
                return suborder;
            }
            else
            {
                return null;
            }
        }

        private void UpdateParentPendingAmount(WorkOrder suborder, DataContext dataContext)
        {
            if (suborder.PendingAmount <= 0)
            {
                suborder.FK_Status_Id = statusManager.Get("completed", dataContext: dataContext).Id;
                equipmentManager.UnassignAllEquipmentFromWorkOrder(suborder.Id, dataContext);

            }
            if (suborder.FK_Parent_WorkOrder_Id != null)
            {
                var parenetOrder = dataContext.WorkOrders.FirstOrDefault(w => w.Id == suborder.FK_Parent_WorkOrder_Id && w.IsDeleted == false);
                var deliveredAmount = suborder.Amount - suborder.PendingAmount;
                parenetOrder.PendingAmount = parenetOrder.Amount - deliveredAmount;
                if (parenetOrder.PendingAmount <= 0)
                {
                    parenetOrder.FK_Status_Id = statusManager.Get("completed", dataContext: dataContext).Id;
                }
            }
        }

        public async Task ChangeStatusToInProgress(int orderId)
        {
            var inProgressStatusId = (await statusManager.Get("in progress")).Id;
            var order = await GetAsync(orderId);
            order.FK_Status_Id = inProgressStatusId;
            if (order.FK_Parent_WorkOrder_Id != null)
            {
                var parentOrder = await GetAsync(order.FK_Parent_WorkOrder_Id);
                parentOrder.FK_Status_Id = inProgressStatusId;
                await UpdateAsync(parentOrder);

            }
            await UpdateAsync(order);
        }

        public async Task ChangeStatusToDispatched(int orderId)
        {
            var isDispatched = (await statusManager.Get("dispatched")).Id;
            var order = await GetAsync(orderId);
            order.FK_Status_Id = isDispatched;
            if (order.FK_Parent_WorkOrder_Id != null)
            {
                var parentOrder = await GetAsync(order.FK_Parent_WorkOrder_Id);
                parentOrder.FK_Status_Id = isDispatched;
                await UpdateAsync(parentOrder);

            }
            await UpdateAsync(order);
        }

        private async Task<bool> SuborderExists(int parentWorkOrderId, int factoryId)
        {
            var suborders = await GetAllAsync().Where(order => order.FK_Parent_WorkOrder_Id == parentWorkOrderId && order.FK_Factory_Id == factoryId).ToListAsync();
            var result = false;
            if (suborders.Count > 0)
            {
                result = true;
            }
            return result;
        }

        private Task<List<WorkOrder>> GetSuborders(int parentWorkOrderId)
        {
            return GetAllAsync().Include(x => x.Items).Include(x => x.ParentWorkOrder).Where(order => order.FK_Parent_WorkOrder_Id == parentWorkOrderId).ToListAsync();
            //result = FillNavProperties(result);
        }

        private async Task<bool> DeleteSuborders(int parentWorkorderId)
        {
            bool result = false;
            var suborders = await GetSuborders(parentWorkorderId);
            if (suborders.Count == 0)
            {
                return true;
            }
            foreach (var item in suborders)
            {
                result = await DeleteSuborder(parentWorkorderId, (int)item.FK_Factory_Id);
            }
            return result;
        }

        private Task<WorkOrder> GetSuborder(int parentWorkOrderId, int factoryId)
        {
            return GetAllAsync().FirstOrDefaultAsync(order => order.FK_Parent_WorkOrder_Id == parentWorkOrderId && order.FK_Factory_Id == factoryId);

        }

        private Task<WorkOrder> GetSuborder(int parentWorkOrderId, int factoryId, DataContext dataContext)
        {
            return dataContext.WorkOrders.FirstOrDefaultAsync(order => order.FK_Parent_WorkOrder_Id == parentWorkOrderId &&
                                                                         order.FK_Factory_Id == factoryId);

        }

        private async Task<WorkOrder> FillSubOrderBasicProperties(WorkOrder parentOrder, WorkOrder subOrder)
        {
            subOrder.Items = parentOrder.Items;
            subOrder.FK_Parent_WorkOrder_Id = parentOrder.Id;
            subOrder.FK_Customer_Id = parentOrder.FK_Customer_Id;
            subOrder.FK_Contract_Id = parentOrder.FK_Contract_Id;
            subOrder.FK_Location_Id = parentOrder.FK_Location_Id;
            subOrder.FK_Priority_Id = parentOrder.FK_Priority_Id;
            subOrder.FK_Status_Id = parentOrder.FK_Status_Id;
            subOrder.IsDeleted = parentOrder.IsDeleted;
            subOrder.Code = await GeneratOrderUniqueCode();
            subOrder.StartDate = parentOrder.StartDate;
            subOrder.EndDate = parentOrder.EndDate;
            subOrder.CurrentUser = parentOrder.CurrentUser;
            return subOrder;
        }

        private async Task<WorkOrder> FillSubOrderProperties(WorkOrder parentOrder, WorkOrder subOrder, double percentage, int factoryId)
        {
            subOrder = await FillSubOrderBasicProperties(parentOrder, subOrder);
            subOrder.Amount = (parentOrder.Amount) * (percentage) / 100;
            subOrder.PendingAmount = subOrder.Amount;
            subOrder.HasSuborders = false;
            subOrder.FK_Factory_Id = factoryId;
            subOrder.Percentage = percentage;
            return subOrder;
        }

        private async Task<bool> DeleteAssignedSubOrders(int parentWorkOrderId)
        {
            bool result = false;
            var suborders = await GetSuborders(parentWorkOrderId);
            if (suborders != null)
            {
                result = await DeleteAssignedSubOrders(suborders);
            }
            return result;
        }
        private async Task<bool> DeleteAssignedSubOrdersPhysically(List<WorkOrder> orders)
        {
            bool result = false;
            foreach (var suborder in orders)
            {
                if (await workOrderItemsManager.DeleteItemsWorkOrderPhysically(suborder.Id))
                {
                    Context.WorkOrders.Remove(suborder);
                    result = true;
                }
            }
            return result;
        }
        private async Task<bool> DeleteAssignedSubOrders(List<WorkOrder> orders)
        {
            bool result = false;
            foreach (var suborder in orders)
            {
                if (await workOrderItemsManager.DeleteItemsWorkOrder(suborder.Id))
                {
                    result = await DeleteAsync(suborder);
                }
            }
            return result;
        }

        public async Task<WorkOrder> ChangeWorkOrderStatus(WorkOrder workOrder, int statusId)
        {
            if (await statusManager.IsCancelled(statusId) ||
                await statusManager.IsCompleted(statusId) ||
                await statusManager.IsClosed(statusId))
            {
                await equipmentManager.UnassignAllEquipmentFromWorkOrder(workOrder.Id);
            }
            //if (statusManager.IsCancelled(statusId))
            //{
            //    CancelOrder(workOrder);
            //}
            workOrder.FK_Status_Id = statusId;
            bool isUpdated = await UpdateAsync(workOrder);
            if (isUpdated)
            {
                return workOrder;
            }
            else
            {
                return null;
            }
        }

        public override async Task<bool> DeleteAsync(WorkOrder entity)
        {
            bool result = false;
            if (await IsPendingOrder(entity) || await IsApprovedOrder(entity) || await IsConfirmedByCustomer(entity))
            {
                if (await DeleteSuborders(entity.Id))
                {
                    if (await orderConfirmationHistoryManager.DeleteByOrderId(entity.Id))
                    {
                        //ReleaseContractItemsAmount(entity);
                        await equipmentManager.UnassignAllEquipmentFromWorkOrder(entity.Id);

                        if (await workOrderItemsManager.DeleteItemsWorkOrder(entity.Id))
                        {
                            result = await base.DeleteAsync(entity);
                        }
                    }
                }
            }
            return result;
        }

        public override async Task<bool> Delete(List<WorkOrder> entitylst)
        {
            bool result = false;
            foreach (var entity in entitylst)
            {
                result = await DeleteAsync(entity);
            }
            return result;
        }

        //private void ReleaseContractItemsAmount(WorkOrder entity)
        //{
        //    var contractItems = contractItemsManager.GetByContractId(entity.FK_Contract_Id);
        //    var orderItemIds = workOrderItemsManager.GetItemsWorkOrder(entity.Id).Select(i => i.Id).ToList();
        //    foreach (var itemId in orderItemIds)
        //    {
        //        var contractItem = contractItems.FirstOrDefault(ci => ci.FK_Item_Id == itemId);
        //        contractItem.AvailableAmount = contractItem.AvailableAmount + entity.Amount;
        //        contractItemsManager.Update(contractItem);
        //    }
        //}

        public Task<List<WorkOrder>> GetParentOrdersByContractId(int contractId)
        {
            return GetAllAsync().Include(x => x.Items).Include(x => x.ParentWorkOrder).Where(w => w.FK_Contract_Id == contractId && w.FK_Parent_WorkOrder_Id == null).ToListAsync();
            //result = FillNavProperties(result);

        }

        public async Task<bool> DeleteByContractId(int contractId)
        {
            var result = false;
            var workorders = await GetParentOrdersByContractId(contractId);
            result = await Delete(workorders);
            return result;
        }

        public async Task<bool> IsContractHasOrders(int contractId)
        {
            var workorders = await GetParentOrdersByContractId(contractId);
            return workorders.Count > 0;
        }
        public Task<List<WorkOrder>> GetByContractId(int contractId)
        {
            return GetAllAsync().Include(x => x.Items).Include(x => x.ParentWorkOrder).Where(w => w.FK_Contract_Id == contractId).ToListAsync();
            //result = FillNavProperties(result);
        }
        public Task<List<WorkOrder>> GetByStartDate(DateTime startDate, DataContext dataContext)
        {
            return dataContext.WorkOrders.Include(x => x.Items).Include(x => x.ParentWorkOrder).Where(w => w.StartDate.Date.Equals(startDate.Date) && !w.IsDeleted).ToListAsync();
            //result = FillNavProperties(result);
        }

        public List<WorkOrder> GetByStartDateSync(DateTime startDate, DataContext dataContext)
        {
            return dataContext.WorkOrders.Include(x => x.Items).Include(x => x.ParentWorkOrder).Where(w => w.StartDate.Date.Equals(startDate.Date) && !w.IsDeleted).ToList();
            //result = FillNavProperties(result);
        }


        public Task<WorkOrder> GetByCode(string workOrderCode)
        {
            return GetAllAsync().Include(x => x.Items).Include(x => x.ParentWorkOrder).FirstOrDefaultAsync(w => w.Code.ToLower().Equals(workOrderCode.ToLower()));
            //result = FillNavProperties(new List<WorkOrder> { result }).First();

        }

        public Task<List<WorkOrder>> GetByFactoryId(int factoryId)
        {
            return GetAllAsync().Include(x => x.Items).Include(x => x.ParentWorkOrder)
                .Where(order => order.FK_Factory_Id == factoryId).ToListAsync();
        }

        public async Task<WorkOrder> AssignToFactory(WorkOrder workOrder, int factoryId)
        {
            if (factoryId > 0)
            {
                workOrder.FK_Factory_Id = factoryId;
                //workOrder.FK_Status_Id = (int)statusManager.Get("dispatched")?.Id;

                bool isUpdated = await UpdateAsync(workOrder);
                if (isUpdated)
                {
                    return workOrder;
                }
                else
                {
                    return null;
                }
            }

            else
            {
                return null;

            }
        }

        public async Task<bool> CanAssign(int workOrderId, int? factoryId, string googleServiceUrl, string googleApiKey)
        {
            int workorderLocationId = (await GetAsync(workOrderId)).FK_Location_Id;
            Location workorderLocation = await locationManager.GetAsync(workorderLocationId);
            Factory factory = await factoryManager.GetAsync(factoryId);
            if (workorderLocation.Latitude != 0 && workorderLocation.Longitude != 0)
            {
                long travelTime = GoogleMapsAPIHelper.GetTravelTime(googleServiceUrl, googleApiKey,
                                factory.Latitude, factory.Longitude,
                                workorderLocation.Latitude, workorderLocation.Longitude);

                int itemId = (await workOrderItemsManager.GetItemsWorkOrder(workOrderId)).FirstOrDefault().Id;

                LKPConditionManager lKPConditionManager = new LKPConditionManager(Context);
                var condition = await lKPConditionManager.Get("Validity Time");
                LKP_Condition_ItemManager lKP_Condition_ItemManager = new LKP_Condition_ItemManager(Context);

                var itemCondition = await lKP_Condition_ItemManager.Get(itemId, condition.Id);
                if (itemCondition == null)
                {
                    itemCondition = new LKP_Condition_Item { Value = "2" };
                }
                double validityTimeHrs = Convert.ToDouble(itemCondition.Value);
                double travelTimeHrs = (Convert.ToDouble(travelTime) / 60) / 60;

                return validityTimeHrs > travelTimeHrs;
            }
            else
            {
                return true;
            }

        }

        public async Task<PaginatedItemsViewModel<WorkOrderReportResult>> FilterWithPaging(int pageIndex, int pageSize, List<ReportFilter> filters)
        {
            try
            {
                var predicate = PredicateBuilder.True<WorkOrder>();
                foreach (var filter in filters)
                {
                    if (filter.Type.ToLower().Contains("amount") ||
                        filter.Type.ToLower().Contains("date"))
                    {

                        predicate = predicate.And(CreateGreaterThanOrLessThanSingleExpression(filter.Type, filter.Value));
                    }
                    else
                    {
                        predicate = predicate.And(CreateEqualSingleExpression(filter.Type, filter.Value));
                    }
                }
                predicate = predicate.And(wo => wo.IsDeleted == false);
                var count = Context.WorkOrders.Where(predicate).Count();
                List<WorkOrderReportResult> orders = new List<WorkOrderReportResult>();

                orders = await Context.WorkOrders
                          .Include(w => w.Contract)
                              .ThenInclude(c => c.ContractType)
                          .Include(w => w.Customer)
                                                   .ThenInclude(c => c.Phones)

                          .Include(w => w.Factory)
                                                   .ThenInclude(c => c.Vehicles)

                          .Include(w => w.Location)

                          .Include(w => w.Jobs)
                          .Include(w => w.Priority)
                                                      .Include(w => w.ElementToBePoured)

                          .Include(w => w.Status)
                          .Where(predicate)
                          .Select(w => new WorkOrderReportResult
                          {
                              Id = w.Id,
                              Customer_ID = w.Customer == null ? 0 : w.Customer.Id,
                              EngStatus = w.Status == null ? "" : w.Status.Name,
                              ArStatus = w.Status == null ? "" : w.Status.ArabicName,
                              Priority = w.Priority == null ? "" : w.Priority.Name,
                              Code = w.Code,
                              Amount = w.Amount,
                              KCRM_Customer_Id = w.Customer == null ? "" : w.Customer.KCRM_Customer_Id,
                              EndDate = w.EndDate,
                              SelectedLang = w.Location == null ? "" : w.Location.SelectedLang,
                              Location = w.Location == null ? "" : string.Concat(w.Location.Governorate, ",", w.Location.Area),
                              LocationBlock = w.Location == null ? "" : string.IsNullOrWhiteSpace(w.Location.Block) ? null : w.Location.Block,
                              LocationStreet = w.Location == null ? "" : string.IsNullOrWhiteSpace(w.Location.Street) ? null : w.Location.Street,
                              LocationRemarks = w.Location == null ? "" : string.IsNullOrWhiteSpace(w.Location.AddressNote) ? null : w.Location.AddressNote,
                              Customer_Name = w.Customer == null ? "" : w.Customer.Name,
                              Customer_Phone = w.Customer == null ?null : w.Customer.Phones,
                              Poured_Element = w.ElementToBePoured==null?"": w.ElementToBePoured.Name,
                              FactoryName = w.Factory == null ? "" : w.Factory.Name,
                              StartDate = w.StartDate,
                              CreatedDate = w.CreatedDate,
                              Description = w.Description,

                          }).ToListAsync();


                return new PaginatedItemsViewModel<WorkOrderReportResult>(pageIndex, pageSize, count, orders);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<WorkOrderReportResultToFile>> Filter(List<ReportFilter> filters)
        {
            var predicate = PredicateBuilder.True<WorkOrder>();
            foreach (var item in filters)
            {
                if (item.Type.ToLower().Contains("amount") ||
                    item.Type.ToLower().Contains("date"))
                {
                    predicate = predicate.And(CreateGreaterThanOrLessThanSingleExpression(item.Type, item.Value));
                }
                else
                {
                    predicate = predicate.And(CreateEqualSingleExpression(item.Type, item.Value));
                }
            }
            predicate = predicate.And(wo => wo.IsDeleted == false);
            var orders = await Context.WorkOrders
                         .Include(w => w.Contract)
                             .ThenInclude(c => c.ContractType)
                         .Include(w => w.Customer)
                         .Include(w => w.Factory)
                         .Include(w => w.Jobs)
                         .Include(w => w.Priority)
                         .Include(w => w.Status)
                         .Where(predicate)
                         .Select(w => new WorkOrderReportResultToFile
                         {
                             Id = w.Id,
                             Amount = w.Amount,
                             Status = w.Status.Name,
                             Priority = w.Priority.Name,
                             Code = w.Code,
                             ContractEndDate = w.Contract.EndDate,
                             EndDate = w.EndDate,
                             PendingAmount = w.PendingAmount,
                             ContractNumber = w.Contract.ContractNumber,
                             CustomerName = w.Customer.Name,
                             FactoryName = w.Factory.Name,
                             ContractStartDate = w.Contract.StartDate,
                             StartDate = w.StartDate,
                             CreatedDate = w.CreatedDate,
                             CustomerCivilId = w.Customer.CivilId,
                             CustomerCompanyName = w.Customer.CompanyName,
                             CustomerDivision = w.Customer.Division,
                             JobsCount = w.Jobs.Count,
                             Percentage = w.Percentage,
                             UpdatedDate = w.UpdatedDate
                         }).ToListAsync();
            return orders;
        }

        public async Task<PagedResult<WorkOrder>> FilterSuborders(List<ReportFilter> filters)
        {
            var predicate = PredicateBuilder.True<WorkOrder>();
            DateTime date = DateTime.Now;
            foreach (var item in filters)
            {
                if (item.Type.IndexOf("amount", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                    item.Type.IndexOf("date", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    predicate = predicate.And(CreateGreaterThanOrLessThanSingleExpression(item.Type, item.Value));
                }
                else if (item.Type.ToLower().Equals("customername"))
                {
                    predicate = predicate.And(w => w.Customer.Name.ToLower().Trim().Contains(item.Value.ToString().ToLower().Trim()));


                }
                else if (item.Type.ToLower().Equals("locationtitle"))
                {
                    predicate = predicate.And(w => w.Location.Title.ToLower().Trim().Equals(item.Value.ToString().ToLower()) );

                }
                else if (item.Type.ToLower().Equals("phonecus"))
                {
                    var phone = (await _customerPhoneManager.GetBy(item.Value.ToString().ToLower().Trim())).ToList();
                   var customerIDS= phone.Select(w => w.Fk_Customer_Id).ToList();
                    if (customerIDS != null)
                    {
                        predicate = predicate.And(w =>customerIDS.Contains(w.FK_Customer_Id));
                    }

                }
                else if (item.Type.Equals("kcrmid", StringComparison.CurrentCultureIgnoreCase))
                {
                    predicate = predicate.And(w => w.Customer.KCRM_Customer_Id.ToLower().Trim().Equals(item.Value.ToString().ToLower().Trim()));
                }
                else
                {
                    predicate = predicate.And(CreateEqualSingleExpression(item.Type, item.Value));
                }
            }
            predicate = predicate.And(wo => wo.IsDeleted == false);
            var result = GetAllAsync()
                .Include(w => w.Location)
                .Include(w => w.Status)
                .Include(w => w.ParentWorkOrder)
                .Include(w => w.Contract)
                .Include(w => w.Priority)
                .Include(w => w.Items)
                .Include(w => w.Customer)
                    .ThenInclude(c => c.Phones)
                .Where(predicate);

            var orders = await GetAllByPaginationAsync(result, filters[0].pagingParameterModel);

            foreach (var item in orders.Result)
            {
                item.ConfirmationHistories = await orderConfirmationHistoryManager.GetLatestByOrderId(item.Id);
            }
            return orders;
        }



        public async Task<List<WorkOrder>> FilterSubordersForDispatcher(List<ReportFilter> filters)
        {
            var predicate = PredicateBuilder.True<WorkOrder>();
            DateTime date = DateTime.Now;
            foreach (var item in filters)
            {
                if (item.Type.IndexOf("amount", StringComparison.CurrentCultureIgnoreCase) >= 0 ||
                    item.Type.IndexOf("date", StringComparison.CurrentCultureIgnoreCase) >= 0)
                {
                    predicate = predicate.And(CreateGreaterThanOrLessThanSingleExpression(item.Type, item.Value));
                }
                else if (item.Type.Equals("customername", StringComparison.CurrentCultureIgnoreCase))
                {
                    predicate = predicate.And(w => w.Location.Title.ToLower().Trim().Equals(item.Value.ToString().ToLower()) || w.Customer.Name.ToLower().Trim().Contains(item.Value.ToString().ToLower().Trim()));


                }
                else if (item.Type.Equals("locationtitle", StringComparison.CurrentCultureIgnoreCase))
                {
                    predicate = predicate.And(w => w.Location.Title.ToLower().Trim().Equals(item.Value.ToString().ToLower()) || w.Customer.Name.ToLower().Trim().Contains(item.Value.ToString().ToLower().Trim()));

                }
                else if (item.Type.ToLower().Equals("phonecus"))
                {
                    var phone = (await _customerPhoneManager.GetBy(item.Value.ToString().ToLower().Trim())).ToList();
                    var customerIDS = phone.Select(w => w.Fk_Customer_Id).ToList();
                    if (customerIDS != null)
                    {
                        predicate = predicate.And(w => customerIDS.Contains(w.FK_Customer_Id));
                    }

                }
                else if (item.Type.Equals("kcrmid", StringComparison.CurrentCultureIgnoreCase))
                {
                    predicate = predicate.And(w => w.Customer.KCRM_Customer_Id.ToLower().Trim().Equals(item.Value.ToString().ToLower().Trim()));
                }
                else
                {
                    predicate = predicate.And(CreateEqualSingleExpression(item.Type, item.Value));
                }
            }
            predicate = predicate.And(wo => wo.IsDeleted == false);
            var result = await GetAllAsync()
                .Include(w => w.Location)
                .Include(w => w.Status)
                .Include(w => w.ParentWorkOrder)
                .Include(w => w.Contract)
                .Include(w => w.Priority)
                .Include(w => w.Items)
                .Include(w => w.Customer)
                    .ThenInclude(c => c.Phones)
                .Where(predicate).ToListAsync();


            foreach (var item in result)
            {
                item.ConfirmationHistories = await orderConfirmationHistoryManager.GetLatestByOrderId(item.Id);
            }
            return result;
        }


        public async Task<List<WorkOrder>> GetTodaySuborders(bool isMvmtUser)
        {
            var approvedStatusId = (await statusManager.Get("approved")).Id;
            var pendingStatusId = (await statusManager.Get("pending")).Id;
            var dispatchedStatusId = (await statusManager.Get("dispatched")).Id;
            var result = new List<WorkOrder>();
            if (isMvmtUser)
            {
                result = await GetAllAsync()
                .Include(w => w.Location)
                .Include(w => w.Factory)
                .Include(w => w.Contract)
                  .Include(w => w.Items)
                .Include(w => w.ParentWorkOrder)


                .Include(w => w.Customer)
                    .ThenInclude(c => c.Phones)

                .Include(w => w.Status)
                .Include(w => w.Priority)
                .Where(w => w.HasSuborders == false &&
                            //w.StartDate.Date <= DateTime.UtcNow.Date &&
                            (w.FK_Status_Id == approvedStatusId ||
                             w.FK_Status_Id == pendingStatusId)).ToListAsync();
            }
            else
            {
                var inProgressStatusId = statusManager.Get("in progress").Id;

                result = await GetAllAsync()
                .Include(w => w.Location)
                .Include(w => w.Factory)
                 .Include(w => w.ParentWorkOrder)
                .Include(w => w.Customer)
                                    .ThenInclude(c => c.Phones)

                .Include(w => w.Status)
                .Include(w => w.Priority)
                .Where(w => w.HasSuborders == false &&
                            w.StartDate.Date <= DateTime.Today &&
                            (w.FK_Status_Id == approvedStatusId ||
                             w.FK_Status_Id == pendingStatusId ||
                             w.FK_Status_Id == inProgressStatusId ||
                             w.FK_Status_Id == dispatchedStatusId)).ToListAsync();
            }
            //result = FillNavProperties(result);

            return result;
        }


        public async Task<PagedResult<WorkOrder>> GetTodaySubordersAsync(bool isMvmtUser, PaginatedItems pagingParameterModel)
        {
            var approvedStatusId = (await statusManager.Get("approved")).Id;
            var pendingStatusId = (await statusManager.Get("pending")).Id;
            var dispatchedStatusId = (await statusManager.Get("dispatched")).Id;
            var declinedId = (await statusManager.Get("Declined")).Id;
            IQueryable<WorkOrder> result;
            if (isMvmtUser)
            {
                result = GetAllAsync()
                .Include(w => w.Location)
                .Include(w => w.Factory)
                .Include(w => w.Contract)
                  .Include(w => w.Items)
                .Include(w => w.ParentWorkOrder)
                .Include(w => w.Customer)
                    .ThenInclude(c => c.Phones)

                .Include(w => w.Status)
                .Include(w => w.Priority)
                .Where(w => w.HasSuborders == false &&
                            //w.StartDate.Date <= DateTime.UtcNow.Date &&
                            (w.FK_Status_Id == approvedStatusId || w.FK_Status_Id == declinedId ||
                             w.FK_Status_Id == pendingStatusId));
            }
            else
            {
                var inProgressStatusId = statusManager.Get("in progress").Id;

                result = GetAllAsync()
                .Include(w => w.Location)
                .Include(w => w.Factory)
                 .Include(w => w.ParentWorkOrder)
                .Include(w => w.Customer)
                                    .ThenInclude(c => c.Phones)

                .Include(w => w.Status)
                .Include(w => w.Priority)
                .Where(w => w.HasSuborders == false &&
                            w.StartDate.Date <= DateTime.Today &&
                            (w.FK_Status_Id == approvedStatusId ||
                             w.FK_Status_Id == pendingStatusId ||
                             w.FK_Status_Id == inProgressStatusId ||
                             w.FK_Status_Id == dispatchedStatusId));
            }
            //result = FillNavProperties(result);

            var orders = await GetAllByPaginationAsync(result, pagingParameterModel);


            return orders;
        }



        private async Task<List<WorkOrder>> GetPendingSuborders()
        {
            var pendingStatusId = statusManager.Get("pending").Id;

            var result = await GetAllAsync()
                .Include(w => w.Location)
                .Include(w => w.Status)
                .Include(w => w.Customer)
                .Include(w => w.Items)
                .Include(w => w.ParentWorkOrder)
                .Where(w => w.HasSuborders == false &&
                             w.FK_Status_Id == pendingStatusId).ToListAsync();

            //result = FillNavProperties(result);
            return result;
        }

        public async Task<List<WorkOrder>> GetPendingUnassignedOrders()
        {
            var result = new List<WorkOrder>();
            var pendingStatusId = statusManager.Get("pending").Id;

            result = await GetAllAsync()
                        .Include(w => w.Status)
                        .Include(w => w.Priority)
                        .Include(w => w.Customer)
                           .Include(w => w.Items)
                .Include(w => w.ParentWorkOrder)
                        .Where(w => w.FK_Status_Id == pendingStatusId && w.FK_Factory_Id == null && !w.HasSuborders)
                        .ToListAsync();
            //result = FillNavProperties(result);
            return result;
        }

        public async Task<List<WorkOrder>> GetTodaySubordersByFactoryId(int factoryId)
        {
            var dispatchedStatusId = (await statusManager.Get("dispatched")).Id;
            var inProgressStatusId = (await statusManager.Get("in progress")).Id;

            return (await GetTodaySuborders(false)).Where(order => order.FK_Factory_Id == factoryId &&
                                                          (order.FK_Status_Id == inProgressStatusId ||
                                                          order.FK_Status_Id == dispatchedStatusId)).ToList();
            //result = FillNavProperties(result);
        }


        public async Task<List<WorkOrder>> GetCalendarSuborders(string controllerId)
        {
            var completedStatusId = statusManager.Get("completed").Id;

            var result = GetAllAsync()
              .Include(w => w.Location)
              .Include(w => w.Status)
              .Include(w => w.Priority)
              .Where(w => w.HasSuborders == false &&
                          w.StartDate.Date <= DateTime.Today &&
                           w.FK_Status_Id == completedStatusId).ToList();

            result.AddRange(await GetTodaySuborders(true));
            return result;
        }

        private async Task<PagedResult<WorkOrder>> GetTodaySubordersWithAllPending(bool isMvmtUser, PaginatedItems PaginatedItems)
        {
            var result = await GetTodaySubordersAsync(isMvmtUser, PaginatedItems);// get all the orders 
            //var pendingSuborders = await GetPendingSuborders();
            //foreach (var item in pendingSuborders)
            //{
            //    if (!result.Select(w => w.Id).Contains(item.Id))
            //    {
            //        result.Add(item);
            //    }
            //}
            return result;
        }

        public Task<List<WorkOrder>> GetAllSuborders()
        {
            var approvedStatusId = statusManager.Get("approved").Id;
            var inProgressStatusId = statusManager.Get("in progress").Id;
            var pendingStatusId = statusManager.Get("pending").Id;
            var dispatchedStatusId = statusManager.Get("dispatched").Id;

            var result = GetAllAsync()
                .Include(w => w.Location)
                                .Include(w => w.Items)
                                                                .Include(w => w.ParentWorkOrder)


                .Where(w => w.HasSuborders == false &&
                            (w.FK_Status_Id == approvedStatusId ||
                             w.FK_Status_Id == pendingStatusId ||
                             w.FK_Status_Id == inProgressStatusId ||
                             w.FK_Status_Id == dispatchedStatusId)).ToListAsync();

            //result = FillNavProperties(result);

            return result;
        }

        public async Task<List<WorkOrder>> GetSubordersByFactoryId(int factoryId)
        {
            var dispatchedStatusId = (await statusManager.Get("dispatched")).Id;
            var inProgressStatusId = (await statusManager.Get("in progress")).Id;

            return (await GetAllSuborders()).Where(order => order.FK_Factory_Id == factoryId &&
                                                          (order.FK_Status_Id == inProgressStatusId ||
                                                          order.FK_Status_Id == dispatchedStatusId)).ToList();
            //result = FillNavProperties(result);
        }

        public async Task<List<WorkOrder>> GetAllSuborders(string controllerId)
        {
            List<WorkOrder> result = new List<WorkOrder>();
            bool isDispatcherUser = await applicationUserManager.IsUserInRole(controllerId, "Dispatcher");

            if (isDispatcherUser)
            {
                var factoryId = await controllerFactoryManager.GetControllerFactory(controllerId);
                result = await GetSubordersByFactoryId(factoryId);
            }
            else
            {
                result = await GetAllSuborders();
            }
            return result;
        }

        //private List<WorkOrder> FillNavProperties(List<WorkOrder> orders)
        //{
        //    foreach (var order in orders)
        //    {
        //        //order.Items = workOrderItemsManager.GetItemsWorkOrder(order.Id);
        //        if (order.FK_Parent_WorkOrder_Id != null)
        //        {
        //            var parentOrder = Get(order.FK_Parent_WorkOrder_Id);
        //            order.ParentWorkOrder = parentOrder;
        //        }
        //    }
        //    return orders;
        //}

        public async Task<List<WorkOrder>> GetAllPendingAndApprovedOrders()
        {
            var pendingStatusId = statusManager.Get("pending").Id;
            var approvedStatusId = statusManager.Get("approved").Id;
            var cancelledStatusId = statusManager.Get("cancelled").Id;

            var result = await GetAllAsync()
                            .Include(w => w.Factory)
                            .Include(w => w.Status)
                            .Include(w => w.Priority)
                            .Include(w => w.Location)
                            .Include(w => w.Contract)
                                                        .Include(w => w.Items)

                            .Include(w => w.Customer)
                                .ThenInclude(c => c.Phones)
                            .Where(w => w.FK_Status_Id == pendingStatusId || w.FK_Status_Id == approvedStatusId || w.FK_Status_Id == cancelledStatusId)
                            .ToListAsync();
            foreach (var item in result)
            {
                item.ConfirmationHistories = await orderConfirmationHistoryManager.GetLatestByOrderId(item.Id);
                //item.Items = workOrderItemsManager.GetItemsWorkOrder(item.Id);

            }
            return result;
        }

        public async Task<List<WorkOrder>> GetAllApprovedOrders()
        {
            var approvedStatusId = (await statusManager.Get("approved")).Id;

            var result = await GetAllAsync()
                            .Include(w => w.Items)
                            .Include(w => w.Items)

                            .Include(w => w.Customer)
                                .ThenInclude(c => c.Phones)
                            .Where(w => w.FK_Status_Id == approvedStatusId)
                            .ToListAsync();

            return result;
        }


        public async Task<int> GetOrdersCount(DateTime startDate)
        {
            var approvedStatusId = statusManager.Get("approved").Id;
            var inProgressStatusId = statusManager.Get("in progress").Id;
            var pendingStatusId = statusManager.Get("pending").Id;
            var dispatchedStatusId = statusManager.Get("dispatched").Id;

            var result = await GetAllAsync()
                .Where(w => w.HasSuborders == false &&
                            w.StartDate.Date == startDate.Date &&
                            (w.FK_Status_Id == approvedStatusId ||
                             w.FK_Status_Id == pendingStatusId ||
                             w.FK_Status_Id == inProgressStatusId ||
                             w.FK_Status_Id == dispatchedStatusId)).CountAsync();
            return result;
        }

        public async Task<List<WorkOrder>> GetInProgressOrdersForFacory(int facctoryId)
        {
            var inProgressStatusId = (await statusManager.Get("in progress")).Id;

            return await Context.WorkOrders.Where(x =>
               x.FK_Factory_Id == facctoryId &&
               !x.IsDeleted &&
               x.FK_Status_Id == inProgressStatusId).ToListAsync();

        }

        public async Task UpdateCanConfirmFlag(int restrictedHoursToConfirmOrder, DataContext dataContext)
        {
            var pendingStatusId = (await statusManager.Get("pending", dataContext)).Id;
            var dispatchedStatusId = (await statusManager.Get("dispatched", dataContext)).Id;

            var orders = await dataContext.WorkOrders.Where(x => !x.IsDeleted && x.CanConfirm).ToListAsync();
            foreach (var item in orders)
            {
                if ((item.StartDate - DateTime.UtcNow <= new TimeSpan(restrictedHoursToConfirmOrder, 0, 0) && item.FK_Status_Id == pendingStatusId) ||
                    item.FK_Status_Id == dispatchedStatusId)
                {
                    item.CanConfirm = false;
                    await UpdateAsync(item, dataContext);
                }
            }
        }

        public async Task<bool> IsOrderHaveContractItem(int contractId, int itemId)
        {
            var orders = await Context.WorkOrders.Where(x => x.FK_Contract_Id == contractId).ToListAsync();
            foreach (var item in orders)
            {
                var items = await workOrderItemsManager.GetItemsWorkOrder(item.Id);
                if (items.Select(x => x.Id).Contains(itemId))
                {
                    return true;
                }
            }
            return false;
        }
    }
}

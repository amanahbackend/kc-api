﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilites.PACI;

namespace DispatchingSystem.BLL.Managers
{
    public class GovernrateManager : Repository<Governrate>, IGovernrateManager
    {
        public GovernrateManager(DataContext context)
  : base(context)
        {

        }

        public async Task<List<Governrate>> GetAllGovernoratesAsync()
        {

            try
            {
                return await GetAllAsync().ToListAsync();

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public Task<Governrate> GetGovernrate(string governrateId)
        {
            return base.GetAsync(governrateId);
        }

        public async Task<List<Governrate>> SaveGovernrates(List<PACIHelper.DropPACI> governrates)
        {
            Governrate governrate = new Governrate();
            List<Area> areas = new List<Area>();
            foreach (var item in governrates)
            {
                governrate.Id = item.Id;
                governrate.Name = item.Name;
                governrate = await AddAsync(governrate);
            }
            return await GetAllAsync().ToListAsync();
        }
    }
}

﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class ApplicationRoleManager : IApplicationRoleManager
    {
        RoleManager<ApplicationRole> _identityRoleManager;
        public ApplicationRoleManager(RoleManager<ApplicationRole> identityRoleManager)
        {
            _identityRoleManager = identityRoleManager;
        }

        public async Task<ApplicationRole> GetRoleAsyncByName(string roleName)
        {
            return await _identityRoleManager.FindByNameAsync(roleName);
        }
        public async Task<ApplicationRole> GetRoleAsyncById(string Id)
        {
            return await _identityRoleManager.FindByIdAsync(Id);
        }
        public async Task<ApplicationRole> GetRoleAsync(ApplicationRole role)
        {
            ApplicationRole result = null;
            if (!string.IsNullOrEmpty(role.Name))
                result = await GetRoleAsyncByName(role.Name);
            if (result == null)
            {
                if (!string.IsNullOrEmpty(role.Id))
                    result = await GetRoleAsyncById(role.Id);
            }
            return result;
        }
        public List<ApplicationRole> GetAllRoles()
        {
            return _identityRoleManager.Roles.Where(r => r.IsDeleted == false).ToList();
        }

        public async Task<ApplicationRole> AddRoleAsync(ApplicationRole applicationRole)
        {
            applicationRole.FK_CreatedBy_Id = applicationRole.CurrentUser?.Id;
            applicationRole.CreatedDate = DateTime.UtcNow;
            applicationRole.IsDeleted = false;
            IdentityResult result = await _identityRoleManager.CreateAsync(applicationRole);
            if (result.Succeeded)
            {
                return applicationRole;
            }
            else
            {
                return null;
            }
        }


        public async Task<ApplicationRole> AddRoleAsync(string roleName)
        {
            ApplicationRole applicationRole = new ApplicationRole(new BaseEntity(), roleName);

            return await AddRoleAsync(applicationRole);
        }

        public async Task<bool> UpdateRoleAsync(ApplicationRole applicationRole)
        {
            var result = false;
            var oldRole = await GetRoleAsync(applicationRole);
            if (applicationRole != null)
            {
                oldRole.Name = applicationRole.Name;
                oldRole.FK_UpdatedBy_Id = applicationRole.CurrentUser?.Id;
                oldRole.UpdatedDate = DateTime.UtcNow;

                IdentityResult callBack = await _identityRoleManager.UpdateAsync(oldRole);
                if (callBack.Succeeded)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }

        public async Task<bool> DeleteRoleAsync(ApplicationRole applicationRole)
        {
            var result = false;

            var role = await GetRoleAsyncById(applicationRole.Id);
            role.IsDeleted = true;
            role.FK_UpdatedBy_Id = applicationRole.CurrentUser?.Id;
            role.UpdatedDate = DateTime.UtcNow;
            IdentityResult callBack = await _identityRoleManager.UpdateAsync(role);
            if (callBack.Succeeded)
            {
                result = true;
            }
            else
            {
                result = false;

            }
            return result;
        }


        public async Task<bool> IsRoleExistAsync(string roleName)
        {
            return await _identityRoleManager.RoleExistsAsync(roleName);
        }
    }
}

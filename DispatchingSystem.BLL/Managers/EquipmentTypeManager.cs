﻿using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
namespace DispatchingSystem.BLL.Managers
{
    public class EquipmentTypeManager : Repository<EquipmentType>,IEquipmentTypeManager
    {
        public EquipmentTypeManager(DataContext context)
            : base(context)
        {

        }
    }
}

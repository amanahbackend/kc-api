﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchingSystem.Models;
using Utilites;
using Newtonsoft.Json;
using DispatchingSystem.BLL.IManagers;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class WashingAreaManager : Repository<WashingArea>, IWashingAreaManager
    {

        public WashingAreaManager(DataContext context) : base(context)
        {
        }

        public Task<WashingArea> AddWashingAreaAsync(WashingArea entity)
        {
            entity.Geometry = JsonConvert.SerializeObject(entity.Polygon);
            return base.AddAsync(entity);
        }

        public async  Task<WashingArea> GetWashingAreaAsync(params object[] id)
        {
            var result = await base.GetAsync(id);
            result.Polygon = JsonConvert.DeserializeObject<List<Point>>(result.Geometry);
            return result;
        }

        public override IQueryable<WashingArea> GetAllAsync()
        {
            var result = base.GetAllAsync();
            var resultLst = result.ToList();
            foreach (var item in resultLst)
            {
                item.Polygon = JsonConvert.DeserializeObject<List<Point>>(item.Geometry);
            }
            return resultLst.AsQueryable();
        }

        public IQueryable<WashingArea> GetAll(DataContext dataContext)
        {
            var result = dataContext.WashingAreas.ToList();
            foreach (var item in result)
            {
                item.Polygon = JsonConvert.DeserializeObject<List<Point>>(item.Geometry);
            }
            return result.AsQueryable();
        }
    }
}

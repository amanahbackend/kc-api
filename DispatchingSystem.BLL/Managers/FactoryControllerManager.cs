﻿using DispatchingSystem.Models.Entities;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.IManagers;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DispatchingSystem.BLL.Managers
{
    public class FactoryControllerManager : Repository<FactoryController>, IFactoryControllerManager
    {
        public FactoryControllerManager(DataContext context) : base(context)
        {
        }

        

        public async Task<int> GetControllerFactory(string controllerId)
        {
            var result = await GetAllAsync().FirstOrDefaultAsync(m => m.Fk_Controller_Id == controllerId);
            if (result != null)
            {
                return result.Fk_Factory_Id;
            }
            return 0;
        }
    }
}

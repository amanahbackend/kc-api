﻿
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class LKP_Condition_ItemManager : Repository<LKP_Condition_Item>,ILKP_Condition_ItemManager
    {
        public LKP_Condition_ItemManager(DataContext context)
            : base(context)
        {

        }

        public Task<LKP_Condition_Item> Get(int itemId, int conditionId)
        {
            return Context.LKP_Condition_Items.FirstOrDefaultAsync(c => c.FK_Item_Id == itemId && c.FK_Condition_Id == conditionId);
        }
    }
}

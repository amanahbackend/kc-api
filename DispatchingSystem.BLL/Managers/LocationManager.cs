﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PACI;

namespace DispatchingSystem.BLL.Managers
{
    public class LocationManager : Repository<Location>, ILocationManager
    {
        public LocationManager(DataContext context)
            : base(context)
        {

        }

        public Task<Location> Add(Location entity, string proxyUrl, string paciServiceUrl, string streetServiceUrl, string paciNumberFieldName, string streetNameFieldName, string blockNameFieldName)
        {
            //if (string.IsNullOrEmpty(entity.PACINumber) && !string.IsNullOrEmpty(entity.Street))
            //{
            //    var streetModel = PACIHelper.GetStreetPACItModel(entity.Street, entity.Block, proxyUrl,
            //                            streetServiceUrl, streetNameFieldName, blockNameFieldName);
            //    if (streetModel != null)
            //    {
            //        entity.Latitude = (double)streetModel.features[0].attributes.CENTROID_Y;
            //        entity.Longitude = (double)streetModel.features[0].attributes.CENTROID_X;
            //    }
            //}
            //else if (!string.IsNullOrEmpty(entity.PACINumber))
            //{
            //    var paciModel = PACIHelper.GetPACIModel(Convert.ToInt32(entity.PACINumber), proxyUrl, paciServiceUrl, paciNumberFieldName);
            //    entity.Longitude = (double)paciModel.features[0].attributes.lon;
            //    entity.Latitude = (double)paciModel.features[0].attributes.lat;
            //}
            return base.AddAsync(entity);
        }

        public Task<Location> AddLocationAsync(Location entity)
        {
            //if (string.IsNullOrEmpty(entity.PACINumber) && !string.IsNullOrEmpty(entity.Street))
            //{
            //    var streetModel = PACIHelper.GetStreetPACItModel(entity.Street, entity.Block, proxyUrl,
            //                            streetServiceUrl, streetNameFieldName, blockNameFieldName);
            //    if (streetModel != null)
            //    {
            //        entity.Latitude = (double)streetModel.features[0].attributes.CENTROID_Y;
            //        entity.Longitude = (double)streetModel.features[0].attributes.CENTROID_X;
            //    }
            //}
            //else if (!string.IsNullOrEmpty(entity.PACINumber))
            //{
            //    var paciModel = PACIHelper.GetPACIModel(Convert.ToInt32(entity.PACINumber), proxyUrl, paciServiceUrl, paciNumberFieldName);
            //    entity.Longitude = (double)paciModel.features[0].attributes.lon;
            //    entity.Latitude = (double)paciModel.features[0].attributes.lat;
            //}
            return base.AddAsync(entity);
        }

        public  Task<List<Location>> GetByContractIdAsync(int contractId)
        {
            return  GetAllAsync().Where(l => l.Fk_Contract_Id == contractId).ToListAsync();
            
        }

        public  Task<List<Location>> GetByContractIdsAsync(int[] contractIds)
        {
            return GetAllAsync().Where(l => contractIds.Contains(l.Fk_Contract_Id.Value)).ToListAsync();
           
        }

        public async Task<bool> DeleteByContractIdAsync(int contractId)
        {
            var locations = await GetByContractIdAsync(contractId);
            return await base.Delete(locations);
           
        }

    }


}

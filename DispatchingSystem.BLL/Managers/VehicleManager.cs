﻿
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilites;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class VehicleManager : Repository<Vehicle>, IVehicleManager
    {
        ILKP_StatusManager _statusManager;
        IWashingAreaManager _washingAreaManager;
        IFactoryManager _factoryManager;
        private readonly IFactoryControllerManager _controllerFactoryManager;

        public VehicleManager(DataContext context, ILKP_StatusManager statusManager,
            IWashingAreaManager washingAreaManager, IFactoryManager factoryManager,
            IFactoryControllerManager controllerFactoryManager)
            : base(context)
        {
            _controllerFactoryManager = controllerFactoryManager;
            _factoryManager = factoryManager;
            _statusManager = statusManager;
            _washingAreaManager = washingAreaManager;
        }

        public async Task<Vehicle> AddAVechilesync(Vehicle entity)
        {
            if (entity.FK_Factory_Id == 0 || entity.FK_Factory_Id == null)
            {
                var factory = (await _factoryManager.GetAsync("Name", "others")).FirstOrDefault();
                if (factory == null)
                {
                    factory = await _factoryManager.AddOthersFactory();
                }
                entity.FK_Factory_Id = factory.Id;
            }
            entity.IsAvailable = true;
            entity.Fk_WashingStatus_Id = WashingStatus.Washed;
            if (entity.FK_Driver_Id != null)
            {
                var driver = await Context.Drivers.FirstOrDefaultAsync(x => x.Id == entity.FK_Driver_Id);
                driver.IsAvailable = false;
                await Context.SaveChangesAsync();
            }
            return await base.AddAsync(entity);
        }

        public override Task AddRangeAsync(IEnumerable<Vehicle> entityLst)
        {
            foreach (var item in entityLst)
            {
                item.IsAvailable = true;
            }
            return base.AddRangeAsync(entityLst);
        }

        public Task<bool> AssignVehicleToFactory(int factoryId, Vehicle vehicle)
        {
            vehicle.FK_Factory_Id = factoryId;
            return UpdateAsync(vehicle);
        }

        public Task<bool> AssignDriverToVehicle(int driverId, Vehicle vehicle)
        {
            vehicle.FK_Driver_Id = driverId;
            return UpdateAsync(vehicle);
        }

        public Task<List<Vehicle>> GetAvailableVehiclesInFactory(int factoryId)
        {
            return GetAllAsync().Where(v => v.FK_Factory_Id == factoryId && v.IsAvailable).ToListAsync();

        }

        public async Task<Vehicle> GetAvailableVehicleByOrder(int factoryId, int skippedNumber)
        {
            var vehicles = (await GetAvailableVehiclesInFactory(factoryId)).OrderBy(v => v.WashingTime);
            if (skippedNumber < vehicles.Count())
            {
                var result = vehicles.Skip(skippedNumber).Take(1);
                return result.FirstOrDefault();
            }
            return null;
        }

        public async Task<bool> IsVehicleAvailable(int vehicleId)
        {
            var result = await GetAsync(vehicleId);
            return result.IsAvailable;
        }

        public async Task<List<VehicleTracker>> GetByWorkorder(int orderId)
        {
            var result = await GetAllAsync()
                            .Include(v => v.Driver)
                            .Include(v => v.Factory)
                            .Include(v => v.Job)
                                .ThenInclude(j => j.WorkOrder)
                                .Where(v => v.Job.WorkOrder.Id == orderId)
                                .Select(v => new VehicleTracker
                                {
                                    DriverId = v.FK_Driver_Id != null ? (int)v.FK_Driver_Id : 0,
                                    DriverName = v.Driver.Name,
                                    FactoryId = v.FK_Factory_Id != null ? (int)v.FK_Factory_Id : 0,
                                    FactoryName = v.Factory.Name,
                                    JobId = v.Job.Id,
                                    JobStatus = v.Job.Status.Name,
                                    JobStatusId = v.Job.FK_Status_Id,
                                    OrderId = orderId,
                                    OrderNumber = v.Job.WorkOrder.Code,
                                    VehicleId = v.Id,
                                    VehicleNumber = v.Number
                                }).ToListAsync();

            foreach (var item in result)
            {
                var tracker = await Context.TB_VEHICLE_DISPLAY.FirstOrDefaultAsync(v => v.VReg.Equals(item.VehicleNumber));
                if (tracker != null)
                {
                    item.Lat = Convert.ToDouble(tracker.Lat);
                    item.Long = Convert.ToDouble(tracker.Long);
                }
            }
            return result;
        }

        public async Task<List<VehicleTracker>> GetByFactory(int factoryId)
        {
            var result = await GetAllAsync()
                            .Include(v => v.Driver)
                            .Include(v => v.Factory)
                            .Include(v => v.Job)
                                .ThenInclude(j => j.WorkOrder)
                                .Where(v => v.FK_Factory_Id == factoryId)
                                .Select(v => new VehicleTracker
                                {
                                    DriverId = v.FK_Driver_Id != null ? (int)v.FK_Driver_Id : 0,
                                    DriverName = v.Driver.Name,
                                    FactoryId = factoryId,
                                    FactoryName = v.Factory.Name,
                                    JobId = v.Job.Id,
                                    JobStatus = v.Job.Status.Name,
                                    JobStatusId = v.Job.FK_Status_Id,
                                    OrderId = v.Job.FK_WorkOrder_ID,
                                    OrderNumber = v.Job.WorkOrder.Code,
                                    VehicleId = v.Id,
                                    VehicleNumber = v.Number
                                }).ToListAsync();

            foreach (var item in result)
            {
                var tracker = await Context.TB_VEHICLE_DISPLAY.FirstOrDefaultAsync(v => v.VReg.Equals(item.VehicleNumber));
                if (tracker != null)
                {
                    item.Lat = Convert.ToDouble(tracker.Lat);
                    item.Long = Convert.ToDouble(tracker.Long);
                }
            }
            return result;
        }

        public async Task<VehicleTracker> GetByVehicleNumber(string panicButtonValue, string vehicleNumber, DataContext dataContext)
        {
            var cancelledStatusId = _statusManager.Get("cancelled", dataContext).Id;
            var completedStatusId = _statusManager.Get("completed", dataContext).Id;
            VehicleTracker result = null;
            if (panicButtonValue.Equals("on", StringComparison.OrdinalIgnoreCase))
            {
                result = await dataContext.Vehicles
               .Include(v => v.Driver)
               .Include(v => v.Factory)
               .Include(v => v.Job)
                   .ThenInclude(j => j.WorkOrder)
               .Include(v => v.Job)
                   .ThenInclude(j => j.Status)
               .Include(v => v.WashingStatus)
               .Where(v => v.Number.Equals(vehicleNumber) &&
                           v.IsDeleted == false &&
                           v.Job.FK_Status_Id != cancelledStatusId &&
                           v.Job.FK_Status_Id != completedStatusId)
               .Select(v => new VehicleTracker
               {
                   DriverId = v.FK_Driver_Id != null ? (int)v.FK_Driver_Id : 0,
                   DriverName = v.Driver != null ? v.Driver.Name : null,
                   FactoryId = (int)v.FK_Factory_Id,
                   FactoryName = v.Factory.Name,
                   JobId = v.Job.Id,
                   JobStatus = v.Job.Status.Name,
                   JobStatusId = v.Job.FK_Status_Id,
                   OrderId = v.Job.FK_WorkOrder_ID,
                   OrderNumber = v.Job.WorkOrder.Code,
                   VehicleId = v.Id,
                   VehicleNumber = v.Number,
                   WashingStatus = v.WashingStatus.Name,
                   WashingStatusId = v.Fk_WashingStatus_Id
               }).FirstOrDefaultAsync();
            }
            else if (panicButtonValue.Equals("off", StringComparison.CurrentCultureIgnoreCase))
            {
                var vehicleId = (await dataContext.Vehicles.FirstOrDefaultAsync(v => v.Number.Equals(vehicleNumber)))?.Id;
                if (vehicleId != null)
                {
                    if (await IsVehicleHaveJobs((int)vehicleId, dataContext))
                    {
                        result = await dataContext.Vehicles
                                    .Include(v => v.Driver)
                                    .Include(v => v.Factory)
                                    .Include(v => v.Job)
                                    .Include(v => v.WashingStatus)
                                    .Where(v => v.Number.Equals(vehicleNumber) &&
                                                v.IsDeleted == false &&
                                                v.Job.FK_Status_Id != cancelledStatusId &&
                                                v.Job.FK_Status_Id != completedStatusId)
                                    .Select(v => new VehicleTracker
                                    {
                                        JobId = v.Job.Id,
                                        DriverId = v.FK_Driver_Id != null ? (int)v.FK_Driver_Id : 0,
                                        DriverName = v.Driver != null ? v.Driver.Name : null,
                                        FactoryId = (int)v.FK_Factory_Id,
                                        FactoryName = v.Factory.Name,
                                        VehicleId = v.Id,
                                        VehicleNumber = v.Number,
                                        WashingStatus = v.WashingStatus.Name,
                                        WashingStatusId = v.Fk_WashingStatus_Id
                                    }).FirstOrDefaultAsync();
                    }
                    else
                    {
                        result = await dataContext.Vehicles
                        .Include(v => v.Driver)
                        .Include(v => v.Factory)
                        .Include(v => v.WashingStatus)
                        .Where(v => v.Number.Equals(vehicleNumber) &&
                                    v.IsDeleted == false)
                        .Select(v => new VehicleTracker
                        {
                            DriverId = v.FK_Driver_Id != null ? (int)v.FK_Driver_Id : 0,
                            DriverName = v.Driver != null ? v.Driver.Name : null,
                            FactoryId = (int)v.FK_Factory_Id,
                            FactoryName = v.Factory.Name,
                            VehicleId = v.Id,
                            VehicleNumber = v.Number,
                            WashingStatus = v.WashingStatus.Name,
                            WashingStatusId = v.Fk_WashingStatus_Id
                        }).FirstOrDefaultAsync();
                    }
                }
            }
            return result;
        }




        private async Task<bool> IsVehicleHaveJobs(int vehicleId, DataContext dataContext)
        {
            var cancelledStatusId = _statusManager.Get("cancelled", dataContext).Id;
            var completedStatusId = _statusManager.Get("completed", dataContext).Id;
            var vehiclesCount = await dataContext.Jobs
                                    .CountAsync(j => j.FK_Vehicle_Id == vehicleId &&
                                                j.FK_Status_Id != cancelledStatusId &&
                                                j.FK_Status_Id != completedStatusId);
            return vehiclesCount > 0;
        }

        public Task<List<Job>> GetVehicleJobs(int vehicleId)
        {
            var cancelledStatusId = _statusManager.Get("cancelled", Context).Id;
            var completedStatusId = _statusManager.Get("completed", Context).Id;
            return Context.Jobs
                .Include(j => j.Status)
                .Where(j => j.FK_Vehicle_Id == vehicleId &&
                            j.FK_Status_Id != cancelledStatusId &&
                            j.FK_Status_Id != completedStatusId).ToListAsync();
        }
      
        public async Task<bool> SetVehicleAvailable(int vehicleId)
        {
            try
            {
                var vehicle = await GetAsync(vehicleId);
                vehicle.IsAvailable = true;
                vehicle.Fk_WashingStatus_Id = WashingStatus.Washed;
                return await UpdateAsync(vehicle);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<bool> SetVehicleWashed(int vehicleId)
        {
            var vehicle = await GetAsync(vehicleId);
            vehicle.IsAvailable = true;
            vehicle.Fk_WashingStatus_Id = WashingStatus.Washed;
            return await UpdateAsync(vehicle);
        }

        public async Task<bool> SetVehicleAvailable(int vehicleId, DataContext dataContext)
        {
            var vehicle = await GetAsync(vehicleId);
            vehicle.IsAvailable = true;
            vehicle.Fk_WashingStatus_Id = WashingStatus.Washed;
            return await UpdateAsync(vehicle);

        }

        public async Task<bool> SetVehicleAvailableAsync(string vehicleId, DataContext dataContext)
        {
            var vehicle = await GetAsync(vehicleId);
            vehicle.IsAvailable = true;
            vehicle.Fk_WashingStatus_Id = WashingStatus.Washed;
            return await UpdateAsync(vehicle, dataContext);

        }



    





        public async Task<Vehicle> SetVehicleNotAvailable(int vehicleId)
        {
            var vehicle = await GetAsync(vehicleId);
            vehicle.IsAvailable = false;
            vehicle.Fk_WashingStatus_Id = WashingStatus.NotWashed;
            await UpdateAsync(vehicle);
            return vehicle;
        }


        public async Task<bool> SetVehicleInWashing(int vehicleId)
        {
            var vehicle = await GetAsync(vehicleId);
            vehicle.IsAvailable = false;
            vehicle.Fk_WashingStatus_Id = WashingStatus.InWashing;
            vehicle.WashingTime = DateTime.UtcNow;
            return await UpdateAsync(vehicle);
        }

        private async Task<bool> SetVehicleInWashing(string vehicleNumber, DataContext dataContext)
        {
            var vehicle = await dataContext.Vehicles.FirstOrDefaultAsync(v => v.Number.Equals(vehicleNumber));
            if (vehicle != null)
            {
                vehicle.IsAvailable = false;
                vehicle.Fk_WashingStatus_Id = WashingStatus.InWashing;
                vehicle.WashingTime = DateTime.UtcNow;
                return await UpdateAsync(vehicle);
            }
            return false;
        }

        private async Task<bool> IsVehicleInWashing(DataContext dataContext, string vehicleNumber)
        {
            bool result = false;
            var vehicle = await dataContext.Vehicles.AsNoTracking().FirstOrDefaultAsync(v => v.Number.Equals(vehicleNumber));
            if (vehicle != null)
            {
                result = vehicle.Fk_WashingStatus_Id == WashingStatus.InWashing;
            }
            return result;
        }

        private async Task<bool> SetVehicleWashed(DataContext dataContext, string vehicleNumber)
        {
            var vehicle = await dataContext.Vehicles.FirstOrDefaultAsync(v => v.Number.Equals(vehicleNumber));
            vehicle.IsAvailable = true;
            vehicle.Fk_WashingStatus_Id = WashingStatus.Washed;
            return await UpdateAsync(vehicle);
        }

        public async Task HandleVehicleAvailability(string vehicleNumber, DataContext dataContext,
            double lat, double lng, double washingLeavingTimeInMins, string panicButtonValue)
        {
            if (panicButtonValue.Equals("off", StringComparison.CurrentCultureIgnoreCase))
            {
                TimeSpan washingLeavingTimeInMinsTimeSpan = TimeSpan.FromMinutes(washingLeavingTimeInMins);
                var vehicle = await dataContext.Vehicles.AsNoTracking().FirstOrDefaultAsync(v => v.Number.Equals(vehicleNumber));
                var washingAreas = await _washingAreaManager.GetAll(dataContext).ToListAsync();
                Point vehicleLocation = new Point { Lat = lat, Lng = lng };
                bool isVehicleInWashingArea = false;

                foreach (var item in washingAreas)
                {
                    isVehicleInWashingArea = GeoUtilities.IsPointInPolygon(item.Polygon, vehicleLocation);
                    if (isVehicleInWashingArea)
                    {
                        await SetVehicleInWashing(vehicleNumber, dataContext);
                        break;
                    }
                }

                if (!isVehicleInWashingArea)
                {
                    if (await IsVehicleInWashing(dataContext, vehicleNumber))
                    {
                        if (vehicle.WashingTime != null)
                        {
                            var timeDiff = DateTime.UtcNow - vehicle.WashingTime;
                            if (timeDiff >= washingLeavingTimeInMinsTimeSpan)
                            {
                                await SetVehicleWashed(dataContext, vehicleNumber);
                            }
                        }
                    }
                }
            }
        }


        private Task<Vehicle> GetByNumber(string number)
        {
            return GetAllAsync()
                    .Include(v => v.Driver)
                    .Include(v => v.Factory)
                    .Include(v => v.WashingStatus)
                    .FirstOrDefaultAsync(x => x.Number.Equals(number));
        }

        public async Task<int?> GetVichileIDByNumber(string number)
        {
            var vechile= await GetAllAsync().FirstOrDefaultAsync(x => x.Number.Equals(number));
            if (vechile.Id > 0)
            {
                return vechile.Id;
            }
            return null;
        }

        public async Task<List<VehicleTracker>> GetAllVehicleLocations(List<string> roles, string userId)
        {
            var result = new List<VehicleTracker>();
            var vehicles = await Context.TB_VEHICLE_DISPLAY.ToListAsync();
            if (roles.Contains("Dispatcher"))
            {
                var factoryId = await _controllerFactoryManager.GetControllerFactory(userId);

                vehicles = await FilterVehiclesInSameFactory(vehicles, factoryId);
            }

            foreach (var item in vehicles)
            {
                var vehicle = await GetByNumber(item.VReg);
                if (vehicle != null && await IsVehicleHaveJobs(vehicle.Id, Context))
                {
                    var tracker = new VehicleTracker()
                    {
                        DriverId = vehicle.FK_Driver_Id ?? 0,
                        DriverName = vehicle.Driver?.Name,
                        FactoryId = vehicle.FK_Factory_Id ?? 0,
                        FactoryName = vehicle.Factory?.Name,
                        Lat = Convert.ToDouble(item.Lat),
                        Long = Convert.ToDouble(item.Long),
                        VehicleId = vehicle.Id,
                        VehicleNumber = vehicle.Number,
                        WashingStatus = vehicle.WashingStatus?.Name,
                        WashingStatusId = vehicle.Fk_WashingStatus_Id,
                    };
                    var jobs = await GetVehicleJobs(vehicle.Id);
                    if (jobs.Count > 0)
                    {
                        var order = await Context.WorkOrders.FirstOrDefaultAsync(x => x.Id == jobs.FirstOrDefault().FK_WorkOrder_ID);
                        tracker.OrderId = order?.Id ?? 0;
                        tracker.OrderNumber = order?.Code ?? "";
                        tracker.JobId = jobs.FirstOrDefault().Id;
                        tracker.JobStatusId = jobs.FirstOrDefault().FK_Status_Id;
                        tracker.JobStatus = jobs.FirstOrDefault().Status.Name;
                    }
                    result.Add(tracker);
                }
            }

            return result;
        }

        private async Task<List<TB_VEHICLE_DISPLAY>> FilterVehiclesInSameFactory(List<TB_VEHICLE_DISPLAY> vehicles, int factoryId)
        {
            List<TB_VEHICLE_DISPLAY> vehiclesInFactory = new List<TB_VEHICLE_DISPLAY>();
            foreach (var item in vehicles)
            {
                var count = await Context.Vehicles.CountAsync(x => x.Number.Equals(item.VReg) && x.FK_Factory_Id == factoryId);
                if (count > 0)
                {
                    vehiclesInFactory.Add(item);
                }
            }
            return vehiclesInFactory;
        }

        public Task<Vehicle> GetVechileById(int vechileID)
        {
            return GetAllAsync().Include(x => x.Job).Where(v => v.Id == vechileID).FirstOrDefaultAsync();

        }

        public async Task<VehicleTracker> AddUnassignedVehicle(string vehicleNumber, DataContext dataContext)
        {
            Factory factory = await dataContext.Factories.FirstOrDefaultAsync(f => f.Name.Equals("others"));
            if (factory == null)
            {
                factory = await _factoryManager.AddOthersFactory(dataContext);
            }
            Vehicle vehicle = new Vehicle
            {
                Number = vehicleNumber,
                Fk_WashingStatus_Id = WashingStatus.NotWashed,
                FK_Factory_Id = factory.Id,
                IsAvailable = false,
                IsDeleted = false,
                CreatedDate = DateTime.UtcNow
            };
            await dataContext.AddAsync(vehicle);
            await dataContext.SaveChangesAsync();
            var tracker = new VehicleTracker
            {
                FactoryId = factory.Id,
                FactoryName = factory.Name,
                VehicleId = vehicle.Id,
                VehicleNumber = vehicle.Number,
                WashingStatusId = vehicle.Fk_WashingStatus_Id,
                WashingStatus = Enum.GetName(typeof(WashingStatus), vehicle.Fk_WashingStatus_Id),
            };
            return tracker;
        }


    }
}

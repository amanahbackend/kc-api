﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class UpcomingWorkOrderManager : IUpcomingWorkOrderManager
    {
        public async Task Add(DataContext dataContext, List<string> orderCodes)
        {
            List<UpcomingWorkOrder> orders = new List<UpcomingWorkOrder>();
            foreach (var item in orderCodes)
            {
                orders.Add(new UpcomingWorkOrder() { WorkOrderCode = item });
            }
            await  dataContext.AddRangeAsync(orders);
           await  dataContext.SaveChangesAsync();
        }

        public Task<List<UpcomingWorkOrder>> GetAll(DataContext dataContext)
        {
            return dataContext.UpcomingWorkOrders.ToListAsync();
        }
        public List<UpcomingWorkOrder>  GetAllSync(DataContext dataContext)
        {
            return dataContext.UpcomingWorkOrders.ToList();
        }

        public async Task DeleteAll(DataContext dataContext)
        {
            dataContext.UpcomingWorkOrders.RemoveRange( await GetAll(dataContext));
            await dataContext.SaveChangesAsync();
        }


        public void  DeleteAllSync(DataContext dataContext)
        {
            dataContext.UpcomingWorkOrders.RemoveRange( GetAllSync(dataContext));
             dataContext.SaveChanges();
        }
    }
}

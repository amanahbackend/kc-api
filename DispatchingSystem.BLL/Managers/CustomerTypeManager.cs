﻿
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using System.Threading.Tasks;
using System.Linq;

namespace DispatchingSystem.BLL.Managers
{
    public class CustomerTypeManager : Repository<CustomerType>, ICustomerTypeManager
    {
        public CustomerTypeManager(DataContext context)
            : base(context)
        {

        }

        public async Task<bool> CustomerTypeExists(string type)
        {
            var customerType = await Context.CustometrTypes.FirstOrDefaultAsync(c => c.Name.Equals(type));
            if (customerType != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

﻿using DispatchingSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilities.Paging;

namespace DispatchingSystem.BLL.Managers
{
    public interface IRepository<TEntity>
    {
        IQueryable<TEntity> GetAllAsync();
        PagedResult<TEntity> GetAllByPagination(IQueryable<TEntity> listQuery, PaginatedItems pagingparametermodel);
        Task<PagedResult<TEntity>> GetAllByPaginationAsync(IQueryable<TEntity> listQuery, PaginatedItems pagingparametermodel);
        Task<TEntity> GetAsync(params object[] id);
        Task<TEntity> AddAsync(TEntity entity);
        //TEntity AddAsync(TEntity entity);
        Task<bool> UpdateAsync(TEntity entity);
        Task<bool> DeleteAsync(TEntity entity);
        Task<bool> UpdateAsync(TEntity entity, DataContext dataContext);
        Task<bool> DeleteById(params object[] id);
        int SaveChanges();
        Expression<Func<TEntity, bool>> CreateEqualSingleExpression(string property, object value);
        Task<List<TEntity>> GetAsync(string property, object value);
        Task<bool> UpdateAsync(IEnumerable<TEntity> entityLst);
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(DataContext dataContext);
    }
}

﻿using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
namespace DispatchingSystem.BLL.Managers
{
    public class ContractTypeManager : Repository<ContractType>,IContractTypeManager
    {
        public ContractTypeManager(DataContext context)
            : base(context)
        {

        }
    }
}

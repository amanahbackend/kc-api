﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilities.Paging;

namespace DispatchingSystem.BLL.Managers
{
    public class ContractManager : Repository<Contract>, IContractManager
    {
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IWorkOrderManager _workOrderManager;
        private readonly IContractItemsManager _contractItemsManager;
        private readonly ILocationManager _locationManager;
        private readonly IServiceProvider _serviceprovider;

        public ContractManager(DataContext context, IHostingEnvironment hostingEnv,
            IWorkOrderManager workOrderManager, IContractItemsManager contractItemsManager,
            ILocationManager locationManager, IServiceProvider serviceprovider) : base(context)
        {
            _serviceprovider = serviceprovider;
            _locationManager = locationManager;
            _hostingEnv = hostingEnv;
            _workOrderManager = workOrderManager;
            _contractItemsManager = contractItemsManager;
        }


        private ICustomerManager CustomerManager
        {
            get
            {
                return _serviceprovider.GetService<ICustomerManager>();
            }
        }
        //public override Contract Get(params object[] id)
        //{
        //    var result = base.Get(id);
        //    if (result != null)
        //    {
        //        result.FileName = GetContractFileName(result.Id, result.FK_Customer_Id);
        //        result.Items = _contractItemsManager.GetByContractIdAsync(result.Id);
        //    }
        //    return result;
        //}

        //public override async Task<IQueryable<Contract>> GetAllAsync()
        //{
        //    var result = Context.Contracts
        //                            .Include(c => c.ContractType)
        //                            .Include(c => c.Customer)
        //                                .ThenInclude(c => c.Phones)
        //                            .Where(x => x.IsDeleted == false)
        //                            .AsNoTracking().ToList();
        //    foreach (var contract in result)
        //    {
        //        contract.FileName =await  GetContractFileName(contract.Id, contract.FK_Customer_Id);
        //        contract.Items = _contractItemsManager.GetByContractIdAsync(contract.Id);
        //    }
        //    return result;
        //}

        private IQueryable<Contract> GetAllContracts()
        {
            var result = Context.Contracts
                                    .Include(c => c.ContractType)
                                    .Include(c => c.ContractItems)
                                         .ThenInclude(c => c.Item)


                                    .Include(c => c.Customer)
                                        .ThenInclude(c => c.Phones)

                                    .Where(x => x.IsDeleted == false)
                                    .AsNoTracking();

            return result;
        }


        public async Task<PagedResult<Contract>> GetAllContractsByCustomerIdAsync(PaginatedItems pagingParameterModel)
        {

            var result = GetAllContracts().Where(cust => cust.FK_Customer_Id == pagingParameterModel.Id);
            var allContracts = await GetAllByPaginationAsync(result, pagingParameterModel);

            foreach (var item in allContracts.Result)
            {
                item.FileName = await GetContractFileName(item.Id, item.FK_Customer_Id);
                //item.Items =await _contractItemsManager.GetContractItsmsAsync(item.Id);

            }
            return allContracts;

        }



        public async Task<List<Contract>> GetNonexpiredContractByCustomerIdAsync(int customerId)
        {

            try
            {
               // var result = await GetAllContracts().Where(c => c.FK_Customer_Id == customerId).Where(c => c.EndDate >= DateTime.UtcNow.Date).ToListAsync();

                var result =(await GetContractsByCustomerIdAsync(customerId)).Where(c => c.EndDate == null ? 1 == 1 : c.EndDate.Value.Date >= DateTime.UtcNow.Date).ToList();
                return result;
            }
            catch (Exception e)
            {

                throw;
            }

        }

        public async Task<List<Contract>> GetContractsByCustomerIdAsync(int customerId)
        {
            try
            {
                var result = await GetAllContracts().Where(cust => cust.FK_Customer_Id == customerId).ToListAsync();
              
                return result;
            }
            catch (Exception e)
            {

                throw;
            }
        }


        //public override Contract AddContractAsync(Contract entity)
        //{
        //    // save conteact entity
        //    //if (entity.Items != null && entity.Items.Count > 0)
        //    //{
        //    //    entity.Amount = 0;
        //    //    foreach (var item in entity.Items)
        //    //    {
        //    //        entity.Amount += item.Amount;
        //    //    }
        //    //}
        //    //entity.PendingAmount = entity.Amount;
        //    entity = base.AddAsync(entity);

        //    // save contract file
        //    if (entity.File != null)
        //    {
        //        string fileBase64 = entity.File.Clone().ToString();
        //        if (fileBase64 != null)
        //        {
        //            entity = SaveContractFileAsync(fileBase64, entity).Result;
        //            //Update(entity);
        //        }
        //    }

        //    //// save contract items
        //    //if (entity.Items != null && entity.Items.Count > 0)
        //    //{
        //    //    foreach (var item in entity.Items)
        //    //    {
        //    //        item.FK_Contract_Id = entity.Id;
        //    //    }
        //    //    _contractItemsManager.Add(entity.Items);
        //    //}
        //    return entity;
        //}



        public async Task<Contract> AddContractAsync(Contract entity)
        {
            try
            {

                entity = await base.AddAsync(entity);

                // save contract file
                if (entity.File != null)
                {
                    string fileBase64 = entity.File.Clone().ToString();
                    if (fileBase64 != null)
                    {
                        entity = await SaveContractFileAsync(fileBase64, entity);
                        //Update(entity);
                    }
                }

                // save contract items
                //if (entity.ContractItems != null && entity.ContractItems.Count > 0)
                //{
                //    foreach (var item in entity.ContractItems)
                //    {
                //        item.FK_Contract_Id = entity.Id;
                //    }
                //    _contractItemsManager.Add(entity.ContractItems);
                //}
                return entity;
            }
            catch (Exception e)
            {

                throw;
            }
        }
        public override async Task<bool> UpdateAsync(Contract entity)
        {
            var result = false;
            var isItemsUpdated = true;
            if (entity.ContractItems != null && entity.ContractItems.Count > 0)
            {
                foreach (var item in entity.ContractItems)
                {
                    item.FK_Contract_Id = entity.Id;
                }
                isItemsUpdated = await _contractItemsManager.UpdateContractItems(entity.ContractItems);

                if (isItemsUpdated)
                {
                    //entity.Amount = 0;
                    //foreach (var item in entity.Items)
                    //{
                    //    entity.Amount += item.Amount;
                    //}
                    //entity.PendingAmount = entity.Amount;
                    result = await base.UpdateAsync(entity);
                }
            }
            return result;
        }

        private async Task<Contract> SaveContractFileAsync(string fileBase64, Contract entity)
        {
            if (fileBase64 != null)
            {
                var customerId = entity.FK_Customer_Id;
                var contractId = entity.Id;
                if (customerId > 0)
                {
                    var path = await GetContractDirectoryAsync(customerId, contractId);
                    await FileUtilities.CreateIfMissingAsync(path);
                    string filePath = Path.Combine(path, entity.FileName);
                    FileUtilities.WriteFileToPath(fileBase64, filePath);
                    entity.Path = filePath;
                }
            }
            return entity;
        }

        private Task<string> GetContractFileName(int contractId, int customerId)
        {
            return Task.Run(() =>
            {

                string result = null;
                var filePath = GetContractDirectoryAsync(customerId, contractId).Result;
                DirectoryInfo directoryInfo = new DirectoryInfo(filePath);
                if (directoryInfo.Exists)
                {
                    result = directoryInfo.GetFiles().FirstOrDefault()?.Name;
                }
                return result;

            });
        }

        public async Task<string> GetContractFileAsync(int contractId, int customerId)
        {
            string result = null;
            var filePath = await GetContractDirectoryAsync(customerId, contractId);
            DirectoryInfo directoryInfo = new DirectoryInfo(filePath);
            if (directoryInfo.Exists)
            {
                result = FileUtilities.GetFileBase64(directoryInfo.GetFiles().FirstOrDefault()?.FullName);
            }

            return result;
        }

        private Task<string> GetContractDirectoryAsync(int customerId, int contractId)
        {
            return Task.Run(() =>
            {
                var webRootInfo = _hostingEnv.WebRootPath;
                var dirPath = Path.Combine(webRootInfo, @"Attachments\Customers\" + customerId + @"\Contracts\" + contractId);
                return dirPath;
            });
        }

        public async Task<bool> DeleteByCustomerIdAsync(int customerId)
        {
            bool result = false;
            var contracts = await GetContractsByCustomerIdAsync(customerId);
            foreach (var item in contracts)
            {
                if (await _workOrderManager.DeleteByContractId(item.Id))
                {
                    if (await _locationManager.DeleteByContractIdAsync(item.Id))
                    {
                        result = await DeleteAsync(item);
                    }
                }
            }
            return result;
        }
        public override async Task<bool> DeleteAsync(Contract entity)
        {
            bool result = false;
            if (await CanDelete(entity.Id))
            {
                if (await _contractItemsManager.DeleteByContractIdAsync(entity.Id))
                {
                    result =await base.DeleteAsync(entity);
                }
            }
            return result;
        }
        public override async Task<bool> DeleteById(params object[] id)
        {
            bool result = false;
            int idInt = (int)id[0];
            if (await CanDelete(idInt))
            {
                if (await _contractItemsManager.DeleteByContractIdAsync(idInt))
                {
                    result = await base.DeleteById(id);

                }
            }
            return result;
        }
        public  override async Task<bool> Delete(List<Contract> entitylst)
        {
            bool result = false;
            foreach (var item in entitylst)
            {
                result = await DeleteAsync(item);
            }
            return result;
        }

        public async Task<bool> CanDelete(int contractId)
        {
            var result = false;
            if (! await _workOrderManager.IsContractHasOrders(contractId))
            {
                result = true;
            }
            return result;
        }

        public async Task<CustomContract> CreateCustomContractAsync(CustomContract customContract)
        {
            try
            {
                if (customContract.Contract != null)
                {
                    customContract.Contract = await AddAsync(customContract.Contract);
                }
                if (customContract.Locations != null && customContract.Locations.Count > 0)
                {
                    foreach (var item in customContract.Locations)
                    {
                        item.Fk_Contract_Id = customContract.Contract.Id;
                        await _locationManager.AddLocationAsync(item);
                    }
                }
                return customContract;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<bool> ContractNumberExists(string number)
        {
            return await Context.Contracts.CountAsync(x => x.ContractNumber.Equals(number)) > 0;
        }


    }
}

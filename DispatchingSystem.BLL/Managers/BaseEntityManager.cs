﻿
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;

namespace DispatchingSystem.BLL.Managers
{
    public class BaseEntityManager : IBaseEntityManager
    {
        public static void AddAuditingData(IEnumerable<EntityEntry> dbEntityEntries)
        {
            try
            {
                foreach (var entry in dbEntityEntries)
                {
                    if (entry.Entity as IBaseEntity != null)
                    {
                        if (entry.State == EntityState.Added)
                        {
                            var entity = (entry.Entity as IBaseEntity);
                            if (entity != null)
                            {
                                entity.FK_CreatedBy_Id = (entity.CurrentUser != null) ? entity.CurrentUser.Id : null;
                                entity.CreatedDate = DateTime.UtcNow;
                            }
                        }
                        else if (entry.State == EntityState.Modified)
                        {
                            var entity = (entry.Entity as IBaseEntity);
                            if (entity != null)
                            {
                                entity.FK_UpdatedBy_Id = (entity.CurrentUser != null) ? entity.CurrentUser.Id : null;
                                entity.UpdatedDate = DateTime.UtcNow;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //Handle saving auditing data exception.
            }
        }
    }
}

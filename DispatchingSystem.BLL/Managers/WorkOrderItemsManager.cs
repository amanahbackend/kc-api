﻿
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.IManagers
{
    public class WorkOrderItemsManager : Repository<WorkOrderItems>, IWorkOrderItemsManager
    {
        private readonly IItemManager itemManager;
        private readonly IContractItemsManager contractItemsManager;
        public WorkOrderItemsManager(DataContext context, IItemManager itemManager,
            IContractItemsManager contractItemsManager)
            : base(context)
        {
            this.contractItemsManager = contractItemsManager;
            this.itemManager = itemManager;
        }

        public async  Task<bool> DeleteItemsWorkOrder(int workorderId)
        {
            bool result = false;
            var items = await GetAllAsync().Where(item => item.FK_Workorder_Id == workorderId).ToListAsync();
            if (items != null && items.Count != 0)
            {
                foreach (var item in items)
                {
                    result = await DeleteAsync(item);
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        public async Task<bool> DeleteItemsWorkOrderPhysically(int workorderId)
        {
            bool result = false;
            var items =await  GetAllAsync().Where(item => item.FK_Workorder_Id == workorderId).ToListAsync();
            if (items != null && items.Count != 0)
            {
                Context.RemoveRange(items);
                result = true;
            }
            else
            {
                result = true;
            }
            return result;
        }

        public async Task<List<Item>> GetItemsWorkOrder(int workorderId)
        {
            var result = new List<Item>();
            var workorderitems = await  GetAllAsync().Where(item => item.FK_Workorder_Id == workorderId).ToListAsync();
            foreach (var workorderitem in workorderitems)
            {
                var item = await Context.Items.AsNoTracking().FirstOrDefaultAsync(x => x.Id == workorderitem.FK_Item_Id);
                if (item != null)
                {
                    item.Price = workorderitem.Price;
                    result.Add(item);
                }
            }
            return result;
        }

        public async Task UpdateItemsWorkOrder(WorkOrder workOrder)
        {
            var dbOrderItem = await GetAllAsync().FirstOrDefaultAsync(x => x.FK_Workorder_Id == workOrder.Id);
            dbOrderItem.FK_Item_Id = workOrder.Items.Id;
            dbOrderItem.Price =( await contractItemsManager.GetAllAsync().FirstOrDefaultAsync(x => x.FK_Item_Id == dbOrderItem.FK_Item_Id
                                    && x.FK_Contract_Id == workOrder.FK_Contract_Id)).Price;
           await UpdateAsync(dbOrderItem);
        }

        public async Task<WorkOrder> AddItemsWorkOrder(WorkOrder workOrder)
        {
            var items = workOrder.Items;
            if (items != null)
            {
               
                    var dbContractItem = await contractItemsManager.GetAllAsync().FirstOrDefaultAsync(x => x.FK_Item_Id == items.Id
                                                                                && x.FK_Contract_Id == workOrder.FK_Contract_Id);
                    var itemOrder = new WorkOrderItems
                    {
                        FK_Workorder_Id = workOrder.Id,
                        FK_Item_Id = items.Id,
                        Price = dbContractItem.Price,
                        CurrentUser = workOrder.CurrentUser
                    };
                    //items[i] = dbItem;
                    await AddAsync(itemOrder);
                
                workOrder.Items = items;
            }
            return workOrder;
        }

        public async Task<WorkOrderItems> GetBy(DataContext dataContext, int workOrderId, int itemId)
        {
            var result = await dataContext.Set<WorkOrderItems>()
                                    .FirstOrDefaultAsync(wi => !wi.IsDeleted &&
                                                 wi.FK_Workorder_Id == workOrderId &&
                                                 wi.FK_Item_Id == itemId);
            return result;
        }
    }
}

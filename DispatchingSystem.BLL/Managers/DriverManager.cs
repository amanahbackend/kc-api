﻿
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using System.Collections.Generic;
using System.Linq;

namespace DispatchingSystem.BLL.Managers
{
    public class DriverManager : Repository<Driver>,IDriverManager
    {
        public DriverManager(DataContext context)
            : base(context)
        {

        }

        public List<Driver> GetAvailableDrivers()
        {
            return Context.Drivers.Where(x => x.IsAvailable).ToList();
        }
    }
}

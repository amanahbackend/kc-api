﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class CustomerPhoneManager : Repository<CustomerPhone>, ICustomerPhoneManager
    {
        public CustomerPhoneManager(DataContext dataContext) : base(dataContext)
        {

        }

        public Task<List<CustomerPhone>> GetBy(string phone)
        {
         return GetAllAsync().Where(cp => cp.Phone.Contains(phone)).ToListAsync();
             
        }

        public Task<List<CustomerPhone>> GetByAsync(int customerId)
        {
           return  GetAllAsync().Where(cp => cp.Fk_Customer_Id == customerId).ToListAsync();
            
        }

        public async Task<bool> IsPhoneExistAsync(string phone)
        {
            var result =await  GetAllAsync().Where(cp => cp.Phone.Equals(phone)&& !cp.IsDeleted).ToListAsync();
            return result.Count > 0;
        }

        public async Task<bool> DeleteByCustomerIdAsync(int customerId)
        {
            var phones = await GetByAsync(customerId);
            foreach (var item in phones)
            {
                item.IsDeleted = true;
            }
            return await base.UpdateAsync(phones);
        }

        //public  async Task<bool> UpdateAsync(CustomerPhone entity)
        //{
        //    var oldPhones = await GetByAsync(entity.Fk_Customer_Id);
        //    Context.CustomerPhones.RemoveRange(oldPhones);
        //    var result = await AddAsync(entity);
        //    if (result != null)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        public async Task UpdateCustomerPhonesAsync(IEnumerable<CustomerPhone> entityLst)
        {
            var oldPhones = await GetByAsync(entityLst.FirstOrDefault().Fk_Customer_Id);
            Context.CustomerPhones.RemoveRange(oldPhones);
           await  AddRangeAsync(entityLst);
        }
    }
}

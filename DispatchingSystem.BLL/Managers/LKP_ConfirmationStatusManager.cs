﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchingSystem.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DispatchingSystem.BLL.Managers
{
    public class LKP_ConfirmationStatusManager : Repository<LKP_ConfirmationStatus>, ILKP_ConfirmationStatusManager
    {

        public string NeedMovementCtrlConfirmation
        {
            get
            {
                return "Need Movement Controller Confirmation";
            }
        }
    public string Declined
        {
            get
            {
                return "Declined";
            }
        }
        public string ConfirmedByMovementCtrl
        {
            get
            {
                return "Confirmed By Movement Controller";
            }
        }

        public string NeedToChangeDateByMovementCtrl
        {
            get
            {
                return "Need To Change Date By Movement Controller";
            }
        }

        public string ConfirmedByCustomer
        {
            get
            {
                return "Confirmed By Customer";
            }
        }

        public string NeedToRescheduleByCustomer
        {
            get
            {
                return "Need To Reschedule By Customer";
            }
        }


        public LKP_ConfirmationStatusManager(DataContext context) : base(context)
        {
        }

        public async Task Seed()
        {
            await base.AddAsync(new LKP_ConfirmationStatus() { Name = NeedMovementCtrlConfirmation });
            await base.AddAsync(new LKP_ConfirmationStatus() { Name = ConfirmedByMovementCtrl });
            await base.AddAsync(new LKP_ConfirmationStatus() { Name = NeedToChangeDateByMovementCtrl });
            await base.AddAsync(new LKP_ConfirmationStatus() { Name = ConfirmedByCustomer });
            await base.AddAsync(new LKP_ConfirmationStatus() { Name = NeedToRescheduleByCustomer });
    
        }

        public Task<LKP_ConfirmationStatus> GetByName(string name)
        {
            return GetAllAsync().FirstOrDefaultAsync(x => x.Name.Equals(name));
        }

        public async Task<List<LKP_ConfirmationStatus>> GetByRole(string roleName)
        {
            var result = new List<LKP_ConfirmationStatus>();
            if (roleName.Equals("callcenter", StringComparison.CurrentCultureIgnoreCase))
            {
                result.Add(await GetByName(ConfirmedByCustomer));
                result.Add(await GetByName(NeedToRescheduleByCustomer));
                result.Add(await GetByName(NeedMovementCtrlConfirmation));
            }
            else if (roleName.Equals("movementcontroller", StringComparison.CurrentCultureIgnoreCase))
            {
                result.Add(await GetByName(ConfirmedByMovementCtrl));
                result.Add(await GetByName(NeedToChangeDateByMovementCtrl));
                result.Add(await GetByName(Declined));


            }
            else
            {
                result = await GetAllAsync().ToListAsync();
            }
            return result;
        }

        public async  Task<bool> StatusExists(string name)
        {
            var status = await GetAllAsync().FirstOrDefaultAsync(s => s.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase));
            return status != null;
        }

        public Task<LKP_ConfirmationStatus> GetById(int id)
        {
            return GetAllAsync().FirstOrDefaultAsync(x => x.Id==id);
        }
    }
}

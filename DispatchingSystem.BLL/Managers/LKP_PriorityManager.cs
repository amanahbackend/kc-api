﻿using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using System.Threading.Tasks;
using System.Linq;

namespace DispatchingSystem.BLL.Managers
{
    public class LKP_PriorityManager : Repository<LKP_Priority>, ILKP_PriorityManager
    {
        public LKP_PriorityManager(DataContext context)
            : base(context)
        {

        }

        public async Task<bool> PriorityExits(string name)
        {
            var priority = await Context.LKP_Priorities.FirstOrDefaultAsync(p => p.Name.Equals(name));
            if (priority != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

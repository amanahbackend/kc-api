﻿
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class ItemManager : Repository<Item>, IItemManager
    {
        public ItemManager(DataContext context)
            : base(context)
        {

        }


        public async Task<bool> ItemCodeExists(string code)
        {
            var result=await Context.Items.CountAsync(x => x.Code.Equals(code)) ;

            return result > 0;
        }

    }
}

﻿
using System.Linq;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System;

namespace DispatchingSystem.BLL.Managers
{
    public class PrivilgeManager : Repository<Privilge>, IPrivilgeManager
    {
        public PrivilgeManager(DataContext context)
            : base(context)
        {

        }

        public Task Add(string privilegeName, string userId)
        {
            var privilege = new Privilge()
            {
                CreatedDate = DateTime.UtcNow,
                FK_CreatedBy_Id = userId,
                IsDeleted = false,
                Name = privilegeName.ToLower(),
                UpdatedDate = DateTime.UtcNow
            };

            return base.AddAsync(privilege);
        }

        public Task<Privilge> GetByAsync(string privelegeName)
        {
            return Context.Privilge.FirstOrDefaultAsync(p => string.Equals(p.Name, privelegeName, StringComparison.CurrentCultureIgnoreCase));
        }

        public async Task<bool> PrivelegeExistsAsync(string privelegeName)
        {
            var privelege = await Context.Privilge.FirstOrDefaultAsync(p => string.Equals(p.Name, privelegeName, StringComparison.CurrentCultureIgnoreCase));
            if (privelege != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Task<Privilge> GetByAsync(int id)
        {
            return Context.Privilge.FirstOrDefaultAsync(p => p.Id == id);
        }
    }
}

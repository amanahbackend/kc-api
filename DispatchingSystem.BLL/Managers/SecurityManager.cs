﻿using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using DispatchingSystem.BLL.IManagers;
using Utilites;

namespace DispatchingSystem.BLL.Managers
{
    public class SecurityManager : ISecurityManager
    {
        UserManager<ApplicationUser> _identityUserManager;
        IPasswordHasher<ApplicationUser> _passwordHasher;

        public SecurityManager(UserManager<ApplicationUser> identityUserManager, IPasswordHasher<ApplicationUser> passwordHasher)
        {
            _identityUserManager = identityUserManager;
            _passwordHasher = passwordHasher;
        }

        public async Task<JwtSecurityToken> GetToken(string userName, string password,
            string jwtSecurityTokenKey, string issuer, string audience)
        {
            ApplicationUserManager appUserManager = new ApplicationUserManager(_identityUserManager);
            ApplicationUser user = await appUserManager.GetByUserNameAsync(userName);
            if (user == null)
            {
                return null;
            }
            var passwordCheckResult = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, password);
            if (passwordCheckResult == PasswordVerificationResult.Success)
            {
                var userClaims = await appUserManager.GetClaimsAsync(user);

                var claims = new[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                        new Claim(JwtRegisteredClaimNames.Jti, user.Id),
                        new Claim(JwtRegisteredClaimNames.Email, user.Email)
                    }.Union(userClaims);

                byte[] jwtSecurityTokenBytes = Encoding.UTF8.GetBytes(jwtSecurityTokenKey);
                var symmetricSecurityKey = new SymmetricSecurityKey(jwtSecurityTokenBytes);
                var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

                var result = new JwtSecurityToken(
                        issuer: issuer,
                        audience: audience,
                        claims: claims,
                        expires: DateTime.UtcNow.AddHours(24),
                        signingCredentials: signingCredentials);

                return result;
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> IsTokenValid(JwtSecurityToken jwtTokenObj)
        {
            bool result = false;
            var exprirationDate = await DateTimeUtilities.GetDateTimeFromEpochSecond((int)jwtTokenObj.Payload.Exp);
            TimeSpan diff = DateTime.UtcNow - exprirationDate;
            TimeSpan day = new TimeSpan(24, 0, 0);
            if (diff <= day)
            {
                result = true;
            }
            return result;
        }
    }
}

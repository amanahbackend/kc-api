﻿using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using Utilites.GoogleMapsAPI;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace DispatchingSystem.BLL.Managers
{
    public class FactoryManager : Repository<Factory>, IFactoryManager
    {
        public FactoryManager(DataContext context)
            : base(context)
        {
        }

        public Task<Factory> AddOthersFactory()
        {
            return base.AddAsync(new Factory
            {
                Name = "others",
                Latitude = 29.3750747,
                Longitude = 47.9847351
            });
        }

        public async Task<Factory> AddOthersFactory(DataContext dataContext)
        {
            var factory = await dataContext.Factories.AddAsync(new Factory
            {
                Name = "others",
                Latitude = 29.3750747,
                Longitude = 47.9847351
            });
           await  dataContext.SaveChangesAsync();
            return factory.Entity;
        }
    }
}

﻿
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class ComplainManager: Repository<Complain>, IComplainManager
    {
        public ComplainManager(DataContext context)
            : base(context)
        {

        }

        public async Task<List<Complain>> GetComplainsByCustomerIdAsync(int customerId)
        {
            try
            {
                List<Complain> result = null;
                result = await GetAllAsync().Where(complain => complain.FK_Customer_Id == customerId && complain.IsDeleted == false).ToListAsync();
                return result;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public async Task<bool> DeleteByCustomerIdAsync(int customerId)
        {
            var complains = await GetComplainsByCustomerIdAsync(customerId);
            bool result = await Delete(complains);
            return result;
        }
    }
}

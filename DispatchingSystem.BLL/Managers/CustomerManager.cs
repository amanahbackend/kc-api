﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites;
using Utilities.Paging;

namespace DispatchingSystem.BLL.Managers
{
    public class CustomerManager : Repository<Customer>, ICustomerManager
    {
        private readonly IContractManager contractManager;
        private readonly IWorkOrderManager workOrderManager;
        private readonly ILocationManager locationManger;
        private readonly ICustomerPhoneManager customerPhoneManager;

        public CustomerManager(DataContext context, IContractManager _contractManager,
            IWorkOrderManager _workOrderManager, IComplainManager _complainManger,
            ILocationManager _locationManger, ICustomerPhoneManager _customerPhoneManager)
            : base(context)
        {
            contractManager = _contractManager;
            workOrderManager = _workOrderManager;
            locationManger = _locationManger;
            customerPhoneManager = _customerPhoneManager;
        }

        public override Task<Customer> GetAsync(params object[] id)
        {
          return Context.Customers
                                .Include(c => c.CustomerCategory)
                                .Include(c => c.CustomerType)
                                .Include(c => c.Phones)
                                .FirstOrDefaultAsync(c => c.Id == (int)id[0]);
        }
       
        public Task<Customer> GetSelctedCustomer(int id)
        {
           return Context.Customers.FirstOrDefaultAsync(c => c.Id == id);             
        }

        public override IQueryable<Customer> GetAllAsync()
        {
           return base.GetAllAsync().Include(x => x.Phones)
                                            .Include(x => x.CustomerCategory)
                                            .Include(x => x.CustomerType)
                                            .Where(x => !x.IsDeleted);
            
        }

        public Task<PagedResult<Customer>> GetAllCustomerByPagingAsync(PaginatedItems pagingParameterModel)
        {
            var result = base.GetAllAsync().Include(x => x.Phones)
                                                    .Include(x => x.CustomerCategory)
                                                    .Include(x => x.CustomerType)
                                                    .Where(x => !x.IsDeleted);

            return GetAllByPaginationAsync(result, pagingParameterModel);
        }



        public async Task<List<Customer>> SearchForCustomerAsync(string searchToken)
        {

            searchToken = searchToken.Trim();
            List<Customer> result = new List<Customer>();
            var customerId = 0;
            int.TryParse(searchToken, out customerId);
            var customerPhones = await customerPhoneManager.GetBy(searchToken);
            foreach (var item in customerPhones)
            {
                var customer =await  GetAsync(item.Fk_Customer_Id);
                if (customer != null)
                {
                    result.Add(customer);
                }
            }
            return await Task.Run(() =>
             {
                 result.AddRange(GetAllAsync().Where(c => c.Name.Contains(searchToken) || c.Id== customerId));
                 result = result.DistinctBy(cus => cus.Id).ToList();
                 return result;

             });
             
              
                
           
        }




        public Task<Customer> SearchForCustomerAsync(int ID)
        {
            return GetAsync(ID);
        }


    
        public  Task<Customer> AddCustomerAsync(Customer entity)
        {
            entity.Balance = 0;
            return  base.AddAsync(entity);
        }

        public async Task<CustomCustomer> CreateCustomCustomerAsync(CustomCustomer customCustomer)
        {
            if (customCustomer.Customer != null)
            {
                customCustomer.Customer = await AddCustomerAsync(customCustomer.Customer);
            }
            if (customCustomer.Contract != null)
            {
                customCustomer.Contract.FK_Customer_Id = customCustomer.Customer.Id;
                customCustomer.Contract = await contractManager.AddContractAsync(customCustomer.Contract);
            }
            if (customCustomer.WorkOrderFactories != null)
            {
                customCustomer.WorkOrderFactories.WorkOrder.FK_Customer_Id = customCustomer.Customer.Id;
                customCustomer.WorkOrderFactories.WorkOrder.FK_Contract_Id = customCustomer.Contract.Id;
                customCustomer.WorkOrderFactories = await workOrderManager.HandleWorkOrderAssigning(customCustomer.WorkOrderFactories);
            }
            if (customCustomer.Location != null)
            {

                customCustomer.Location.Fk_Contract_Id = customCustomer.Contract.Id;
                  await  locationManger.AddLocationAsync(customCustomer.Location);
                
            }
            return customCustomer;
        }

        public override async Task<bool> DeleteById(params object[] id)
        {
            var entity = await GetAsync(id);
            return await base.DeleteAsync(entity);
        }
        public override async Task<bool> Delete(List<Customer> entitylst)
        {
            bool result = false;
            foreach (var item in entitylst)
            {
                result = await DeleteAsync(item);
            }
            return result;
        }


        //public CustomerHistory GetCustomerHistory(int customerId)
        //{
        //    try
        //    {
        //        var result = new CustomerHistory();
        //        result.Customer = Get(customerId);
        //        result.WorkOrders = workOrderManager.GetDetailedWorkOrdersByCustomerId(customerId);
        //        result.Complains = complainManger.GetComplainsByCustomerIdAsync(customerId);
        //        result.Contracts = contractManager.GetContractsByCustomerIdAsync(customerId).Result;
        //        return result;

        //    }
        //    catch (Exception e)
        //    {

        //        throw;
        //    }

        //}

        public async Task<bool> IsCustomerNameExist(string name)
        {
            var customers = await GetAllAsync().Where(c => c.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && c.IsDeleted == false).ToListAsync();
            return customers.Count > 0;
        }

        public override async Task<bool> DeleteAsync(Customer entity)
        {

            //contractManager.DeleteByCustomerId(entity.Id);
            // complainManger.DeleteByCustomerId(entity.Id);
           await  customerPhoneManager.DeleteByCustomerIdAsync(entity.Id);
           return await base.DeleteAsync(entity);

             
        }

     
        public async Task<bool> UpdateCustomerAsync(Customer entity)
        {
            if (entity.IncreaseAmount > 0)
            {
                var diff = 0.0;

                if (entity.IncreaseAmount >= entity.Balance)
                {
                    diff = entity.IncreaseAmount - entity.Balance;
                    entity.Balance = 0;
                    entity.Debit = entity.Debit + diff;
                }
                else if (entity.IncreaseAmount < entity.Balance)
                {
                    entity.Balance = entity.Balance - entity.IncreaseAmount;
                }
            }
            if (entity.Phones != null && entity.Phones.Count > 0)
            {
                foreach (var item in entity.Phones)
                {
                    item.Fk_Customer_Id = entity.Id;
                }

                await customerPhoneManager.UpdateCustomerPhonesAsync(entity.Phones);
            }

            return await base.UpdateAsync(entity);
        }

        public Task UpdateBalanceAsync(Customer customer)
        {
            return base.UpdateAsync(customer);
        }

        public Task<List<Customer>> GetLatestCustomers(int number)
        {
            return  Context.Customers.Include(x => x.Phones)
                                            .Include(x => x.CustomerCategory)
                                            .Include(x => x.CustomerType)
                                            .Where(x => !x.IsDeleted)
                                            .OrderByDescending(x => x.CreatedDate)
                                            .Take(number).ToListAsync();
        }

        public async Task<bool> CivilIdExist(string civilId)
        {
            var result=await  Context.Customers.CountAsync(x => x.CivilId.Equals(civilId) && !x.IsDeleted);
           return result > 0;
        }

        public async Task<bool> KCRMIdExist(string kcrmId)
        {
            var result=await  Context.Customers.CountAsync(x => x.CivilId.Equals(kcrmId) && !x.IsDeleted);
           return result > 0;
        }
    }
}

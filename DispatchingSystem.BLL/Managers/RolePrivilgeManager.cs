﻿
using System.Threading.Tasks;
using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Collections.Generic;

namespace DispatchingSystem.BLL.Managers
{
    public class RolePrivilgeManager : Repository<RolePrivilge>, IRolePrivilgeManager
    {
        public RolePrivilgeManager(DataContext context)
            : base(context)
        {

        }


        public async Task AddPrivelegToRole(string roleName, string privilegeName, RoleManager<ApplicationRole> identityRoleManager, string userId)
        {
            ApplicationRoleManager applicationRoleManager = new ApplicationRoleManager(identityRoleManager);
            var role = await applicationRoleManager.GetRoleAsyncByName(roleName);

            PrivilgeManager privilgeManager = new PrivilgeManager(Context);
            var privilege = await privilgeManager.GetByAsync(privilegeName);

            await base.AddAsync(new RolePrivilge()
            {
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow,
                FK_CreatedBy_Id = userId,
                FK_ApplicationRole_Id = role.Id,
                FK_Privilge_Id = privilege.Id,
                IsDeleted = false,
                ApplicationRole = role,
                Privilge = privilege
            });
        }

        public async Task<List<string>> GetPrivelegesByRole(string roleName)
        {
            var role = await Context.Roles.FirstOrDefaultAsync(r => r.Name == roleName);
            List<string> result = new List<string>();
            var rolePrivileges = Context.RolePrivilges.Where(rp => rp.FK_ApplicationRole_Id.Equals(role.Id)).ToList();
            List<int> privilegeIds = rolePrivileges.Select(x => x.FK_Privilge_Id).ToList();
            PrivilgeManager privilgeManager = new PrivilgeManager(Context);

            foreach (var privilegeId in privilegeIds)
            {
                result.Add((await privilgeManager.GetByAsync(privilegeId)).Name);
            }
            return result;
        }
    }
}

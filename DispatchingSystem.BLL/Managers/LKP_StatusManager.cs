﻿using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace DispatchingSystem.BLL.Managers
{
    public class LKP_StatusManager : Repository<LKP_Status>, ILKP_StatusManager
    {
        public LKP_StatusManager(DataContext context)
            : base(context)
        {

        }

        public Task<List<LKP_Status>> GetByRole(string role)
        {
            return Task.Run(async () =>
            {


                List<LKP_Status> result = new List<LKP_Status>();
                if (role.Equals("callcenter", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    result.Add(await Get("Pending"));
                    result.Add(await Get("Cancelled"));
                    result.Add(await Get("Closed"));
                }
                else if (role.Equals("dispatcher", System.StringComparison.CurrentCultureIgnoreCase))
                {
                    result.Add(await Get("In Progress"));
                    result.Add(await Get("Confirmed by customer"));

                    result.Add(await Get("Dispatched"));
                    result.Add(await Get("Completed"));
                }
                else
                {
                    result.Add(await Get("Approved"));
                    result.Add(await Get("Cancelled"));
                    result.Add(await Get("Dispatched"));
                    result.Add(await Get("Pending"));
                    result.Add(await Get("Need To Change Date By Movement Ctrl"));

                }
                return result;
            });
        }

        public Task<LKP_Status> Get(string statusName)
        {
            return GetAllAsync().FirstOrDefaultAsync(s => s.Name.Equals(statusName));
        }

        public Task<LKP_Status> Get(string statusName, DataContext dataContext)
        {
            return dataContext.LKP_Status.FirstOrDefaultAsync(s => s.Name.Equals(statusName, System.StringComparison.CurrentCultureIgnoreCase) && s.IsDeleted == false);
        }
        public async Task<bool> StatusExists(string name)
        {
            var status = await GetAllAsync().FirstOrDefaultAsync(s => s.Name.Equals(name, System.StringComparison.CurrentCultureIgnoreCase));
            if (status != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> IsCompleted(int statusId)
        {
            var result = false;
            var status = await GetAllAsync().FirstOrDefaultAsync(s => s.Id == statusId);
            if (status != null && status.Name.Equals("completed", System.StringComparison.CurrentCultureIgnoreCase))
            {
                result = true;
            }
            return result;
        }

        public async Task<bool> IsCancelled(int statusId)
        {
            var result = false;
            var status = await GetAllAsync().FirstOrDefaultAsync(s => s.Id == statusId);
            if (status != null && status.Name.Equals("cancelled", System.StringComparison.CurrentCultureIgnoreCase))
            {
                result = true;
            }
            return result;
        }

        public async Task<bool> IsClosed(int statusId)
        {
            var result = false;
            var status = await GetAllAsync().FirstOrDefaultAsync(s => s.Id == statusId);
            if (status != null && status.Name.Equals("closed", System.StringComparison.CurrentCultureIgnoreCase))
            {
                result = true;
            }
            return result;
        }
    }
}


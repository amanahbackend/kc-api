﻿using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using DispatchingSystem.Models;
using DispatchingSystem.BLL.IManagers;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class OrderConfirmationHistoryManager : Repository<OrderConfirmationHistory>, IOrderConfirmationHistoryManager
    {
        private ILKP_ConfirmationStatusManager _confirmationStatusManager;
        public OrderConfirmationHistoryManager(DataContext context,
            ILKP_ConfirmationStatusManager confirmationStatusManager) : base(context)
        {
            _confirmationStatusManager = confirmationStatusManager;
        }

        public async Task<OrderConfirmationHistory> AddOrderConfirmationHistory(OrderConfirmationHistory entity)
        {
            if (entity.OrderAlternativeDate == default(DateTime))
            {
                entity.OrderAlternativeDate = null;
            }
            int needMvmtConfirm = _confirmationStatusManager.GetByName(_confirmationStatusManager.NeedMovementCtrlConfirmation).Id;
            int mvmtNeedToChangeDate = _confirmationStatusManager.GetByName(_confirmationStatusManager.NeedToChangeDateByMovementCtrl).Id;
            int customerNeedToReschedule = _confirmationStatusManager.GetByName(_confirmationStatusManager.NeedToRescheduleByCustomer).Id;
            int confirmedByCustomer = _confirmationStatusManager.GetByName(_confirmationStatusManager.ConfirmedByCustomer).Id;
            int confirmedByMvmt = _confirmationStatusManager.GetByName(_confirmationStatusManager.ConfirmedByMovementCtrl).Id;

            if (entity.Fk_ConfirmationStatus_Id == needMvmtConfirm ||
                entity.Fk_ConfirmationStatus_Id == confirmedByMvmt ||
                entity.Fk_ConfirmationStatus_Id == customerNeedToReschedule ||
                entity.Fk_ConfirmationStatus_Id == confirmedByCustomer)
            {
                if (entity.OrderDate != null || entity.OrderAlternativeDate != null)
                {
                    entity = FillCreatorProperties(entity);
                    entity = await base.AddAsync(entity);
                    entity.ConfirmationStatus = await _confirmationStatusManager.GetAsync(entity.Fk_ConfirmationStatus_Id);
                    return entity;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                if (entity.OrderDate != null && entity.OrderAlternativeDate != null)
                {
                    entity = FillCreatorProperties(entity);
                    entity = await base.AddAsync(entity);
                    entity.ConfirmationStatus = await _confirmationStatusManager.GetAsync(entity.Fk_ConfirmationStatus_Id);
                    return entity;
                }
                else
                {
                    return null;
                }
            }
        }

        private OrderConfirmationHistory FillCreatorProperties(OrderConfirmationHistory entity)
        {
            entity.CreatorName = entity.CurrentUser?.UserName;
            entity.CreatorRole = entity.CurrentUser?.RoleNames.Length > 0 ? entity.CurrentUser?.RoleNames[0] : null;
            return entity;
        }

        //public  void AddAsync(IEnumerable<OrderConfirmationHistory> entityLst)
        //{
        //    foreach (var item in entityLst)
        //    {
        //        Add(item);
        //    }
        //}

        public Task<List<OrderConfirmationHistory>> GetByOrderId(int orderId)
        {
            return GetAllAsync()
                              .Include(x => x.ConfirmationStatus)
                              .Where(x => x.Fk_WorkOrder_Id == orderId).ToListAsync();
        }

        public Task<List<OrderConfirmationHistory>> GetLatestByOrderId(int orderId)
        {
         return  GetAllAsync()
                            .Include(x => x.ConfirmationStatus)
                            .Where(x => x.Fk_WorkOrder_Id == orderId)
                            .OrderByDescending(x => x.CreatedDate).Take(1).ToListAsync();
        }
        public async Task<bool> DeleteByOrderId(int orderId)
        {
            var entities = await GetAllAsync().Where(x => x.Fk_WorkOrder_Id == orderId).ToListAsync();
            return await base.Delete(entities);
        }
    }
}

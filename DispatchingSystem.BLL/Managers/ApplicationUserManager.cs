﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class ApplicationUserManager : IApplicationUserManager
    {
        public UserManager<ApplicationUser> _identityUserManager;
        public ApplicationUserManager(UserManager<ApplicationUser> identityUserManager)
        {
            _identityUserManager = identityUserManager;
        }

        /// <summary>
        /// Method to add user in the database using identity provider.
        /// </summary>
        /// <param name="user">user object to be added.</param>
        /// <returns>id of the added user in case of operation succeed. null in case of failure.</returns>
        public async Task<ApplicationUser> AddUserAsync(ApplicationUser user, string password)
        {
            user.FK_CreatedBy_Id = user.CurrentUser?.Id;
            user.CreatedDate = DateTime.UtcNow;
            user.IsDeleted = false;
            IdentityResult identityResult = await _identityUserManager.CreateAsync(user, password);
            if (identityResult.Succeeded)
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        public async Task<ApplicationUser> AddUserAsync(ApplicationUser user, string password, string[] roleNames)
        {
            var userResult = await AddUserAsync(user, password);
            if (userResult != null)
            {
                foreach (var role in roleNames)
                {
                    await AddUserToRoleAsync(userResult.UserName, role);
                }
                return userResult;
            }
            else
            {
                return null;
            }

        }

        private void CopyEditingProperties(ApplicationUser source, ApplicationUser destination)
        {
            destination.Email = source.Email;
            destination.FirstName = source.FirstName;
            destination.LastName = source.LastName;
            destination.PhoneNumber = source.PhoneNumber;
            destination.RoleNames = source.RoleNames;
            destination.UserName = source.UserName;
        }

        public async Task<ApplicationUser> UpdateUserAsync(ApplicationUser user, string password)
        {
            var dbUser = await _identityUserManager.FindByIdAsync(user.Id);
            CopyEditingProperties(user, dbUser);
            dbUser.FK_UpdatedBy_Id = user.CurrentUser?.Id;
            dbUser.UpdatedDate = DateTime.UtcNow;
            IdentityResult identityResult = await _identityUserManager.UpdateAsync(dbUser);
            if (identityResult.Succeeded)
            {
                var result = await AddUserToRolesAsync(dbUser);
                return result ? dbUser : null;
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> AddUserToRolesAsync(ApplicationUser user)
        {
            if (user != null && user.RoleNames != null)
            {
                var roles = await _identityUserManager.GetRolesAsync(user);
                var result = await _identityUserManager.RemoveFromRolesAsync(user, roles.ToArray());

                result = await _identityUserManager.AddToRolesAsync(user, user.RoleNames);
                return result.Succeeded;
            }
            return false;
        }
        public async Task<ApplicationUser> AddUserAsync(string userName, string email, string password)
        {
            ApplicationUser applicationUser = new ApplicationUser(new BaseEntity())
            {
                Email = email,
                UserName = userName
            };
            return await AddUserAsync(applicationUser, password);
        }

        public async Task<ApplicationUser> GetByUserNameAsync(string userName)
        {
            try
            {
                ApplicationUser user = await _identityUserManager.FindByNameAsync(userName);
                user.RoleNames = await GetRolesAsync(user);
                return user;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<bool> DeleteAsync(ApplicationUser user)
        {
            var result = false;
            user = await GetAsync(user);
            user.FK_UpdatedBy_Id = user.CurrentUser?.Id;
            user.UpdatedDate = DateTime.UtcNow;
            user.IsDeleted = true;
            var callBack = await _identityUserManager.UpdateAsync(user);
            if (callBack.Succeeded)
            {
                result = true;
            }
            return result;
        }
        public async Task<ApplicationUser> GetAsync(ApplicationUser user)
        {
            if (user.UserName != null)
                user = await _identityUserManager.FindByNameAsync(user.UserName);
            if (user == null && user.Email != null)
                user = await _identityUserManager.FindByEmailAsync(user.Email);
            if (user == null && user.Id != null)
                user = await _identityUserManager.FindByIdAsync(user.Id);
            return user;
        }

        public async Task<List<ApplicationUser>> GetAll()
        {
            var users = _identityUserManager.Users.Where(u => u.IsDeleted == false).ToList();
            foreach (var item in users)
            {
                item.RoleNames = (await GetRolesAsync(item.UserName)).ToArray();
            }
            return users;
        }

        public async Task<IList<Claim>> GetClaimsAsync(ApplicationUser user)
        {
            return await _identityUserManager.GetClaimsAsync(user);
        }

        private async Task<string[]> GetRolesAsync(ApplicationUser user)
        {
            string[] rolesNames = ((await _identityUserManager.GetRolesAsync(user)) as List<string>).ToArray();
            return rolesNames;
        }

        public async Task<IList<string>> GetRolesAsync(string userName)
        {
            var user = await GetByUserNameAsync(userName);
            IList<string> rolesNames = await _identityUserManager.GetRolesAsync(user);
            return rolesNames;
        }

        public async Task<bool> AddUserToRoleAsync(string userName, string roleName)
        {
            var applicationUser = await GetByUserNameAsync(userName);
            IdentityResult result = await _identityUserManager.AddToRoleAsync(applicationUser, roleName);
            if (result.Succeeded)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> AddUserToRoleAsync(ApplicationUser applicationUser, ApplicationRole applicationRole)
        {
            IdentityResult result = await _identityUserManager.AddToRoleAsync(applicationUser, applicationRole.Name);
            if (result.Succeeded)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> IsUserNameExistAsync(string userName)
        {
            var user = await _identityUserManager.FindByNameAsync(userName);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> IsEmailExistAsync(string email)
        {
            var user = await _identityUserManager.FindByEmailAsync(email);
            if (user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<IList<ApplicationUser>> GetUsersInRole(string roleName)
        {
            var result = await _identityUserManager.GetUsersInRoleAsync(roleName);
            return result;
        }

        public async Task<bool> IsUserInRole(string userId, string roleName)
        {
            var user = await _identityUserManager.FindByIdAsync(userId);
            var result = await _identityUserManager.IsInRoleAsync(user, roleName);
            return result;
        }


        public async Task<bool> ResetPassword(string username, string newPassword)
        {
            var user = await GetByUserNameAsync(username);
            var currentPassword = user.PasswordHash;
            var token = await _identityUserManager.GeneratePasswordResetTokenAsync(user);
            var identityResult = await _identityUserManager.ResetPasswordAsync(user, token, newPassword);
            return identityResult.Succeeded;
        }
    }
}

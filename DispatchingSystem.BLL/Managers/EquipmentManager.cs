﻿
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL.Managers
{
    public class EquipmentManager : Repository<Equipment>, IEquipmentManager
    {
        public EquipmentManager(DataContext context)
            : base(context)
        {

        }

        public Task<List<Equipment>> GetByFactory(int factoryId)
        {
           return GetAllAsync().Where(e => e.Fk_Factory_Id == factoryId).ToListAsync();
        }

        public Task<List<Equipment>> GetAvailableEquipment()
        {
            return GetAllAsync().Where(e => e.IsAvailable).ToListAsync();
           
        }

        public Task<List<Equipment>> GetAvailableEquipmentInFactory(int factoryId)
        {
            return GetAllAsync().Where(e => e.Fk_Factory_Id == factoryId && e.IsAvailable).ToListAsync();
        }

        public Task<List<Equipment>> GetEquipmentUnassignedToFactory()
        {
            return GetAllAsync().Where(e => e.Fk_Factory_Id == null).ToListAsync();
        }

        public Task<List<Equipment>> GetEquipmentAssignedToWorkOrder(int workorderId)
        {
            return GetAllAsync().Where(e => e.FK_WorkOrder_Id == workorderId).ToListAsync();
        }

        public Task<List<Equipment>> GetEquipmentAssignedToWorkOrder(int workorderId, DataContext dataContext)
        {
            return dataContext.Equipments.Where(e => e.FK_WorkOrder_Id == workorderId).ToListAsync();
        }

        public Task<bool> UnAssignEquipmentFromWorkOrder(int workOrderId, Equipment equipment)
        {
            equipment.IsAvailable = true;
            equipment.FK_WorkOrder_Id = null;
            return UpdateAsync(equipment);
        }

        public async Task<bool> UnassignAllEquipmentFromWorkOrder(int workOrderId, DataContext dataContext)
        {
            var equipment = await GetEquipmentAssignedToWorkOrder(workOrderId, dataContext);
            bool isAccepted = true;
            foreach (var item in equipment)
            {
                item.IsAvailable = true;
                item.FK_WorkOrder_Id = null;
                isAccepted =await UpdateAsync(item, dataContext);

            }
            return isAccepted;
        }

        public async Task<bool> UnassignAllEquipmentFromWorkOrder(int workOrderId)
        {
            var equipment = await  GetEquipmentAssignedToWorkOrder(workOrderId);
            foreach (var item in equipment)
            {
                item.IsAvailable = true;
                item.FK_WorkOrder_Id = null;
            }
            return await UpdateAsync(equipment);
        }

        private async Task<bool> UnassignEquipmentFromWorkOrderExcept(int workOrderId, List<Equipment> exceptedEquipmentLst)
        {
            var dbEquipmentLst = await GetEquipmentAssignedToWorkOrder(workOrderId);
            var updateLst = new List<Equipment>();
            foreach (var item in dbEquipmentLst)
            {
                if (!exceptedEquipmentLst.Any(e => e.Id == item.Id))
                {
                    item.FK_WorkOrder_Id = null;
                    item.IsAvailable = true;
                    updateLst.Add(item);
                }
            }
            return await UpdateAsync(updateLst);
        }

        public async Task<List<Equipment>> AssignEquipmentToWorkOrder(int workOrderId, List<Equipment> equipmentLst)
        {
           await  UnassignEquipmentFromWorkOrderExcept(workOrderId, equipmentLst);

            foreach (var equipment in equipmentLst)
            {
                if (equipment.IsAvailable && equipment.FK_WorkOrder_Id != workOrderId)
                {
                    equipment.FK_WorkOrder_Id = workOrderId;
                    equipment.IsAvailable = false;
                   await  UpdateAsync(equipment);
                }
            }
            return equipmentLst;
        }

        public Task<bool> AssignEquipmentToFactory(int factoryId, Equipment equipment)
        {
            equipment.Fk_Factory_Id = factoryId;
            return  UpdateAsync(equipment);
        }

        public  Task<Equipment> AddEquipmentAsync(Equipment entity)
        {
            entity.IsAvailable = true;
            return base.AddAsync(entity);
        }

        public override Task AddRangeAsync(IEnumerable<Equipment> entityLst)
        {
            foreach (var item in entityLst)
            {
                item.IsAvailable = true;
            }
            return base.AddRangeAsync(entityLst);
        }


    }
}

﻿using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using System.Threading.Tasks;
using System.Linq;

namespace DispatchingSystem.BLL.Managers
{
    public class LKPConditionManager : Repository<LKPCondition>, ILKPConditionManager
    {
        public LKPConditionManager(DataContext context)
            : base(context)
        {

        }

        public async Task<bool> ConditionExists(string conditionName)
        {
            var condition =await   Context.LKPConditions.FirstOrDefaultAsync(c => c.Name.Equals(conditionName));
            return condition != null;
        }

        public Task<LKPCondition> Get(string name)
        {
            return Context.LKPConditions.FirstOrDefaultAsync(c => c.Name.Equals(name, System.StringComparison.CurrentCultureIgnoreCase));
        }
    }
}

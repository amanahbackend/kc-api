﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DispatchingSystem.BLL.Managers
{
    public class LKP_CustomerCategoryManager : Repository<LKP_CustomerCategory>, ILKP_CustomerCategoryManager
    {
        public LKP_CustomerCategoryManager(DataContext context) : base(context)
        {
        }
    }
}

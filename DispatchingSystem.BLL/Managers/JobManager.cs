﻿
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using DispatchingSystem.BLL.IManagers;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace DispatchingSystem.BLL.Managers
{
    public class JobManager : Repository<Job>, IJobManager
    {
        ILKP_StatusManager _statusManager;
        IVehicleManager _vehicleManager;
        IWorkOrderManager _workOrderManager;
        IWorkOrderItemsManager _workOrderItemsManager;
        public JobManager(DataContext context, ILKP_StatusManager statusManager,
            IVehicleManager vehicleManager, IWorkOrderManager workOrderManager,
            IWorkOrderItemsManager workOrderItemsManager)
            : base(context)
        {
            _vehicleManager = vehicleManager;
            _statusManager = statusManager;
            _workOrderManager = workOrderManager;
            _workOrderItemsManager = workOrderItemsManager;
        }

        public async Task<bool> UpdateDeliveryNote(Job entity)
        {
            var dbJob = await GetAsync(entity.Id);
            dbJob.DeliveryNote = entity.DeliveryNote;
            return await UpdateAsync(dbJob);
        }

        public async Task<Job> SetVehicleStatusCompleted(string vehicleNumber, string pushButtonMsg, DataContext dataContext)
        {
            int ? result = 0;
            Job dispatchedJob = new Job();
            if (vehicleNumber != null)
                result = await _vehicleManager.GetVichileIDByNumber(vehicleNumber);
            if (result != null && result > 0)
            {
                dispatchedJob = await GetVechileJobDispatched(result);

                if (dispatchedJob==null)
                    return dispatchedJob;
                
                dispatchedJob.FK_Status_Id = (await _statusManager.Get("completed", dataContext)).Id;

                 await UpdateAsync(dispatchedJob, dataContext);
            }


            return dispatchedJob;
        }






        public async Task<Job> CheckConcreteMixedTemperature(string vehicleId, string tempDegree,string name, DataContext dataContext)
        {
            int result = 0;
            int.TryParse(vehicleId, out result);

            var InProgressJob = await GetVechileJobInProgress(result);
            InProgressJob.IsDamaged = true;
            InProgressJob.DiscriptionOFDamage = "The concrete mix has been exceeded " + " " + tempDegree;
             await UpdateAsync(InProgressJob, dataContext);

            return InProgressJob;




        }

        private async Task<Job> GetVechileJobDispatched(int ? vehicleId)
        {
            var dispatchedStatusId = (await _statusManager.Get("Dispatched", Context)).Id;
            return await Context.Jobs
                .Include(j => j.Status)
                                .Include(j => j.WorkOrder)

                .Where(j => j.FK_Vehicle_Id == vehicleId &&
                            j.FK_Status_Id== dispatchedStatusId).FirstOrDefaultAsync();
        }

        private async  Task<Job> GetVechileJobInProgress(int vehicleId)
        {
            var inProgressStatusId = (await _statusManager.Get("In Progress", Context)).Id;
            return await Context.Jobs
                .Include(j => j.Status).Include(j => j.WorkOrder)
                .Where(j => j.FK_Vehicle_Id == vehicleId &&
                            j.FK_Status_Id == inProgressStatusId).FirstOrDefaultAsync();
        }

        public async Task<Job> AddJobAsync(Job entity)
        {
            var skippedStatus = await _statusManager.Get("Skipped");
            if (entity.FK_Status_Id == skippedStatus.Id)
            {
                entity.VehicleNumber = _vehicleManager.GetAsync(entity.FK_Vehicle_Id)?.Result.Number;
                entity.FK_Vehicle_Id = null;
                return await base.AddAsync(entity);
            }
            else
            {
                //var order = _workOrderManager.Get(entity.FK_WorkOrder_ID);

                var order = await _workOrderManager.GetAllOrdersByIdAsync(entity.FK_WorkOrder_ID);

                if (entity.Amount <= order.Amount && entity.Amount <= order.PendingAmount)
                {
                    WorkOrderFactories workOrderFactories = new WorkOrderFactories();
                    workOrderFactories.WorkOrder = order;
                    var isCustomerHasBalance = await _workOrderManager.ValidateCustomerBalanceWithinJob(workOrderFactories, entity.Amount);
                    var isVIP = await _workOrderManager.CheckIfCustomerVIP(workOrderFactories);
                    var isAvailable = await _vehicleManager.IsVehicleAvailable((int)entity.FK_Vehicle_Id);
                    if (isAvailable && (isCustomerHasBalance || isVIP))
                    {
                        order.PendingAmount -= entity.Amount;
                        await _workOrderManager.UpdateCustomerBalanceWithinJob(workOrderFactories, entity.Amount);
                        await _workOrderManager.UpdateAsync(order);
                        entity.StartDate = DateTime.UtcNow;
                        Vehicle vehicle = await _vehicleManager.SetVehicleNotAvailable((int)entity.FK_Vehicle_Id);
                        entity.VehicleNumber = vehicle.Number;
                        entity.FK_Status_Id = (await _statusManager.Get("In Progress")).Id;
                        if (order.PendingAmount == 0)
                        {
                            await _workOrderManager.ChangeStatusToDispatched(entity.FK_WorkOrder_ID);

                        }
                        else
                        {
                            await _workOrderManager.ChangeStatusToInProgress(entity.FK_WorkOrder_ID);

                        }
                        var oldJobs = await Context.Jobs.Where(j => j.FK_Vehicle_Id == entity.FK_Vehicle_Id).ToListAsync();
                        foreach (var oldJob in oldJobs)
                        {
                            oldJob.FK_Vehicle_Id = null;
                        }
                        if (true)
                        {

                        }
                        var job = await base.AddAsync(entity);
                        job.Vehicle = null;
                        job.WorkOrder = null;
                        return job;
                    }
                }
                return null;
            }
        }



        public async Task<List<Job>> GetCompletedCancelledBy(int workOrderId)
        {
            try
            {
                //  int cancelledStatusId = (await _statusManager.Get("cancelled")).Id;
                var skippedStatusId = (await _statusManager.Get("Skipped")).Id;

                return await GetAllAsync().Where(j => j.FK_WorkOrder_ID == workOrderId &&
                                                 j.FK_Status_Id != skippedStatusId).ToListAsync();
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public async Task<List<Job>> GetAllByWorkOrder(int orderId)
        {
            var result = await GetAllAsync()
                            .Include(j => j.Status)
                            .Where(j => j.FK_WorkOrder_ID == orderId).ToListAsync();
            foreach (var item in result)
            {
                if (item.FK_Vehicle_Id != null)
                {
                    item.Vehicle = await Context.Vehicles.Include(v => v.Driver).FirstOrDefaultAsync(v => v.Id == item.FK_Vehicle_Id);
                }
            }
            return result;
        }

        public async Task<bool> IsInProgressJob(int jobId)
        {
            bool result = false;
            int jobStatusId = (await GetAsync(jobId)).FK_Status_Id;
            int inProgressStatusId = (await _statusManager.Get("In Progress")).Id;
            if (jobStatusId == inProgressStatusId)
            {
                result = true;
            }
            return result;
        }

        public async Task<bool> IsInProgressJob(int jobId, DataContext context)
        {
            bool result = false;
            int jobStatusId = (await context.Jobs.FirstOrDefaultAsync(j => j.Id == jobId)).FK_Status_Id;
            int inProgressStatusId = (await context.LKP_Status.FirstOrDefaultAsync(s => s.Name.Equals("in progress", StringComparison.CurrentCultureIgnoreCase))).Id;
            if (jobStatusId == inProgressStatusId)
            {
                result = true;
            }
            return result;
        }

        public async Task<List<Job>> GetInProgressJobs(DataContext context)
        {
            int inProgressStatusId = (await context.LKP_Status.FirstOrDefaultAsync(s => s.Name.Equals("in progress", StringComparison.CurrentCultureIgnoreCase) && !s.IsDeleted)).Id;
            return await context.Jobs.Where(j => j.FK_Status_Id == inProgressStatusId && !j.IsDeleted).ToListAsync();

        }
        public List<Job> GetInProgressJobsSync(DataContext context)
        {
            int inProgressStatusId = context.LKP_Status.FirstOrDefault(s => s.Name.Equals("in progress", StringComparison.CurrentCultureIgnoreCase) && !s.IsDeleted).Id;
            return context.Jobs.Where(j => j.FK_Status_Id == inProgressStatusId && !j.IsDeleted).ToList();

        }
        public async Task<Job> ReassignJob(Job job, int workorderId)
        {
            job.FK_WorkOrder_ID = workorderId;
            await UpdateAsync(job);
            return new Job
            {
                Amount = job.Amount,
                CreatedDate = job.CreatedDate,
                EndDate = job.EndDate,
                FK_CreatedBy_Id = job.FK_CreatedBy_Id,
                FK_Status_Id = job.FK_Status_Id,
                FK_UpdatedBy_Id = job.FK_UpdatedBy_Id,
                FK_Vehicle_Id = job.FK_Vehicle_Id,
                FK_WorkOrder_ID = job.FK_WorkOrder_ID,
                Id = job.Id,
                IsDeleted = job.IsDeleted,
                StartDate = job.StartDate,
                UpdatedDate = job.UpdatedDate,
                VehicleNumber = job.VehicleNumber
            };
        }

        public async Task<Job> CancelJob(Job job)
        {

            job.FK_Status_Id = (await _statusManager.Get("cancelled")).Id;
            await _vehicleManager.SetVehicleAvailable((int)job.FK_Vehicle_Id);
            var order = await _workOrderManager.GetAllOrdersByIdAsync(job.FK_WorkOrder_ID);

            order.PendingAmount += job.Amount;
            await _workOrderManager.UpdateAsync(order);

            if (order.PendingAmount > 0)
            {

                //  job.FK_Status_Id = _statusManager.Get("In Progress").Id;

                await _workOrderManager.ChangeStatusToInProgress(job.FK_WorkOrder_ID);
            }
            else if (order.PendingAmount == 0)
            {
                job.FK_Status_Id = _statusManager.Get("dispatched").Id;
                await _workOrderManager.ChangeStatusToDispatched(job.FK_WorkOrder_ID);

            }
            await _workOrderManager.UpdateCustomerBalanceAfterCancelJob(order, job.Amount);
            await UpdateAsync(job);
            return new Job
            {
                Amount = job.Amount,
                CancellationReason = job.CancellationReason,
                CreatedDate = job.CreatedDate,
                EndDate = job.EndDate,
                FK_CreatedBy_Id = job.FK_CreatedBy_Id,
                FK_Status_Id = job.FK_Status_Id,
                FK_UpdatedBy_Id = job.FK_UpdatedBy_Id,
                FK_Vehicle_Id = job.FK_Vehicle_Id,
                FK_WorkOrder_ID = job.FK_WorkOrder_ID,
                Id = job.Id,
                IsDeleted = job.IsDeleted,
                StartDate = job.StartDate,
                UpdatedDate = job.UpdatedDate,
                VehicleNumber = job.VehicleNumber
            };
        }


        public async Task<Job> JobDone(Job job)
        {

            int statusId = (await _statusManager.Get("dispatched")).Id;
            job.FK_Status_Id = statusId;
            await _vehicleManager.SetVehicleAvailable((int)job.FK_Vehicle_Id);

            await UpdateAsync(job);
            return new Job
            {
                Amount = job.Amount,
                CreatedDate = job.CreatedDate,
                EndDate = job.EndDate,
                FK_CreatedBy_Id = job.FK_CreatedBy_Id,
                FK_Status_Id = job.FK_Status_Id,
                FK_UpdatedBy_Id = job.FK_UpdatedBy_Id,
                FK_Vehicle_Id = job.FK_Vehicle_Id,
                FK_WorkOrder_ID = job.FK_WorkOrder_ID,
                Id = job.Id,
                IsDeleted = job.IsDeleted,
                StartDate = job.StartDate,
                UpdatedDate = job.UpdatedDate,
                VehicleNumber = job.VehicleNumber
            };
        }

        public async Task<Job> Get(DataContext dataContext, params object[] id)
        {
            int cancelledStatusId = 0;
            int completedStatusId = 0;
            int skippedStatusId = 0;
            await Task.Run(() =>
            {
                cancelledStatusId = _statusManager.Get("cancelled", dataContext).Id;
                completedStatusId = _statusManager.Get("completed", dataContext).Id;
                skippedStatusId = _statusManager.Get("Skipped", dataContext).Id;

            });

            return await dataContext.Jobs.
                                       FirstOrDefaultAsync(j => j.Id == (int)id[0] &&
                                                           j.IsDeleted == false &&
                                                           j.FK_Status_Id != cancelledStatusId &&
                                                           j.FK_Status_Id != completedStatusId &&
                                                           j.FK_Status_Id != skippedStatusId);

        }

        public async Task ChangeJobToCompleted(Job job, DataContext dataContext)
        {
            //_vehicleManager.SetVehicleAvailable((int)job.FK_Vehicle_Id, dataContext);
            job.FK_Status_Id = (await _statusManager.Get("completed", dataContext: dataContext)).Id;
            job.EndDate = DateTime.UtcNow;
            await UpdateAsync(job, dataContext);
        }

        public async Task ChangeJobToCompletedManual(Job job)
        {
            //_vehicleManager.SetVehicleAvailable((int)job.FK_Vehicle_Id, dataContext);
            job.FK_Status_Id = (await _statusManager.Get("completed")).Id;
            job.EndDate = DateTime.UtcNow;
            await UpdateAsync(job);
        }



        public async Task HandlePanicButton(string panicButtonValue, int jobId, DataContext dataContext)
        {
            if (panicButtonValue.Equals("off", StringComparison.CurrentCultureIgnoreCase) && jobId > 0)
            {
                var job = await Get(dataContext: dataContext, id: jobId);
                await ChangeJobToCompleted(job, dataContext);
                await UpdateWorkorderAmount(dataContext, job);
            }
        }

        private async Task UpdateWorkorderAmount(DataContext dataContext, Job job)
        {

            var order = await _workOrderManager.Get(dataContext: dataContext, id: job.FK_WorkOrder_ID);
            order.PendingAmount -= job.Amount;
            await _workOrderManager.UpdateSuborder(order, dataContext);
        }
    }
}

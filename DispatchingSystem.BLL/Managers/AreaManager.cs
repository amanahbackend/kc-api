﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilites.PACI;

namespace DispatchingSystem.BLL.Managers
{
    public class AreaManager : Repository<Area>, IAreaManager
    {
        public AreaManager(DataContext context)
    : base(context)
        {

        }

        public async Task<List<Area>> GetAreasAsync(string govId)
        {

            try
            {
                return await  GetAllAsync().Where(a => a.FK_Governrate_Id == govId).ToListAsync();

            }
            catch (Exception e)
            {

                throw;
            }
        }

        //public void SaveAreas(List<PACIHelper.DropPACI> areas, string govId)
        //{
        //    Area area = new Area();
        //    foreach (var item in areas)
        //    {
        //        area.Id = item.Id;
        //        area.Name = item.Name;
        //        area.FK_Governrate_Id = govId;
        //        area = Add(area);
        //    }
        //}
    }
}

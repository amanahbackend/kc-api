﻿using DispatchingSystem.BLL.IManagers;
using DispatchingSystem.BLL.Managers;
using DispatchingSystem.Models;
using DispatchingSystem.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DispatchingSystem.BLL
{
    public class DbInitializer : IDbInitializer
    {
        public readonly UserManager<ApplicationUser> _identityUserManager;
        public readonly RoleManager<ApplicationRole> _identityRoleManager;

        public DbInitializer(UserManager<ApplicationUser> identityUserManager,
                            RoleManager<ApplicationRole> identityRoleManager)
        {
            _identityRoleManager = identityRoleManager;
            _identityUserManager = identityUserManager;
        }

        public async Task InitializeUsersRoles()
        {
            // Add Roles
            ApplicationRoleManager applicationRoleManager = new ApplicationRoleManager(_identityRoleManager);

            if (!await applicationRoleManager.IsRoleExistAsync("Admin"))
                await applicationRoleManager.AddRoleAsync("Admin");

            if (!await applicationRoleManager.IsRoleExistAsync("CallCenter"))
                await applicationRoleManager.AddRoleAsync("CallCenter");

            if (!await applicationRoleManager.IsRoleExistAsync("MovementController"))
                await applicationRoleManager.AddRoleAsync("MovementController");

            if (!await applicationRoleManager.IsRoleExistAsync("Dispatcher"))
                await applicationRoleManager.AddRoleAsync("Dispatcher");


            //Add users
            ApplicationUserManager applicationUserManager = new ApplicationUserManager(_identityUserManager);
            if (!await applicationUserManager.IsUserNameExistAsync("User1"))
                await applicationUserManager.AddUserAsync("User1", "abc@xyz.com", "P@ssw0rd");

            if (!await applicationUserManager.IsUserNameExistAsync("User2"))
                await applicationUserManager.AddUserAsync("User2", "dyz@xyz.com", "P@ssw0rd");

            if (!await applicationUserManager.IsUserNameExistAsync("User3"))
                await applicationUserManager.AddUserAsync("User3", "asd@xyz.com", "P@ssw0rd");

            if (!await applicationUserManager.IsUserNameExistAsync("User4"))
                await applicationUserManager.AddUserAsync("User4", "qwe@xyz.com", "P@ssw0rd");

            if (!await applicationUserManager.IsUserNameExistAsync("User5"))
                await applicationUserManager.AddUserAsync("User5", "zxc@xyz.com", "P@ssw0rd");


            await applicationUserManager.AddUserToRoleAsync("User1", "Admin");
            await applicationUserManager.AddUserToRoleAsync("User1", "Dispatcher");
            await applicationUserManager.AddUserToRoleAsync("User2", "CallCenter");
            await applicationUserManager.AddUserToRoleAsync("User3", "CallCenter");
            await applicationUserManager.AddUserToRoleAsync("User4", "MovementController");
            await applicationUserManager.AddUserToRoleAsync("User5", "Dispatcher");



        }

        public async Task InitializePriveleges(IPrivilgeManager privilgeManager, IRolePrivilgeManager rolePrivilgeManager)
        {
            ApplicationUserManager applicationUserManager = new ApplicationUserManager(_identityUserManager);
            var userId = (await applicationUserManager.GetByUserNameAsync("User1")).Id;

            string priv1 = "api/Complain/GetAll";
            string priv2 = "api/Complain/Add";
            string priv3 = "api/Complain/Update";

            if (!await privilgeManager.PrivelegeExistsAsync(priv1))
                await privilgeManager.Add(priv1, userId);


            if (!await privilgeManager.PrivelegeExistsAsync(priv2))
                await privilgeManager.Add(priv2, userId);


            if (!await privilgeManager.PrivelegeExistsAsync(priv3))
                await privilgeManager.Add(priv3, userId);


            await rolePrivilgeManager.AddPrivelegToRole("Admin", priv3, _identityRoleManager, userId);
            await rolePrivilgeManager.AddPrivelegToRole("CallCenter", priv2, _identityRoleManager, userId);
            await rolePrivilgeManager.AddPrivelegToRole("CallCenter", priv1, _identityRoleManager, userId);

        }

        public async Task InitializeLookups(ILKP_PriorityManager priorityManager,
            ILKP_StatusManager statusManager, ICustomerTypeManager customerTypeManager,
            ILKPConditionManager conditionManager)
        {
            ApplicationUserManager applicationUserManager = new ApplicationUserManager(_identityUserManager);
            var userId = (await applicationUserManager.GetByUserNameAsync("User1")).Id;

            string urgentPriority = "Urgent";
            string importantPriority = "Important";
            string normalPriority = "Normal";
            string lowPriority = "Low";

            if (! await priorityManager.PriorityExits(urgentPriority))
                await priorityManager.AddAsync(new LKP_Priority()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    Name = urgentPriority,
                    IsDeleted = false,
                    FK_CreatedBy_Id = userId
                });
            if (!await priorityManager.PriorityExits(importantPriority))
               await  priorityManager.AddAsync(new LKP_Priority()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    Name = importantPriority,
                    IsDeleted = false,
                    FK_CreatedBy_Id = userId
                });
            if (!await priorityManager.PriorityExits(normalPriority))
                await priorityManager.AddAsync(new LKP_Priority()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    Name = normalPriority,
                    IsDeleted = false,
                    FK_CreatedBy_Id = userId
                });
            if (!await priorityManager.PriorityExits(lowPriority))
               await  priorityManager.AddAsync(new LKP_Priority()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    Name = lowPriority,
                    IsDeleted = false,
                    FK_CreatedBy_Id = userId
                });

            string apprrovedStatus = "Approved";
            string pendingStatus = "Pending";
            string rejectedStatus = "Rejected";
            string cancelledStatus = "Cancelled";
            string completedStatus = "Completed";
            string inProgressStatus = "In Progress";

            if (!await statusManager.StatusExists(apprrovedStatus))
               await  statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    FK_CreatedBy_Id = userId,
                    IsDeleted = false,
                    Name = apprrovedStatus
                });
            if (!await statusManager.StatusExists(pendingStatus))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    FK_CreatedBy_Id = userId,
                    IsDeleted = false,
                    Name = pendingStatus
                });
            if (!await statusManager.StatusExists(rejectedStatus))
               await  statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    FK_CreatedBy_Id = userId,
                    IsDeleted = false,
                    Name = rejectedStatus
                });
            if (!await statusManager.StatusExists(cancelledStatus))
                await statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    FK_CreatedBy_Id = userId,
                    IsDeleted = false,
                    Name = cancelledStatus
                });
            if (! await statusManager.StatusExists(completedStatus))
                await  statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    FK_CreatedBy_Id = userId,
                    IsDeleted = false,
                    Name = completedStatus
                });
            if (!await statusManager.StatusExists(inProgressStatus))
                await  statusManager.AddAsync(new LKP_Status()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    FK_CreatedBy_Id = userId,
                    IsDeleted = false,
                    Name = inProgressStatus
                });

            string maleType = "Male";
            string femaleType = "Female";
            string companyType = "Company";

            if (!await customerTypeManager.CustomerTypeExists(maleType))
                await  customerTypeManager.AddAsync(new CustomerType()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    FK_CreatedBy_Id = userId,
                    Name = maleType
                });
            if (!await customerTypeManager.CustomerTypeExists(femaleType))
                await  customerTypeManager.AddAsync(new CustomerType()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    FK_CreatedBy_Id = userId,
                    Name = femaleType
                });
            if (!await customerTypeManager.CustomerTypeExists(companyType))
                await  customerTypeManager.AddAsync(new CustomerType()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    FK_CreatedBy_Id = userId,
                    Name = companyType
                });

            string validityCondition = "Validity Time";

            if (!await conditionManager.ConditionExists(validityCondition))
                await  conditionManager.AddAsync(new LKPCondition()
                {
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                    IsDeleted = false,
                    FK_CreatedBy_Id = userId,
                    Name = "Validity Time"
                });
        }

        public async Task InitializeDrivers(IDriverManager driverManager)
        {
            ApplicationUserManager applicationUserManager = new ApplicationUserManager(_identityUserManager);
            var userId = (await applicationUserManager.GetByUserNameAsync("User1")).Id;

            await  driverManager.AddAsync(new Driver()
            {
                FK_CreatedBy_Id = userId,
                IsAvailable = true,
                IsDeleted = false,
                Name = "Ahmed"
            });
            await driverManager.AddAsync(new Driver()
            {
                FK_CreatedBy_Id = userId,
                IsAvailable = true,
                IsDeleted = false,
                Name = "Mohamed"
            });
            await driverManager.AddAsync(new Driver()
            {
                FK_CreatedBy_Id = userId,
                IsAvailable = true,
                IsDeleted = false,
                Name = "Ismail"
            });
            await driverManager.AddAsync(new Driver()
            {
                FK_CreatedBy_Id = userId,
                IsAvailable = true,
                IsDeleted = false,
                Name = "Hossam"
            });

        }

        public async Task InitializeVehicles(IVehicleManager vehicleManager, IDriverManager driverManager)
        {
            ApplicationUserManager applicationUserManager = new ApplicationUserManager(_identityUserManager);
            var user = (await applicationUserManager.GetByUserNameAsync("User1"));
            var driverId = driverManager.GetAllAsync().First().Id;
            await vehicleManager.AddAsync(new Vehicle()
            {
                IsAvailable = true,
                IsDeleted = false,
                Number = "Vehicle1234",
                CurrentUser = user,
                FK_Driver_Id = driverId,
                Fk_WashingStatus_Id = WashingStatus.Washed
            });
            await vehicleManager.AddAsync(new Vehicle()
            {
                IsAvailable = true,
                IsDeleted = false,
                Number = "Vehicle4567",
                CurrentUser = user,
                FK_Driver_Id = driverId,
                Fk_WashingStatus_Id = WashingStatus.Washed
            });
            await vehicleManager.AddAsync(new Vehicle()
            {
                IsAvailable = true,
                IsDeleted = false,
                Number = "Vehicle7890",
                CurrentUser = user,
                FK_Driver_Id = driverId,
                Fk_WashingStatus_Id = WashingStatus.Washed
            });
            await vehicleManager.AddAsync(new Vehicle()
            {
                IsAvailable = true,
                IsDeleted = false,
                Number = "Vehicle3456",
                CurrentUser = user,
                FK_Driver_Id = driverId,
                Fk_WashingStatus_Id = WashingStatus.Washed
            });
        }
    }
}
